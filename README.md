# HGamDiff_EFT

The project saves the code used in EFT interpretation of HGam diff xs.
## Worflow

1. MG5 production

**Tool: MG5_aMC (interfaced with Pythia8) https://launchpad.net/mg5amcnlo**

 produce BSM samples based on SMEFT model. The config Cards used have been put in MG5_aMC directory.

2. rivet routine

**Tool: HGamRivet https://gitlab.cern.ch/atlas-hgam-sw/h-yy-xsec/HGamRivetRoutine**

 loop over the events produced by MG5_aMC. Extract the truth information and categorize the events into differential bins in terms of different observables, to obtain the fraction of each differential category w.r.t inclusive phase space. Then the BSM differential cross-sections would be calculated with inclusive xs*Fraction

3. Interpolation/Fitting

**Tool: Professor2 https://professor.hepforge.org (can be run on lxplus directly)**

 Parameterize the BSM differential cross-sections as a function of Wilson coefficient. The Professor2 was usually used to interpolate the predicted points from MG5_aMC samples. For interference-only case, linear function was used in interpolation, while parabola was used for interference + quadratic case.
The parameterization was performed in each differential category independently, which aims to provide k-factor of BSM_xs/SM_xs at LO to nominal SM cross-sections.

 Eventually, those coefficients from interpolation/fitting would serve as inputs of next step


4. Limit setting

**Tool: gammacombo https://github.com/gammacombo/gammacombo**

UserGuide: https://github.com/gammacombo/gammacombo/blob/master/manual/GammaComboManual.pdf
_You can go through the README under ./gammacombo directory to follow the step directly._

Profile likelihood method was used to estimate the confidence intervals of the Wilson coefficients in SMEFT model. The package would accept the parameterization results from Step3, measurements of diff. xs in Hyy channel, covariance matrix among the differential bins included and so on.
