#!/bin/bash


##########################################################################################
##
##  2D Plot
##
##########################################################################################

# bin/HGamEFTScanner --var cHW --var cHWt\
#   -c 3  --color 10 --title " "\
#   --2dcl 1 --ncontours 3\
#   --scanrange -0.2:0.2 --scanrangey -0.4:0.39\
#   --qh 12 --qh 26 --qh 28\
#   --group "#bf{#it{ATLAS}} Internal"\
#   --leg 0.18:0.62 --legsize 0.3:0.3\
#   --ps 0\
#   --npoints 100\
#    -a plot
#     
# 
# bin/HGamEFTScanner --var cHW --var cHWt\
#   -c 4  --color 9 --title "N_{jets} only"\
#   -c 5  --color 8 --title "p_{T}^{H} only"\
#   -c 3  --color 10 --title "N_{jets}:p_{T}^{H}:m_{jj}:|#Delta#phi_{jj}|:p_{T}^{j1}"\
#   --2dcl 1 --ncontours 2\
#   --scanrange -0.25:0.25 --scanrangey -0.50:0.51\
#   --qh 12 --qh 26 --qh 15\
#   --group "#bf{#it{ATLAS}} Internal"\
#   --leg 0.20:0.22 --legsize 0.18:0.18\
#   --ps 0\
#   --npoints 100\
#    -a plot
# 
# bin/HGamEFTScanner --var cg --var cgt \
#    -c 8  --color 10 --title " "\
#    --2dcl 1 --ncontours 2\
#    --scanrange -0.0006:0.0002125 --scanrangey -0.00029:0.00029\
#    --qh 12\
#   --group "#bf{#it{ATLAS}} Internal"\
#    --leg 0.18:0.62 --legsize 0.3:0.3\
#    --ps 0\
#    --npoints 100\
#    -a plot
#  
# bin/HGamEFTScanner --var cg --var cgt\
#   --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#   -c 9  --color 9 --title "N_{jets} only"\
#   -c 10  --color 8 --title "p_{T}^{H} only"\
#   -c 8  --color 10 --title "N_{jets}:p_{T}^{H}:m_{jj}:|#Delta#phi_{jj}|:p_{T}^{j1}"\
#   --2dcl 1 --ncontours 2\
#   --scanrange -0.0008:0.0004125 --scanrangey -0.00049:0.00049\
#   --qh 12\
#   --group "#scale[0.8]{ATLAS Preliminary}"\
#   --leg 0.18:0.62 --legsize 0.3:0.3\
#   --ps 0\
#   --npoints 150\
#    -a plot

#  --scanrange -0.0008:0.0004125 --scanrangey -0.000424:0.000424\


# bin/HGamEFTScanner --var ca --var cg \
#     --fix 'cgt=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 13  --color 10 --title "N_{jets}:p_{T}^{H}:m_{jj}:|#Delta#phi_{jj}|:p_{T}^{j1}"\
#     --2dcl 1 --ncontours 2\
#     --scanrange -0.002:0.006\
#     --scanrangey -0.0015:0.002\
#     --qh 12\
#     --group "#scale[0.8]{Asimov Scan}"\
#     --leg 0.18:0.62 --legsize 0.3:0.3\
#     --ps 0\
#     --npoints 100\
#     -a plot


# bin/HGamEFTScanner --var mu_ggF --var mu_vbf \
#     -c 16  --color 10\
#     --2dcl 1 --ncontours 3\
#     --scanrange 0:2\
#     --scanrangey 0:4\
#     --qh 12\
#     --group "#scale[0.8]{Run-II Scan}"\
#     --leg 0.18:0.62 --legsize 0.3:0.3\
#     --ps 0\
#     --npoints 100\
#     -a plot
#     
# bin/HGamEFTScanner --var mu_vbf --var mu_vh \
#     -c 16  --color 10\
#     --2dcl 1 --ncontours 3\
#     --scanrange 0:4\
#     --scanrangey 0:4\
#     --qh 12\
#     --group "#scale[0.8]{Run-II Scan}"\
#     --leg 0.18:0.62 --legsize 0.3:0.3\
#     --ps 0\
#     --npoints 100\
#     -a plot    
