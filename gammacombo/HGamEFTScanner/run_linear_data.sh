param=$1
postfix=$2

if [ $param == "cg" ];then
#./bin/HGamEFTScanner --var cg --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,cHWB=0,cHWBt=0,mu=1' -c 1 --scanrange -0.0001:0.0001 --npoints 150 --ps 1
#./bin/HGamEFTScanner --var cg --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,cHWB=0,cHWBt=0,mu=1' -c 1 --scanrange -0.001:0.001 --npoints 150 --ps 1
./bin/HGamEFTScanner --var cg --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,cHWB=0,cHWBt=0,mu=1' -c 1 --scanrange -0.01:0.01 --npoints 150 --ps 1 --ext ${postfix}
#./bin/HGamEFTScanner --var cg --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,cHWB=0,cHWBt=0,mu=1' -c 1 --scanrange -0.0001:0.0001 --npoints 150 --ps 1 --ext ${postfix}
#./bin/HGamEFTScanner --var cg --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,cHWB=0,cHWBt=0,mu=1' -c 1 --scanrange -0.001:0.001 --npoints 150 --ps 1 

elif [ $param == "cHW" ];then
#./bin/HGamEFTScanner --var cHW --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cg=0,cHWt=0,cHB=0,cHBt=0,cHWB=0,cHWBt=0,mu=1' -c 2 --scanrange -0.01:0.01 --npoints 150 --ps 1
#./bin/HGamEFTScanner --var cHW --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cg=0,cHWt=0,cHB=0,cHBt=0,cHWB=0,cHWBt=0,mu=1' -c 2 --scanrange -0.012:0.02 --npoints 150 --ps 1
./bin/HGamEFTScanner --var cHW --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cg=0,cHWt=0,cHB=0,cHBt=0,cHWB=0,cHWBt=0,mu=1' -c 2 --scanrange -0.002:0.002 --npoints 150 --ps 1 --ext ${postfix}

elif [ $param == "cgt" ];then
./bin/HGamEFTScanner --var cgt --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,cHWB=0,cHWBt=0,mu=1' -c 3 --scanrange -0.030:0.030 --npoints 300 --ps 1 --saveAtMin --probforce --ext $postfix
#./bin/HGamEFTScanner --var cgt  -c 3 --scanrange -0.003:0.003 --npoints 300 --ps 1 --saveAtMin --probforce --ext $postfix

elif [ $param == "cHWt" ];then
./bin/HGamEFTScanner --var cHWt --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cg=0,cHW=0,cHB=0,cHBt=0,cHWB=0,cHWBt=0,mu=1' -c 4 --scanrange -1.0:1.0 --npoints 300 --ps 1 --ext $postfix

elif [ $param == "cHB" ];then
./bin/HGamEFTScanner --var cHB --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cg=0,cHWt=0,cHW=0,cHBt=0,cHWB=0,cHWBt=0,mu=1' -c 5 --scanrange -0.004:0.004 --npoints 300 --ps 1 --probforce --ext $postfix

elif [ $param == "cHWB" ];then
./bin/HGamEFTScanner --var cHWB --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cg=0,cHWt=0,cHW=0,cHBt=0,cHB=0,cHWBt=0,mu=1' -c 6 --scanrange -0.005:0.005 --npoints 300 --ps 1 --probforce --ext $postfix

elif [ $param == "cHBt" ];then
./bin/HGamEFTScanner --var cHBt --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cg=0,cHWt=0,cHW=0,cHB=0,cHWB=0,cHWBt=0,mu=1' -c 7 --scanrange -200:100 --npoints 300 --ps 1 --ext $postfix
#./bin/HGamEFTScanner --var cHBt --fix 'mu=1' -c 7 --scanrange -50:50 --npoints 300 --ps 1 --ext $postfix

elif [ $param == "cHWBt" ];then
./bin/HGamEFTScanner --var cHWBt --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cg=0,cHWt=0,cHW=0,cHBt=0,cHB=0,cHWB=0,mu=1' -c 8 --scanrange -50:50 --npoints 300 --ps 1 --probforce --ext $postfix
#./bin/HGamEFTScanner --var cHWBt --fix 'mu=1' -c 8 --scanrange -50:50 --npoints 300 --ps 1 --probforce --ext $postfix

elif [ $param == "cg_vs_cgt" ];then
./bin/HGamEFTScanner --var cg --var cgt --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHW=0,cHBt=0,cHB=0,cHWB=0,cHWBt=0,mu=1' -c 9 --scanrange -0.001:0.001 --scanrangey -0.015:0.025 --npoints2dx 70 --npoints2dy 70 --ps 1  --ext $postfix --2dcl 1
#raw Warsaw
#./bin/HGamEFTScanner --var cg --var cgt --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHW=0,cHBt=0,cHB=0,cHWB=0,cHWBt=0,mu=1' -c 9 --scanrange -0.017:0.017 --scanrangey -0.25:0.42 --npoints2dx 70 --npoints2dy 70 --ps 1  --ext $postfix --2dcl 1

elif [ ${param} == "cHW_vs_cHWt" ];then
./bin/HGamEFTScanner --var cHW --var cHWt --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cgt=0,cg=0,cHBt=0,cHB=0,cHWB=0,cHWBt=0,mu=1' -c 10 --scanrange -0.002:0.002 --scanrangey -1.0:0.8 --npoints2dx 50 --npoints2dy 50 --ps 1  --ext $postfix --2dcl 1
#raw Warsaw
#./bin/HGamEFTScanner --var cHW --var cHWt --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cgt=0,cg=0,cHBt=0,cHB=0,cHWB=0,cHWBt=0,mu=1' -c 10 --scanrange -0.034:0.034 --scanrangey -16:13 --npoints2dx 50 --npoints2dy 50 --ps 1  --ext $postfix --2dcl 1

elif [ ${param} == "cHB_vs_cHBt" ];then
./bin/HGamEFTScanner --var cHB --var cHBt --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHW=0,cgt=0,cg=0,cHWB=0,cHWBt=0,mu=1' -c 11 --scanrange -0.0005:0.0004 --scanrangey -200:40 --2dcl 1 --npoints2dx 50 --npoints2dy 50 --ps 1  --ext $postfix
#raw Warsaw
#./bin/HGamEFTScanner --var cHB --var cHBt --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHW=0,cgt=0,cg=0,cHWB=0,cHWBt=0,mu=1' -c 11 --scanrange -0.0083:0.0067 --scanrangey -3333:666 --2dcl 1 --npoints2dx 50 --npoints2dy 50 --ps 1  --ext $postfix

elif [ ${param} == "cHWB_vs_cHWBt" ];then
./bin/HGamEFTScanner --var cHWB --var cHWBt --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHW=0,cHBt=0,cHB=0,cg=0,cgt=0,mu=1' -c 12 --scanrange -0.001:0.001 --scanrangey -20:50 --npoints2dx 50 --npoints2dy 50 --ps 1  --ext $postfix --2dcl 1
#raw Warsaw
#./bin/HGamEFTScanner --var cHWB --var cHWBt --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHW=0,cHBt=0,cHB=0,cg=0,cgt=0,mu=1' -c 12 --scanrange -0.017:0.017 --scanrangey -333:833 --npoints2dx 50 --npoints2dy 50 --ps 1  --ext $postfix --2dcl 1

else
echo "Please give an available Wilson coefficient"
fi
#./bin/HGamEFTScanner --var cHW --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cg=0,cHWt=0,cHB=0,cHBt=0,mu=1' -c 1 --scanrange -0.25:0.25 --npoints 150 --ps 1
#./bin/HGamEFTScanner --var cHW --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cg=0,cHWt=0,cHB=0,cHBt=0,cHWB=0,cHWBt=0,mu=1' -c 1 --scanrange -0.001:0.001 --npoints 150 --ps 1
#./bin/HGamEFTScanner --var cHW --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cg=0,cHWt=0,cHB=0,cHBt=0,cHWB=0,cHWBt=0,mu=1' -c 1 --scanrange -0.25:0.25 --npoints 150 --ps 1
