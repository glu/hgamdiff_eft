#!/bin/bash

##########################################################################################
##
##  Scan cHW and cHWt Wilson coefficients 
##
##########################################################################################

bin/HGamEFTScanner --var cHW \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 1\
    --scanrange -0.1:0.1\
    --npoints 150

bin/HGamEFTScanner --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
    -c 2\
    --scanrange -0.5:0.5\
    --npoints 150    
    

bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --2dcl 1\
    --scanrange -0.125:0.125\
    --scanrangey -0.225:0.225\
    --npoints 150
    
bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 4\
    --2dcl 1\
    --scanrange -0.3:0.3\
    --scanrangey -0.5:0.5\
    --npoints 150&
    
bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0'\
    -c 5\
    --2dcl 1\
    --scanrange -0.3:0.3\
    --scanrangey -0.5:0.5\
    --npoints 150&              

bin/HGamEFTScanner --var cHWt --var mu \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,cHW=0'\
    -c 5\
    --2dcl 1\
    --scanrange -0.5:0.5\
    --scanrangey 0:3\
    --npoints 150&   

##########################################################################################
##
##  Scan cg and cgt Wilson coefficients 
##
##########################################################################################

# bin/HGamEFTScanner --var cg \
#     --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0'\
#     -c 6\
#     --scanrange -0.001:0.001\
#     --npoints 100&
# 
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0'\
#     -c 7\
#     --scanrange -0.001:0.001\
#     --npoints 100&  
# 
# bin/HGamEFTScanner --var cg --var cgt \
#      --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0'\
#      -c 8\
#      --scanrange -0.0006:0.0002125 --scanrangey -0.00029:0.00029\
#      --npoints 150& 
#  
# bin/HGamEFTScanner --var cg --var cgt \
#     --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0'\
#     -c 9\
#     --scanrange -0.001:0.001\
#     --scanrangey -0.001:0.001\
#     --npoints 150&  
#     
# bin/HGamEFTScanner --var cg --var cgt \
#     --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0'\
#     -c 10\
#     --scanrange -0.001:0.001\
#     --scanrangey -0.001:0.001\
#     --npoints 150&          

##########################################################################################  

# bin/HGamEFTScanner --var ca \
#     --fix 'cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0'\
#     -c 11\
#     --scanrange -0.002:0.006\
#     --npoints 400
#     
# bin/HGamEFTScanner --var cat \
#     --fix 'ca=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0'\
#     -c 12\
#     --scanrange -0.002:0.002\
#     --npoints 400    

# bin/HGamEFTScanner --var ca --var cg \
#     --fix 'cgt=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0'\
#     -c 13\
#     --scanrange -0.002:0.006\
#     --scanrangey -0.0015:0.002\
#     --npoints 150 


##########################################################################################
##
##  Scan coupling strengths  
##
##########################################################################################

# bin/HGamEFTScanner --var mu_ggF --var mu_vbf \
#     -c 16\
#     --scanrange 0:2\
#     --scanrangey 0:4\
#     --npoints 100\
#     
# bin/HGamEFTScanner --var mu_vbf --var mu_vh \
#     -c 16\
#     --scanrange 0:4\
#     --scanrangey 0:4\
#     --npoints 100\
#     
    
