*author: AAD
*reference: Physical Review D., Feb (2018) : 2018
*reference: CERN-EP-2017-288 : 2018
*reference: ARXIV:1802.04146 : 2018
*reference: https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/XXX/ : 2017
*status: Encoded 24 MAY 2017 by J. Lacey
*title:  Measurements of cross sections and differential distributions for Higgs production in the diphoton channel are presented based on 36.1 fb-1 proton-proton collisions at sqrt(s)=13 TeV 
*comment: Measurements of cross sections and differential distributions for Higgs production in the diphoton channel are presented based on 36.1 fb-1 proton-proton collisions at sqrt(s)=13 TeV 
*durhamId: 1111
*inspireId: 04146
*cdsId: 2302492
*detector: ATLAS
*experiment: CERN-LHC-ATLAS

*dataset: /HiggsDiphotonDifferentialCrossSection2016/pT_yy
*location: Fig. 26a
*dscomment: Measured differential cross section with associated uncertainties as a function of PT(2GAMMA). Each systematic uncertainty sources is fully uncorrelated with the other sources and fully correlated across bins, except for the background modelling systematics for which an uncorrelated treatment across bins is more appropriate.
*reackey: P P --> HIGGS X
*obskey: DSIG/DPT
*qual: RE : P P --> HIGGS < GAMMA GAMMA > X
*qual: SQRT(S) IN GEV : 13000
*qual: PT(GAMMA1)/M(2GAMMA) : > 0.35
*qual: PT(GAMMA2)/M(2GAMMA) : > 0.25
*qual: ABS(ETARAP(GAMMA1)) : <1.37 & >1.52 & < 2.37 
*qual: ABS(ETARAP(GAMMA2)) : <1.37 & >1.52 & < 2.37
*xheader: PT(2GAMMA)
*yheader: D(SIG)/DPT(2GAMMA) IN FB
*data: x : y
 0.00 TO  5.00; 0.57129320 +- 0.30478492 (DSYS=0.02127437:bkg_model_uncorr,DSYS=0.00234230:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00137110:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.02016665:PESPER,DSYS=0.01422520:photon_id_iso,DSYS=0.00537016:eff_trigger,DSYS=0.00765533:prw,DSYS=0.00971198:lumi,DSYS=0.00257082:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
 5.00 TO 10.00; 1.20176040 +- 0.41328540 (DSYS=0.10004409:bkg_model_uncorr,DSYS=0.00444651:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00252370:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.03665369:PESPER,DSYS=0.03016419:photon_id_iso,DSYS=0.01117637:eff_trigger,DSYS=0.01742553:prw,DSYS=0.02042993:lumi,DSYS=0.00540792:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
10.00 TO 15.00; 1.28980340 +- 0.43234210 (DSYS=0.09576892:bkg_model_uncorr,DSYS=0.00490125:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00000000:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.03972594:PESPER,DSYS=0.03263203:photon_id_iso,DSYS=0.01199517:eff_trigger,DSYS=0.01728337:prw,DSYS=0.02192666:lumi,DSYS=0.00580412:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
15.00 TO 20.00; 1.19119900 +- 0.42585364 (DSYS=0.13980092:bkg_model_uncorr,DSYS=0.00440744:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00393096:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.03895221:PESPER,DSYS=0.03025645:photon_id_iso,DSYS=0.01119727:eff_trigger,DSYS=0.01751063:prw,DSYS=0.02025038:lumi,DSYS=0.00536040:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
20.00 TO 25.00; 1.04443700 +- 0.38853056 (DSYS=0.03296820:bkg_model_uncorr,DSYS=0.00375997:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00000000:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.03248199:PESPER,DSYS=0.02652870:photon_id_iso,DSYS=0.00992215:eff_trigger,DSYS=0.01357768:prw,DSYS=0.01775543:lumi,DSYS=0.00469997:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
25.00 TO 30.00; 0.90098180 +- 0.35886105 (DSYS=0.04455585:bkg_model_uncorr,DSYS=0.00351383:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00243265:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.02784034:PESPER,DSYS=0.02288494:photon_id_iso,DSYS=0.00864943:eff_trigger,DSYS=0.01297414:prw,DSYS=0.01531669:lumi,DSYS=0.00405442:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
30.00 TO 35.00; 0.77225920 +- 0.32419441 (DSYS=0.01927957:bkg_model_uncorr,DSYS=0.00285736:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00038613:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.02478952:PESPER,DSYS=0.01961538:photon_id_iso,DSYS=0.00741369:eff_trigger,DSYS=0.01003937:prw,DSYS=0.01312841:lumi,DSYS=0.00347517:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
35.00 TO 45.00; 0.61751370 +- 0.17988174 (DSYS=0.03625400:bkg_model_uncorr,DSYS=0.00240830:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00037051:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.01920468:PESPER,DSYS=0.01556135:photon_id_iso,DSYS=0.00598988:eff_trigger,DSYS=0.00845994:prw,DSYS=0.01049773:lumi,DSYS=0.00277881:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
45.00 TO 60.00; 0.43256987 +- 0.11329005 (DSYS=0.01395205:bkg_model_uncorr,DSYS=0.00168702:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00043257:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.01353944:PESPER,DSYS=0.01077099:photon_id_iso,DSYS=0.00415267:eff_trigger,DSYS=0.00575318:prw,DSYS=0.00735369:lumi,DSYS=0.00194656:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
60.00 TO 80.00; 0.27325195 +- 0.07713903 (DSYS=0.01224886:bkg_model_uncorr,DSYS=0.00114766:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00065580:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.00975509:PESPER,DSYS=0.00669467:photon_id_iso,DSYS=0.00259589:eff_trigger,DSYS=0.00368890:prw,DSYS=0.00464528:lumi,DSYS=0.00122963:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
80.00 TO 100.00; 0.16854580 +- 0.05469311 (DSYS=0.01004359:bkg_model_uncorr,DSYS=0.00070789:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00053935:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.00632047:PESPER,DSYS=0.00402824:photon_id_iso,DSYS=0.00161804:eff_trigger,DSYS=0.00219110:prw,DSYS=0.00286528:lumi,DSYS=0.00075846:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
100.00 TO 120.00; 0.10638685 +- 0.03455445 (DSYS=0.00442564:bkg_model_uncorr,DSYS=0.00048938:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00024469:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.00379801:PESPER,DSYS=0.00243626:photon_id_iso,DSYS=0.00103195:eff_trigger,DSYS=0.00123409:prw,DSYS=0.00180858:lumi,DSYS=0.00047874:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
120.00 TO 140.00; 0.07253220 +- 0.02292018 (DSYS=0.00163451:bkg_model_uncorr,DSYS=0.00036991:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00029738:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.00247335:PESPER,DSYS=0.00159571:photon_id_iso,DSYS=0.00071082:eff_trigger,DSYS=0.00062378:prw,DSYS=0.00123305:lumi,DSYS=0.00032639:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
140.00 TO 170.00; 0.04718660 +- 0.01271207 (DSYS=0.00181889:bkg_model_uncorr,DSYS=0.00024537:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00013212:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.00175062:PESPER,DSYS=0.00097676:photon_id_iso,DSYS=0.00046715:eff_trigger,DSYS=0.00043884:prw,DSYS=0.00080217:lumi,DSYS=0.00021234:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
170.00 TO 200.00; 0.02868070 +- 0.00860134 (DSYS=0.00061126:bkg_model_uncorr,DSYS=0.00015774:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00004015:model_MG5,DSYS=0.00001147:model_sigcomp,DSYS=0.00115010:PESPER,DSYS=0.00055354:photon_id_iso,DSYS=0.00028394:eff_trigger,DSYS=0.00024092:prw,DSYS=0.00048757:lumi,DSYS=0.00012906:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
200.00 TO 250.00; 0.01541276 +- 0.00416145 (DSYS=0.00022857:bkg_model_uncorr,DSYS=0.00009248:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00001387:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.00071361:PESPER,DSYS=0.00027589:photon_id_iso,DSYS=0.00015259:eff_trigger,DSYS=0.00009864:prw,DSYS=0.00026202:lumi,DSYS=0.00006936:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
250.00 TO 300.00; 0.00714194 +- 0.00256610 (DSYS=0.00005325:bkg_model_uncorr,DSYS=0.00005571:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00000643:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.00040566:PESPER,DSYS=0.00012213:photon_id_iso,DSYS=0.00007071:eff_trigger,DSYS=0.00004642:prw,DSYS=0.00012141:lumi,DSYS=0.00003214:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
300.00 TO 450.00; 0.00211287 +- 0.00069894 (DSYS=0.00003926:bkg_model_uncorr,DSYS=0.00001690:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00001141:model_MG5,DSYS=0.00000085:model_sigcomp,DSYS=0.00012973:PESPER,DSYS=0.00003951:photon_id_iso,DSYS=0.00002092:eff_trigger,DSYS=0.00001014:prw,DSYS=0.00003592:lumi,DSYS=0.00000951:trig,DSYS=0.00000000,+0.00000021:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
450.00 TO 650.00; 0.00029079 +- 0.00019355 (DSYS=0.00006936:bkg_model_uncorr,DSYS=0.00000288:bkg_model_dalitz,DSYS=0.00000035:model_UEPS,DSYS=0.00000137:model_MG5,DSYS=0.00000000:model_sigcomp,DSYS=0.00002137:PESPER,DSYS=0.00000625:photon_id_iso,DSYS=0.00000291:eff_trigger,DSYS=0.00000000:prw,DSYS=0.00000494:lumi,DSYS=0.00000131:trig,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
*dataend:

*dataset: /HiggsDiphotonDifferentialCrossSection2016/pT_j1_30
*location: Fig. 27a
*dscomment: Measured differential cross section with associated uncertainties as a function of PT(JET1). Each systematic uncertainty sources is fully uncorrelated with the other sources and fully correlated across bins, except for the background modelling systematics for which an uncorrelated treatment across bins is more appropriate.
*reackey: P P --> HIGGS X
*obskey: DSIG/DPT
*qual: RE : P P --> HIGGS < GAMMA GAMMA > X
*qual: SQRT(S) IN GEV : 13000
*qual: PT(GAMMA1)/M(2GAMMA) : > 0.35
*qual: PT(GAMMA2)/M(2GAMMA) : > 0.25
*qual: ABS(ETARAP(GAMMA1)) : <1.37 & >1.52 & < 2.37 
*qual: ABS(ETARAP(GAMMA2)) : <1.37 & >1.52 & < 2.37
*xheader: PT(JET1)
*yheader: D(SIG)/DPT(JET1) IN FB
*data: x : y
 30.0 TO  60.0; 0.47700200 +- 0.11891660 (DSYS=0.03007582:bkg_model_uncorr,DSYS=0.00195571:bkg_model_dalitz,DSYS=0.00019080:model_UEPS,DSYS=0.00352981:model_MG5,DSYS=0.00047700:model_sigcomp,DSYS=0.01621807:PESPER,DSYS=0.01173425:photon_id_iso,DSYS=0.00443612:eff_trigger,DSYS=0.01120955:prw,DSYS=0.00810903:lumi,DSYS=0.00214651:trig,DSYS=0.05957755:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
 60.0 TO  90.0; 0.20027927 +- 0.06639258 (DSYS=0.01169972:bkg_model_uncorr,DSYS=0.00084117:bkg_model_dalitz,DSYS=0.00020028:model_UEPS,DSYS=0.00080112:model_MG5,DSYS=0.00086120:model_sigcomp,DSYS=0.00602841:PESPER,DSYS=0.00474662:photon_id_iso,DSYS=0.00192268:eff_trigger,DSYS=0.00286399:prw,DSYS=0.00340475:lumi,DSYS=0.00090126:trig,DSYS=0.00486679:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
 90.0 TO 120.0; 0.09751360 +- 0.04253543 (DSYS=0.00342053:bkg_model_uncorr,DSYS=0.00047782:bkg_model_dalitz,DSYS=0.00004876:model_UEPS,DSYS=0.00024378:model_MG5,DSYS=0.00024378:model_sigcomp,DSYS=0.00328621:PESPER,DSYS=0.00221356:photon_id_iso,DSYS=0.00094588:eff_trigger,DSYS=0.00075085:prw,DSYS=0.00165773:lumi,DSYS=0.00043881:trig,DSYS=0.00414433:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
120.0 TO 350.0; 0.01614063 +- 0.00398512 (DSYS=0.00025069:bkg_model_uncorr,DSYS=0.00008877:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00004197:model_MG5,DSYS=0.00002905:model_sigcomp,DSYS=0.00080058:PESPER,DSYS=0.00032443:photon_id_iso,DSYS=0.00015818:eff_trigger,DSYS=0.00011944:prw,DSYS=0.00027439:lumi,DSYS=0.00007263:trig,DSYS=0.00054555:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
*dataend:

*dataset: /HiggsDiphotonDifferentialCrossSection2016/N_j_30
*location: Fig. 25a
*dscomment: Measured differential cross section with associated uncertainties as a function of MULT(JET,PT>30 GEV). Each systematic uncertainty sources is fully uncorrelated with the other sources and fully correlated across bins, except for the background modelling systematics for which an uncorrelated treatment across bins is more appropriate.
*reackey: P P --> HIGGS X
*obskey: SIG
*qual: RE : P P --> HIGGS < GAMMA GAMMA > X
*qual: SQRT(S) IN GEV : 13000
*qual: PT(GAMMA1)/M(2GAMMA) : > 0.35
*qual: PT(GAMMA2)/M(2GAMMA) : > 0.25
*qual: ABS(ETARAP(GAMMA1)) : <1.37 & >1.52 & < 2.37 
*qual: ABS(ETARAP(GAMMA2)) : <1.37 & >1.52 & < 2.37
*xheader: MULT(JET,PT>30 GEV)
*yheader: SIG(fiducial) IN FB
*data: x : y
 0.00 TO  1.00; 36.60455300 +- 4.50968093 (DSYS=1.99690454:bkg_model_uncorr,DSYS=0.13909730:bkg_model_dalitz,DSYS=0.02562319:model_UEPS,DSYS=0.09517184:model_MG5,DSYS=0.05490683:model_sigcomp,DSYS=1.22991298:PESPER,DSYS=0.92609519:photon_id_iso,DSYS=0.34774325:eff_trigger,DSYS=0.95903929:prw,DSYS=0.62227740:lumi,DSYS=0.16472049:trig,DSYS=1.94736222:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
 1.00 TO  2.00; 16.97177900 +- 3.71851678 (DSYS=0.79655293:bkg_model_uncorr,DSYS=0.06958429:bkg_model_dalitz,DSYS=0.01188025:model_UEPS,DSYS=0.10352785:model_MG5,DSYS=0.01697178:model_sigcomp,DSYS=0.61777276:PESPER,DSYS=0.41071705:photon_id_iso,DSYS=0.16123190:eff_trigger,DSYS=0.14595730:prw,DSYS=0.28852024:lumi,DSYS=0.07637301:trig,DSYS=0.80785668:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
 2.00 TO  3.00; 7.32717500 +- 2.38499546 (DSYS=0.18945540:bkg_model_uncorr,DSYS=0.03517044:bkg_model_dalitz,DSYS=0.02198153:model_UEPS,DSYS=0.05641925:model_MG5,DSYS=0.08352980:model_sigcomp,DSYS=0.29381972:PESPER,DSYS=0.16559415:photon_id_iso,DSYS=0.06960816:eff_trigger,DSYS=0.08792610:prw,DSYS=0.12456198:lumi,DSYS=0.03297229:trig,DSYS=0.61328455:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
 3.00 TO 10.00; 0.40322786 +- 0.18516223 (DSYS=0.00615568:bkg_model_uncorr,DSYS=0.00209678:bkg_model_dalitz,DSYS=0.00306453:model_UEPS,DSYS=0.00963715:model_MG5,DSYS=0.00334679:model_sigcomp,DSYS=0.01766138:PESPER,DSYS=0.00870972:photon_id_iso,DSYS=0.00379034:eff_trigger,DSYS=0.01741944:prw,DSYS=0.00685487:lumi,DSYS=0.00181453:trig,DSYS=0.06943584:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
*dataend:

*dataset: /HiggsDiphotonDifferentialCrossSection2016/Dphi_j_j_30_signed
*location: Fig. 28b
*dscomment: Measured differential cross section with associated uncertainties as a function of DPHI(2JET). Each systematic uncertainty sources is fully uncorrelated with the other sources and fully correlated across bins, except for the background modelling systematics for which an uncorrelated treatment across bins is more appropriate.
*reackey: P P --> HIGGS X
*obskey: DSIG/DPHI
*qual: RE : P P --> HIGGS < GAMMA GAMMA > X
*qual: SQRT(S) IN GEV : 13000
*qual: PT(GAMMA1)/M(2GAMMA) : > 0.35
*qual: PT(GAMMA2)/M(2GAMMA) : > 0.25
*qual: ABS(ETARAP(GAMMA1)) : <1.37 & >1.52 & < 2.37 
*qual: ABS(ETARAP(GAMMA2)) : <1.37 & >1.52 & < 2.37
*xheader: DPHI(2JET)
*yheader: D(SIG)/DDPHI(2JET) IN FB
*data: x : y
-3.1500 TO -1.5708; 1.72401532 +- 0.94182957 (DSYS=0.04864937:bkg_model_uncorr,DSYS=0.00793047:bkg_model_dalitz,DSYS=0.00293083:model_UEPS,DSYS=0.03413550:model_MG5,DSYS=0.01586094:model_sigcomp,DSYS=0.06844341:PESPER,DSYS=0.04034196:photon_id_iso,DSYS=0.01637815:eff_trigger,DSYS=0.03396310:prw,DSYS=0.02930826:lumi,DSYS=0.00775807:trig,DSYS=0.21584672:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
-1.5708 TO 0.0000; 1.49881334 +- 0.56295429 (DSYS=0.03244012:bkg_model_uncorr,DSYS=0.00839335:bkg_model_dalitz,DSYS=0.00524585:model_UEPS,DSYS=0.01873517:model_MG5,DSYS=0.01483825:model_sigcomp,DSYS=0.06609767:PESPER,DSYS=0.03132520:photon_id_iso,DSYS=0.01438861:eff_trigger,DSYS=0.03492235:prw,DSYS=0.02547983:lumi,DSYS=0.00674466:trig,DSYS=0.19709395:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
0.0000 TO 1.5708; 1.49668640 +- 0.56709448 (DSYS=0.07767566:bkg_model_uncorr,DSYS=0.00838144:bkg_model_dalitz,DSYS=0.00553774:model_UEPS,DSYS=0.00523840:model_MG5,DSYS=0.01421852:model_sigcomp,DSYS=0.06540520:PESPER,DSYS=0.03113108:photon_id_iso,DSYS=0.01436819:eff_trigger,DSYS=0.03382511:prw,DSYS=0.02544367:lumi,DSYS=0.00673509:trig,DSYS=0.19831095:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
1.5708 TO 3.1500; 1.72358346 +- 0.90212358 (DSYS=0.07124702:bkg_model_uncorr,DSYS=0.00792848:bkg_model_dalitz,DSYS=0.00275773:model_UEPS,DSYS=0.02757734:model_MG5,DSYS=0.01568461:model_sigcomp,DSYS=0.06084250:PESPER,DSYS=0.04033185:photon_id_iso,DSYS=0.01620168:eff_trigger,DSYS=0.03550582:prw,DSYS=0.02930092:lumi,DSYS=0.00775613:trig,DSYS=0.21458614:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
*dataend:

*dataset: /HiggsDiphotonDifferentialCrossSection2016/m_jj_30
*location: Fig. 29c
*dscomment: Measured differential cross section with associated uncertainties as a function of M(2JET). Each systematic uncertainty sources is fully uncorrelated with the other sources and fully correlated across bins, except for the background modelling systematics for which an uncorrelated treatment across bins is more appropriate.
*reackey: P P --> HIGGS X
*obskey: DSIG/DM
*qual: RE : P P --> HIGGS < GAMMA GAMMA > X
*qual: SQRT(S) IN GEV : 13000
*qual: PT(GAMMA1)/M(2GAMMA) : > 0.35
*qual: PT(GAMMA2)/M(2GAMMA) : > 0.25
*qual: ABS(ETARAP(GAMMA1)) : <1.37 & >1.52 & < 2.37 
*qual: ABS(ETARAP(GAMMA2)) : <1.37 & >1.52 & < 2.37
*xheader: M(2JET)
*yheader: D(SIG)/DM(2JET) IN FB
*data: x : y
 0.00 TO 120.00; 0.02732332 +- 0.01283103 (DSYS=0.00109778:bkg_model_uncorr,DSYS=0.00012842:bkg_model_dalitz,DSYS=0.00012295:model_UEPS,DSYS=0.00008197:model_MG5,DSYS=0.00034427:model_sigcomp,DSYS=0.00107381:PESPER,DSYS=0.00060658:photon_id_iso,DSYS=0.00025957:eff_trigger,DSYS=0.00092626:prw,DSYS=0.00046450:lumi,DSYS=0.00012295:trig,DSYS=0.00451928:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
120.00 TO 450.00; 0.01354288 +- 0.00523432 (DSYS=0.00050072:bkg_model_uncorr,DSYS=0.00007042:bkg_model_dalitz,DSYS=0.00000000:model_UEPS,DSYS=0.00030878:model_MG5,DSYS=0.00005146:model_sigcomp,DSYS=0.00055390:PESPER,DSYS=0.00030336:photon_id_iso,DSYS=0.00012866:eff_trigger,DSYS=0.00018960:prw,DSYS=0.00023023:lumi,DSYS=0.00006094:trig,DSYS=0.00139763:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
450.00 TO 3000.00; 0.00092828 +- 0.00030067 (DSYS=0.00002748:bkg_model_uncorr,DSYS=0.00000483:bkg_model_dalitz,DSYS=0.00000576:model_UEPS,DSYS=0.00001123:model_MG5,DSYS=0.00001931:model_sigcomp,DSYS=0.00003825:PESPER,DSYS=0.00002024:photon_id_iso,DSYS=0.00000882:eff_trigger,DSYS=0.00001912:prw,DSYS=0.00001578:lumi,DSYS=0.00000418:trig,DSYS=0.00012049:jet,DSYS=0.00000000,+0.00000000:lep,DSYS=0.00000000,+0.00000000:met,DSYS=0.00000000,+0.00000000:flavour);
*dataend:

*E
