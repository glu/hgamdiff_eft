/**
 * EFT ProfDriver Wrapper
 * 
 * Authors: Holger Schulz, holger.schulz@cern.ch,
 *          Florian Urs Bernlochner, florian.bernlochner@cern.ch,
 *          Christopher Meyer, chris.mejer@cern.ch,
 *          Jim Lacey, jim.lacey@cern.ch,
 *          Yanping Huang, yanping.huang@cern.ch,
 *          Maria Josefina Alconada Verzini, josefina.alconada@cern.ch
 * 
 * Date: October 2016
 *
 **/

#ifndef _RooProfDriverWrapper
#define _RooProfDriverWrapper

#include "RooRealProxy.h"
#include "RooCategoryProxy.h"
#include "RooAbsReal.h"
#include "RooAbsCategory.h"

#include "HGamUtils.h"

#include "ProfDriver.h"

using namespace std;
using namespace RooFit;
 
class RooProfDriverWrapper : public RooAbsReal 
{

public:
  
  RooProfDriverWrapper() {};
    RooProfDriverWrapper(const char *name, const char *title, StrV ProfDriverIpolFiles, StrVV ProfDriverIpolVars, double xs_ggh, double xs_vbf_vh, double xs_tth, bool NormalizedScan,VecD xs_vbf_vh_tot, RooAbsReal& cg, RooAbsReal& cgt, RooAbsReal& ca, RooAbsReal& cat, RooAbsReal& cH, RooAbsReal& cT, RooAbsReal& cB, RooAbsReal& cW, RooAbsReal& cHW, RooAbsReal& cHWt, RooAbsReal& cHB, RooAbsReal& cHBt,RooAbsReal& cHWB,RooAbsReal& cHWBt, RooAbsReal& mu);
  RooProfDriverWrapper(const char *name, const char *title, StrV ProfDriverIpolFiles, StrVV ProfDriverIpolVars, double xs_ggh, double xs_vbf_vh, double xs_tth, bool NormalizedScan,bool LinearScan,int indexLow,int indexHigh ,VecD xs_vbf_vh_tot, RooAbsReal& cg, RooAbsReal& cgt, RooAbsReal& ca, RooAbsReal& cat, RooAbsReal& cH, RooAbsReal& cT, RooAbsReal& cB, RooAbsReal& cW, RooAbsReal& cHW, RooAbsReal& cHWt, RooAbsReal& cHB, RooAbsReal& cHBt,RooAbsReal& cHWB,RooAbsReal& cHWBt, RooAbsReal& mu);
  
  RooProfDriverWrapper(const RooProfDriverWrapper& other, const char* name=0) ;
  virtual ~RooProfDriverWrapper();
  virtual TObject* clone(const char* newname) const { return new RooProfDriverWrapper(*this,newname); }
  Double_t evaluate() const;  
  Double_t calculateHiggsDecayWidthRatio() const;
  Double_t calculateHiggstoDiphotonDecayWidthRatio() const;
  // updated by glu
  Str outputfilenames;
  Double_t calculateBRRatio_linear() const;
  Double_t calculateBRRatio_quad() const;
  Double_t calculateBRRatio_linear_quad() const;

  // ggF, VBF+VH, ttH predictions
  Double_t evaluate_ratio_ggF() const;  
  Double_t evaluate_ratio_VBF_VH() const;  
  Double_t evaluate_ratio_ttH() const;  
 
protected:

 RooArgSet *cPars;
 RooRealProxy cg, cgt, ca, cat, cH, cT, cB, cW, cHW, cHWt, cHB, cHBt,cHWB,cHWBt,mu;
 
 double xs_ggh, xs_vbf_vh, xs_tth;
 
 // For normalized scan
 VecD xs_vbf_vh_tot;
 
 bool has_ggf_pd, has_vbf_vh_pd, has_tth_pd;
  
private:

 Str WrapperName, HistName; 
 
 map <Str,Str> RunMap;
 
 int BinNumber; bool NormalizedScan;
 int indexLow; int indexHigh; //updated by glu 
 bool LinearScan; //updated by glu
 StrV ProfDriverIpolFiles;
 StrVV ProfDriverIpolVars;

 vector <ProfDriver> v_pd; 

 ClassDef(RooProfDriverWrapper, 1);
  
};
 
#endif
