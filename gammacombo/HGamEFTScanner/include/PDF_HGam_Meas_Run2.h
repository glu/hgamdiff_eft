/**
 * EFT module for Gamma Combination code
 * 
 * Authors: Florian Urs Bernlochner, florian.bernlochner@cern.ch,
 *          Christopher Meyer, chris.mejer@cern.ch,
 *          Jim Lacey, jim.lacey@cern.ch,
 *          Yanping Huang, yanping.huang@cern.ch,
 *          Maria Josefina Alconada Verzini, josefina.alconada@cern.ch
 * 
 * Date: October 2016
 *
 **/

#ifndef PDF_HGam_Meas_Run2_h
#define PDF_HGam_Meas_Run2_h

#include "PDF_Abs.h"

#include "HGamUtils.h"

#include "EFT_Parameters.h"
#include "RooProfDriverWrapper.h"
#include "HGamMeasurementReader.h"
#include "RooHGamEFTMultiVarGaussian.h"

using namespace RooFit;
using namespace std;
using namespace Utils;

class PDF_HGam_Meas_Run2 : public PDF_Abs
{

public:

  PDF_HGam_Meas_Run2(Str SettingsFileName, Str IpolFileName);
  ~PDF_HGam_Meas_Run2();
  void          buildPdf();
  void          initObservables();
  virtual void  initParameters();
  virtual void  initRelations();
  void          setCorrelations(TString c);
  void          setObservables(TString c);
  void          setUncertainties(TString c);
  
  StrV HepDataKeys;
  HGMR::HGamMeasurementReader *MeasurementReader;
  int nObs;
  TEnv *set,*ipol;

  
};

#endif
