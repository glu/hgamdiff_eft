#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ class Professor;
#pragma link C++ class ProfDriver; 
#pragma link C++ class RooProfDriverWrapper+;
#pragma link C++ class RooHGamEFTMultiVarGaussian+;
#pragma link C++ class HGamMeasurementReader;

#endif
