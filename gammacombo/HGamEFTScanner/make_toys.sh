
##########################################################################################
##
##  Make toys for cHW
##
##########################################################################################

# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 50 --nrun 1&
# 
# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 50 --nrun 2&
#     
# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 50 --nrun 3&    
#     
# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 50 --nrun 4&       
#     
# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 50 --nrun 5&   
#     
# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 50 --nrun 6&
# 
# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 50 --nrun 7&
#     
# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 50 --nrun 8&    
#     
# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 50 --nrun 9&       
#     
# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 50 --nrun 10&           
# 

# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 150 --nrun 11&
# 
# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 150 --nrun 12&
#     
# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 150 --nrun 13&    
#     
# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 150 --nrun 14&       
#     
# bin/HGamEFTScanner --var cHW \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 1\
#     --scanrange -0.1:0.1\
#     -a pluginbatch --ntoys 150 --nrun 15&           
        

##########################################################################################
##
##  Make toys for cHWt
##
##########################################################################################

# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 50 --nrun 1&
# 
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 50 --nrun 2&
#     
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 50 --nrun 3&
#     
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 50 --nrun 4&
#     
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 50 --nrun 5&                
#     
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 50 --nrun 6&
# 
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 50 --nrun 7&
#     
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 50 --nrun 8&
#     
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 50 --nrun 9&
#     
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 50 --nrun 10&                
#         
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 150 --nrun 11&
# 
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 150 --nrun 12&
#     
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 150 --nrun 13&
#     
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 150 --nrun 14&
#     
# bin/HGamEFTScanner --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0,mu=1'\
#     -c 2\
#     --scanrange -0.5:0.5\
#     -a pluginbatch --ntoys 150 --nrun 15&           
    
##########################################################################################
##
##  Make toys for cg
##
##########################################################################################

# bin/HGamEFTScanner --var cg \
#     --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 6\
#     --scanrange -0.001:0.001\
#     -a pluginbatch --ntoys 50 --nrun 1&
# 
# bin/HGamEFTScanner --var cg \
#     --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 6\
#     --scanrange -0.001:0.001\
#     -a pluginbatch --ntoys 50 --nrun 2&
# 
# bin/HGamEFTScanner --var cg \
#     --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 6\
#     --scanrange -0.001:0.001\
#     -a pluginbatch --ntoys 50 --nrun 3&
# 
# bin/HGamEFTScanner --var cg \
#     --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 6\
#     --scanrange -0.001:0.001\
#     -a pluginbatch --ntoys 50 --nrun 4&
# 
# bin/HGamEFTScanner --var cg \
#     --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 6\
#     --scanrange -0.001:0.001\
#     -a pluginbatch --ntoys 50 --nrun 5&

##########################################################################################
##
##  Make toys for cgt
##
##########################################################################################

bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrangey -0.00029:0.00029\
    -a pluginbatch --ntoys 20 --nrun 1&
    
bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrangey -0.00029:0.00029\
    -a pluginbatch --ntoys 20 --nrun 2&
    
bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrangey -0.00029:0.00029\
    -a pluginbatch --ntoys 20 --nrun 3&
    
bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrangey -0.00029:0.00029\
    -a pluginbatch --ntoys 20 --nrun 4&
    
bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrangey -0.00029:0.00029\
    -a pluginbatch --ntoys 20 --nrun 5&
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 6&
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 7&
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 8&
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 9&
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 10&    
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 11&
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 12&
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 13&
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 14&
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 15&    
#     
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 16&
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 17&
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 18&
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 19&
#     
# bin/HGamEFTScanner --var cgt \
#     --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
#     -c 7\
#     --scanrangey -0.00029:0.00029\
#     -a pluginbatch --ntoys 50 --nrun 20&    
            
    
##########################################################################################
##
##  Make toys for cHW:cHWt 
##
##########################################################################################    
    
bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 1&

bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 2&

bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --2dcl 1\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 3&
    
bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --2dcl 1\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 4&
    
bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --2dcl 1\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 5&        


bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --2dcl 1\
   --scanrange -0.1:0.1\
   --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 10 --nrun 6&

bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --2dcl 1\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 10 --nrun 7&


bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --2dcl 1\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 10 --nrun 8&
    
bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --2dcl 1\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 10 --nrun 9&
    
bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --2dcl 1\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 10 --nrun 10&  
#       
#  
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 11&
# 
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 12&
# 
# 
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 13&
#     
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 14&
#     
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 15&    
#     
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 10 --nrun 16&  
#      
# 
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 17&
# 
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 18&
# 
# 
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 19&
#     
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 20&
      
     
# nohup bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 50 --npointstoy 50\
#     -a pluginbatch --ntoys 50 --nrun 21&  
#      
# 
# nohup bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 50 --npointstoy 50\
#     -a pluginbatch --ntoys 50 --nrun 22&
# 
# nohup bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 50 --npointstoy 50\
#     -a pluginbatch --ntoys 50 --nrun 23&
# 
# 
# nohup bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 50 --npointstoy 50\
#     -a pluginbatch --ntoys 50 --nrun 24&
#     
# nohup bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 50 --npointstoy 50\
#     -a pluginbatch --ntoys 50 --nrun 25&
#     
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 100 --npointstoy 50\
#     -a pluginbatch --ntoys 50 --nrun 26&  
#      
# 
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 100 --npointstoy 50\
#     -a pluginbatch --ntoys 50 --nrun 27&
# 
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 100 --npointstoy 50\
#     -a pluginbatch --ntoys 50 --nrun 28&
# 
# 
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 100 --npointstoy 50\
#     -a pluginbatch --ntoys 50 --nrun 29&
#     
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 100 --npointstoy 50\
#     -a pluginbatch --ntoys 50 --nrun 30&    
#          
# 
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 30&
# 
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 31&
# 
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 32&
#     
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 33&
#     
# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.1:0.1\
#     --scanrangey -0.2:0.2\
#     --npoints 30 --npointstoy 30\
#     -a pluginbatch --ntoys 50 --nrun 34&        
#          

bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 101&

bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 102&

bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --2dcl 1\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 103&
    
bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --2dcl 1\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 104&
    
bin/HGamEFTScanner --var cHW --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0,mu=1'\
    -c 3\
    --2dcl 1\
    --scanrange -0.1:0.1\
    --scanrangey -0.2:0.2\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 105&           
         

##########################################################################################
##
##  Make toys for cg:cgt
##
##########################################################################################    
                 
bin/HGamEFTScanner --var cg --var cgt \
    --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 8\
    --scanrange -0.0006:0.0002\
    --scanrangey -0.0005:0.0005\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 1&   
    
bin/HGamEFTScanner --var cg --var cgt \
    --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 8\
    --scanrange -0.0006:0.0002\
    --scanrangey -0.0005:0.0005\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 2&   
    
bin/HGamEFTScanner --var cg --var cgt \
    --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 8\
    --scanrange -0.0006:0.0002\
    --scanrangey -0.0005:0.0005\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 3&   
    
bin/HGamEFTScanner --var cg --var cgt \
    --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 8\
    --scanrange -0.0006:0.0002\
    --scanrangey -0.0005:0.0005\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 4&   
    
bin/HGamEFTScanner --var cg --var cgt \
    --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 8\
    --scanrange -0.0006:0.0002\
    --scanrangey -0.0005:0.0005\
    --npoints 30 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 5&
                     
##########################################################################################
##
##  Make Test Toys
##
##########################################################################################    

bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 1&

bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 2&

bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 3&


bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 4&

bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 5&
    
bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 6&

bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 7&

bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 8&


bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 9&

bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 10&    
    
bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 11&

bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 12&

bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 13&


bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 14&

bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0,mu=1'\
    -c 7\
    --scanrange -0.001:0.001\
    --npoints 100 --npointstoy 30\
    -a pluginbatch --ntoys 50 --nrun 15&    
    
