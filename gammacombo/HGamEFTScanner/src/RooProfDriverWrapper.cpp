/**
 * EFT ProfDriver Wrapper
 * 
 * Authors: Holger Schulz, holger.schulz@cern.ch,
 *          Florian Urs Bernlochner, florian.bernlochner@cern.ch,
 *          Christopher Meyer, chris.mejer@cern.ch,
 *          Jim Lacey, jim.lacey@cern.ch,
 *          Yanping Huang, yanping.huang@cern.ch,
 *          Maria Josefina Alconada Verzini, josefina.alconada@cern.ch
 * 
 * Date: October 2016
 *
 **/

#include "Riostream.h"

#include "RooProfDriverWrapper.h"

#include "RooAbsReal.h"
#include "RooAbsCategory.h"
#include <math.h>
#include "TMath.h"

// ---------------------------------------------------------------------------------------
// Constructors

// vector <ProfDriver> RooProfDriverWrapper::v_pd;


RooProfDriverWrapper::RooProfDriverWrapper(const char *name, const char *title, StrV _ProfDriverIpolFiles, StrVV _ProfDriverIpolVars, double _xs_ggh, double _xs_vbf_vh, double _xs_tth, bool _NormalizedScan,VecD _xs_vbf_vh_tot,
         RooAbsReal& _cg, 
         RooAbsReal& _cgt, 
         RooAbsReal& _ca, 
         RooAbsReal& _cat, 
         RooAbsReal& _cH, 
         RooAbsReal& _cT, 
         RooAbsReal& _cB, 
         RooAbsReal& _cW, 
         RooAbsReal& _cHW, 
         RooAbsReal& _cHWt, 
         RooAbsReal& _cHB, 
         RooAbsReal& _cHBt,
	 RooAbsReal& _cHWB,
	 RooAbsReal& _cHWBt,
         RooAbsReal& _mu              
) :
	     RooAbsReal(name,title),
	     xs_ggh(_xs_ggh),
	     xs_vbf_vh(_xs_vbf_vh),
	     xs_tth(_xs_tth),
	     NormalizedScan(_NormalizedScan),
	     LinearScan(false),
	     indexLow(-1),
             indexHigh(999),
	     xs_vbf_vh_tot(_xs_vbf_vh_tot),
     	 cg("cg","cg",this,_cg),
     	 cgt("cgt","cgt",this,_cgt),
     	 ca("ca","ca",this,_ca),
     	 cat("cat","act",this,_cat),
     	 cH("cH","cH",this,_cH),
     	 cT("cT","cT",this,_cT),
     	 cB("cB","cB",this,_cB),
     	 cW("cW","cW",this,_cW),
     	 cHW("cHW","cHW",this,_cHW),
     	 cHWt("cHWt","cHWt",this,_cHWt),
     	 cHB("cHB","cHB",this,_cHB),
     	 cHBt("cHBt","cHBt",this,_cHBt),
	 cHWB("cHWB","cHWB",this,_cHWB),
	 cHWBt("cHWBt","cHWBt",this,_cHWBt),
     	 mu("mu","mu",this,_mu)
{

     WrapperName = (Str) title;
     HistName = Vectorize(WrapperName,":").front();
     
     Str Bin = Vectorize(WrapperName.Data(),":").back().ReplaceAll("_th","");     
     BinNumber = atoi( Bin.Data() );
          
     ProfDriverIpolFiles = _ProfDriverIpolFiles;
     ProfDriverIpolVars  = _ProfDriverIpolVars;

     has_ggf_pd = has_vbf_vh_pd = has_tth_pd = false;
          
     // Setup ProfDriver if the static vector is empty, otherwise we are done     
     for(auto ProfDriverIpolFile : ProfDriverIpolFiles) {

         v_pd.push_back( ProfDriver( (std::string) ProfDriverIpolFile.Data() ) );

         if(ProfDriverIpolFile.Contains("ggf"))    has_ggf_pd = true;
         if(ProfDriverIpolFile.Contains("vbf_vh")) has_vbf_vh_pd = true;
         if(ProfDriverIpolFile.Contains("tth"))    has_tth_pd = true;
        
      }
      
      
     cout << "has_ggf_pd = " << has_ggf_pd << "\t" << "has_vbf_vh_pd = " << has_vbf_vh_pd << "\t" << "has_tth_pd = " << has_tth_pd << endl;
     
      
}

RooProfDriverWrapper::RooProfDriverWrapper(const char *name, const char *title, StrV _ProfDriverIpolFiles, StrVV _ProfDriverIpolVars, double _xs_ggh, double _xs_vbf_vh, double _xs_tth, bool _NormalizedScan, bool _LinearScan,int _indexLow, int _indexHigh,VecD _xs_vbf_vh_tot,
         RooAbsReal& _cg, 
         RooAbsReal& _cgt, 
         RooAbsReal& _ca, 
         RooAbsReal& _cat, 
         RooAbsReal& _cH, 
         RooAbsReal& _cT, 
         RooAbsReal& _cB, 
         RooAbsReal& _cW, 
         RooAbsReal& _cHW, 
         RooAbsReal& _cHWt, 
         RooAbsReal& _cHB, 
         RooAbsReal& _cHBt,
	 RooAbsReal& _cHWB,
	 RooAbsReal& _cHWBt,
         RooAbsReal& _mu              
) :
	     RooAbsReal(name,title),
	     xs_ggh(_xs_ggh),
	     xs_vbf_vh(_xs_vbf_vh),
	     xs_tth(_xs_tth),
	     NormalizedScan(_NormalizedScan),
             LinearScan(_LinearScan),
	     indexLow(_indexLow),
	     indexHigh(_indexHigh),
	     xs_vbf_vh_tot(_xs_vbf_vh_tot),
     	 cg("cg","cg",this,_cg),
     	 cgt("cgt","cgt",this,_cgt),
     	 ca("ca","ca",this,_ca),
     	 cat("cat","act",this,_cat),
     	 cH("cH","cH",this,_cH),
     	 cT("cT","cT",this,_cT),
     	 cB("cB","cB",this,_cB),
     	 cW("cW","cW",this,_cW),
     	 cHW("cHW","cHW",this,_cHW),
     	 cHWt("cHWt","cHWt",this,_cHWt),
     	 cHB("cHB","cHB",this,_cHB),
     	 cHBt("cHBt","cHBt",this,_cHBt),
	 cHWB("cHWB","cHWB",this,_cHWB),
	 cHWBt("cHWBt","cHWBt",this,_cHWBt),
     	 mu("mu","mu",this,_mu)
{

     WrapperName = (Str) title;
     HistName = Vectorize(WrapperName,":").front();
     
     Str Bin = Vectorize(WrapperName.Data(),":").back().ReplaceAll("_th","");     
     BinNumber = atoi( Bin.Data() );
          
     ProfDriverIpolFiles = _ProfDriverIpolFiles;
     ProfDriverIpolVars  = _ProfDriverIpolVars;

     has_ggf_pd = has_vbf_vh_pd = has_tth_pd = false;
     // Setup ProfDriver if the static vector is empty, otherwise we are done     
     for(auto ProfDriverIpolFile : ProfDriverIpolFiles) {

         v_pd.push_back( ProfDriver( (std::string) ProfDriverIpolFile.Data() ) );

         if(ProfDriverIpolFile.Contains("ggf"))    has_ggf_pd = true;
         if(ProfDriverIpolFile.Contains("vbf_vh")) has_vbf_vh_pd = true;
         if(ProfDriverIpolFile.Contains("tth"))    has_tth_pd = true;
        
      }
      
      
     cout << "has_ggf_pd = " << has_ggf_pd << "\t" << "has_vbf_vh_pd = " << has_vbf_vh_pd << "\t" << "has_tth_pd = " << has_tth_pd << endl;
     
      
}


RooProfDriverWrapper::RooProfDriverWrapper(const RooProfDriverWrapper& other, const char* name) :
	RooAbsReal(other,name),
	xs_ggh(other.xs_ggh),
	has_ggf_pd(other.has_ggf_pd),
	xs_vbf_vh(other.xs_vbf_vh),
	has_vbf_vh_pd(other.has_vbf_vh_pd),
	xs_tth(other.xs_tth),
	has_tth_pd(other.has_tth_pd),
	NormalizedScan(other.NormalizedScan),
	LinearScan(other.LinearScan),
        indexLow(other.indexLow),
        indexHigh(other.indexHigh),	
	xs_vbf_vh_tot(other.xs_vbf_vh_tot),
    cg("cg",this,other.cg),
    cgt("cgt",this,other.cgt),
    ca("ca",this,other.ca),
    cat("cat",this,other.cat),
    cH("cH",this,other.cH),
    cT("cT",this,other.cT),
    cB("cB",this,other.cB),
    cW("cW",this,other.cW),
    cHW("cHW",this,other.cHW), 
    cHWt("cHWt",this,other.cHWt),
    cHB("cHB",this,other.cHB), 
    cHBt("cHBt",this,other.cHBt),
    cHWB("cHWB",this,other.cHWB),
    cHWBt("cHWBt",this,other.cHWBt),
    mu("mu",this,other.mu)
{

     WrapperName = other.WrapperName;
     HistName = other.HistName;
     BinNumber = other.BinNumber;
  	 ProfDriverIpolFiles = other.ProfDriverIpolFiles;
  	 ProfDriverIpolVars = other.ProfDriverIpolVars;
  	 v_pd = other.v_pd;  	 
  	 RunMap = other.RunMap;
 	 
  	 
}

// ---------------------------------------------------------------------------------------
// Destructors

RooProfDriverWrapper::~RooProfDriverWrapper() { }


// ---------------------------------------------------------------------------------------
// Evaluate Function  

Double_t RooProfDriverWrapper::evaluate() const
{
  double value(0);
    
  map <Str,RooRealProxy> ParMap;

  ParMap["cg"]   = cg;  ParMap["cgt"]  = cgt;
  ParMap["ca"]   = ca;  ParMap["cat"]  = cat;
  ParMap["cH"]   = cH;  ParMap["cT"]   = cT;
  ParMap["cB"]   = cB;  ParMap["cW"]   = cW;
  ParMap["cHW"]  = cHW; ParMap["cHWt"] = cHWt;
  ParMap["cHB"]  = cHB; ParMap["cHBt"] = cHBt; 
  ParMap["cHWB"]  = cHWB; ParMap["cHWBt"] = cHWBt;
  ParMap["mu"]   = mu;
  
  // Loop over all ProfDrivers and produce a prediction 
  int ni(0);  
  for(auto pd : v_pd) {
    
   // ggf ProvDriver: 
   if(ProfDriverIpolFiles[ni].Contains("ggf")) {
    
     VecD cs, csSM;

     for(Str Var : ProfDriverIpolVars[ni]) {
        cs.push_back(Double_t(ParMap[Var])/0.06);  //Scale by SMEFT
      // cs.push_back(Double_t(ParMap[Var]));
        csSM.push_back(0.);
     }
      
     //updated by glu
     double variation = 1.0;
     if (LinearScan)
        variation = pd.value( (string) HistName.Data(), BinNumber, cs) / pd.value( (string) HistName.Data(), BinNumber, csSM) + calculateBRRatio_linear() - 1.0;
      else
        variation = pd.value( (string) HistName.Data(), BinNumber, cs) / pd.value( (string) HistName.Data(), BinNumber, csSM) + calculateBRRatio_linear() + calculateBRRatio_quad() -2.0;

      value += xs_ggh == 0 ? pd.value( (string) HistName.Data(), BinNumber, cs)
                        : xs_ggh * variation;
//     value += xs_ggh == 0 ? pd.value( (string) HistName.Data(), BinNumber, cs)
//                         : xs_ggh * pd.value( (string) HistName.Data(), BinNumber, cs) / pd.value( (string) HistName.Data(), BinNumber, csSM);
    cout<<"Ratio of "<<HistName<<" "<<BinNumber<<" "<<cs.back()<<": "<<value/xs_ggh<<endl;
   //updated by glu
   //cout<<"index Low "<<indexLow<<" index High "<<indexHigh<<" BinNumber "<<BinNumber<<endl; 
   
   if (BinNumber < indexLow || BinNumber > indexHigh)
   {
     // set back the k-factor to 1.0
     value = xs_ggh;
   }
	
     if(value < 0) {
 // cout << "Warning: negative cross section encountered: xs_ggh = " << value << "\t" << cg << endl; 
 // value = 0.; //commented by glu
  }
      
    
   // vbf_vh ProvDriver:
   } else if (ProfDriverIpolFiles[ni].Contains("vbf_vh")) {
    
     VecD cs, csSM;

     for(Str Var : ProfDriverIpolVars[ni]) {     
        cs.push_back(Double_t(ParMap[Var])/0.06);  //Scale by SMEFT
      // cs.push_back(Double_t(ParMap[Var]));
	 csSM.push_back(0.);
     }
      
     double variation = 1.0;
     if (LinearScan)
        variation = pd.value( (string) HistName.Data(), BinNumber, cs) / pd.value( (string) HistName.Data(), BinNumber, csSM) + calculateBRRatio_linear() - 1.0;
      else
        variation = pd.value( (string) HistName.Data(), BinNumber, cs) / pd.value( (string) HistName.Data(), BinNumber, csSM) + calculateBRRatio_linear() + calculateBRRatio_quad() -2.0;

      value += xs_vbf_vh == 0 ? pd.value( (string) HistName.Data(), BinNumber, cs)
                        : xs_vbf_vh * variation;

//     value += xs_vbf_vh == 0 ? pd.value( (string) HistName.Data(), BinNumber, cs) 
//                            : xs_vbf_vh * pd.value( (string) HistName.Data(), BinNumber, cs) / pd.value( (string) HistName.Data(), BinNumber, csSM);
    
     if(value < 0) 
     {
	//cout << "Warning: negative cross section encountered: xs_vbf_vh = " << value << "\t" << cHW << endl;
	//value = 1; //added by glu
     }
    
     // Do we perform a normalized scan? (Only works for VBF+VH)
     if(NormalizedScan) {
       double factor = 0., stot = 0.;
       for(int nb = 0; nb < pd.nbins((string) HistName.Data()); nb++) {
          stot   += xs_vbf_vh_tot[nb];  
          if (LinearScan)  // updated by glu
            factor += xs_vbf_vh_tot[nb] * ( pd.value( (string) HistName.Data(), nb, cs) / pd.value( (string) HistName.Data(), nb, csSM) + calculateBRRatio_linear() - 1.0);
          else
            factor += xs_vbf_vh_tot[nb] * ( pd.value( (string) HistName.Data(), nb, cs) / pd.value( (string) HistName.Data(), nb, csSM) + calculateBRRatio_linear() + calculateBRRatio_quad() -2.0);
       }
       value *= stot / factor;
     }
   //updated by glu
    if (BinNumber < indexLow || BinNumber > indexHigh)
        {
            // set back the k-factor to 1.0
                  value = xs_vbf_vh;
        }           
   // tth ProvDriver:
   } else if (ProfDriverIpolFiles[ni].Contains("tth")) {
    
     VecD cs, csSM;

     for(Str Var : ProfDriverIpolVars[ni]) {
        cs.push_back(Double_t(ParMap[Var])); 
        csSM.push_back(0.);
     }
      
     value += xs_tth == 0 ? pd.value( (string) HistName.Data(), BinNumber, cs) 
                            : xs_tth * pd.value( (string) HistName.Data(), BinNumber, cs) / pd.value( (string) HistName.Data(), BinNumber, csSM);
    
     if(value < 0) cout << "Warning: negative cross section encountered: xs_tth = " << value << "\t" << cHW << endl;
  
   } else Fatal("RooProfDriverWrapper::evaluate(): Unknown ProfDriver encountered, please specify what should be done","");
  
   // Up our counter 
   ni++;
   
  }
  
  
   /*
  if (LinearScan)
  	value *= calculateBRRatio_linear(); // interference-only
  else
  	value *= calculateBRRatio_linear_quad();      
  */

  double BRvariation = (LinearScan) ? calculateBRRatio_linear() : calculateBRRatio_linear_quad();
  if (NormalizedScan)
    BRvariation = 1.0;
  // If we don't have ggf or vbf_vh handled in a ProfDriver, we want to sum the SM cross sections in
  if(!has_ggf_pd)
   value += xs_ggh*BRvariation;
  if(!has_vbf_vh_pd) 
   value += xs_vbf_vh*BRvariation;
  if(!has_tth_pd) 
   value += xs_tth*BRvariation;

  // Correct the partial Higgs decay width to yy 
  //value *= calculateHiggstoDiphotonDecayWidthRatio();   
     
  // Correct the total cross section with the ratios of widths 
  //value *= calculateHiggsDecayWidthRatio();
  
  // Multiply Parameter with 'mu'
  //value *= ParMap["mu"];
             
  // And we are done
 //return value >=0 ? value : 0.; // updated by glu
 return value;  
}

// ---------------------------------------------------------------------------------------
// Evaluate Function for ggF

Double_t RooProfDriverWrapper::evaluate_ratio_ggF() const
{
  double ratio(1);
    
  map <Str,RooRealProxy> ParMap;

  ParMap["cg"]   = cg;  ParMap["cgt"]  = cgt;
  ParMap["ca"]   = ca;  ParMap["cat"]  = cat;
  ParMap["cH"]   = cH;  ParMap["cT"]   = cT;
  ParMap["cB"]   = cB;  ParMap["cW"]   = cW;
  ParMap["cHW"]  = cHW; ParMap["cHWt"] = cHWt;
  ParMap["cHB"]  = cHB; ParMap["cHBt"] = cHBt; 
  ParMap["cHWB"]  = cHWB; ParMap["cHWBt"] = cHWBt; 
  // Loop over all ProfDrivers and produce a prediction 
  int ni(0);  
  for(auto pd : v_pd) {
    
   // ggf ProvDriver: 
   if(ProfDriverIpolFiles[ni].Contains("ggf")) {
    
     VecD cs, csSM;

     for(Str Var : ProfDriverIpolVars[ni]) {
        cs.push_back(Double_t(ParMap[Var])); 
        csSM.push_back(0.);
     }
      
     ratio = pd.value( (string) HistName.Data(), BinNumber, cs) / pd.value( (string) HistName.Data(), BinNumber, csSM);
    

     if(ratio < 0) { cout << "Warning: negative cross section encountered: ratio_xs_ggh = " << ratio << "\t" << cg << endl; ratio = 1.;}
      
   }
  
  }  
          
  // Correct the partial Higgs decay width to yy 
  ratio *= calculateHiggstoDiphotonDecayWidthRatio();   
     
  // Correct the total cross section with the ratios of widths 
  ratio *= calculateHiggsDecayWidthRatio();
              
  // And we are done
  return ratio; 
   
}

// ---------------------------------------------------------------------------------------
// Evaluate Function for VBF+VH

Double_t RooProfDriverWrapper::evaluate_ratio_VBF_VH() const
{
  double ratio(1);
    
  map <Str,RooRealProxy> ParMap;

  ParMap["cg"]   = cg;  ParMap["cgt"]  = cgt;
  ParMap["ca"]   = ca;  ParMap["cat"]  = cat;
  ParMap["cH"]   = cH;  ParMap["cT"]   = cT;
  ParMap["cB"]   = cB;  ParMap["cW"]   = cW;
  ParMap["cHW"]  = cHW; ParMap["cHWt"] = cHWt;
  ParMap["cHB"]  = cHB; ParMap["cHBt"] = cHBt; 
  ParMap["cHWB"]  = cHWB; ParMap["cHWBt"] = cHWBt;  
  // Loop over all ProfDrivers and produce a prediction 
  int ni(0);  
  for(auto pd : v_pd) {
  
  // VBF+VH ProvDriver:     
  if (ProfDriverIpolFiles[ni].Contains("vbf_vh")) {
    
     VecD cs, csSM;

     for(Str Var : ProfDriverIpolVars[ni]) {
        cs.push_back(Double_t(ParMap[Var])); 
        csSM.push_back(0.);
     }
      
     ratio = pd.value( (string) HistName.Data(), BinNumber, cs) / pd.value( (string) HistName.Data(), BinNumber, csSM);
    
     if(ratio < 0) cout << "Warning: negative cross section encountered: ratio_xs_vbf_vh = " << ratio << "\t" << cHW << endl;
  
   } 
   
   // Up our counter 
   ni++;
   
  }
          
  // Correct the partial Higgs decay width to yy 
  ratio *= calculateHiggstoDiphotonDecayWidthRatio();   
     
  // Correct the total cross section with the ratios of widths 
  ratio *= calculateHiggsDecayWidthRatio();
              
  // And we are done
  return ratio; 
   
}

// ---------------------------------------------------------------------------------------
// Evaluate Function for ttH

Double_t RooProfDriverWrapper::evaluate_ratio_ttH() const
{
  double ratio(1);
    
  map <Str,RooRealProxy> ParMap;

  ParMap["cg"]   = cg;  ParMap["cgt"]  = cgt;
  ParMap["ca"]   = ca;  ParMap["cat"]  = cat;
  ParMap["cH"]   = cH;  ParMap["cT"]   = cT;
  ParMap["cB"]   = cB;  ParMap["cW"]   = cW;
  ParMap["cHW"]  = cHW; ParMap["cHWt"] = cHWt;
  ParMap["cHB"]  = cHB; ParMap["cHBt"] = cHBt; 
  ParMap["cHWB"]  = cHWB; ParMap["cHWBt"] = cHWBt; 
  // Loop over all ProfDrivers and produce a prediction 
  int ni(0);  
  for(auto pd : v_pd) {
    
   // ttH ProvDriver: 
   if (ProfDriverIpolFiles[ni].Contains("tth")) {
    
     VecD cs, csSM;

     for(Str Var : ProfDriverIpolVars[ni]) {
        cs.push_back(Double_t(ParMap[Var])); 
        csSM.push_back(0.);
     }
      
     ratio = pd.value( (string) HistName.Data(), BinNumber, cs) / pd.value( (string) HistName.Data(), BinNumber, csSM);
    
     if(ratio < 0) cout << "Warning: negative cross section ratio encountered: ratio_xs_tth = " << ratio << "\t" << cHW << endl;
  
   }
  
   // Up our counter 
   ni++;
   
  }
        
  // Correct the partial Higgs decay width to yy 
  ratio *= calculateHiggstoDiphotonDecayWidthRatio();   
     
  // Correct the total cross section with the ratios of widths 
  ratio *= calculateHiggsDecayWidthRatio();
              
  // And we are done
  return ratio; 
   
}

// ---------------------------------------------------------------------------------------
// Function that calculates the total Higgs Decay width for a given set of Wilson coefficients

Double_t RooProfDriverWrapper::calculateHiggsDecayWidthRatio() const
{

   // Higgs boson width at mH = 125.09 GeV
   double sm_width = 4.074;

   // These values were obtained from MG5 
/*
   double new_width = sm_width + 1934.7*Double_t(ca)*Double_t(ca) - 8.4655*Double_t(ca)     //increase in partial width to photons due to cA
                           + 1937.9* Double_t(cat)* Double_t(cat)                           //increase in partial width to photons due to tcA
                           + 6810354* Double_t(cg)*Double_t(cg) + 3057.12*Double_t(cg)      //increase in partial width to gluons due to cG
                           + 6806751*Double_t(cgt)*Double_t(cgt)                            //increase in partial width to gluons due to tcG
                           + 3.3611*Double_t(cHW)*Double_t(cHW) + 3.368*Double_t(cHW)       //increase in partial width to Ws due to cHW
                           + 0.271* Double_t(cHWt)* Double_t(cHWt)                          //increase in partial width to Ws due to tcHW
                           + 0.3872* Double_t(cHW)* Double_t(cHW) + 0.4462* Double_t(cHW);  //increase in partial width to Zs due to cHW (cHB=cHW)
   
  return sm_width / new_width; 
*/
// updated by glu
	double ratio =  1 - 1.5*Double_t(cHB) + 50.6*Double_t(cg) - 1.21*Double_t(cHW) + 1.21*Double_t(cHWB);
//        if (ratio <= 0.) return 0;
	if (ratio == 0.) return 0;
	return 1/ratio;
}

// ---------------------------------------------------------------------------------------  
// Function that calculates the total Higgs Decay width for a given set of Wilson coefficients

Double_t RooProfDriverWrapper::calculateHiggstoDiphotonDecayWidthRatio() const
{

   double ratio;

   // Higgs boson width to yy at mH = 125.09 GeV
   // double sm_yy_width = 0.00924798;
   double sm_yy_width = 0.00928872;
/*
   // These values were obtained from MG5 
   double new_yy_width = sm_yy_width + 1934.7*Double_t(ca)*Double_t(ca) - 8.4655*Double_t(ca)     //increase in partial width to photons due to cA
                           + 1937.9* Double_t(cat)* Double_t(cat);                          //increase in partial width to photons due to tcA
   
   
  ratio = new_yy_width / sm_yy_width;
*/
//updated by glu
//need to comment out the BSM^2 terms when considering the the interference only limits.
  ratio = 1 - 2.4e02*Double_t(cHW) //Interference cHW
	-8e02*Double_t(cHB) // Interference cHB
	+4.4e02*Double_t(cHWB) // Interference cHWB
	+143.1*Double_t(cHW)*Double_t(cHW)  // BSM^2 cHW
	+143.1*Double_t(cHWt)*Double_t(cHWt) // BSM^2 cHWt 
	+1541*Double_t(cHB)*Double_t(cHB) // BSM^2 cHB 
	+1541*Double_t(cHBt)*Double_t(cHBt); // BSM^2 cHBt 
	
  if(ratio < 0) cout << "Warning: negative Higgs to diphoton width encountered: width ratio to SM = " << ratio << "\t" << ca << endl;
                
 // return ratio > 0 ? ratio : 0.;
 return ratio;

}

Double_t RooProfDriverWrapper::calculateBRRatio_linear_quad() const
{

  double ratio_aa = 1
  -233*Double_t(cHW) // Interfernce cHW
  -765*Double_t(cHB)  //Interfernce cHB
  + 422*Double_t(cHWB) // Interfernce cHWB
  + 13572*Double_t(cHW)*Double_t(cHW) // BSM^2 cHW
  + 146306*Double_t(cHB)*Double_t(cHB) // BSM^2 cHB
  + 44521*Double_t(cHWB)*Double_t(cHWB) // Interfernce cHWB
  + 13572*Double_t(cHWt)*Double_t(cHWt) // BSM^2 cHWt
  + 146306*Double_t(cHBt)*Double_t(cHBt) // BSM^2 cHBt
  + 44521*Double_t(cHWBt)*Double_t(cHWBt); // BSM^2 cHWBt
  //+ 90029 *Double_t(cHW)*Double_t(cHB)	 // BSM^2 cHW*cHB
  //+ 90029 *Double_t(cHWt)*Double_t(cHBt);   // BSM^2 cHWt*cHBt
  // change in H total width
  double ratio_total = 1  -  1.40*Double_t(cHB)   + 50.6*Double_t(cg)   - 1.22*Double_t(cHW) + 2.89* Double_t(cHWB);
  return ratio_aa/ratio_total;

}
// ---------------------------------------------------------------------------------------
Double_t RooProfDriverWrapper::calculateBRRatio_linear() const
{

  // change in the H > a a width
  double ratio_aa = 1
  -233*Double_t(cHW) // Interfernce cHW
  -765*Double_t(cHB) //Interfernce cHB
  + 422*Double_t(cHWB); // Interfernce cHWB
//  + 143.1e02*Double_t(cHW)*Double_t(cHW) // BSM^2 cHW
//  + 1541e02*Double_t(cHB)*Double_t(cHB) // BSM^2 cHB
// + 479e02*Double_t(cHWB)*Double_t(cHWB) // BSM^2 cHWB
//  + 143.1e02*Double_t(cHWt)*Double_t(cHWt) // BSM^2 cHWt
//  + 1541e02*Double_t(cHBt)*Double_t(cHBt) // BSM^2 cHBt
//  + 479e02*Double_t(cHWBt)*Double_t(cHWBt); // BSM^2 cHWBt

  // change in H total width
  double ratio_total = 1  - 1.40*Double_t(cHB)   + 50.6*Double_t(cg)   - 1.22*Double_t(cHW) + 2.89* Double_t(cHWB);
  //double ratio_total = 1  - 1.5*Double_t(cHB)   + 50*Double_t(cg)   - 1.21*Double_t(cHW) + 1.21* Double_t(cHWB); // TODO: to be updated
  return ratio_aa/ratio_total;

}
// ---------------------------------------------------------------------------------------
Double_t RooProfDriverWrapper::calculateBRRatio_quad() const
{
  double ratio_aa = 1
  + 13572*Double_t(cHW)*Double_t(cHW) // BSM^2 cHW
  + 146306*Double_t(cHB)*Double_t(cHB) // BSM^2 cHB
  + 44521*Double_t(cHWB)*Double_t(cHWB) // BSM^2 cHWB
  + 13572*Double_t(cHWt)*Double_t(cHWt) // BSM^2 cHWt
  + 146306*Double_t(cHBt)*Double_t(cHBt) // BSM^2 cHBt
  + 44521*Double_t(cHWBt)*Double_t(cHWBt); // BSM^2 cHWBt

 double ratio_total = 1  - 1.40*Double_t(cHB)   + 50.6*Double_t(cg)   - 1.22*Double_t(cHW) + 2.89* Double_t(cHWB);
//  double ratio_total = 1  - 1.5*Double_t(cHB)   + 50*Double_t(cg)   - 1.21*Double_t(cHW) + 1.21* Double_t(cHWB); // TODO: to be updated
  return ratio_aa/ratio_total;
}

// ---------------------------------------------------------------------------------------



// ---------------------------------------------------------------------------------------
ClassImp(RooProfDriverWrapper)
// ---------------------------------------------------------------------------------------
