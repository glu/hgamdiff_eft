/**
 * EFT module for Gamma Combination code
 * 
 * Authors: Florian Urs Bernlochner, florian.bernlochner@cern.ch,
 *          Christopher Meyer, chris.mejer@cern.ch,
 *          Jim Lacey, jim.lacey@cern.ch,
 *          Yanping Huang, yanping.huang@cern.ch,
 *          Maria Josefina Alconada Verzini, josefina.alconada@cern.ch
 * 
 * Date: October 2016
 *
 **/

#include "PDF_HGam_Meas_Run1.h"
#include "PDF_HGam_Meas_Run1.h"

// ---------------------------------------------------------------------------------------
// Constructor

PDF_HGam_Meas_Run1::PDF_HGam_Meas_Run1(Str SettingsFileName, Str IpolFileName) : PDF_Abs(1) 
{
    
    // Open Settings file 
    set = OpenSettingsFile(SettingsFileName);
    ipol = OpenSettingsFile(IpolFileName);

    // Fetch HepDataFile and Keys
    Str HepDataFile = set->GetValue("HepDataFile","");    
    HepDataKeys = Vectorize(set->GetValue("HepDataKeys","")," ");
    MeasurementReader = new HGMR::HGamMeasurementReader(set);

    // Read in measurements         
    MeasurementReader->loadMeasurements(HepDataFile,HepDataKeys);

    // Determine number of Bins and set Keys
    nObs = 0;
    for(auto HepDataKey : HepDataKeys)
     nObs += MeasurementReader->getXS(HepDataKey).size();
    this->ResizeTo(nObs);
 
    // Name of the PDF
	name = "HGam_Meas_Run1"; 
	
	// Initialize the Parameters, Relations and Observables
	initParameters();
	initRelations();
	initObservables();
	
	// Setup the Observables, Uncertainties and Correlations
	setObservables("");
	setUncertainties("");
	setCorrelations("");
	
	// Build the Covariance matrix and eventual PDF
	buildCov();
	buildPdf();
	
}

// ---------------------------------------------------------------------------------------
// Destructor

PDF_HGam_Meas_Run1::~PDF_HGam_Meas_Run1() {}

// ---------------------------------------------------------------------------------------
// Init free parameters of the scan

void PDF_HGam_Meas_Run1::initParameters() 
{

	EFT_Parameters p;
	
	parameters = new RooArgList("parameters");
	
	parameters->add(*(p.get("cg")));
	parameters->add(*(p.get("cgt")));
	parameters->add(*(p.get("ca")));
	parameters->add(*(p.get("cat")));
	parameters->add(*(p.get("cH")));
	parameters->add(*(p.get("cT")));
	parameters->add(*(p.get("cB")));
	parameters->add(*(p.get("cW")));
	parameters->add(*(p.get("cHW")));
	parameters->add(*(p.get("cHWt")));
	parameters->add(*(p.get("cHB")));
	parameters->add(*(p.get("cHBt")));
	parameters->add(*(p.get("cHWB")));
        parameters->add(*(p.get("cHWBt")));
	parameters->add(*(p.get("mu")));

	
}

// ---------------------------------------------------------------------------------------
// Init relations that relate the free parameters with the measured observables

void PDF_HGam_Meas_Run1::initRelations() 
{
   
   // Fetch all Wilson coefficients
   RooRealVar *cg = (RooRealVar*)parameters->find("cg");
   RooRealVar *cgt = (RooRealVar*)parameters->find("cgt");
   RooRealVar *ca = (RooRealVar*)parameters->find("ca");
   RooRealVar *cat = (RooRealVar*)parameters->find("cat");
   RooRealVar *cH = (RooRealVar*)parameters->find("cH");
   RooRealVar *cT = (RooRealVar*)parameters->find("cT");
   RooRealVar *cB = (RooRealVar*)parameters->find("cB");
   RooRealVar *cW = (RooRealVar*)parameters->find("cW");
   RooRealVar *cHW = (RooRealVar*)parameters->find("cHW");
   RooRealVar *cHWt = (RooRealVar*)parameters->find("cHWt");
   RooRealVar *cHB = (RooRealVar*)parameters->find("cHB");
   RooRealVar *cHBt = (RooRealVar*)parameters->find("cHBt");
   RooRealVar *cHWB = (RooRealVar*)parameters->find("cHWB");
   RooRealVar *cHWBt = (RooRealVar*)parameters->find("cHWBt");
   RooRealVar *mu = (RooRealVar*)parameters->find("mu");

   // Setup theory list
   theory = new RooArgList("theory");
   
   // Add observables
   int nKey(0);
   
   for(auto HepDataKey: HepDataKeys) {

     VecD XSecs = MeasurementReader->getXS(HepDataKey);
     for(auto XSec : XSecs) {
      Str Tag = Form("%s:%d_th",HepDataKey.Data(),nKey);     
      StrV PDIFiles = Vectorize(set->GetValue("ProfDriverIpolFiles","")," ");
      StrVV PDIVars =  Vectorize(ipol->GetValue("ProfDriverIpolVars",""),":"," ");  
      VecD emptyVector;    
      theory->add( *(new RooProfDriverWrapper(Tag,Tag,PDIFiles,PDIVars, 0., 0., 0., false, emptyVector, *cg, *cgt, *ca, *cat, *cH, *cT, *cB, *cW, *cHW, *cHWt, *cHB, *cHBt,*cHWB,*cHWBt ,*mu)) );      
      nKey++;
     }

   }

   
}

// ---------------------------------------------------------------------------------------
// Initalize Observables 

void PDF_HGam_Meas_Run1::initObservables() 
{

   // Setup observable list
   observables = new RooArgList("observables");
	
   // Add observables
   int nKey(0);
   
   for(auto HepDataKey: HepDataKeys) {

     VecD XSecs = MeasurementReader->getXS(HepDataKey);
     for(auto XSec : XSecs) {
      Str Tag = Form("%s:%d_obs",HepDataKey.Data(),nKey);
      observables->add(*(new RooRealVar(Tag,Tag, 0, -1e4, 1e4)));  
      nKey++;
     }

   }
   

	
}

// ---------------------------------------------------------------------------------------
// Init Observables 

void PDF_HGam_Meas_Run1::setObservables(TString c) 
{

    if(c.EqualTo("truth"))
	  setObservablesTruth();
    else if(c.EqualTo("toy"))
	  setObservablesToy();
	  
   // Add observables
   int nKey(0);
   
   for(auto HepDataKey: HepDataKeys) {

     VecD XSecs = MeasurementReader->getXS(HepDataKey);
     for(auto XSec : XSecs) {
      Str Tag = Form("%s:%d_obs",HepDataKey.Data(),nKey);
	  setObservable(Tag, XSec);
      nKey++;
     }

   }

}

// ---------------------------------------------------------------------------------------
// Init Observable Uncertainties 

void PDF_HGam_Meas_Run1::setUncertainties(TString c) 
{

   // The first error is stat. and the second systematical
   // The second will be assumed to be a 100% correlated by default
   // unless we intervene, but since it's set to zero it does not matter

   // Add observables
   int nKey(0);
   
   for(auto HepDataKey: HepDataKeys) {

     VecD XSecTotErrors = MeasurementReader->getXS_TotErr(HepDataKey);
     for(auto XSecTotError : XSecTotErrors) {
      Str Tag = Form("%s:%d_obs",HepDataKey.Data(),nKey);
	  setUncertainty(Tag, XSecTotError, 0.);
      nKey++;
     }

   } 

}

// ---------------------------------------------------------------------------------------
// Set Correlations

void PDF_HGam_Meas_Run1::setCorrelations(TString c) 
{
	resetCorrelations();

    TMatrixD CovTot(nObs,nObs);
    CovTot = MeasurementReader->getFullCov();
    
    for(int xi = 0; xi < nObs; xi++)
     for(int yi = 0; yi < nObs; yi++)
      corStatMatrix[xi][yi] = CovTot[xi][yi]/sqrt(CovTot[xi][xi])/sqrt(CovTot[yi][yi]);

	return;
}


// ---------------------------------------------------------------------------------------
// Build final PDF -- we model the observables / measurements with a multivariate Gaussian

void PDF_HGam_Meas_Run1::buildPdf() 
{
	
	pdf = new RooMultiVarGaussian("pdf_"+name, "pdf_"+name, *(RooArgSet*)observables, *(RooArgSet*)theory, covMatrix);
			
}

// ---------------------------------------------------------------------------------------

