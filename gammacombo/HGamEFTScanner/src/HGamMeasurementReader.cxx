
#include "../include/HGamMeasurementReader.h"

#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"

using namespace HGMR;

//HGamMeasurementReader::HGamMeasurementReader(Str measurements_name ){};
//HGamMeasurementReader::HGamMeasurementReader( Str hepdata_file, StrV var_keys ){};

HGamMeasurementReader::HGamMeasurementReader(TEnv *set)
: _set(set) 
{
  cout<<" -----------  >>> MEAS READER CONSTRUCTER START"<<endl;                             
  Str HepDataFile = set->GetValue("HepDataFile","");                                              
  StrV HepDataStatCorrKeys = vectorize(set->GetValue("HepDataStatCor.Keys","")," ");     
  loadMeasurements( HepDataFile, HepDataStatCorrKeys );
  _binsAll = _bins;
  _binKeysCorr = _keys;
  int nbins = 0;
  for ( auto key:_binKeysCorr ) {    
    _blockIndexCorr[key] = nbins;
    nbins += _binsAll[key].size()-1;
  }
  initFullMatrixStatCorr( );    
  initFullTheoryCovMatrix( );
  
  if ( (_statCorr.GetNrows() != _thCov.GetNrows()) || 
       (_statCorr.GetNcols() != _thCov.GetNcols()) ) {
    fatal(Form("Dimension missmatch between stat.(%d,%d) and th. (%d,%d) covariance matrices!",
	       _statCorr.GetNrows(),_statCorr.GetNcols(),
	       _thCov.GetNrows(),_thCov.GetNcols()));
  }
  cout<<" -----------  >>> MEAS READER CONSTRUCTER DONE!"<<endl;
};

void out(Str msg) {
   std::cout << msg << std::endl;
}

void HGamMeasurementReader::printTime()
{
   static bool first=true;
   static time_t start;
   if (first) { first=false; ::time(&start); }
   time_t aclock; ::time( &aclock );
   std::cout << "* Current time: " << asctime( localtime( &aclock ) )
   << "*   ( "<< ::difftime( aclock, start) <<" s elapsed )" << std::endl;
}




void HGamMeasurementReader::fatal(Str msg) {
   // print the error message
   std::cerr <<
   "\n*********************\n" <<
   "* HGamMeasurementReader, Fatal Error:  \n\n" <<
   "  " << msg <<
   "\n\n*********************\n";
   // print the time elapsed
   std::cout << "\n* Run aborted due to error.\n";
   printTime(); std::cout << std::endl;   
   // exit the session
   exit(1);
}

bool HGamMeasurementReader::fileExists(Str fn) { 
  return !(gSystem->AccessPathName(fn.Data()));
}

TH2F* HGamMeasurementReader::matrix2TH2(TMatrixD matrix, TString name)
{
int nbins = matrix.GetNrows();
TH2F* histo = new TH2F(name,name,nbins,0,nbins,nbins,0,nbins);
for (int i = 0 ; i < nbins; i++)
{
	for (int j = 0 ; j < nbins; j++)
		histo->SetBinContent(i+1,j+1,matrix[nbins-j-1][i]);		
}
return histo;
}

// produce a vector of words from a text line separated by sep
// (by default space separated)
StrV HGamMeasurementReader::vectorize(Str str, Str sep) {
   StrV result; TObjArray *strings = str.Tokenize(sep.Data());
   if (strings->GetEntries()==0) { delete strings; return result; }
   TIter istr(strings);
   while (TObjString* os=(TObjString*)istr()) {
      if (os->GetString()[0]=='#') break;
      result.push_back(os->GetString());
   }
   delete strings; return result;
}

// helper function to return string of particular index in vector of strings
// after producing tokenized vector of strings
Str HGamMeasurementReader::stringSection(Str str, Str sep, int index) {
  StrV vec = vectorize(str,sep);
  if ( (int)vec.size()<index ) return "oops";
  else return vec[index];
}

// convert a text line containing a list of numbers to a vector<double>
VecD HGamMeasurementReader::vectorizeD(Str str, Str sep) {
   VecD result; StrV vecS = vectorize(str,sep);
   for (uint i=0;i<(uint)vecS.size();++i)
      result.push_back(atof(vecS[i]));
   return result;
}

//bin/HGamEFTScanner --var cgt --fix 'cg=0' -c 2 --scanrange -1:1 --npoints 100


void HGamMeasurementReader::initFullTheoryCovMatrix( )
{
  //cout<<" -----------  >>> THEORY CORR START "<<endl;
  TFile *f = TFile::Open( _set->GetValue("TheoryCorr.File","") );
/*
  TH1 *h_SM_COV = (TH1*)f->Get("SM_COV"), 
      *h_ggF_COV = (TH1*) f->Get("ggF_COV"), 
      *h_VBFVH_COV = (TH1*) f->Get("VBFVH_COV"), 
      *h_ttH_COV = (TH1*) f->Get("ttHbbH_COV");
*/
//updated by glu

  TH1 *h_SM_COV = (TH1*)f->Get("SM_COV_signedDphi"), 
      *h_ggF_COV = (TH1*) f->Get("ggF_COV_signedDphi"), 
      *h_VBFVH_COV = (TH1*) f->Get("VBFVH_COV_signedDphi"), 
      *h_ttH_COV = (TH1*) f->Get("ttHbbH_COV_signedDphi");

      
  int nbins = h_SM_COV->GetNbinsX();
  
  _thCov.ResizeTo(0,nbins-1,0,nbins-1);
  _th_ggf_Cov.ResizeTo(0,nbins-1,0,nbins-1); 
  _th_vbf_vh_Cov.ResizeTo(0,nbins-1,0,nbins-1); 
  _th_ttH_Cov.ResizeTo(0,nbins-1,0,nbins-1);
    
  if(_set->GetValue("AdditionalTheoryVBFVH",0.) > 0.) {
    double scale = 1. + _set->GetValue("AdditionalTheoryVBFVH",0.);
    h_VBFVH_COV->Scale(scale*scale);
  }

  for ( int ib=0; ib<nbins; ib++ )
    for ( int jb=0; jb<nbins; jb++ ) {
     //_thCov(ib,jb) = h_SM_COV->GetBinContent(ib+1,jb+1);
     _th_ggf_Cov(ib,jb) = h_ggF_COV->GetBinContent(ib+1,jb+1);
     _th_vbf_vh_Cov(ib,jb) = h_VBFVH_COV->GetBinContent(ib+1,jb+1);
     _th_ttH_Cov(ib,jb) = h_ttH_COV->GetBinContent(ib+1,jb+1);
     
     // Sum the total covariance from the individual pieces
     _thCov(ib,jb) +=  _th_ggf_Cov(ib,jb) + _th_vbf_vh_Cov(ib,jb) + _th_ttH_Cov(ib,jb);
     
    } 
     

  gStyle->SetPaintTextFormat(".2f");
  gStyle->SetOptStat("0000");
  TCanvas c("c1","c1",500,500);
  TH2F* hCOV = matrix2TH2(_thCov,"hCOV");
  hCOV->SetMarkerSize(0.5);
 hCOV->Draw("colztext"); 
 //_thCov.Draw("colztext");
  c.Print("full_theory_cov.pdf");
  c.Print("full_theory_cov.C");
  cout<<" -----------  >>> THEORY COV END "<<endl; 
  
  /*
  // Jim's old code to read in the theory covariance
  
  TMatrixD *m_theoryCorrPDF = (TMatrixD*)f->Get("m_PDF_CORR");
  TMatrixD *m_theoryCorrQCD = (TMatrixD*)f->Get("m_QCD_CORR");
  TH1D *h_pdf_diagonal_SD = (TH1D*)f->Get("h_PDF_SUMCOV_SD");
  TH1D *h_qcd_diagonal_SD = (TH1D*)f->Get("h_QCD_SUMCOV_SD");

  int nbins = h_qcd_diagonal_SD->GetNbinsX();
  //cout<<" ==================== >> NBINS TH COV :: "<<nbins<<endl;
  _thCov.ResizeTo(0,nbins-1,0,nbins-1);

  TMatrixD pdf_Cov, qcd_Cov;
  pdf_Cov.ResizeTo(0,nbins-1,0,nbins-1);
  qcd_Cov.ResizeTo(0,nbins-1,0,nbins-1);
  
  //cout<<" =============== 1"<<endl;
  //pdf_Cov.Print();
  //cout<<" =============== 2 "<<endl;
  for ( int ib=0; ib<nbins; ib++ )
    for ( int jb=0; jb<nbins; jb++ ){
      
      pdf_Cov(ib,jb) = (*m_theoryCorrPDF)(ib+1,jb+1) * 
	h_pdf_diagonal_SD->GetBinContent(ib+1) * 
	h_pdf_diagonal_SD->GetBinContent(jb+1);
      
      qcd_Cov(ib,jb) = (*m_theoryCorrQCD)(ib+1,jb+1) * 
	h_qcd_diagonal_SD->GetBinContent(ib+1) * 
	h_qcd_diagonal_SD->GetBinContent(jb+1);

    }
  
  //cout<<" -----------  >>> THEORY PDF COV "<<endl;
  //pdf_Cov.Print();
  _thCov = pdf_Cov + qcd_Cov;

  //_thCov.Print();
  gStyle->SetPaintTextFormat(".2f");
  TCanvas c("c1","c1",500,500);
  _thCov.Draw("colztext");
  c.Print("full_theory_cov.pdf");
  cout<<" -----------  >>> THEORY COV END "<<endl;
  */
  
}

void HGamMeasurementReader::initFullMatrixStatCorr( )
{

  cout<<" -----------  >>> STAT CORR START "<<endl;
    
  VecD HepDataStatCorr;
  if ( _set->GetValue("DataStatCorr.File","") != "" ){
    TEnv *set = OpenSettingsFile(_set->GetValue("DataStatCorr.File","") );
    HepDataStatCorr = vectorizeD( set->GetValue( _set->GetValue("DataStatCorr.Name",""), "" ), " " );
  }else
    HepDataStatCorr = vectorizeD(_set->GetValue("HepDataStatCor","")," ");
  
  int nbins = 0;
  for ( auto key:_binKeysCorr ) {    
    //cout<<" NBINS KEY ==> "<<_binsAll[key].size()-1<<endl;
    nbins += _binsAll[key].size()-1;
    //cout<<" NBINS ADD ==> "<<nbins<<endl;
  }  

  VecD rows;
  for ( int i = 0; i < nbins ; i++ ){
    //cout<<" ------------------------------------  >>> STAT CORR :: row: ==============> "<<i<<endl;
    VecD row;
    for ( int j = 0; j < nbins; j++ ){
      int index = j+i*(nbins);
      double stat = HepDataStatCorr[index];
      //cout<<" ->>> row: "<<i<<" -- col: "<<j<<" -- vec index: "<<j+i*(nbins)<<" --stat: "<< stat <<endl;
      row.push_back( stat );
      rows.push_back( stat );
    }
  }

  //cout<<" -----------  >>> STAT CORR MTRX "<<endl;
  TMatrixD statCorr(0,nbins-1,0,nbins-1, &rows[0] );
  //statCorr.Print();  
  _statCorr.ResizeTo(0,nbins-1,0,nbins-1);
  _statCorr = statCorr;
  //_statCorr.Print();
  gStyle->SetPaintTextFormat(".2f");
  gStyle->SetOptStat("0000");
  TCanvas c("c1","c1",500,500);
  TH2F* hStat = matrix2TH2(_statCorr,"hStat");
  hStat->SetMarkerSize(0.5);
  hStat->Draw("colztext");
 // _statCorr.Draw("colztext");
  c.Print("full_stat_corr.pdf");
   c.Print("full_stat_corr.C");
  //cout<<" -----------  >>> STAT CORR END "<<endl;
}

void HGamMeasurementReader::setSubMatrices(StrV keys)
{ setSubMatrix( keys, false ); setSubMatrix( keys, true ); }

void HGamMeasurementReader::setFullCov(StrV keys)
{  
  
  setSubMatrices(keys);
    
  if ( (_statCovSub.GetNrows() != _thCovSub.GetNrows()) || 
       (_statCovSub.GetNcols() != _thCovSub.GetNcols()) ) 
    fatal("Dimension of the sub stat. and th. covariance matrices are incompatable!");
  if ( (_cov.GetNrows() != _thCovSub.GetNrows()) || 
       (_cov.GetNcols() != _thCovSub.GetNcols()) ) 
    fatal("Dimension of the sub stat. and th. covariance matrices are incompatable!");

  _totalCov.ResizeTo(0,_thCovSub.GetNrows()-1,0,_thCovSub.GetNcols()-1);

  //printf(" Nrows :: tot(%d) -- th(%d) -- stat(%d) \n",_totalCov.GetNrows(),_thCovSub.GetNrows(),_statCovSub.GetNrows());
  //printf(" Ncols :: tot(%d) -- th(%d) -- stat(%d) \n",_totalCov.GetNcols(),_thCovSub.GetNcols(),_statCovSub.GetNcols());

// _totalCov = _statCovSub;
//_totalCov = _thCovSub;
 // _totalCov = _statCovSub+ _cov; //updated by glu  
_totalCov = _thCovSub + _cov + _statCovSub;
  
// Store Covariance matrix in a root file for later use
  /*
  TFile f("total_cov_julia.root","RECREATE");
  _totalCov.Write("TotalCov");
  _cov.Write("ExpCov");
  _statCovSub.Write("StatCov");
  _thCovSub.Write("TheoCov");
  f.Write(); f.Close();
  */
  // ----------------------------------------
  

  cout << " Total Covariance: " << endl;
  _totalCov.Print("all");

  cout << " Experimental Covariance: " << endl;
  _cov.Print("all");

  cout << " Statistical Covariance: " << endl;
  _statCovSub.Print("all");

  cout << "Theory Covariance: " << endl;
  _thCovSub.Print("all");

  gStyle->SetPaintTextFormat(".2f");
  gStyle->SetOptStat("0000");
  TCanvas c("c1","c1",500,500);
  TH2F* hExpCov = matrix2TH2(_cov,"ExpCov");
  hExpCov->SetMarkerSize(0.5);
  hExpCov->Draw("colztext");
//  _cov.Draw("colztext");
  c.Print("exp_cov.pdf");
  c.Print("exp_cov.C");
  c.Clear();

  TH2F* hTotalCov = matrix2TH2(_totalCov,"totalCov");
//  _totalCov.Draw("colztext");
  hTotalCov->SetMarkerSize(0.5);
  hTotalCov->Draw("colztext");
  c.Print("total_cov.pdf");
  c.Print("total_cov.C");

}


void HGamMeasurementReader::setSubMatrix(StrV keys, bool doStat)
{  

  ///////////////////////////////////////////
  // count the total number of bins in the reduced stat corr and initialize the sub-matrix 
  ///////////////////////////////////////////
  
  int nbins = 0;
  for ( auto key:keys ) nbins += _binsAll[key].size()-1;
  //TMatrixD statCorr(0,nbins-1,0,nbins-1);
  if ( doStat )
    _statCovSub.ResizeTo(0,nbins-1,0,nbins-1);
  else {
    _thCovSub.ResizeTo(0,nbins-1,0,nbins-1);
    _th_ggf_CovSub.ResizeTo(0,nbins-1,0,nbins-1);
    _th_vbf_vh_CovSub.ResizeTo(0,nbins-1,0,nbins-1);
    _th_ttH_CovSub.ResizeTo(0,nbins-1,0,nbins-1);
   }
   
  ///////////////////////////////////////////
  // fill only the diagonal blocks
  // --> all values should be 0 ;-)
  // -----> but if the input stat corr matrix has 1.0 for the diagonal elements will need to be changed :-/
  ///////////////////////////////////////////

  int nb = 0; 
  for ( int ik=0; ik<keys.size(); ik++ ){
    Str key = keys[ik];
    VecD xs_stat_key = getXS_StatErr(key);
    //for ( auto xs : xs_stat_key ) cout<<" --> key1 XS :: "<<xs<<endl;
    for ( int ix=0; ix<_binsAll[key].size()-1 ; ix++ ) 
      for ( int iy=0; iy<_binsAll[key].size()-1 ; iy++ ){
	if ( doStat ){
	  double value = _statCorr( _blockIndexCorr[key]+ix, _blockIndexCorr[key]+iy );
	 // _statCovSub(ix+nb,iy+nb) = value;// * xs_stat_key[ix];
	// if (ix == iy) value = 1;  //added by glu; in order to obtain the statistical-only cov matrix; should be commented in normal case 
	_statCovSub(ix+nb,iy+nb) = value * xs_stat_key[ix]* xs_stat_key[iy]; //updated by glu
	}else {
	  _thCovSub(ix+nb,iy+nb)         = _thCov( _blockIndexCorr[key]+ix,_blockIndexCorr[key]+iy );
	  _th_ggf_CovSub(ix+nb,iy+nb)    = _th_ggf_Cov( _blockIndexCorr[key]+ix,_blockIndexCorr[key]+iy );
	  _th_vbf_vh_CovSub(ix+nb,iy+nb) = _th_vbf_vh_Cov( _blockIndexCorr[key]+ix,_blockIndexCorr[key]+iy );
	  _th_ttH_CovSub(ix+nb,iy+nb)    = _th_ttH_Cov( _blockIndexCorr[key]+ix,_blockIndexCorr[key]+iy );
	  }
     }
    nb += _binsAll[key].size()-1; 
  }

  ///////////////////////////////////////////
  // fill only the off diagonal blocks
  // --> map the entries in the full stat corr matrix to the reduced corr matrix
  ///////////////////////////////////////////

  int nbcol = 0;
  for ( int ik=0; ik<keys.size()-1; ik++ ){ // rows    
    Str key1 = keys[ik];
    cout<<" ========================================================> key1:: "<<key1<<endl;
    int nbrow = 0;
    for ( int ik2=0; ik2<keys.size(); ik2++ ){ // cols
      Str key2 = keys[ik2];      
      if ( nbrow==0 ) nbrow += _binsAll[key1].size()-1 + nbcol;
      if ( ik < ik2 ) {
	VecD xs_stat_key1,  xs_stat_key2;
	if ( doStat ){
	  xs_stat_key1 = getXS_StatErr(key1);
	  xs_stat_key2 = getXS_StatErr(key2);
	  for ( auto xs : xs_stat_key1 ) cout<<" --> key1 XS :: "<<xs<<endl;
	  for ( auto xs : xs_stat_key2 ) cout<<" --> key2 XS :: "<<xs<<endl;
	}
	cout<<" =============================================> key2:: "<<key2<<endl;
	for ( int row=0; row<_binsAll[key2].size()-1 ; row++ ){ // bins for key2 --> rows for filling
	  for ( int col=0; col<_binsAll[key1].size()-1 ; col++ ){ // bins for key1 --> cols for filling
	    //statCorr(row+nbrow, col+nbcol) = value;
	    //statCorr(col+nbcol, row+nbrow) = value;
	    if ( doStat ){
	      double value = _statCorr( _blockIndexCorr[key2]+row, _blockIndexCorr[key1]+col );
	      //updated by glu; transform the correlation matrix into cov matrix
	      _statCovSub(row+nbrow, col+nbcol) = value * xs_stat_key1[col] * xs_stat_key2[row];
	      _statCovSub(col+nbcol, row+nbrow) = value * xs_stat_key1[col] * xs_stat_key2[row];
	      //_statCovSub(row+nbrow, col+nbcol) = value;
	      //_statCovSub(col+nbcol, row+nbrow) = value;
	      printf("row(%d) col(%d) M_row(%d) M_col(%d) fullRow(%d) fullCol(%d) val(%.3f) \n"
		     ,row ,col
		     ,row+nbrow ,col+nbcol
		     ,_blockIndexCorr[key1]+row ,_blockIndexCorr[key2]+col
		     ,value );
	    } else {
	      
	      double value = _thCov( _blockIndexCorr[key2]+row,_blockIndexCorr[key1]+col );
	      _thCovSub(row+nbrow,col+nbcol) = value; _thCovSub(col+nbcol,row+nbrow) = value;
	      
	      double value_ggf = _th_ggf_Cov( _blockIndexCorr[key2]+row,_blockIndexCorr[key1]+col );
	      _th_ggf_CovSub(row+nbrow,col+nbcol) = value_ggf; _th_ggf_CovSub(col+nbcol,row+nbrow) = value_ggf;

	      double value_vbf_vh = _th_vbf_vh_Cov( _blockIndexCorr[key2]+row,_blockIndexCorr[key1]+col );
	      _th_vbf_vh_CovSub(row+nbrow,col+nbcol) = value_vbf_vh; _th_ggf_CovSub(col+nbcol,row+nbrow) = value_vbf_vh;

	      double value_ttH = _th_ttH_Cov( _blockIndexCorr[key2]+row,_blockIndexCorr[key1]+col );
	      _th_ttH_CovSub(row+nbrow,col+nbcol) = value_ttH; _th_ggf_CovSub(col+nbcol,row+nbrow) = value_ttH;
	      
	    }


	  } // cols (key1)
	} // rows (key2)
	nbrow += _binsAll[key2].size()-1;
      } // ik < ik2
    } // key2
    nbcol += _binsAll[key1].size()-1;
  } // key1

  //out("***************** SUB MATRIX");
  //statCorr.Print();
  gStyle->SetPaintTextFormat(".2f");
  gStyle->SetOptStat("0000");
  TCanvas c("c2","c2",500,500);
  if ( doStat ){
    //_statCovSub.Print("all");
    //abort();
   TH2F* hstatCovSub = matrix2TH2(_statCovSub,"statCovSub");
   hstatCovSub->SetMarkerSize(0.5); 
//   _statCovSub.Draw("colztext");
   hstatCovSub->Draw("colztext");
    c.Print("subCov_stat.pdf");
    c.Print("subCov_stat.C");
  }else{
   TH2F* hthCovSub = matrix2TH2(_thCovSub, "theoCovSub"); 
   hthCovSub->SetMarkerSize(0.5);
   hthCovSub->Draw("colztext");
//    _thCovSub.Draw("colztext");
    c.Print("subCov_th.pdf");
    c.Print("subCov_th.C");
  }
  //out("***************** SUB MATRIX END");
  
  //return statCorr;

}





void HGamMeasurementReader::loadMeasurements( Str hepdata_file, StrV hepdata_keys )
{
  out("*****************");
  out("* Reading measurements from HepData input");
  out(Form("* File: %s", hepdata_file.Data()));
  for(auto key:hepdata_keys)
    out(Form("* Key(s):  %s",key.Data()));
  readHepDataTable( hepdata_file, hepdata_keys);
}


void HGamMeasurementReader::readHepDataTable( Str file, StrV keys, int column )
{
  _keys.clear(); _keys = keys;
  _bins.erase ( _bins.begin(), _bins.end() ); 
  _fullkeys.clear();
  _binkeys.clear();
  _xsec.Clear(); 
  _cov.Clear(); 
  _errs.Clear(); 
  _errV.clear();  
  _xsec.ResizeTo(0,0);
  _cov.ResizeTo(0,0);
  _errs.ResizeTo(0,0);
  
  //_cov.Print();
  //cout<<" -----------  >>> READ HepData FILE "<<endl;
  
  StrVV err_keys; 
  // Loop over all keys
  for(int i = 0; i < (int)keys.size(); i++) {
    //std::cout<<"======================= KEY FOR BINS :: "<<keys[i]<<endl;
    VecD single_bins;
    TMatrixD single_xsec, single_cov, single_error; StrV single_err_keys;
    readHepDataTable(file,keys[i],single_bins,single_xsec,single_cov,single_error, single_err_keys, column );
    // Append variable bins to the global vector
    _bins[keys[i]].insert(_bins[keys[i]].begin(), single_bins.begin(), single_bins.end());
    //for ( auto bin:_bins[keys[i]] ) cout<<" --> bin :: "<<bin<<endl;
    // Append variable xsec to the total xsec vector
    AddMatrices(_xsec,single_xsec,true);   
    // Append variable covariance to the total covariance matrix
    AddMatrices(_cov,single_cov);
    // Save error source to later build the total systematic covariance
    err_keys.push_back(single_err_keys);
    _errV.push_back(single_error);
    // Save bin keys for later use
    for(int j = 0; j < single_bins.size()-1; j++)
      _fullkeys.push_back(Form("%s:%d",keys[i].Data(),j));
    for(int j = 0; j < single_bins.size(); j++)
      _binkeys.push_back(Form("%s:%d",keys[i].Data(),j));
  }

  //_xsec.Print(); 
  //_cov.Print(); 
  //_errs.Print();
  // If only one measurement is loaded, we are done 
 // if(keys.size() == 1) return; //commented by glu
  
  // Build systematic cross correlations 
  int i_index(0);
  // Loop over variables ... all errors
  for(int i = 0; i < _errV.size(); i++) {
    int j_index = i_index;
    for(int j = i+1; j < _errV.size(); j++) {
      // Loop over all sources and identify matching ones
      for(int k = 0; k <  err_keys[i].size(); k++) 
	for(int l = 0; l <  err_keys[j].size(); l++) { 
	  // Check if the source should be uncorrelated
	  if(err_keys[i][k].Contains("uncorr")) continue;  
	  // If a matching one is found, correlate them
	  if( err_keys[i][k] == err_keys[j][l] ) {
	    // i_index & j_index keep track where we are in the covariance matrix 
	    for(int m = 0; m < _errV[i].GetNrows(); m++)
	      for(int n = 0; n < _errV[j].GetNrows(); n++) {
		_cov(m+i_index,_errV[i].GetNrows()+n+j_index) +=
		  _errV[i](m,k+1) * _errV[j](n,l+1);
		_cov(_errV[i].GetNrows()+n+j_index,m+i_index) +=
		  _errV[i](m,k+1) * _errV[j](n,l+1);
	      } // Done loop over all covariance matrix elements 
	  } // End if matching keys found
	} // End loop over all keys    
      j_index += _errV[j].GetNrows(); // Update the j index
    } //  End loop over all errors
    i_index += _errV[i].GetNrows(); // Update the i index
  } // End loop over all errors
  
  //_xsec.Print(); _cov.Print(); _errs.Print();

}



void HGamMeasurementReader::readHepDataTable( Str file, Str key, VecD &bins, TMatrixD &xsec, TMatrixD &cov, TMatrixD &errs, StrV &err_keys, int column) {


  //cout<<" -----------  >>> READ HepData TABLE "<<endl;

  if ( !fileExists(file) )
    fatal(Form("Could not find file %s",file.Data()));

  std::ifstream input (file.Data());
  Str data_table_start = "*data:";    // start of HepData table
  Str data_table_end   = "*dataend:"; // end of HepData table  
  StrV data_table, syst_errs;
  VecD bin_edges, xsecs;
  Str line;
  bool abs_err;
  int nerrs = -1;

  bins.clear();
  xsec.Clear();
  cov.Clear();
  errs.Clear();
  err_keys.clear();
  //_errV.clear();

  // Check the input file is open
  if ( input.is_open() ) {
    while ( line.ReadLine (input) ) { // Loop over lines in file
      if ( line.Contains(key) ) { // Found table of interest
	// Find the start of the data table
        while ( line.ReadLine (input) && !line.Contains(data_table_start) ) ;
        // Now read the table lines until the end
        while ( line.ReadLine (input) && !line.Contains(data_table_end) ) {
          // Save table rows
          data_table.push_back(line);
	} // End of data
        break; // End of table
      } // End of distribution of interest
    } // End of file
    input.close();
  } // End input.isOpen() 
  else return;

  // Loop over rows in table and extract bin edges, cross sections and uncertainties
  for ( int i=0; i<data_table.size(); ++i) {

    // split up row based on semi-colon ; deliminators
    StrV row = vectorize(data_table[i],";");
    if ( row.size()<=column )
      fatal("Unexpected number of columns in HepData table! Check input file.");

    // axes information
    Str axes = row[0];
    // table content
    Str content = row[column]; 

    // save bin edges
    if ( axes.Contains("TO") ) {
      // bin edges of bin with non-zero width
      bin_edges.push_back( vectorizeD(axes," TO ")[0] ); 
      if (i==data_table.size()-1)
	bin_edges.push_back( vectorizeD(axes," TO ")[1] );
    }
    else // bin centre for bins with zero width
      bin_edges.push_back( atof(axes) ); 
    
    // save cross-section
    xsecs.push_back( atof(stringSection(content,"+-",0)) );
    double stat_err = atof(stringSection(stringSection(content,"+-",1),"(",0));

    Str systs = stringSection(content, "(", 1);

    systs.ReplaceAll(")","");

    // check whether errors absolute or percent
    if ( systs.Contains("PCT") ) {
      abs_err = false;
      systs.ReplaceAll("PCT","");
    } else abs_err = true;

    syst_errs = vectorize(systs,"=");
    // number of rows in table = number of bins in distribution
    int nbins = data_table.size(); 
    if ( nerrs==-1 ) {
      nerrs = syst_errs.size();
      errs.ResizeTo(nbins,nerrs);
    }
    errs[i][0] = stat_err;

    // loop over systematic error sources
    for (int j=0; j<syst_errs.size(); ++j ) {
      syst_errs[j].ReplaceAll(",DSYS","");
      if (syst_errs[j]=="DSYS") continue;
      Str err = stringSection(syst_errs[j],":",0);
      // Store the name of the error sources of the first bin
      if(i == 0) 
       err_keys.push_back(stringSection(syst_errs[j],":",1));
         
      double sym_err;
      // symmetrise uncertainty if needed (keeping sign of 'up' component)
      if ( err.Contains("+") || err.Contains("-") ) {
	double u = atof(stringSection(err,",",0).ReplaceAll("+",""));
	double d = atof(stringSection(err,",",1).ReplaceAll("-",""));
	sym_err = u==0 ? -d/2 : u/fabs(u)*(fabs(u)+fabs(d))/2;
      }
      else sym_err = atof(err);
      
      if ( !abs_err ) sym_err *= xsecs[i];
      errs[i][j] = sym_err;
    }    
  }



  int nbins = bin_edges.size()-1;
  
  // multiply xsecs and errors by bin width 
  for (int ibin=0; ibin<nbins; ++ibin) {
    double binw = bin_edges[ibin+1]-bin_edges[ibin];
    xsecs[ibin] *= binw;
    for (int si=0; si<nerrs; ++si) {
      errs[ibin][si] *= binw;
    }
  }
  
  
  // set bin edges, xsecs and initialise covariance matrix
  bins = bin_edges;
  xsec.ResizeTo(1,nbins); cov.ResizeTo(nbins,nbins);
  xsec = TMatrixD(1,nbins,&xsecs[0]);

  // build covariance matrix
  for (int xi=0; xi<nbins; ++xi) {
    for (int yi=0; yi<nbins; ++yi) {
      double covariance = 0;
      for (int si=0; si<nerrs; ++si) { // loop over the systematics
	 if ( (si==0 || si==1 ) && xi!=yi ) // stat/bkg model off-diagonal
         // if  (si==0 || si==1 ) // updated by glu; didn't consider statistical  
        continue;
        covariance += errs[xi][si]*errs[yi][si];
      }
      cov[xi][yi] = covariance;
    }
  }
  
  //cout<<" -----------  >>> READ HepData TABLE DONE  "<<endl;
  //xsec.Print();  cov.Print();

  return;
}
 
 void HGamMeasurementReader::AddMatrices(TMatrixD &vec1, TMatrixD &vec2, bool isvector) {
  vec1.ResizeTo(!isvector ? vec1.GetNrows()+vec2.GetNrows() : 1, vec1.GetNcols()+vec2.GetNcols());
  int nrow_start = !isvector ? vec1.GetNrows()-vec2.GetNrows() : 0 , 
       ncol_start = vec1.GetNcols()-vec2.GetNcols();
  for(int i = nrow_start; !isvector ?  i < vec1.GetNrows() : i < 1; i++)
   for(int j = ncol_start; j < vec1.GetNcols(); j++)
     vec1(i,j) = vec2(i-nrow_start,j-ncol_start); 
 }

