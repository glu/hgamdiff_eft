# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'cHW', 'min':'-0.018', 'max':'0.025', 'central':'-0.000', 'neg':'0.018', 'pos':'0.025', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'cHW', 'min':'-0.027', 'max':'0.035', 'central':'-0.000', 'neg':'0.027', 'pos':'0.035', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'cHW', 'min':'-0.034', 'max':'0.042', 'central':'-0.000', 'neg':'0.034', 'pos':'0.042', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
