# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'cgt', 'min':'-0.00097', 'max':'0.00080', 'central':'-0.00002', 'neg':'0.00095', 'pos':'0.00082', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'cgt', 'min':'-0.0014', 'max':'0.0012', 'central':'-0.0000', 'neg':'0.0014', 'pos':'0.0012', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'cgt', 'min':'-0.0017', 'max':'0.0015', 'central':'-0.0000', 'neg':'0.0017', 'pos':'0.0015', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
