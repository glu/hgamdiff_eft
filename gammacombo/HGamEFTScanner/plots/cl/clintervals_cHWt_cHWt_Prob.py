# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'cHWt', 'min':'-0.0031', 'max':'0.0030', 'central':'-0.0015', 'neg':'0.0016', 'pos':'0.0045', 'cl':'0.68', 'unit':'', 'method':'Prob'},
    {'var':'cHWt', 'min':'-0.0031', 'max':'0.0030', 'central':'0.0015', 'neg':'0.0046', 'pos':'0.0015', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'cHWt', 'min':'-0.0040', 'max':'0.0040', 'central':'-0.0015', 'neg':'0.0025', 'pos':'0.0055', 'cl':'0.95', 'unit':'', 'method':'Prob'},
    {'var':'cHWt', 'min':'-0.0040', 'max':'0.0040', 'central':'0.0015', 'neg':'0.0055', 'pos':'0.0025', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'cHWt', 'min':'-0.0048', 'max':'0.0048', 'central':'-0.0015', 'neg':'0.0033', 'pos':'0.0063', 'cl':'1.00', 'unit':'', 'method':'Prob'},
    {'var':'cHWt', 'min':'-0.0048', 'max':'0.0048', 'central':'0.0015', 'neg':'0.0063', 'pos':'0.0033', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
