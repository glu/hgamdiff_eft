# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'cg', 'min':'-0.00012', 'max':'0.00036', 'central':'0.00012', 'neg':'0.00024', 'pos':'0.00024', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'cg', 'min':'-0.00039', 'max':'0.00052', 'central':'0.00012', 'neg':'0.00051', 'pos':'0.00040', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'cg', 'min':'-0.00068', 'max':'0.00063', 'central':'0.00012', 'neg':'0.00080', 'pos':'0.00051', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
