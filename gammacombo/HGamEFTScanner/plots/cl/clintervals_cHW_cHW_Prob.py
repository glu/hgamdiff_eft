# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'cHW', 'min':'-0.00053', 'max':'0.00029', 'central':'-0.00013', 'neg':'0.00040', 'pos':'0.00042', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'cHW', 'min':'-0.00091', 'max':'0.00072', 'central':'-0.00013', 'neg':'0.00078', 'pos':'0.00085', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'cHW', 'min':'-0.00100', 'max':'0.00119', 'central':'-0.00013', 'neg':'0.00087', 'pos':'0.00132', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
