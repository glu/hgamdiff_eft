# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'mu', 'min':'0.58', 'max':'0.81', 'central':'0.70', 'neg':'0.12', 'pos':'0.11', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'mu', 'min':'0.47', 'max':'0.93', 'central':'0.70', 'neg':'0.23', 'pos':'0.23', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'mu', 'min':'0.36', 'max':'1.04', 'central':'0.70', 'neg':'0.34', 'pos':'0.34', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
