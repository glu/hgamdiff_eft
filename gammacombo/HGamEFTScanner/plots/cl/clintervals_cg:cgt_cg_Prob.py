# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'cg', 'min':'-0.00016', 'max':'0.00016', 'central':'-0.00000', 'neg':'0.00016', 'pos':'0.00016', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'cg', 'min':'-0.00032', 'max':'0.00027', 'central':'-0.00000', 'neg':'0.00032', 'pos':'0.00027', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'cg', 'min':'-0.00048', 'max':'0.00038', 'central':'-0.00000', 'neg':'0.00048', 'pos':'0.00038', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
