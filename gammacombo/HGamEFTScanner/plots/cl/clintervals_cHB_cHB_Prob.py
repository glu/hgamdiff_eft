# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'cHB', 'min':'-0.00016', 'max':'0.00009', 'central':'-0.00004', 'neg':'0.00012', 'pos':'0.00013', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'cHB', 'min':'-0.00028', 'max':'0.00021', 'central':'-0.00004', 'neg':'0.00024', 'pos':'0.00025', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'cHB', 'min':'-0.00039', 'max':'0.00035', 'central':'-0.00004', 'neg':'0.00035', 'pos':'0.00039', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
