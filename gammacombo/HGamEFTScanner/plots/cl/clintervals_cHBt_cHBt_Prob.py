# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'cHBt', 'min':'-0.00093', 'max':'0.00093', 'central':'-0.00046', 'neg':'0.00047', 'pos':'0.00139', 'cl':'0.68', 'unit':'', 'method':'Prob'},
    {'var':'cHBt', 'min':'-0.00093', 'max':'0.00093', 'central':'0.00046', 'neg':'0.00139', 'pos':'0.00047', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'cHBt', 'min':'-0.00123', 'max':'0.00123', 'central':'-0.00046', 'neg':'0.00077', 'pos':'0.00169', 'cl':'0.95', 'unit':'', 'method':'Prob'},
    {'var':'cHBt', 'min':'-0.00123', 'max':'0.00123', 'central':'0.00046', 'neg':'0.00169', 'pos':'0.00077', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'cHBt', 'min':'-0.0015', 'max':'0.0015', 'central':'-0.0005', 'neg':'0.0010', 'pos':'0.0020', 'cl':'1.00', 'unit':'', 'method':'Prob'},
    {'var':'cHBt', 'min':'-0.0015', 'max':'0.0015', 'central':'0.0005', 'neg':'0.0020', 'pos':'0.0010', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
