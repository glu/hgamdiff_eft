# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'cHBt', 'min':'-0.0071', 'max':'0.0072', 'central':'-0.0001', 'neg':'0.0070', 'pos':'0.0073', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'cHBt', 'min':'-0.010', 'max':'0.010', 'central':'-0.000', 'neg':'0.010', 'pos':'0.010', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'cHBt', 'min':'-0.012', 'max':'0.012', 'central':'-0.000', 'neg':'0.012', 'pos':'0.012', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
