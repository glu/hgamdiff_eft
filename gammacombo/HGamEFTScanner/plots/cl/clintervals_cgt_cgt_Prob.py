# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'cgt', 'min':'-0.00118', 'max':'0.00123', 'central':'0.00043', 'neg':'0.00161', 'pos':'0.00080', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'cgt', 'min':'-0.0017', 'max':'0.0017', 'central':'0.0004', 'neg':'0.0021', 'pos':'0.0013', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'cgt', 'min':'-0.0020', 'max':'0.0020', 'central':'0.0004', 'neg':'0.0024', 'pos':'0.0016', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
