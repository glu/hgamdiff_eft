# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'cHWB', 'min':'-0.00016', 'max':'0.00029', 'central':'0.00007', 'neg':'0.00023', 'pos':'0.00022', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'cHWB', 'min':'-0.00040', 'max':'0.00050', 'central':'0.00007', 'neg':'0.00047', 'pos':'0.00043', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'cHWB', 'min':'-0.00066', 'max':'0.00070', 'central':'0.00007', 'neg':'0.00073', 'pos':'0.00063', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
