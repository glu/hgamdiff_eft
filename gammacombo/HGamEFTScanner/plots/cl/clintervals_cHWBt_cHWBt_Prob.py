# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'cHWBt', 'min':'-0.00168', 'max':'0.00168', 'central':'0.00083', 'neg':'0.00251', 'pos':'0.00085', 'cl':'0.68', 'unit':'', 'method':'Prob'},
    {'var':'cHWBt', 'min':'-0.00168', 'max':'0.00168', 'central':'-0.00083', 'neg':'0.00085', 'pos':'0.00251', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'cHWBt', 'min':'-0.0022', 'max':'0.0022', 'central':'0.0008', 'neg':'0.0030', 'pos':'0.0014', 'cl':'0.95', 'unit':'', 'method':'Prob'},
    {'var':'cHWBt', 'min':'-0.0022', 'max':'0.0022', 'central':'-0.0008', 'neg':'0.0014', 'pos':'0.0030', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'cHWBt', 'min':'-0.0027', 'max':'0.0027', 'central':'0.0008', 'neg':'0.0035', 'pos':'0.0019', 'cl':'1.00', 'unit':'', 'method':'Prob'},
    {'var':'cHWBt', 'min':'-0.0027', 'max':'0.0027', 'central':'-0.0008', 'neg':'0.0019', 'pos':'0.0035', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
