void HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBt()
{
//=========Macro generated from canvas: HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41/SMEFT Run2
//=========  (Thu Jul 22 09:39:39 2021) by ROOT version6.08/02
   TCanvas *HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41 = new TCanvas("HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41", "SMEFT Run2",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->SetHighLightColor(2);
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->Range(-0.0005620253,-143.5897,0.0004506329,112.8205);
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->SetFillColor(0);
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->SetBorderMode(0);
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->SetBorderSize(2);
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->SetLeftMargin(0.16);
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->SetRightMargin(0.05);
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->SetTopMargin(0.05);
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->SetBottomMargin(0.17);
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->SetFrameBorderMode(0);
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->SetFrameBorderMode(0);
   
   TH2F *haxesUID42__1 = new TH2F("haxesUID42__1","haxes",100,-0.0004,0.0004,100,-100,100);
   haxesUID42__1->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   haxesUID42__1->SetLineColor(ci);
   haxesUID42__1->GetXaxis()->SetTitle("#it{#bar{C}_{HB}}");
   haxesUID42__1->GetXaxis()->SetNdivisions(0);
   haxesUID42__1->GetXaxis()->SetLabelFont(133);
   haxesUID42__1->GetXaxis()->SetLabelSize(35);
   haxesUID42__1->GetXaxis()->SetTitleSize(45);
   haxesUID42__1->GetXaxis()->SetTitleOffset(0.85);
   haxesUID42__1->GetXaxis()->SetTitleFont(133);
   haxesUID42__1->GetYaxis()->SetTitle("#it{#tilde{C}_{HB}}");
   haxesUID42__1->GetYaxis()->SetNdivisions(0);
   haxesUID42__1->GetYaxis()->SetLabelFont(133);
   haxesUID42__1->GetYaxis()->SetLabelOffset(0.01);
   haxesUID42__1->GetYaxis()->SetLabelSize(35);
   haxesUID42__1->GetYaxis()->SetTitleSize(45);
   haxesUID42__1->GetYaxis()->SetTitleOffset(0.95);
   haxesUID42__1->GetYaxis()->SetTitleFont(133);
   haxesUID42__1->GetZaxis()->SetLabelFont(42);
   haxesUID42__1->GetZaxis()->SetLabelSize(0.035);
   haxesUID42__1->GetZaxis()->SetTitleSize(0.035);
   haxesUID42__1->GetZaxis()->SetTitleFont(42);
   haxesUID42__1->Draw("");
   
   Double_t Graph0_fx1[145] = {
   5.6e-05,
   7.2e-05,
   8.8e-05,
   9.84557e-05,
   0.000104,
   0.00012,
   0.000136,
   0.0001441416,
   0.000152,
   0.000168,
   0.0001751205,
   0.000184,
   0.0001992087,
   0.0002,
   0.000216,
   0.0002186154,
   0.000232,
   0.0002348312,
   0.000248,
   0.0002485662,
   0.0002600603,
   0.000264,
   0.0002698136,
   0.0002780629,
   0.00028,
   0.0002847746,
   0.0002902116,
   0.0002944959,
   0.000296,
   0.0002975523,
   0.0002994603,
   0.0003003103,
   0.0003001124,
   0.0002988767,
   0.000296613,
   0.000296,
   0.0002931847,
   0.00028866,
   0.0002830834,
   0.00028,
   0.000276262,
   0.0002681752,
   0.000264,
   0.0002587099,
   0.000248,
   0.0002478569,
   0.0002351347,
   0.000232,
   0.000220516,
   0.000216,
   0.0002036226,
   0.0002,
   0.000184,
   0.0001839512,
   0.000168,
   0.0001601989,
   0.000152,
   0.000136,
   0.0001310803,
   0.00012,
   0.000104,
   9.214919e-05,
   8.8e-05,
   7.2e-05,
   5.6e-05,
   4e-05,
   2.4e-05,
   2.081036e-05,
   8e-06,
   -8e-06,
   -2.4e-05,
   -4e-05,
   -5.568743e-05,
   -5.6e-05,
   -7.2e-05,
   -8.8e-05,
   -0.000104,
   -0.00012,
   -0.0001246169,
   -0.000136,
   -0.000152,
   -0.0001616143,
   -0.000168,
   -0.000184,
   -0.0001888572,
   -0.0002,
   -0.0002105074,
   -0.000216,
   -0.0002283926,
   -0.000232,
   -0.0002433509,
   -0.000248,
   -0.0002559888,
   -0.000264,
   -0.0002667561,
   -0.0002757503,
   -0.00028,
   -0.000283277,
   -0.0002893797,
   -0.0002943134,
   -0.000296,
   -0.0002979787,
   -0.000300471,
   -0.0003018912,
   -0.0003022496,
   -0.0003015564,
   -0.0002998218,
   -0.0002970558,
   -0.000296,
   -0.0002931179,
   -0.0002880558,
   -0.0002819385,
   -0.00028,
   -0.0002744753,
   -0.0002658093,
   -0.000264,
   -0.0002555822,
   -0.000248,
   -0.0002438664,
   -0.000232,
   -0.0002303745,
   -0.000216,
   -0.00021474,
   -0.0002,
   -0.0001964878,
   -0.000184,
   -0.0001749911,
   -0.000168,
   -0.000152,
   -0.0001491411,
   -0.000136,
   -0.00012,
   -0.0001161711,
   -0.000104,
   -8.8e-05,
   -7.2e-05,
   -6.852426e-05,
   -5.6e-05,
   -4e-05,
   -2.4e-05,
   -8e-06,
   8e-06,
   2.4e-05,
   4e-05,
   5.6e-05};
   Double_t Graph0_fy1[145] = {
   64.02926,
   63.43797,
   62.64738,
   62,
   61.63335,
   60.3609,
   58.86976,
   58,
   57.09876,
   55.02751,
   54,
   52.61658,
   50,
   49.85208,
   46.58096,
   46,
   42.74375,
   42,
   38.17653,
   38,
   34,
   32.44772,
   30,
   26,
   24.89576,
   22,
   18,
   14,
   12.08951,
   10,
   6,
   2,
   -2,
   -6,
   -10,
   -10.74529,
   -14,
   -18,
   -22,
   -23.86033,
   -26,
   -30,
   -31.81914,
   -34,
   -37.95022,
   -38,
   -42,
   -42.90021,
   -46,
   -47.12181,
   -50,
   -50.78021,
   -53.99093,
   -54,
   -56.75821,
   -58,
   -59.2209,
   -61.39589,
   -62,
   -63.27784,
   -64.92605,
   -66,
   -66.35439,
   -67.53062,
   -68.51783,
   -69.31434,
   -69.91846,
   -70,
   -70.30947,
   -70.51118,
   -70.5267,
   -70.35448,
   -70,
   -69.99248,
   -69.40552,
   -68.61373,
   -67.61523,
   -66.40815,
   -66,
   -64.92259,
   -63.18221,
   -62,
   -61.15488,
   -58.79199,
   -58,
   -56.03244,
   -54,
   -52.84152,
   -50,
   -49.09064,
   -46,
   -44.59443,
   -42,
   -39.07557,
   -38,
   -34,
   -31.80308,
   -30,
   -26,
   -22,
   -20.21713,
   -18,
   -14,
   -10,
   -6,
   -2,
   2,
   6,
   7.112492,
   10,
   14,
   18,
   19.08001,
   22,
   26,
   26.74053,
   30,
   32.64184,
   34,
   37.5439,
   38,
   41.69681,
   42,
   45.27365,
   46,
   48.39794,
   50,
   51.16026,
   53.60236,
   54,
   55.71304,
   57.5981,
   58,
   59.20185,
   60.58925,
   61.78346,
   62,
   62.73637,
   63.49101,
   64.05849,
   64.43713,
   64.62525,
   64.62114,
   64.42305,
   64.02926};
   TGraph *graph = new TGraph(145,Graph0_fx1,Graph0_fy1);
   graph->SetName("Graph0");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#d3e9ff");
   graph->SetFillColor(ci);
   
   TH1F *Graph_Graph1 = new TH1F("Graph_Graph1","Graph",145,-0.0003625055,0.0003605663);
   Graph_Graph1->SetMinimum(-84.0419);
   Graph_Graph1->SetMaximum(78.14044);
   Graph_Graph1->SetDirectory(0);
   Graph_Graph1->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph1->SetLineColor(ci);
   Graph_Graph1->GetXaxis()->SetLabelFont(42);
   Graph_Graph1->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph1->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph1->GetXaxis()->SetTitleFont(42);
   Graph_Graph1->GetYaxis()->SetLabelFont(42);
   Graph_Graph1->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph1->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph1->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph1->GetYaxis()->SetTitleFont(42);
   Graph_Graph1->GetZaxis()->SetLabelFont(42);
   Graph_Graph1->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph1->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph1->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph1);
   
   graph->Draw("f");
   
   Double_t Graph1_fx2[145] = {
   5.6e-05,
   7.2e-05,
   8.8e-05,
   9.84557e-05,
   0.000104,
   0.00012,
   0.000136,
   0.0001441416,
   0.000152,
   0.000168,
   0.0001751205,
   0.000184,
   0.0001992087,
   0.0002,
   0.000216,
   0.0002186154,
   0.000232,
   0.0002348312,
   0.000248,
   0.0002485662,
   0.0002600603,
   0.000264,
   0.0002698136,
   0.0002780629,
   0.00028,
   0.0002847746,
   0.0002902116,
   0.0002944959,
   0.000296,
   0.0002975523,
   0.0002994603,
   0.0003003103,
   0.0003001124,
   0.0002988767,
   0.000296613,
   0.000296,
   0.0002931847,
   0.00028866,
   0.0002830834,
   0.00028,
   0.000276262,
   0.0002681752,
   0.000264,
   0.0002587099,
   0.000248,
   0.0002478569,
   0.0002351347,
   0.000232,
   0.000220516,
   0.000216,
   0.0002036226,
   0.0002,
   0.000184,
   0.0001839512,
   0.000168,
   0.0001601989,
   0.000152,
   0.000136,
   0.0001310803,
   0.00012,
   0.000104,
   9.214919e-05,
   8.8e-05,
   7.2e-05,
   5.6e-05,
   4e-05,
   2.4e-05,
   2.081036e-05,
   8e-06,
   -8e-06,
   -2.4e-05,
   -4e-05,
   -5.568743e-05,
   -5.6e-05,
   -7.2e-05,
   -8.8e-05,
   -0.000104,
   -0.00012,
   -0.0001246169,
   -0.000136,
   -0.000152,
   -0.0001616143,
   -0.000168,
   -0.000184,
   -0.0001888572,
   -0.0002,
   -0.0002105074,
   -0.000216,
   -0.0002283926,
   -0.000232,
   -0.0002433509,
   -0.000248,
   -0.0002559888,
   -0.000264,
   -0.0002667561,
   -0.0002757503,
   -0.00028,
   -0.000283277,
   -0.0002893797,
   -0.0002943134,
   -0.000296,
   -0.0002979787,
   -0.000300471,
   -0.0003018912,
   -0.0003022496,
   -0.0003015564,
   -0.0002998218,
   -0.0002970558,
   -0.000296,
   -0.0002931179,
   -0.0002880558,
   -0.0002819385,
   -0.00028,
   -0.0002744753,
   -0.0002658093,
   -0.000264,
   -0.0002555822,
   -0.000248,
   -0.0002438664,
   -0.000232,
   -0.0002303745,
   -0.000216,
   -0.00021474,
   -0.0002,
   -0.0001964878,
   -0.000184,
   -0.0001749911,
   -0.000168,
   -0.000152,
   -0.0001491411,
   -0.000136,
   -0.00012,
   -0.0001161711,
   -0.000104,
   -8.8e-05,
   -7.2e-05,
   -6.852426e-05,
   -5.6e-05,
   -4e-05,
   -2.4e-05,
   -8e-06,
   8e-06,
   2.4e-05,
   4e-05,
   5.6e-05};
   Double_t Graph1_fy2[145] = {
   64.02926,
   63.43797,
   62.64738,
   62,
   61.63335,
   60.3609,
   58.86976,
   58,
   57.09876,
   55.02751,
   54,
   52.61658,
   50,
   49.85208,
   46.58096,
   46,
   42.74375,
   42,
   38.17653,
   38,
   34,
   32.44772,
   30,
   26,
   24.89576,
   22,
   18,
   14,
   12.08951,
   10,
   6,
   2,
   -2,
   -6,
   -10,
   -10.74529,
   -14,
   -18,
   -22,
   -23.86033,
   -26,
   -30,
   -31.81914,
   -34,
   -37.95022,
   -38,
   -42,
   -42.90021,
   -46,
   -47.12181,
   -50,
   -50.78021,
   -53.99093,
   -54,
   -56.75821,
   -58,
   -59.2209,
   -61.39589,
   -62,
   -63.27784,
   -64.92605,
   -66,
   -66.35439,
   -67.53062,
   -68.51783,
   -69.31434,
   -69.91846,
   -70,
   -70.30947,
   -70.51118,
   -70.5267,
   -70.35448,
   -70,
   -69.99248,
   -69.40552,
   -68.61373,
   -67.61523,
   -66.40815,
   -66,
   -64.92259,
   -63.18221,
   -62,
   -61.15488,
   -58.79199,
   -58,
   -56.03244,
   -54,
   -52.84152,
   -50,
   -49.09064,
   -46,
   -44.59443,
   -42,
   -39.07557,
   -38,
   -34,
   -31.80308,
   -30,
   -26,
   -22,
   -20.21713,
   -18,
   -14,
   -10,
   -6,
   -2,
   2,
   6,
   7.112492,
   10,
   14,
   18,
   19.08001,
   22,
   26,
   26.74053,
   30,
   32.64184,
   34,
   37.5439,
   38,
   41.69681,
   42,
   45.27365,
   46,
   48.39794,
   50,
   51.16026,
   53.60236,
   54,
   55.71304,
   57.5981,
   58,
   59.20185,
   60.58925,
   61.78346,
   62,
   62.73637,
   63.49101,
   64.05849,
   64.43713,
   64.62525,
   64.62114,
   64.42305,
   64.02926};
   graph = new TGraph(145,Graph1_fx2,Graph1_fy2);
   graph->SetName("Graph1");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#e1bf4a");
   graph->SetFillColor(ci);
   graph->SetFillStyle(0);
   graph->SetLineColor(9);
   graph->SetLineWidth(2);
   
   TH1F *Graph_Graph2 = new TH1F("Graph_Graph2","Graph",145,-0.0003625055,0.0003605663);
   Graph_Graph2->SetMinimum(-84.0419);
   Graph_Graph2->SetMaximum(78.14044);
   Graph_Graph2->SetDirectory(0);
   Graph_Graph2->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph2->SetLineColor(ci);
   Graph_Graph2->GetXaxis()->SetLabelFont(42);
   Graph_Graph2->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph2->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph2->GetXaxis()->SetTitleFont(42);
   Graph_Graph2->GetYaxis()->SetLabelFont(42);
   Graph_Graph2->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph2->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph2->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph2->GetYaxis()->SetTitleFont(42);
   Graph_Graph2->GetZaxis()->SetLabelFont(42);
   Graph_Graph2->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph2->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph2->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph2);
   
   graph->Draw("l");
   
   Double_t Graph2_fx3[89] = {
   4e-05,
   5.6e-05,
   7.2e-05,
   8.8e-05,
   8.996857e-05,
   0.000104,
   0.0001177554,
   0.00012,
   0.000136,
   0.000137136,
   0.0001515898,
   0.000152,
   0.0001623784,
   0.000168,
   0.0001706884,
   0.0001765575,
   0.0001805419,
   0.0001826732,
   0.0001829821,
   0.0001814985,
   0.0001782518,
   0.0001732709,
   0.000168,
   0.0001664462,
   0.0001572719,
   0.000152,
   0.0001456897,
   0.000136,
   0.0001310813,
   0.00012,
   0.0001123134,
   0.000104,
   8.8e-05,
   8.753589e-05,
   7.2e-05,
   5.6e-05,
   4.851037e-05,
   4e-05,
   2.4e-05,
   8e-06,
   -8e-06,
   -2.4e-05,
   -4e-05,
   -5.6e-05,
   -6.950851e-05,
   -7.2e-05,
   -8.8e-05,
   -0.000104,
   -0.0001062218,
   -0.00012,
   -0.0001291877,
   -0.000136,
   -0.0001459902,
   -0.000152,
   -0.0001586734,
   -0.000168,
   -0.000168467,
   -0.0001754241,
   -0.0001804577,
   -0.0001836002,
   -0.000184,
   -0.0001848093,
   -0.0001843102,
   -0.000184,
   -0.0001819956,
   -0.0001778845,
   -0.0001720344,
   -0.000168,
   -0.0001641283,
   -0.000153987,
   -0.000152,
   -0.0001409734,
   -0.000136,
   -0.0001245851,
   -0.00012,
   -0.000104,
   -0.0001034763,
   -8.8e-05,
   -7.364318e-05,
   -7.2e-05,
   -5.6e-05,
   -4e-05,
   -2.4e-05,
   -1.300643e-05,
   -8e-06,
   8e-06,
   2.4e-05,
   3.114002e-05,
   4e-05};
   Double_t Graph2_fy3[89] = {
   37.73504,
   36.91573,
   35.74919,
   34.22995,
   34,
   32.15662,
   30,
   29.59795,
   26.26935,
   26,
   22,
   21.86394,
   18,
   15.3863,
   14,
   10,
   6,
   2,
   -2,
   -6,
   -10,
   -14,
   -17.14935,
   -18,
   -22,
   -23.91552,
   -26,
   -28.74555,
   -30,
   -32.47423,
   -34,
   -35.46746,
   -37.93884,
   -38,
   -39.84208,
   -41.41722,
   -42,
   -42.60161,
   -43.42931,
   -43.95188,
   -44.16499,
   -44.06418,
   -43.64495,
   -42.90269,
   -42,
   -41.8142,
   -40.25571,
   -38.3204,
   -38,
   -35.74914,
   -34,
   -32.50443,
   -30,
   -28.22093,
   -26,
   -22.21069,
   -22,
   -18,
   -14,
   -10,
   -8.758591,
   -6,
   -2,
   -1.42492,
   2,
   6,
   10,
   12.12892,
   14,
   18,
   18.662,
   22,
   23.30535,
   26,
   26.95532,
   29.91558,
   30,
   32.2313,
   34,
   34.1831,
   35.65069,
   36.80629,
   37.64513,
   38,
   38.14732,
   38.32021,
   38.19251,
   38,
   37.73504};
   graph = new TGraph(89,Graph2_fx3,Graph2_fy3);
   graph->SetName("Graph2");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#bee7fd");
   graph->SetFillColor(ci);
   
   TH1F *Graph_Graph3 = new TH1F("Graph_Graph3","Graph",100,-0.0002215884,0.0002197612);
   Graph_Graph3->SetMinimum(-52.41351);
   Graph_Graph3->SetMaximum(46.56873);
   Graph_Graph3->SetDirectory(0);
   Graph_Graph3->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph3->SetLineColor(ci);
   Graph_Graph3->GetXaxis()->SetLabelFont(42);
   Graph_Graph3->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph3->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph3->GetXaxis()->SetTitleFont(42);
   Graph_Graph3->GetYaxis()->SetLabelFont(42);
   Graph_Graph3->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph3->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph3->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph3->GetYaxis()->SetTitleFont(42);
   Graph_Graph3->GetZaxis()->SetLabelFont(42);
   Graph_Graph3->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph3->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph3->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph3);
   
   graph->Draw("f");
   
   Double_t Graph3_fx4[89] = {
   4e-05,
   5.6e-05,
   7.2e-05,
   8.8e-05,
   8.996857e-05,
   0.000104,
   0.0001177554,
   0.00012,
   0.000136,
   0.000137136,
   0.0001515898,
   0.000152,
   0.0001623784,
   0.000168,
   0.0001706884,
   0.0001765575,
   0.0001805419,
   0.0001826732,
   0.0001829821,
   0.0001814985,
   0.0001782518,
   0.0001732709,
   0.000168,
   0.0001664462,
   0.0001572719,
   0.000152,
   0.0001456897,
   0.000136,
   0.0001310813,
   0.00012,
   0.0001123134,
   0.000104,
   8.8e-05,
   8.753589e-05,
   7.2e-05,
   5.6e-05,
   4.851037e-05,
   4e-05,
   2.4e-05,
   8e-06,
   -8e-06,
   -2.4e-05,
   -4e-05,
   -5.6e-05,
   -6.950851e-05,
   -7.2e-05,
   -8.8e-05,
   -0.000104,
   -0.0001062218,
   -0.00012,
   -0.0001291877,
   -0.000136,
   -0.0001459902,
   -0.000152,
   -0.0001586734,
   -0.000168,
   -0.000168467,
   -0.0001754241,
   -0.0001804577,
   -0.0001836002,
   -0.000184,
   -0.0001848093,
   -0.0001843102,
   -0.000184,
   -0.0001819956,
   -0.0001778845,
   -0.0001720344,
   -0.000168,
   -0.0001641283,
   -0.000153987,
   -0.000152,
   -0.0001409734,
   -0.000136,
   -0.0001245851,
   -0.00012,
   -0.000104,
   -0.0001034763,
   -8.8e-05,
   -7.364318e-05,
   -7.2e-05,
   -5.6e-05,
   -4e-05,
   -2.4e-05,
   -1.300643e-05,
   -8e-06,
   8e-06,
   2.4e-05,
   3.114002e-05,
   4e-05};
   Double_t Graph3_fy4[89] = {
   37.73504,
   36.91573,
   35.74919,
   34.22995,
   34,
   32.15662,
   30,
   29.59795,
   26.26935,
   26,
   22,
   21.86394,
   18,
   15.3863,
   14,
   10,
   6,
   2,
   -2,
   -6,
   -10,
   -14,
   -17.14935,
   -18,
   -22,
   -23.91552,
   -26,
   -28.74555,
   -30,
   -32.47423,
   -34,
   -35.46746,
   -37.93884,
   -38,
   -39.84208,
   -41.41722,
   -42,
   -42.60161,
   -43.42931,
   -43.95188,
   -44.16499,
   -44.06418,
   -43.64495,
   -42.90269,
   -42,
   -41.8142,
   -40.25571,
   -38.3204,
   -38,
   -35.74914,
   -34,
   -32.50443,
   -30,
   -28.22093,
   -26,
   -22.21069,
   -22,
   -18,
   -14,
   -10,
   -8.758591,
   -6,
   -2,
   -1.42492,
   2,
   6,
   10,
   12.12892,
   14,
   18,
   18.662,
   22,
   23.30535,
   26,
   26.95532,
   29.91558,
   30,
   32.2313,
   34,
   34.1831,
   35.65069,
   36.80629,
   37.64513,
   38,
   38.14732,
   38.32021,
   38.19251,
   38,
   37.73504};
   graph = new TGraph(89,Graph3_fx4,Graph3_fy4);
   graph->SetName("Graph3");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#18aae7");
   graph->SetFillColor(ci);
   graph->SetFillStyle(3005);
   
   TH1F *Graph_Graph4 = new TH1F("Graph_Graph4","Graph",100,-0.0002215884,0.0002197612);
   Graph_Graph4->SetMinimum(-52.41351);
   Graph_Graph4->SetMaximum(46.56873);
   Graph_Graph4->SetDirectory(0);
   Graph_Graph4->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph4->SetLineColor(ci);
   Graph_Graph4->GetXaxis()->SetLabelFont(42);
   Graph_Graph4->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph4->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph4->GetXaxis()->SetTitleFont(42);
   Graph_Graph4->GetYaxis()->SetLabelFont(42);
   Graph_Graph4->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph4->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph4->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph4->GetYaxis()->SetTitleFont(42);
   Graph_Graph4->GetZaxis()->SetLabelFont(42);
   Graph_Graph4->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph4->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph4->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph4);
   
   graph->Draw("f");
   
   Double_t Graph4_fx5[89] = {
   4e-05,
   5.6e-05,
   7.2e-05,
   8.8e-05,
   8.996857e-05,
   0.000104,
   0.0001177554,
   0.00012,
   0.000136,
   0.000137136,
   0.0001515898,
   0.000152,
   0.0001623784,
   0.000168,
   0.0001706884,
   0.0001765575,
   0.0001805419,
   0.0001826732,
   0.0001829821,
   0.0001814985,
   0.0001782518,
   0.0001732709,
   0.000168,
   0.0001664462,
   0.0001572719,
   0.000152,
   0.0001456897,
   0.000136,
   0.0001310813,
   0.00012,
   0.0001123134,
   0.000104,
   8.8e-05,
   8.753589e-05,
   7.2e-05,
   5.6e-05,
   4.851037e-05,
   4e-05,
   2.4e-05,
   8e-06,
   -8e-06,
   -2.4e-05,
   -4e-05,
   -5.6e-05,
   -6.950851e-05,
   -7.2e-05,
   -8.8e-05,
   -0.000104,
   -0.0001062218,
   -0.00012,
   -0.0001291877,
   -0.000136,
   -0.0001459902,
   -0.000152,
   -0.0001586734,
   -0.000168,
   -0.000168467,
   -0.0001754241,
   -0.0001804577,
   -0.0001836002,
   -0.000184,
   -0.0001848093,
   -0.0001843102,
   -0.000184,
   -0.0001819956,
   -0.0001778845,
   -0.0001720344,
   -0.000168,
   -0.0001641283,
   -0.000153987,
   -0.000152,
   -0.0001409734,
   -0.000136,
   -0.0001245851,
   -0.00012,
   -0.000104,
   -0.0001034763,
   -8.8e-05,
   -7.364318e-05,
   -7.2e-05,
   -5.6e-05,
   -4e-05,
   -2.4e-05,
   -1.300643e-05,
   -8e-06,
   8e-06,
   2.4e-05,
   3.114002e-05,
   4e-05};
   Double_t Graph4_fy5[89] = {
   37.73504,
   36.91573,
   35.74919,
   34.22995,
   34,
   32.15662,
   30,
   29.59795,
   26.26935,
   26,
   22,
   21.86394,
   18,
   15.3863,
   14,
   10,
   6,
   2,
   -2,
   -6,
   -10,
   -14,
   -17.14935,
   -18,
   -22,
   -23.91552,
   -26,
   -28.74555,
   -30,
   -32.47423,
   -34,
   -35.46746,
   -37.93884,
   -38,
   -39.84208,
   -41.41722,
   -42,
   -42.60161,
   -43.42931,
   -43.95188,
   -44.16499,
   -44.06418,
   -43.64495,
   -42.90269,
   -42,
   -41.8142,
   -40.25571,
   -38.3204,
   -38,
   -35.74914,
   -34,
   -32.50443,
   -30,
   -28.22093,
   -26,
   -22.21069,
   -22,
   -18,
   -14,
   -10,
   -8.758591,
   -6,
   -2,
   -1.42492,
   2,
   6,
   10,
   12.12892,
   14,
   18,
   18.662,
   22,
   23.30535,
   26,
   26.95532,
   29.91558,
   30,
   32.2313,
   34,
   34.1831,
   35.65069,
   36.80629,
   37.64513,
   38,
   38.14732,
   38.32021,
   38.19251,
   38,
   37.73504};
   graph = new TGraph(89,Graph4_fx5,Graph4_fy5);
   graph->SetName("Graph4");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#f9f90e");
   graph->SetFillColor(ci);
   graph->SetFillStyle(0);

   ci = TColor::GetColor("#18aae7");
   graph->SetLineColor(ci);
   graph->SetLineWidth(2);
   
   TH1F *Graph_Graph5 = new TH1F("Graph_Graph5","Graph",100,-0.0002215884,0.0002197612);
   Graph_Graph5->SetMinimum(-52.41351);
   Graph_Graph5->SetMaximum(46.56873);
   Graph_Graph5->SetDirectory(0);
   Graph_Graph5->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph5->SetLineColor(ci);
   Graph_Graph5->GetXaxis()->SetLabelFont(42);
   Graph_Graph5->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph5->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph5->GetXaxis()->SetTitleFont(42);
   Graph_Graph5->GetYaxis()->SetLabelFont(42);
   Graph_Graph5->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph5->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph5->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph5->GetYaxis()->SetTitleFont(42);
   Graph_Graph5->GetZaxis()->SetLabelFont(42);
   Graph_Graph5->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph5->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph5->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph5);
   
   graph->Draw("l");
   
   Double_t Graph5_fx6[145] = {
   5.6e-05,
   7.2e-05,
   8.8e-05,
   9.84557e-05,
   0.000104,
   0.00012,
   0.000136,
   0.0001441416,
   0.000152,
   0.000168,
   0.0001751205,
   0.000184,
   0.0001992087,
   0.0002,
   0.000216,
   0.0002186154,
   0.000232,
   0.0002348312,
   0.000248,
   0.0002485662,
   0.0002600603,
   0.000264,
   0.0002698136,
   0.0002780629,
   0.00028,
   0.0002847746,
   0.0002902116,
   0.0002944959,
   0.000296,
   0.0002975523,
   0.0002994603,
   0.0003003103,
   0.0003001124,
   0.0002988767,
   0.000296613,
   0.000296,
   0.0002931847,
   0.00028866,
   0.0002830834,
   0.00028,
   0.000276262,
   0.0002681752,
   0.000264,
   0.0002587099,
   0.000248,
   0.0002478569,
   0.0002351347,
   0.000232,
   0.000220516,
   0.000216,
   0.0002036226,
   0.0002,
   0.000184,
   0.0001839512,
   0.000168,
   0.0001601989,
   0.000152,
   0.000136,
   0.0001310803,
   0.00012,
   0.000104,
   9.214919e-05,
   8.8e-05,
   7.2e-05,
   5.6e-05,
   4e-05,
   2.4e-05,
   2.081036e-05,
   8e-06,
   -8e-06,
   -2.4e-05,
   -4e-05,
   -5.568743e-05,
   -5.6e-05,
   -7.2e-05,
   -8.8e-05,
   -0.000104,
   -0.00012,
   -0.0001246169,
   -0.000136,
   -0.000152,
   -0.0001616143,
   -0.000168,
   -0.000184,
   -0.0001888572,
   -0.0002,
   -0.0002105074,
   -0.000216,
   -0.0002283926,
   -0.000232,
   -0.0002433509,
   -0.000248,
   -0.0002559888,
   -0.000264,
   -0.0002667561,
   -0.0002757503,
   -0.00028,
   -0.000283277,
   -0.0002893797,
   -0.0002943134,
   -0.000296,
   -0.0002979787,
   -0.000300471,
   -0.0003018912,
   -0.0003022496,
   -0.0003015564,
   -0.0002998218,
   -0.0002970558,
   -0.000296,
   -0.0002931179,
   -0.0002880558,
   -0.0002819385,
   -0.00028,
   -0.0002744753,
   -0.0002658093,
   -0.000264,
   -0.0002555822,
   -0.000248,
   -0.0002438664,
   -0.000232,
   -0.0002303745,
   -0.000216,
   -0.00021474,
   -0.0002,
   -0.0001964878,
   -0.000184,
   -0.0001749911,
   -0.000168,
   -0.000152,
   -0.0001491411,
   -0.000136,
   -0.00012,
   -0.0001161711,
   -0.000104,
   -8.8e-05,
   -7.2e-05,
   -6.852426e-05,
   -5.6e-05,
   -4e-05,
   -2.4e-05,
   -8e-06,
   8e-06,
   2.4e-05,
   4e-05,
   5.6e-05};
   Double_t Graph5_fy6[145] = {
   64.02926,
   63.43797,
   62.64738,
   62,
   61.63335,
   60.3609,
   58.86976,
   58,
   57.09876,
   55.02751,
   54,
   52.61658,
   50,
   49.85208,
   46.58096,
   46,
   42.74375,
   42,
   38.17653,
   38,
   34,
   32.44772,
   30,
   26,
   24.89576,
   22,
   18,
   14,
   12.08951,
   10,
   6,
   2,
   -2,
   -6,
   -10,
   -10.74529,
   -14,
   -18,
   -22,
   -23.86033,
   -26,
   -30,
   -31.81914,
   -34,
   -37.95022,
   -38,
   -42,
   -42.90021,
   -46,
   -47.12181,
   -50,
   -50.78021,
   -53.99093,
   -54,
   -56.75821,
   -58,
   -59.2209,
   -61.39589,
   -62,
   -63.27784,
   -64.92605,
   -66,
   -66.35439,
   -67.53062,
   -68.51783,
   -69.31434,
   -69.91846,
   -70,
   -70.30947,
   -70.51118,
   -70.5267,
   -70.35448,
   -70,
   -69.99248,
   -69.40552,
   -68.61373,
   -67.61523,
   -66.40815,
   -66,
   -64.92259,
   -63.18221,
   -62,
   -61.15488,
   -58.79199,
   -58,
   -56.03244,
   -54,
   -52.84152,
   -50,
   -49.09064,
   -46,
   -44.59443,
   -42,
   -39.07557,
   -38,
   -34,
   -31.80308,
   -30,
   -26,
   -22,
   -20.21713,
   -18,
   -14,
   -10,
   -6,
   -2,
   2,
   6,
   7.112492,
   10,
   14,
   18,
   19.08001,
   22,
   26,
   26.74053,
   30,
   32.64184,
   34,
   37.5439,
   38,
   41.69681,
   42,
   45.27365,
   46,
   48.39794,
   50,
   51.16026,
   53.60236,
   54,
   55.71304,
   57.5981,
   58,
   59.20185,
   60.58925,
   61.78346,
   62,
   62.73637,
   63.49101,
   64.05849,
   64.43713,
   64.62525,
   64.62114,
   64.42305,
   64.02926};
   graph = new TGraph(145,Graph5_fx6,Graph5_fy6);
   graph->SetName("Graph5");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#e1bf4a");
   graph->SetFillColor(ci);
   graph->SetFillStyle(0);
   graph->SetLineColor(9);
   graph->SetLineStyle(2);
   graph->SetLineWidth(2);
   
   TH1F *Graph_Graph6 = new TH1F("Graph_Graph6","Graph",145,-0.0003625055,0.0003605663);
   Graph_Graph6->SetMinimum(-84.0419);
   Graph_Graph6->SetMaximum(78.14044);
   Graph_Graph6->SetDirectory(0);
   Graph_Graph6->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph6->SetLineColor(ci);
   Graph_Graph6->GetXaxis()->SetLabelFont(42);
   Graph_Graph6->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph6->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph6->GetXaxis()->SetTitleFont(42);
   Graph_Graph6->GetYaxis()->SetLabelFont(42);
   Graph_Graph6->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph6->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph6->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph6->GetYaxis()->SetTitleFont(42);
   Graph_Graph6->GetZaxis()->SetLabelFont(42);
   Graph_Graph6->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph6->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph6->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph6);
   
   graph->Draw("l");
   
   Double_t Graph6_fx7[89] = {
   4e-05,
   5.6e-05,
   7.2e-05,
   8.8e-05,
   8.996857e-05,
   0.000104,
   0.0001177554,
   0.00012,
   0.000136,
   0.000137136,
   0.0001515898,
   0.000152,
   0.0001623784,
   0.000168,
   0.0001706884,
   0.0001765575,
   0.0001805419,
   0.0001826732,
   0.0001829821,
   0.0001814985,
   0.0001782518,
   0.0001732709,
   0.000168,
   0.0001664462,
   0.0001572719,
   0.000152,
   0.0001456897,
   0.000136,
   0.0001310813,
   0.00012,
   0.0001123134,
   0.000104,
   8.8e-05,
   8.753589e-05,
   7.2e-05,
   5.6e-05,
   4.851037e-05,
   4e-05,
   2.4e-05,
   8e-06,
   -8e-06,
   -2.4e-05,
   -4e-05,
   -5.6e-05,
   -6.950851e-05,
   -7.2e-05,
   -8.8e-05,
   -0.000104,
   -0.0001062218,
   -0.00012,
   -0.0001291877,
   -0.000136,
   -0.0001459902,
   -0.000152,
   -0.0001586734,
   -0.000168,
   -0.000168467,
   -0.0001754241,
   -0.0001804577,
   -0.0001836002,
   -0.000184,
   -0.0001848093,
   -0.0001843102,
   -0.000184,
   -0.0001819956,
   -0.0001778845,
   -0.0001720344,
   -0.000168,
   -0.0001641283,
   -0.000153987,
   -0.000152,
   -0.0001409734,
   -0.000136,
   -0.0001245851,
   -0.00012,
   -0.000104,
   -0.0001034763,
   -8.8e-05,
   -7.364318e-05,
   -7.2e-05,
   -5.6e-05,
   -4e-05,
   -2.4e-05,
   -1.300643e-05,
   -8e-06,
   8e-06,
   2.4e-05,
   3.114002e-05,
   4e-05};
   Double_t Graph6_fy7[89] = {
   37.73504,
   36.91573,
   35.74919,
   34.22995,
   34,
   32.15662,
   30,
   29.59795,
   26.26935,
   26,
   22,
   21.86394,
   18,
   15.3863,
   14,
   10,
   6,
   2,
   -2,
   -6,
   -10,
   -14,
   -17.14935,
   -18,
   -22,
   -23.91552,
   -26,
   -28.74555,
   -30,
   -32.47423,
   -34,
   -35.46746,
   -37.93884,
   -38,
   -39.84208,
   -41.41722,
   -42,
   -42.60161,
   -43.42931,
   -43.95188,
   -44.16499,
   -44.06418,
   -43.64495,
   -42.90269,
   -42,
   -41.8142,
   -40.25571,
   -38.3204,
   -38,
   -35.74914,
   -34,
   -32.50443,
   -30,
   -28.22093,
   -26,
   -22.21069,
   -22,
   -18,
   -14,
   -10,
   -8.758591,
   -6,
   -2,
   -1.42492,
   2,
   6,
   10,
   12.12892,
   14,
   18,
   18.662,
   22,
   23.30535,
   26,
   26.95532,
   29.91558,
   30,
   32.2313,
   34,
   34.1831,
   35.65069,
   36.80629,
   37.64513,
   38,
   38.14732,
   38.32021,
   38.19251,
   38,
   37.73504};
   graph = new TGraph(89,Graph6_fx7,Graph6_fy7);
   graph->SetName("Graph6");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#f9f90e");
   graph->SetFillColor(ci);
   graph->SetFillStyle(0);

   ci = TColor::GetColor("#18aae7");
   graph->SetLineColor(ci);
   graph->SetLineStyle(2);
   graph->SetLineWidth(2);
   
   TH1F *Graph_Graph7 = new TH1F("Graph_Graph7","Graph",100,-0.0002215884,0.0002197612);
   Graph_Graph7->SetMinimum(-52.41351);
   Graph_Graph7->SetMaximum(46.56873);
   Graph_Graph7->SetDirectory(0);
   Graph_Graph7->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph7->SetLineColor(ci);
   Graph_Graph7->GetXaxis()->SetLabelFont(42);
   Graph_Graph7->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph7->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph7->GetXaxis()->SetTitleFont(42);
   Graph_Graph7->GetYaxis()->SetLabelFont(42);
   Graph_Graph7->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph7->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph7->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph7->GetYaxis()->SetTitleFont(42);
   Graph_Graph7->GetZaxis()->SetLabelFont(42);
   Graph_Graph7->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph7->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph7->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph7);
   
   graph->Draw("l");
   TGaxis *gaxis = new TGaxis(-0.0004,100,0.0004,100,-0.0004,0.0004,407,"-U");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0.04);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axist");
   gaxis->Draw();
   gaxis = new TGaxis(-0.0004,-100,0.0004,-100,-0.0004,0.0004,407,"");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(35);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisb");
   gaxis->SetLabelFont(133);
   gaxis->Draw();
   gaxis = new TGaxis(-0.0004,-100,-0.0004,100,-100,100,407,"");
   gaxis->SetLabelOffset(0.01);
   gaxis->SetLabelSize(35);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisl");
   gaxis->SetLabelFont(133);
   gaxis->Draw();
   gaxis = new TGaxis(0.0004,-100,0.0004,100,-100,100,407,"+U");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0.04);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisr");
   gaxis->Draw();
   TMarker *marker = new TMarker(-8.322614e-07,-2.941466,3);
   marker->SetMarkerColor(9);
   marker->SetMarkerStyle(3);
   marker->SetMarkerSize(1.6);
   marker->Draw();
   
   TLegend *leg = new TLegend(0.17,0.75,0.55,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(133);
   leg->SetTextSize(29);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","SMEFT Run2","");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(133);
   leg->Draw();
   
   TPaveText *pt = new TPaveText(0.65,0.775,0.875,0.9,"BRNDC");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(32);
   pt->SetTextFont(133);
   pt->SetTextSize(45);
   TText *AText = pt->AddText("GammaCombo");
   pt->Draw();
   
   pt = new TPaveText(0.17,0.15,0.37,0.275,"BRNDC");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);

   ci = TColor::GetColor("#999999");
   pt->SetTextColor(ci);
   pt->SetTextFont(133);
   pt->SetTextSize(21);
   AText = pt->AddText("contours hold 68%, 95% CL");
   pt->Draw();
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->Modified();
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->cd();
   HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41->SetSelected(HGamEFTScanner_2dscan_linear_exp_final_cHB:cHBt_cHB_cHBtUID41);
}
