void HGamEFTScanner_marginal_linear_obs_cg_cg()
{
//=========Macro generated from canvas: HGamEFTScanner_marginal_linear_obs_cg_cgUID20/p-value curves
//=========  (Thu Jul 22 09:13:11 2021) by ROOT version6.08/02
   TCanvas *HGamEFTScanner_marginal_linear_obs_cg_cgUID20 = new TCanvas("HGamEFTScanner_marginal_linear_obs_cg_cgUID20", "p-value curves",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   HGamEFTScanner_marginal_linear_obs_cg_cgUID20->SetHighLightColor(2);
   HGamEFTScanner_marginal_linear_obs_cg_cgUID20->Range(-0.01432432,-0.2833333,0.0127027,1.383333);
   HGamEFTScanner_marginal_linear_obs_cg_cgUID20->SetFillColor(0);
   HGamEFTScanner_marginal_linear_obs_cg_cgUID20->SetBorderMode(0);
   HGamEFTScanner_marginal_linear_obs_cg_cgUID20->SetBorderSize(2);
   HGamEFTScanner_marginal_linear_obs_cg_cgUID20->SetLeftMargin(0.16);
   HGamEFTScanner_marginal_linear_obs_cg_cgUID20->SetTopMargin(0.05);
   HGamEFTScanner_marginal_linear_obs_cg_cgUID20->SetBottomMargin(0.17);
   HGamEFTScanner_marginal_linear_obs_cg_cgUID20->SetFrameBorderMode(0);
   HGamEFTScanner_marginal_linear_obs_cg_cgUID20->SetFrameBorderMode(0);
   
   Double_t UID25_fx1[155] = {
   -0.01,
   -0.01,
   -0.009933333,
   -0.0098,
   -0.009666666,
   -0.009533333,
   -0.0094,
   -0.009266666,
   -0.009133333,
   -0.009,
   -0.008866667,
   -0.008733333,
   -0.008599999,
   -0.008466667,
   -0.008333333,
   -0.0082,
   -0.008066666,
   -0.007933334,
   -0.0078,
   -0.007666667,
   -0.007533333,
   -0.0074,
   -0.007266666,
   -0.007133333,
   -0.007,
   -0.006866666,
   -0.006733333,
   -0.0066,
   -0.006466667,
   -0.006333333,
   -0.0062,
   -0.006066666,
   -0.005933333,
   -0.0058,
   -0.005666667,
   -0.005533333,
   -0.0054,
   -0.005266666,
   -0.005133333,
   -0.005,
   -0.004866667,
   -0.004733333,
   -0.0046,
   -0.004466666,
   -0.004333333,
   -0.0042,
   -0.004066667,
   -0.003933333,
   -0.0038,
   -0.003666667,
   -0.003533333,
   -0.0034,
   -0.003266667,
   -0.003133333,
   -0.003,
   -0.002866667,
   -0.002733333,
   -0.0026,
   -0.002466667,
   -0.002333333,
   -0.0022,
   -0.002066667,
   -0.001933333,
   -0.0018,
   -0.001666667,
   -0.001533333,
   -0.0014,
   -0.001266667,
   -0.001133333,
   -0.0009999999,
   -0.0008666667,
   -0.0007333333,
   -0.0006,
   -0.0004666667,
   -0.0003333333,
   -0.0002,
   -6.666667e-05,
   6.666667e-05,
   0.0002,
   0.0002026397,
   0.0003333333,
   0.0004666667,
   0.0006,
   0.0007333333,
   0.0008666667,
   0.0009999999,
   0.001133333,
   0.001266667,
   0.0014,
   0.001533333,
   0.001666667,
   0.0018,
   0.001933333,
   0.002066667,
   0.0022,
   0.002333333,
   0.002466667,
   0.0026,
   0.002733333,
   0.002866667,
   0.003,
   0.003133333,
   0.003266667,
   0.0034,
   0.003533333,
   0.003666667,
   0.0038,
   0.003933333,
   0.004066667,
   0.0042,
   0.004333333,
   0.004466666,
   0.0046,
   0.004733333,
   0.004866667,
   0.005,
   0.005133333,
   0.005266666,
   0.0054,
   0.005533333,
   0.005666667,
   0.0058,
   0.005933333,
   0.006066666,
   0.0062,
   0.006333333,
   0.006466667,
   0.0066,
   0.006733333,
   0.006866666,
   0.007,
   0.007133333,
   0.007266666,
   0.0074,
   0.007533333,
   0.007666667,
   0.0078,
   0.007933334,
   0.008066666,
   0.0082,
   0.008333333,
   0.008466667,
   0.008599999,
   0.008733333,
   0.008866667,
   0.009,
   0.009133333,
   0.009266666,
   0.0094,
   0.009533333,
   0.009666666,
   0.0098,
   0.009933333,
   0.01,
   0.01};
   Double_t UID25_fy1[155] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   2.802597e-45,
   3.095468e-42,
   2.247599e-39,
   1.306768e-36,
   6.071365e-34,
   2.249473e-31,
   6.632852e-29,
   1.553501e-26,
   2.884792e-24,
   4.239689e-22,
   4.923156e-20,
   4.509744e-18,
   3.253953e-16,
   1.846786e-14,
   8.234093e-13,
   2.880803e-11,
   7.901083e-10,
   1.697413e-08,
   2.854782e-07,
   3.757693e-06,
   3.87144e-05,
   0.0003123795,
   0.001976488,
   0.009827749,
   0.03853714,
   0.1198152,
   0.2977779,
   0.5988247,
   0.9921328,
   1,
   0.6125773,
   0.3059485,
   0.1231126,
   0.03933894,
   0.009877157,
   0.001933536,
   0.0002933777,
   3.434579e-05,
   3.091072e-06,
   2.132237e-07,
   1.124519e-08,
   4.524474e-10,
   1.386199e-11,
   3.228609e-13,
   5.708036e-15,
   7.64968e-17,
   7.761344e-19,
   5.954625e-21,
   3.450851e-23,
   1.509035e-25,
   4.974432e-28,
   1.234988e-30,
   2.307101e-33,
   3.240294e-36,
   3.418655e-39,
   2.707309e-42,
   1.401298e-45,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   TGraph *graph = new TGraph(155,UID25_fx1,UID25_fy1);
   graph->SetName("UID25");
   graph->SetTitle("Graph");

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#9999cc");
   graph->SetFillColor(ci);

   ci = TColor::GetColor("#9999cc");
   graph->SetLineColor(ci);
   graph->SetLineWidth(2);
   
   TH1F *Graph_haxesUID261 = new TH1F("Graph_haxesUID261","",100,-0.01,0.01);
   Graph_haxesUID261->SetMinimum(0);
   Graph_haxesUID261->SetMaximum(1.3);
   Graph_haxesUID261->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID261->SetLineColor(ci);
   Graph_haxesUID261->GetXaxis()->SetTitle("#it{#bar{C}_{HG}}");
   Graph_haxesUID261->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID261->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID261->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID261->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID261->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID261->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID261->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID261->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID261->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID261->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID261->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID261->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID261->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID261->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID261->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID261->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID261->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID261->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_haxesUID261);
   
   graph->Draw(" f a");
   
   TH1F *haxesUID32__1 = new TH1F("haxesUID32__1","",100,-0.01,0.01);
   haxesUID32__1->SetMinimum(0);
   haxesUID32__1->SetMaximum(1.3);
   haxesUID32__1->SetStats(0);

   ci = TColor::GetColor("#000099");
   haxesUID32__1->SetLineColor(ci);
   haxesUID32__1->GetXaxis()->SetTitle("#it{#bar{C}_{HG}}");
   haxesUID32__1->GetXaxis()->SetNdivisions(407);
   haxesUID32__1->GetXaxis()->SetLabelFont(133);
   haxesUID32__1->GetXaxis()->SetLabelSize(35);
   haxesUID32__1->GetXaxis()->SetTitleSize(45);
   haxesUID32__1->GetXaxis()->SetTitleOffset(0.9);
   haxesUID32__1->GetXaxis()->SetTitleFont(133);
   haxesUID32__1->GetYaxis()->SetTitle("1-CL");
   haxesUID32__1->GetYaxis()->SetNdivisions(407);
   haxesUID32__1->GetYaxis()->SetLabelFont(133);
   haxesUID32__1->GetYaxis()->SetLabelOffset(0.01);
   haxesUID32__1->GetYaxis()->SetLabelSize(35);
   haxesUID32__1->GetYaxis()->SetTitleSize(45);
   haxesUID32__1->GetYaxis()->SetTitleOffset(0.85);
   haxesUID32__1->GetYaxis()->SetTitleFont(133);
   haxesUID32__1->GetZaxis()->SetLabelFont(42);
   haxesUID32__1->GetZaxis()->SetLabelSize(0.035);
   haxesUID32__1->GetZaxis()->SetTitleSize(0.035);
   haxesUID32__1->GetZaxis()->SetTitleFont(42);
   haxesUID32__1->Draw("axissame");
   
   Double_t UID31_fx2[155] = {
   -0.01,
   -0.01,
   -0.009933333,
   -0.0098,
   -0.009666666,
   -0.009533333,
   -0.0094,
   -0.009266666,
   -0.009133333,
   -0.009,
   -0.008866667,
   -0.008733333,
   -0.008599999,
   -0.008466667,
   -0.008333333,
   -0.0082,
   -0.008066666,
   -0.007933334,
   -0.0078,
   -0.007666667,
   -0.007533333,
   -0.0074,
   -0.007266666,
   -0.007133333,
   -0.007,
   -0.006866666,
   -0.006733333,
   -0.0066,
   -0.006466667,
   -0.006333333,
   -0.0062,
   -0.006066666,
   -0.005933333,
   -0.0058,
   -0.005666667,
   -0.005533333,
   -0.0054,
   -0.005266666,
   -0.005133333,
   -0.005,
   -0.004866667,
   -0.004733333,
   -0.0046,
   -0.004466666,
   -0.004333333,
   -0.0042,
   -0.004066667,
   -0.003933333,
   -0.0038,
   -0.003666667,
   -0.003533333,
   -0.0034,
   -0.003266667,
   -0.003133333,
   -0.003,
   -0.002866667,
   -0.002733333,
   -0.0026,
   -0.002466667,
   -0.002333333,
   -0.0022,
   -0.002066667,
   -0.001933333,
   -0.0018,
   -0.001666667,
   -0.001533333,
   -0.0014,
   -0.001266667,
   -0.001133333,
   -0.0009999999,
   -0.0008666667,
   -0.0007333333,
   -0.0006,
   -0.0004666667,
   -0.0003333333,
   -0.0002,
   -6.666667e-05,
   6.666667e-05,
   0.0002,
   0.0002026397,
   0.0003333333,
   0.0004666667,
   0.0006,
   0.0007333333,
   0.0008666667,
   0.0009999999,
   0.001133333,
   0.001266667,
   0.0014,
   0.001533333,
   0.001666667,
   0.0018,
   0.001933333,
   0.002066667,
   0.0022,
   0.002333333,
   0.002466667,
   0.0026,
   0.002733333,
   0.002866667,
   0.003,
   0.003133333,
   0.003266667,
   0.0034,
   0.003533333,
   0.003666667,
   0.0038,
   0.003933333,
   0.004066667,
   0.0042,
   0.004333333,
   0.004466666,
   0.0046,
   0.004733333,
   0.004866667,
   0.005,
   0.005133333,
   0.005266666,
   0.0054,
   0.005533333,
   0.005666667,
   0.0058,
   0.005933333,
   0.006066666,
   0.0062,
   0.006333333,
   0.006466667,
   0.0066,
   0.006733333,
   0.006866666,
   0.007,
   0.007133333,
   0.007266666,
   0.0074,
   0.007533333,
   0.007666667,
   0.0078,
   0.007933334,
   0.008066666,
   0.0082,
   0.008333333,
   0.008466667,
   0.008599999,
   0.008733333,
   0.008866667,
   0.009,
   0.009133333,
   0.009266666,
   0.0094,
   0.009533333,
   0.009666666,
   0.0098,
   0.009933333,
   0.01,
   0.01};
   Double_t UID31_fy2[155] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   2.802597e-45,
   3.095468e-42,
   2.247599e-39,
   1.306768e-36,
   6.071365e-34,
   2.249473e-31,
   6.632852e-29,
   1.553501e-26,
   2.884792e-24,
   4.239689e-22,
   4.923156e-20,
   4.509744e-18,
   3.253953e-16,
   1.846786e-14,
   8.234093e-13,
   2.880803e-11,
   7.901083e-10,
   1.697413e-08,
   2.854782e-07,
   3.757693e-06,
   3.87144e-05,
   0.0003123795,
   0.001976488,
   0.009827749,
   0.03853714,
   0.1198152,
   0.2977779,
   0.5988247,
   0.9921328,
   1,
   0.6125773,
   0.3059485,
   0.1231126,
   0.03933894,
   0.009877157,
   0.001933536,
   0.0002933777,
   3.434579e-05,
   3.091072e-06,
   2.132237e-07,
   1.124519e-08,
   4.524474e-10,
   1.386199e-11,
   3.228609e-13,
   5.708036e-15,
   7.64968e-17,
   7.761344e-19,
   5.954625e-21,
   3.450851e-23,
   1.509035e-25,
   4.974432e-28,
   1.234988e-30,
   2.307101e-33,
   3.240294e-36,
   3.418655e-39,
   2.707309e-42,
   1.401298e-45,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   graph = new TGraph(155,UID31_fx2,UID31_fy2);
   graph->SetName("UID31");
   graph->SetTitle("Graph");
   graph->SetFillColor(1);

   ci = TColor::GetColor("#9999cc");
   graph->SetLineColor(ci);
   graph->SetLineStyle(0);
   graph->SetLineWidth(2);
   
   TH1F *Graph_haxesUID322 = new TH1F("Graph_haxesUID322","",100,-0.01,0.01);
   Graph_haxesUID322->SetMinimum(0);
   Graph_haxesUID322->SetMaximum(1.3);
   Graph_haxesUID322->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID322->SetLineColor(ci);
   Graph_haxesUID322->GetXaxis()->SetTitle("#it{#bar{C}_{HG}}");
   Graph_haxesUID322->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID322->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID322->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID322->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID322->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID322->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_haxesUID322);
   
   graph->Draw(" l");
   TGaxis *gaxis = new TGaxis(-0.01,1,0.01,1,-0.01,0.01,407,"-U");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0.04);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axist");
   gaxis->Draw();
   gaxis = new TGaxis(0.01,0,0.01,1.3,0,1.3,407,"+");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(0);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisr");
   gaxis->SetLabelColor(0);
   gaxis->Draw();
   
   TH1F *Graph_haxesUID322 = new TH1F("Graph_haxesUID322","",100,-0.01,0.01);
   Graph_haxesUID322->SetMinimum(0);
   Graph_haxesUID322->SetMaximum(1.3);
   Graph_haxesUID322->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID322->SetLineColor(ci);
   Graph_haxesUID322->GetXaxis()->SetTitle("#it{#bar{C}_{HG}}");
   Graph_haxesUID322->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID322->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID322->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID322->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID322->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID322->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleFont(42);
   Graph_haxesUID322->Draw("axissame");
   TLine *line = new TLine(0.0002026397,0,0.0002026397,1);
   line->Draw();
   line = new TLine(-5.860788e-05,0,-5.860788e-05,1);
   line->SetLineStyle(2);
   line->Draw();
   line = new TLine(0.0004646242,0,0.0004646242,1);
   line->SetLineStyle(2);
   line->Draw();
   
   TPaveText *pt = new TPaveText(0.0002026397,0.6,0.00420264,0.7,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(13);
   pt->SetTextFont(133);
   TText *AText = pt->AddText("0.00020^{+0.00026}_{#font[122]{-}0.00026}");
   AText->SetTextSize(35);
   pt->Draw();
   
   TLegend *leg = new TLegend(0.19,0.78,0.5,0.9440559,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(133);
   leg->SetTextSize(21.75);
   leg->SetLineColor(0);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("UID25","SMEFT Run 2 Scan of cHG (Prob)","lf");

   ci = TColor::GetColor("#9999cc");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#9999cc");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(133);
   leg->Draw();
   
   pt = new TPaveText(-0.007999999,0.3373,0.002,0.3873,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextFont(133);
   pt->SetTextSize(35);
   AText = pt->AddText("68.3%");
   pt->Draw();
   line = new TLine(-0.01,0.3173,0.01,0.3173);
   line->SetLineStyle(3);
   line->Draw();
   
   pt = new TPaveText(-0.007999999,0.0655,0.002,0.1155,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextFont(133);
   pt->SetTextSize(35);
   AText = pt->AddText("95.5%");
   pt->Draw();
   line = new TLine(-0.01,0.0455,0.01,0.0455);
   line->SetLineStyle(3);
   line->Draw();
   
   pt = new TPaveText(0.65,0.6,0.875,0.725,"BRNDC");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(32);
   pt->SetTextFont(133);
   pt->SetTextSize(45);
   AText = pt->AddText("GammaCombo");
   pt->Draw();
   HGamEFTScanner_marginal_linear_obs_cg_cgUID20->Modified();
   HGamEFTScanner_marginal_linear_obs_cg_cgUID20->cd();
   HGamEFTScanner_marginal_linear_obs_cg_cgUID20->SetSelected(HGamEFTScanner_marginal_linear_obs_cg_cgUID20);
}
