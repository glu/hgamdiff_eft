void HGamEFTScanner_linear_marginal_cHWB_cHWB()
{
//=========Macro generated from canvas: HGamEFTScanner_linear_marginal_cHWB_cHWBUID20/p-value curves
//=========  (Tue Jun 22 11:31:21 2021) by ROOT version6.08/02
   TCanvas *HGamEFTScanner_linear_marginal_cHWB_cHWBUID20 = new TCanvas("HGamEFTScanner_linear_marginal_cHWB_cHWBUID20", "p-value curves",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   HGamEFTScanner_linear_marginal_cHWB_cHWBUID20->SetHighLightColor(2);
   HGamEFTScanner_linear_marginal_cHWB_cHWBUID20->Range(-0.007162162,-0.2833333,0.006351351,1.383333);
   HGamEFTScanner_linear_marginal_cHWB_cHWBUID20->SetFillColor(0);
   HGamEFTScanner_linear_marginal_cHWB_cHWBUID20->SetBorderMode(0);
   HGamEFTScanner_linear_marginal_cHWB_cHWBUID20->SetBorderSize(2);
   HGamEFTScanner_linear_marginal_cHWB_cHWBUID20->SetLeftMargin(0.16);
   HGamEFTScanner_linear_marginal_cHWB_cHWBUID20->SetTopMargin(0.05);
   HGamEFTScanner_linear_marginal_cHWB_cHWBUID20->SetBottomMargin(0.17);
   HGamEFTScanner_linear_marginal_cHWB_cHWBUID20->SetFrameBorderMode(0);
   HGamEFTScanner_linear_marginal_cHWB_cHWBUID20->SetFrameBorderMode(0);
   
   Double_t UID25_fx1[305] = {
   -0.005,
   -0.005,
   -0.004983333,
   -0.00495,
   -0.004916667,
   -0.004883333,
   -0.00485,
   -0.004816667,
   -0.004783333,
   -0.00475,
   -0.004716666,
   -0.004683333,
   -0.00465,
   -0.004616667,
   -0.004583333,
   -0.00455,
   -0.004516667,
   -0.004483333,
   -0.00445,
   -0.004416666,
   -0.004383333,
   -0.00435,
   -0.004316667,
   -0.004283333,
   -0.00425,
   -0.004216667,
   -0.004183333,
   -0.00415,
   -0.004116667,
   -0.004083333,
   -0.00405,
   -0.004016667,
   -0.003983333,
   -0.00395,
   -0.003916666,
   -0.003883333,
   -0.00385,
   -0.003816667,
   -0.003783333,
   -0.00375,
   -0.003716666,
   -0.003683333,
   -0.00365,
   -0.003616667,
   -0.003583333,
   -0.00355,
   -0.003516667,
   -0.003483333,
   -0.00345,
   -0.003416667,
   -0.003383333,
   -0.00335,
   -0.003316667,
   -0.003283333,
   -0.00325,
   -0.003216667,
   -0.003183333,
   -0.00315,
   -0.003116667,
   -0.003083333,
   -0.00305,
   -0.003016667,
   -0.002983333,
   -0.00295,
   -0.002916667,
   -0.002883333,
   -0.00285,
   -0.002816667,
   -0.002783333,
   -0.00275,
   -0.002716667,
   -0.002683333,
   -0.00265,
   -0.002616667,
   -0.002583333,
   -0.00255,
   -0.002516667,
   -0.002483333,
   -0.00245,
   -0.002416667,
   -0.002383333,
   -0.00235,
   -0.002316667,
   -0.002283333,
   -0.00225,
   -0.002216667,
   -0.002183333,
   -0.00215,
   -0.002116667,
   -0.002083333,
   -0.00205,
   -0.002016667,
   -0.001983333,
   -0.00195,
   -0.001916667,
   -0.001883333,
   -0.00185,
   -0.001816667,
   -0.001783333,
   -0.00175,
   -0.001716667,
   -0.001683333,
   -0.00165,
   -0.001616667,
   -0.001583333,
   -0.00155,
   -0.001516667,
   -0.001483333,
   -0.00145,
   -0.001416667,
   -0.001383333,
   -0.00135,
   -0.001316667,
   -0.001283333,
   -0.00125,
   -0.001216667,
   -0.001183333,
   -0.00115,
   -0.001116667,
   -0.001083333,
   -0.00105,
   -0.001016667,
   -0.0009833333,
   -0.00095,
   -0.0009166666,
   -0.0008833333,
   -0.00085,
   -0.0008166666,
   -0.0007833333,
   -0.00075,
   -0.0007166666,
   -0.0006833333,
   -0.00065,
   -0.0006166666,
   -0.0005833333,
   -0.00055,
   -0.0005166667,
   -0.0004833333,
   -0.00045,
   -0.0004166666,
   -0.0003833333,
   -0.00035,
   -0.0003166667,
   -0.0002833333,
   -0.00025,
   -0.0002166667,
   -0.0001833333,
   -0.00015,
   -0.0001166667,
   -8.333333e-05,
   -5e-05,
   -1.666667e-05,
   9.616128e-08,
   1.666667e-05,
   5e-05,
   8.333333e-05,
   0.0001166667,
   0.00015,
   0.0001833333,
   0.0002166667,
   0.00025,
   0.0002833333,
   0.0003166667,
   0.00035,
   0.0003833333,
   0.0004166666,
   0.00045,
   0.0004833333,
   0.0005166667,
   0.00055,
   0.0005833333,
   0.0006166666,
   0.00065,
   0.0006833333,
   0.0007166666,
   0.00075,
   0.0007833333,
   0.0008166666,
   0.00085,
   0.0008833333,
   0.0009166666,
   0.00095,
   0.0009833333,
   0.001016667,
   0.00105,
   0.001083333,
   0.001116667,
   0.00115,
   0.001183333,
   0.001216667,
   0.00125,
   0.001283333,
   0.001316667,
   0.00135,
   0.001383333,
   0.001416667,
   0.00145,
   0.001483333,
   0.001516667,
   0.00155,
   0.001583333,
   0.001616667,
   0.00165,
   0.001683333,
   0.001716667,
   0.00175,
   0.001783333,
   0.001816667,
   0.00185,
   0.001883333,
   0.001916667,
   0.00195,
   0.001983333,
   0.002016667,
   0.00205,
   0.002083333,
   0.002116667,
   0.00215,
   0.002183333,
   0.002216667,
   0.00225,
   0.002283333,
   0.002316667,
   0.00235,
   0.002383333,
   0.002416667,
   0.00245,
   0.002483333,
   0.002516667,
   0.00255,
   0.002583333,
   0.002616667,
   0.00265,
   0.002683333,
   0.002716667,
   0.00275,
   0.002783333,
   0.002816667,
   0.00285,
   0.002883333,
   0.002916667,
   0.00295,
   0.002983333,
   0.003016667,
   0.00305,
   0.003083333,
   0.003116667,
   0.00315,
   0.003183333,
   0.003216667,
   0.00325,
   0.003283333,
   0.003316667,
   0.00335,
   0.003383333,
   0.003416667,
   0.00345,
   0.003483333,
   0.003516667,
   0.00355,
   0.003583333,
   0.003616667,
   0.00365,
   0.003683333,
   0.003716666,
   0.00375,
   0.003783333,
   0.003816667,
   0.00385,
   0.003883333,
   0.003916666,
   0.00395,
   0.003983333,
   0.004016667,
   0.00405,
   0.004083333,
   0.004116667,
   0.00415,
   0.004183333,
   0.004216667,
   0.00425,
   0.004283333,
   0.004316667,
   0.00435,
   0.004383333,
   0.004416666,
   0.00445,
   0.004483333,
   0.004516667,
   0.00455,
   0.004583333,
   0.004616667,
   0.00465,
   0.004683333,
   0.004716666,
   0.00475,
   0.004783333,
   0.004816667,
   0.00485,
   0.004883333,
   0.004916667,
   0.00495,
   0.004983333,
   0.005,
   0.005};
   Double_t UID25_fy1[305] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   5.605194e-45,
   5.885454e-44,
   5.465064e-43,
   4.894736e-42,
   4.276202e-41,
   3.641905e-40,
   3.023311e-39,
   2.446493e-38,
   1.929764e-37,
   1.483822e-36,
   1.112166e-35,
   8.12615e-35,
   5.787979e-34,
   4.018792e-33,
   2.720244e-32,
   1.794972e-31,
   1.154682e-30,
   7.241389e-30,
   4.427274e-29,
   2.638899e-28,
   1.533474e-27,
   8.687877e-27,
   4.798776e-26,
   2.584303e-25,
   1.356918e-24,
   6.94645e-24,
   3.467268e-23,
   1.687425e-22,
   8.00738e-22,
   3.704955e-21,
   1.671547e-20,
   7.353559e-20,
   3.154497e-19,
   1.319543e-18,
   5.382555e-18,
   2.141058e-17,
   8.305274e-17,
   3.141766e-16,
   1.159036e-15,
   4.169963e-15,
   1.463152e-14,
   5.00703e-14,
   1.671133e-13,
   5.439939e-13,
   1.727191e-12,
   5.348863e-12,
   1.615731e-11,
   4.76075e-11,
   1.368343e-10,
   3.836526e-10,
   1.049351e-09,
   2.799994e-09,
   7.288921e-09,
   1.851208e-08,
   4.58723e-08,
   1.109094e-07,
   2.616531e-07,
   6.023437e-07,
   1.35315e-06,
   2.966561e-06,
   6.347342e-06,
   1.325522e-05,
   2.701888e-05,
   5.376054e-05,
   0.0001044257,
   0.0001980316,
   0.0003666761,
   0.000662969,
   0.001170604,
   0.002018746,
   0.003400646,
   0.005596366,
   0.008998684,
   0.01413999,
   0.02171665,
   0.03260568,
   0.04786771,
   0.06872945,
   0.09653992,
   0.1326966,
   0.1785418,
   0.235235,
   0.3036103,
   0.3840381,
   0.4763064,
   0.5795454,
   0.6922086,
   0.8121234,
   0.9366084,
   1,
   0.9373479,
   0.8128718,
   0.6929722,
   0.5803256,
   0.4770991,
   0.3848332,
   0.3043934,
   0.2359891,
   0.1792497,
   0.1333431,
   0.09711373,
   0.06922403,
   0.04828157,
   0.03294184,
   0.02198172,
   0.01434292,
   0.009149544,
   0.005705294,
   0.003477054,
   0.002070826,
   0.001205104,
   0.0006851862,
   0.000380587,
   0.000206502,
   0.0001094423,
   5.665081e-05,
   2.863904e-05,
   1.413898e-05,
   6.816495e-06,
   3.208974e-06,
   1.475079e-06,
   6.620491e-07,
   2.901184e-07,
   1.24124e-07,
   5.184622e-08,
   2.114219e-08,
   8.416706e-09,
   3.271021e-09,
   1.240979e-09,
   4.595972e-10,
   1.661553e-10,
   5.863632e-11,
   2.019908e-11,
   6.792063e-12,
   2.229318e-12,
   7.142294e-13,
   2.233546e-13,
   6.817726e-14,
   2.031265e-14,
   5.907136e-15,
   1.676741e-15,
   4.645491e-16,
   1.256237e-16,
   3.315771e-17,
   8.542179e-18,
   2.147934e-18,
   5.271625e-19,
   1.262806e-19,
   2.952546e-20,
   6.73786e-21,
   1.500788e-21,
   3.262717e-22,
   6.923279e-23,
   1.433862e-23,
   2.898519e-24,
   5.718852e-25,
   1.101314e-25,
   2.070089e-26,
   3.797808e-27,
   6.800716e-28,
   1.188624e-28,
   2.027751e-29,
   3.376412e-30,
   5.487489e-31,
   8.705154e-32,
   1.347889e-32,
   2.037135e-33,
   3.005132e-34,
   4.327062e-35,
   6.081573e-36,
   8.343e-37,
   1.117187e-37,
   1.460216e-38,
   1.862987e-39,
   2.320032e-40,
   2.820113e-41,
   3.346301e-42,
   3.881597e-43,
   4.344025e-44,
   4.203895e-45,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   TGraph *graph = new TGraph(305,UID25_fx1,UID25_fy1);
   graph->SetName("UID25");
   graph->SetTitle("Graph");

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#9999cc");
   graph->SetFillColor(ci);

   ci = TColor::GetColor("#9999cc");
   graph->SetLineColor(ci);
   graph->SetLineWidth(2);
   
   TH1F *Graph_haxesUID261 = new TH1F("Graph_haxesUID261","",100,-0.005,0.005);
   Graph_haxesUID261->SetMinimum(0);
   Graph_haxesUID261->SetMaximum(1.3);
   Graph_haxesUID261->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID261->SetLineColor(ci);
   Graph_haxesUID261->GetXaxis()->SetTitle("#bar{C}_{#it{HWB}}");
   Graph_haxesUID261->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID261->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID261->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID261->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID261->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID261->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID261->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID261->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID261->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID261->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID261->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID261->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID261->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID261->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID261->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID261->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID261->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID261->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_haxesUID261);
   
   graph->Draw(" f a");
   
   TH1F *haxesUID32__1 = new TH1F("haxesUID32__1","",100,-0.005,0.005);
   haxesUID32__1->SetMinimum(0);
   haxesUID32__1->SetMaximum(1.3);
   haxesUID32__1->SetStats(0);

   ci = TColor::GetColor("#000099");
   haxesUID32__1->SetLineColor(ci);
   haxesUID32__1->GetXaxis()->SetTitle("#bar{C}_{#it{HWB}}");
   haxesUID32__1->GetXaxis()->SetNdivisions(407);
   haxesUID32__1->GetXaxis()->SetLabelFont(133);
   haxesUID32__1->GetXaxis()->SetLabelSize(35);
   haxesUID32__1->GetXaxis()->SetTitleSize(45);
   haxesUID32__1->GetXaxis()->SetTitleOffset(0.9);
   haxesUID32__1->GetXaxis()->SetTitleFont(133);
   haxesUID32__1->GetYaxis()->SetTitle("1-CL");
   haxesUID32__1->GetYaxis()->SetNdivisions(407);
   haxesUID32__1->GetYaxis()->SetLabelFont(133);
   haxesUID32__1->GetYaxis()->SetLabelOffset(0.01);
   haxesUID32__1->GetYaxis()->SetLabelSize(35);
   haxesUID32__1->GetYaxis()->SetTitleSize(45);
   haxesUID32__1->GetYaxis()->SetTitleOffset(0.85);
   haxesUID32__1->GetYaxis()->SetTitleFont(133);
   haxesUID32__1->GetZaxis()->SetLabelFont(42);
   haxesUID32__1->GetZaxis()->SetLabelSize(0.035);
   haxesUID32__1->GetZaxis()->SetTitleSize(0.035);
   haxesUID32__1->GetZaxis()->SetTitleFont(42);
   haxesUID32__1->Draw("axissame");
   
   Double_t UID31_fx2[305] = {
   -0.005,
   -0.005,
   -0.004983333,
   -0.00495,
   -0.004916667,
   -0.004883333,
   -0.00485,
   -0.004816667,
   -0.004783333,
   -0.00475,
   -0.004716666,
   -0.004683333,
   -0.00465,
   -0.004616667,
   -0.004583333,
   -0.00455,
   -0.004516667,
   -0.004483333,
   -0.00445,
   -0.004416666,
   -0.004383333,
   -0.00435,
   -0.004316667,
   -0.004283333,
   -0.00425,
   -0.004216667,
   -0.004183333,
   -0.00415,
   -0.004116667,
   -0.004083333,
   -0.00405,
   -0.004016667,
   -0.003983333,
   -0.00395,
   -0.003916666,
   -0.003883333,
   -0.00385,
   -0.003816667,
   -0.003783333,
   -0.00375,
   -0.003716666,
   -0.003683333,
   -0.00365,
   -0.003616667,
   -0.003583333,
   -0.00355,
   -0.003516667,
   -0.003483333,
   -0.00345,
   -0.003416667,
   -0.003383333,
   -0.00335,
   -0.003316667,
   -0.003283333,
   -0.00325,
   -0.003216667,
   -0.003183333,
   -0.00315,
   -0.003116667,
   -0.003083333,
   -0.00305,
   -0.003016667,
   -0.002983333,
   -0.00295,
   -0.002916667,
   -0.002883333,
   -0.00285,
   -0.002816667,
   -0.002783333,
   -0.00275,
   -0.002716667,
   -0.002683333,
   -0.00265,
   -0.002616667,
   -0.002583333,
   -0.00255,
   -0.002516667,
   -0.002483333,
   -0.00245,
   -0.002416667,
   -0.002383333,
   -0.00235,
   -0.002316667,
   -0.002283333,
   -0.00225,
   -0.002216667,
   -0.002183333,
   -0.00215,
   -0.002116667,
   -0.002083333,
   -0.00205,
   -0.002016667,
   -0.001983333,
   -0.00195,
   -0.001916667,
   -0.001883333,
   -0.00185,
   -0.001816667,
   -0.001783333,
   -0.00175,
   -0.001716667,
   -0.001683333,
   -0.00165,
   -0.001616667,
   -0.001583333,
   -0.00155,
   -0.001516667,
   -0.001483333,
   -0.00145,
   -0.001416667,
   -0.001383333,
   -0.00135,
   -0.001316667,
   -0.001283333,
   -0.00125,
   -0.001216667,
   -0.001183333,
   -0.00115,
   -0.001116667,
   -0.001083333,
   -0.00105,
   -0.001016667,
   -0.0009833333,
   -0.00095,
   -0.0009166666,
   -0.0008833333,
   -0.00085,
   -0.0008166666,
   -0.0007833333,
   -0.00075,
   -0.0007166666,
   -0.0006833333,
   -0.00065,
   -0.0006166666,
   -0.0005833333,
   -0.00055,
   -0.0005166667,
   -0.0004833333,
   -0.00045,
   -0.0004166666,
   -0.0003833333,
   -0.00035,
   -0.0003166667,
   -0.0002833333,
   -0.00025,
   -0.0002166667,
   -0.0001833333,
   -0.00015,
   -0.0001166667,
   -8.333333e-05,
   -5e-05,
   -1.666667e-05,
   9.616128e-08,
   1.666667e-05,
   5e-05,
   8.333333e-05,
   0.0001166667,
   0.00015,
   0.0001833333,
   0.0002166667,
   0.00025,
   0.0002833333,
   0.0003166667,
   0.00035,
   0.0003833333,
   0.0004166666,
   0.00045,
   0.0004833333,
   0.0005166667,
   0.00055,
   0.0005833333,
   0.0006166666,
   0.00065,
   0.0006833333,
   0.0007166666,
   0.00075,
   0.0007833333,
   0.0008166666,
   0.00085,
   0.0008833333,
   0.0009166666,
   0.00095,
   0.0009833333,
   0.001016667,
   0.00105,
   0.001083333,
   0.001116667,
   0.00115,
   0.001183333,
   0.001216667,
   0.00125,
   0.001283333,
   0.001316667,
   0.00135,
   0.001383333,
   0.001416667,
   0.00145,
   0.001483333,
   0.001516667,
   0.00155,
   0.001583333,
   0.001616667,
   0.00165,
   0.001683333,
   0.001716667,
   0.00175,
   0.001783333,
   0.001816667,
   0.00185,
   0.001883333,
   0.001916667,
   0.00195,
   0.001983333,
   0.002016667,
   0.00205,
   0.002083333,
   0.002116667,
   0.00215,
   0.002183333,
   0.002216667,
   0.00225,
   0.002283333,
   0.002316667,
   0.00235,
   0.002383333,
   0.002416667,
   0.00245,
   0.002483333,
   0.002516667,
   0.00255,
   0.002583333,
   0.002616667,
   0.00265,
   0.002683333,
   0.002716667,
   0.00275,
   0.002783333,
   0.002816667,
   0.00285,
   0.002883333,
   0.002916667,
   0.00295,
   0.002983333,
   0.003016667,
   0.00305,
   0.003083333,
   0.003116667,
   0.00315,
   0.003183333,
   0.003216667,
   0.00325,
   0.003283333,
   0.003316667,
   0.00335,
   0.003383333,
   0.003416667,
   0.00345,
   0.003483333,
   0.003516667,
   0.00355,
   0.003583333,
   0.003616667,
   0.00365,
   0.003683333,
   0.003716666,
   0.00375,
   0.003783333,
   0.003816667,
   0.00385,
   0.003883333,
   0.003916666,
   0.00395,
   0.003983333,
   0.004016667,
   0.00405,
   0.004083333,
   0.004116667,
   0.00415,
   0.004183333,
   0.004216667,
   0.00425,
   0.004283333,
   0.004316667,
   0.00435,
   0.004383333,
   0.004416666,
   0.00445,
   0.004483333,
   0.004516667,
   0.00455,
   0.004583333,
   0.004616667,
   0.00465,
   0.004683333,
   0.004716666,
   0.00475,
   0.004783333,
   0.004816667,
   0.00485,
   0.004883333,
   0.004916667,
   0.00495,
   0.004983333,
   0.005,
   0.005};
   Double_t UID31_fy2[305] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   5.605194e-45,
   5.885454e-44,
   5.465064e-43,
   4.894736e-42,
   4.276202e-41,
   3.641905e-40,
   3.023311e-39,
   2.446493e-38,
   1.929764e-37,
   1.483822e-36,
   1.112166e-35,
   8.12615e-35,
   5.787979e-34,
   4.018792e-33,
   2.720244e-32,
   1.794972e-31,
   1.154682e-30,
   7.241389e-30,
   4.427274e-29,
   2.638899e-28,
   1.533474e-27,
   8.687877e-27,
   4.798776e-26,
   2.584303e-25,
   1.356918e-24,
   6.94645e-24,
   3.467268e-23,
   1.687425e-22,
   8.00738e-22,
   3.704955e-21,
   1.671547e-20,
   7.353559e-20,
   3.154497e-19,
   1.319543e-18,
   5.382555e-18,
   2.141058e-17,
   8.305274e-17,
   3.141766e-16,
   1.159036e-15,
   4.169963e-15,
   1.463152e-14,
   5.00703e-14,
   1.671133e-13,
   5.439939e-13,
   1.727191e-12,
   5.348863e-12,
   1.615731e-11,
   4.76075e-11,
   1.368343e-10,
   3.836526e-10,
   1.049351e-09,
   2.799994e-09,
   7.288921e-09,
   1.851208e-08,
   4.58723e-08,
   1.109094e-07,
   2.616531e-07,
   6.023437e-07,
   1.35315e-06,
   2.966561e-06,
   6.347342e-06,
   1.325522e-05,
   2.701888e-05,
   5.376054e-05,
   0.0001044257,
   0.0001980316,
   0.0003666761,
   0.000662969,
   0.001170604,
   0.002018746,
   0.003400646,
   0.005596366,
   0.008998684,
   0.01413999,
   0.02171665,
   0.03260568,
   0.04786771,
   0.06872945,
   0.09653992,
   0.1326966,
   0.1785418,
   0.235235,
   0.3036103,
   0.3840381,
   0.4763064,
   0.5795454,
   0.6922086,
   0.8121234,
   0.9366084,
   1,
   0.9373479,
   0.8128718,
   0.6929722,
   0.5803256,
   0.4770991,
   0.3848332,
   0.3043934,
   0.2359891,
   0.1792497,
   0.1333431,
   0.09711373,
   0.06922403,
   0.04828157,
   0.03294184,
   0.02198172,
   0.01434292,
   0.009149544,
   0.005705294,
   0.003477054,
   0.002070826,
   0.001205104,
   0.0006851862,
   0.000380587,
   0.000206502,
   0.0001094423,
   5.665081e-05,
   2.863904e-05,
   1.413898e-05,
   6.816495e-06,
   3.208974e-06,
   1.475079e-06,
   6.620491e-07,
   2.901184e-07,
   1.24124e-07,
   5.184622e-08,
   2.114219e-08,
   8.416706e-09,
   3.271021e-09,
   1.240979e-09,
   4.595972e-10,
   1.661553e-10,
   5.863632e-11,
   2.019908e-11,
   6.792063e-12,
   2.229318e-12,
   7.142294e-13,
   2.233546e-13,
   6.817726e-14,
   2.031265e-14,
   5.907136e-15,
   1.676741e-15,
   4.645491e-16,
   1.256237e-16,
   3.315771e-17,
   8.542179e-18,
   2.147934e-18,
   5.271625e-19,
   1.262806e-19,
   2.952546e-20,
   6.73786e-21,
   1.500788e-21,
   3.262717e-22,
   6.923279e-23,
   1.433862e-23,
   2.898519e-24,
   5.718852e-25,
   1.101314e-25,
   2.070089e-26,
   3.797808e-27,
   6.800716e-28,
   1.188624e-28,
   2.027751e-29,
   3.376412e-30,
   5.487489e-31,
   8.705154e-32,
   1.347889e-32,
   2.037135e-33,
   3.005132e-34,
   4.327062e-35,
   6.081573e-36,
   8.343e-37,
   1.117187e-37,
   1.460216e-38,
   1.862987e-39,
   2.320032e-40,
   2.820113e-41,
   3.346301e-42,
   3.881597e-43,
   4.344025e-44,
   4.203895e-45,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   graph = new TGraph(305,UID31_fx2,UID31_fy2);
   graph->SetName("UID31");
   graph->SetTitle("Graph");
   graph->SetFillColor(1);

   ci = TColor::GetColor("#9999cc");
   graph->SetLineColor(ci);
   graph->SetLineStyle(0);
   graph->SetLineWidth(2);
   
   TH1F *Graph_haxesUID322 = new TH1F("Graph_haxesUID322","",100,-0.005,0.005);
   Graph_haxesUID322->SetMinimum(0);
   Graph_haxesUID322->SetMaximum(1.3);
   Graph_haxesUID322->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID322->SetLineColor(ci);
   Graph_haxesUID322->GetXaxis()->SetTitle("#bar{C}_{#it{HWB}}");
   Graph_haxesUID322->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID322->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID322->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID322->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID322->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID322->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_haxesUID322);
   
   graph->Draw(" l");
   TGaxis *gaxis = new TGaxis(-0.005,1,0.005,1,-0.005,0.005,407,"-U");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0.04);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axist");
   gaxis->Draw();
   gaxis = new TGaxis(0.005,0,0.005,1.3,0,1.3,407,"+");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(0);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisr");
   gaxis->SetLabelColor(0);
   gaxis->Draw();
   
   TH1F *Graph_haxesUID322 = new TH1F("Graph_haxesUID322","",100,-0.005,0.005);
   Graph_haxesUID322->SetMinimum(0);
   Graph_haxesUID322->SetMaximum(1.3);
   Graph_haxesUID322->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID322->SetLineColor(ci);
   Graph_haxesUID322->GetXaxis()->SetTitle("#bar{C}_{#it{HWB}}");
   Graph_haxesUID322->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID322->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID322->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID322->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID322->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID322->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleFont(42);
   Graph_haxesUID322->Draw("axissame");
   TLine *line = new TLine(9.616128e-08,0,9.616128e-08,1);
   line->Draw();
   line = new TLine(-0.0002106452,0,-0.0002106452,1);
   line->SetLineStyle(2);
   line->Draw();
   line = new TLine(0.0002110453,0,0.0002110453,1);
   line->SetLineStyle(2);
   line->Draw();
   
   TPaveText *pt = new TPaveText(9.616128e-08,0.6,0.002000096,0.7,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(13);
   pt->SetTextFont(133);
   TText *AText = pt->AddText("0.00000^{+0.00021}_{#font[122]{-}0.00021}");
   AText->SetTextSize(35);
   pt->Draw();
   
   TLegend *leg = new TLegend(0.19,0.78,0.5,0.9440559,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(133);
   leg->SetTextSize(21.75);
   leg->SetLineColor(0);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("UID25","SMEFT Run 2 Scan of cHWB (Prob)","lf");

   ci = TColor::GetColor("#9999cc");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#9999cc");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(133);
   leg->Draw();
   
   pt = new TPaveText(-0.004,0.3373,0.001,0.3873,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextFont(133);
   pt->SetTextSize(35);
   AText = pt->AddText("68.3%");
   pt->Draw();
   line = new TLine(-0.005,0.3173,0.005,0.3173);
   line->SetLineStyle(3);
   line->Draw();
   
   pt = new TPaveText(-0.004,0.0655,0.001,0.1155,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextFont(133);
   pt->SetTextSize(35);
   AText = pt->AddText("95.5%");
   pt->Draw();
   line = new TLine(-0.005,0.0455,0.005,0.0455);
   line->SetLineStyle(3);
   line->Draw();
   
   pt = new TPaveText(0.65,0.6,0.875,0.725,"BRNDC");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(32);
   pt->SetTextFont(133);
   pt->SetTextSize(45);
   AText = pt->AddText("GammaCombo");
   pt->Draw();
   HGamEFTScanner_linear_marginal_cHWB_cHWBUID20->Modified();
   HGamEFTScanner_linear_marginal_cHWB_cHWBUID20->cd();
   HGamEFTScanner_linear_marginal_cHWB_cHWBUID20->SetSelected(HGamEFTScanner_linear_marginal_cHWB_cHWBUID20);
}
