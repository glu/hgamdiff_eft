void HGamEFTScanner_cHW:cHWt_cHWt_cHW()
{
//=========Macro generated from canvas: HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42/EFT Run 2 Scan of cHW:~cHW
//=========  (Fri Feb 15 18:18:16 2019) by ROOT version6.08/02
   TCanvas *HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42 = new TCanvas("HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42", "EFT Run 2 Scan of cHW:~cHW",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->SetHighLightColor(2);
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->Range(-0.7025316,-0.7179487,0.5632911,0.5641026);
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->SetFillColor(0);
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->SetBorderMode(0);
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->SetBorderSize(2);
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->SetLeftMargin(0.16);
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->SetRightMargin(0.05);
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->SetTopMargin(0.05);
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->SetBottomMargin(0.17);
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->SetFrameBorderMode(0);
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->SetFrameBorderMode(0);
   
   TH2F *haxesUID43__1 = new TH2F("haxesUID43__1","haxes",100,-0.5,0.5,100,-0.5,0.5);
   haxesUID43__1->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   haxesUID43__1->SetLineColor(ci);
   haxesUID43__1->GetXaxis()->SetTitle("#tilde{c}_{HW}");
   haxesUID43__1->GetXaxis()->SetNdivisions(0);
   haxesUID43__1->GetXaxis()->SetLabelFont(133);
   haxesUID43__1->GetXaxis()->SetLabelSize(35);
   haxesUID43__1->GetXaxis()->SetTitleSize(45);
   haxesUID43__1->GetXaxis()->SetTitleOffset(0.85);
   haxesUID43__1->GetXaxis()->SetTitleFont(133);
   haxesUID43__1->GetYaxis()->SetTitle("#bar{c}_{HW}");
   haxesUID43__1->GetYaxis()->SetNdivisions(0);
   haxesUID43__1->GetYaxis()->SetLabelFont(133);
   haxesUID43__1->GetYaxis()->SetLabelOffset(0.01);
   haxesUID43__1->GetYaxis()->SetLabelSize(35);
   haxesUID43__1->GetYaxis()->SetTitleSize(45);
   haxesUID43__1->GetYaxis()->SetTitleOffset(0.95);
   haxesUID43__1->GetYaxis()->SetTitleFont(133);
   haxesUID43__1->GetZaxis()->SetLabelFont(42);
   haxesUID43__1->GetZaxis()->SetLabelSize(0.035);
   haxesUID43__1->GetZaxis()->SetTitleSize(0.035);
   haxesUID43__1->GetZaxis()->SetTitleFont(42);
   haxesUID43__1->Draw("");
   
   Double_t Graph0_fx1[13] = {
   0.15,
   0.1600456,
   0.15,
   0.05939066,
   0.05,
   -0.05,
   -0.05919626,
   -0.15,
   -0.1600181,
   -0.15,
   -0.05,
   0.05,
   0.15};
   Double_t Graph0_fy1[13] = {
   0.05369751,
   0.05,
   0.0306033,
   -0.05,
   -0.05122208,
   -0.05115014,
   -0.05,
   0.03073292,
   0.05,
   0.05368972,
   0.06944382,
   0.06943817,
   0.05369751};
   TGraph *graph = new TGraph(13,Graph0_fx1,Graph0_fy1);
   graph->SetName("Graph0");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#d3e9ff");
   graph->SetFillColor(ci);
   
   TH1F *Graph_Graph1 = new TH1F("Graph_Graph1","Graph",100,-0.1920245,0.192052);
   Graph_Graph1->SetMinimum(-0.06328867);
   Graph_Graph1->SetMaximum(0.08151041);
   Graph_Graph1->SetDirectory(0);
   Graph_Graph1->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph1->SetLineColor(ci);
   Graph_Graph1->GetXaxis()->SetLabelFont(42);
   Graph_Graph1->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph1->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph1->GetXaxis()->SetTitleFont(42);
   Graph_Graph1->GetYaxis()->SetLabelFont(42);
   Graph_Graph1->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph1->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph1->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph1->GetYaxis()->SetTitleFont(42);
   Graph_Graph1->GetZaxis()->SetLabelFont(42);
   Graph_Graph1->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph1->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph1->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph1);
   
   graph->Draw("f");
   
   Double_t Graph1_fx2[13] = {
   0.15,
   0.1600456,
   0.15,
   0.05939066,
   0.05,
   -0.05,
   -0.05919626,
   -0.15,
   -0.1600181,
   -0.15,
   -0.05,
   0.05,
   0.15};
   Double_t Graph1_fy2[13] = {
   0.05369751,
   0.05,
   0.0306033,
   -0.05,
   -0.05122208,
   -0.05115014,
   -0.05,
   0.03073292,
   0.05,
   0.05368972,
   0.06944382,
   0.06943817,
   0.05369751};
   graph = new TGraph(13,Graph1_fx2,Graph1_fy2);
   graph->SetName("Graph1");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#e1bf4a");
   graph->SetFillColor(ci);
   graph->SetFillStyle(0);
   graph->SetLineColor(9);
   graph->SetLineWidth(2);
   
   TH1F *Graph_Graph2 = new TH1F("Graph_Graph2","Graph",100,-0.1920245,0.192052);
   Graph_Graph2->SetMinimum(-0.06328867);
   Graph_Graph2->SetMaximum(0.08151041);
   Graph_Graph2->SetDirectory(0);
   Graph_Graph2->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph2->SetLineColor(ci);
   Graph_Graph2->GetXaxis()->SetLabelFont(42);
   Graph_Graph2->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph2->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph2->GetXaxis()->SetTitleFont(42);
   Graph_Graph2->GetYaxis()->SetLabelFont(42);
   Graph_Graph2->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph2->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph2->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph2->GetYaxis()->SetTitleFont(42);
   Graph_Graph2->GetZaxis()->SetLabelFont(42);
   Graph_Graph2->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph2->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph2->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph2);
   
   graph->Draw("l");
   
   Double_t Graph2_fx3[7] = {
   0.08002492,
   0.05,
   -0.05,
   -0.08000522,
   -0.05,
   0.05,
   0.08002492};
   Double_t Graph2_fy3[7] = {
   0.05,
   0.02305812,
   0.02309592,
   0.05,
   0.05486082,
   0.05485954,
   0.05};
   graph = new TGraph(7,Graph2_fx3,Graph2_fy3);
   graph->SetName("Graph2");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#bee7fd");
   graph->SetFillColor(ci);
   
   TH1F *Graph_Graph3 = new TH1F("Graph_Graph3","Graph",100,-0.09600823,0.09602794);
   Graph_Graph3->SetMinimum(0.01987785);
   Graph_Graph3->SetMaximum(0.05804109);
   Graph_Graph3->SetDirectory(0);
   Graph_Graph3->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph3->SetLineColor(ci);
   Graph_Graph3->GetXaxis()->SetLabelFont(42);
   Graph_Graph3->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph3->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph3->GetXaxis()->SetTitleFont(42);
   Graph_Graph3->GetYaxis()->SetLabelFont(42);
   Graph_Graph3->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph3->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph3->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph3->GetYaxis()->SetTitleFont(42);
   Graph_Graph3->GetZaxis()->SetLabelFont(42);
   Graph_Graph3->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph3->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph3->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph3);
   
   graph->Draw("f");
   
   Double_t Graph3_fx4[7] = {
   0.08002492,
   0.05,
   -0.05,
   -0.08000522,
   -0.05,
   0.05,
   0.08002492};
   Double_t Graph3_fy4[7] = {
   0.05,
   0.02305812,
   0.02309592,
   0.05,
   0.05486082,
   0.05485954,
   0.05};
   graph = new TGraph(7,Graph3_fx4,Graph3_fy4);
   graph->SetName("Graph3");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#18aae7");
   graph->SetFillColor(ci);
   graph->SetFillStyle(3005);
   
   TH1F *Graph_Graph4 = new TH1F("Graph_Graph4","Graph",100,-0.09600823,0.09602794);
   Graph_Graph4->SetMinimum(0.01987785);
   Graph_Graph4->SetMaximum(0.05804109);
   Graph_Graph4->SetDirectory(0);
   Graph_Graph4->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph4->SetLineColor(ci);
   Graph_Graph4->GetXaxis()->SetLabelFont(42);
   Graph_Graph4->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph4->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph4->GetXaxis()->SetTitleFont(42);
   Graph_Graph4->GetYaxis()->SetLabelFont(42);
   Graph_Graph4->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph4->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph4->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph4->GetYaxis()->SetTitleFont(42);
   Graph_Graph4->GetZaxis()->SetLabelFont(42);
   Graph_Graph4->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph4->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph4->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph4);
   
   graph->Draw("f");
   
   Double_t Graph4_fx5[7] = {
   0.08002492,
   0.05,
   -0.05,
   -0.08000522,
   -0.05,
   0.05,
   0.08002492};
   Double_t Graph4_fy5[7] = {
   0.05,
   0.02305812,
   0.02309592,
   0.05,
   0.05486082,
   0.05485954,
   0.05};
   graph = new TGraph(7,Graph4_fx5,Graph4_fy5);
   graph->SetName("Graph4");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#f9f90e");
   graph->SetFillColor(ci);
   graph->SetFillStyle(0);

   ci = TColor::GetColor("#18aae7");
   graph->SetLineColor(ci);
   graph->SetLineWidth(2);
   
   TH1F *Graph_Graph5 = new TH1F("Graph_Graph5","Graph",100,-0.09600823,0.09602794);
   Graph_Graph5->SetMinimum(0.01987785);
   Graph_Graph5->SetMaximum(0.05804109);
   Graph_Graph5->SetDirectory(0);
   Graph_Graph5->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph5->SetLineColor(ci);
   Graph_Graph5->GetXaxis()->SetLabelFont(42);
   Graph_Graph5->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph5->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph5->GetXaxis()->SetTitleFont(42);
   Graph_Graph5->GetYaxis()->SetLabelFont(42);
   Graph_Graph5->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph5->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph5->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph5->GetYaxis()->SetTitleFont(42);
   Graph_Graph5->GetZaxis()->SetLabelFont(42);
   Graph_Graph5->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph5->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph5->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph5);
   
   graph->Draw("l");
   
   Double_t Graph5_fx6[13] = {
   0.15,
   0.1600456,
   0.15,
   0.05939066,
   0.05,
   -0.05,
   -0.05919626,
   -0.15,
   -0.1600181,
   -0.15,
   -0.05,
   0.05,
   0.15};
   Double_t Graph5_fy6[13] = {
   0.05369751,
   0.05,
   0.0306033,
   -0.05,
   -0.05122208,
   -0.05115014,
   -0.05,
   0.03073292,
   0.05,
   0.05368972,
   0.06944382,
   0.06943817,
   0.05369751};
   graph = new TGraph(13,Graph5_fx6,Graph5_fy6);
   graph->SetName("Graph5");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#e1bf4a");
   graph->SetFillColor(ci);
   graph->SetFillStyle(0);
   graph->SetLineColor(9);
   graph->SetLineStyle(2);
   graph->SetLineWidth(2);
   
   TH1F *Graph_Graph6 = new TH1F("Graph_Graph6","Graph",100,-0.1920245,0.192052);
   Graph_Graph6->SetMinimum(-0.06328867);
   Graph_Graph6->SetMaximum(0.08151041);
   Graph_Graph6->SetDirectory(0);
   Graph_Graph6->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph6->SetLineColor(ci);
   Graph_Graph6->GetXaxis()->SetLabelFont(42);
   Graph_Graph6->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph6->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph6->GetXaxis()->SetTitleFont(42);
   Graph_Graph6->GetYaxis()->SetLabelFont(42);
   Graph_Graph6->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph6->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph6->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph6->GetYaxis()->SetTitleFont(42);
   Graph_Graph6->GetZaxis()->SetLabelFont(42);
   Graph_Graph6->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph6->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph6->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph6);
   
   graph->Draw("l");
   
   Double_t Graph6_fx7[7] = {
   0.08002492,
   0.05,
   -0.05,
   -0.08000522,
   -0.05,
   0.05,
   0.08002492};
   Double_t Graph6_fy7[7] = {
   0.05,
   0.02305812,
   0.02309592,
   0.05,
   0.05486082,
   0.05485954,
   0.05};
   graph = new TGraph(7,Graph6_fx7,Graph6_fy7);
   graph->SetName("Graph6");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#f9f90e");
   graph->SetFillColor(ci);
   graph->SetFillStyle(0);

   ci = TColor::GetColor("#18aae7");
   graph->SetLineColor(ci);
   graph->SetLineStyle(2);
   graph->SetLineWidth(2);
   
   TH1F *Graph_Graph7 = new TH1F("Graph_Graph7","Graph",100,-0.09600823,0.09602794);
   Graph_Graph7->SetMinimum(0.01987785);
   Graph_Graph7->SetMaximum(0.05804109);
   Graph_Graph7->SetDirectory(0);
   Graph_Graph7->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph7->SetLineColor(ci);
   Graph_Graph7->GetXaxis()->SetLabelFont(42);
   Graph_Graph7->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph7->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph7->GetXaxis()->SetTitleFont(42);
   Graph_Graph7->GetYaxis()->SetLabelFont(42);
   Graph_Graph7->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph7->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph7->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph7->GetYaxis()->SetTitleFont(42);
   Graph_Graph7->GetZaxis()->SetLabelFont(42);
   Graph_Graph7->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph7->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph7->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph7);
   
   graph->Draw("l");
   TGaxis *gaxis = new TGaxis(-0.5,0.5,0.5,0.5,-0.5,0.5,407,"-U");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0.04);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axist");
   gaxis->Draw();
   gaxis = new TGaxis(-0.5,-0.5,0.5,-0.5,-0.5,0.5,407,"");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(35);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisb");
   gaxis->SetLabelFont(133);
   gaxis->Draw();
   gaxis = new TGaxis(-0.5,-0.5,-0.5,0.5,-0.5,0.5,407,"");
   gaxis->SetLabelOffset(0.01);
   gaxis->SetLabelSize(35);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisl");
   gaxis->SetLabelFont(133);
   gaxis->Draw();
   gaxis = new TGaxis(0.5,-0.5,0.5,0.5,-0.5,0.5,407,"+U");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0.04);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisr");
   gaxis->Draw();
   
   TLegend *leg = new TLegend(0.17,0.75,0.55,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(133);
   leg->SetTextSize(29);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","EFT Run 2 Scan of cHW:~cHW","");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(133);
   leg->Draw();
   
   TPaveText *pt = new TPaveText(0.65,0.775,0.875,0.9,"BRNDC");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(32);
   pt->SetTextFont(133);
   pt->SetTextSize(45);
   TText *AText = pt->AddText("GammaCombo");
   pt->Draw();
   
   pt = new TPaveText(0.17,0.15,0.37,0.275,"BRNDC");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);

   ci = TColor::GetColor("#999999");
   pt->SetTextColor(ci);
   pt->SetTextFont(133);
   pt->SetTextSize(21);
   AText = pt->AddText("contours hold 39%, 87% CL");
   pt->Draw();
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->Modified();
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->cd();
   HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42->SetSelected(HGamEFTScanner_cHW:cHWt_cHWt_cHWUID42);
}
