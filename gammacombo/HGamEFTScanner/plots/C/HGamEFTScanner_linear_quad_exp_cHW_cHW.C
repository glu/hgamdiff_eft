void HGamEFTScanner_linear_quad_exp_cHW_cHW()
{
//=========Macro generated from canvas: HGamEFTScanner_linear_quad_exp_cHW_cHWUID20/p-value curves
//=========  (Thu Jul 22 05:54:41 2021) by ROOT version6.08/02
   TCanvas *HGamEFTScanner_linear_quad_exp_cHW_cHWUID20 = new TCanvas("HGamEFTScanner_linear_quad_exp_cHW_cHWUID20", "p-value curves",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   HGamEFTScanner_linear_quad_exp_cHW_cHWUID20->SetHighLightColor(2);
   HGamEFTScanner_linear_quad_exp_cHW_cHWUID20->Range(-0.002864865,-0.2833333,0.002540541,1.383333);
   HGamEFTScanner_linear_quad_exp_cHW_cHWUID20->SetFillColor(0);
   HGamEFTScanner_linear_quad_exp_cHW_cHWUID20->SetBorderMode(0);
   HGamEFTScanner_linear_quad_exp_cHW_cHWUID20->SetBorderSize(2);
   HGamEFTScanner_linear_quad_exp_cHW_cHWUID20->SetLeftMargin(0.16);
   HGamEFTScanner_linear_quad_exp_cHW_cHWUID20->SetTopMargin(0.05);
   HGamEFTScanner_linear_quad_exp_cHW_cHWUID20->SetBottomMargin(0.17);
   HGamEFTScanner_linear_quad_exp_cHW_cHWUID20->SetFrameBorderMode(0);
   HGamEFTScanner_linear_quad_exp_cHW_cHWUID20->SetFrameBorderMode(0);
   
   Double_t UID25_fx1[155] = {
   -0.002,
   -0.002,
   -0.001986667,
   -0.00196,
   -0.001933333,
   -0.001906667,
   -0.00188,
   -0.001853333,
   -0.001826667,
   -0.0018,
   -0.001773333,
   -0.001746667,
   -0.00172,
   -0.001693333,
   -0.001666667,
   -0.00164,
   -0.001613333,
   -0.001586667,
   -0.00156,
   -0.001533333,
   -0.001506667,
   -0.00148,
   -0.001453333,
   -0.001426667,
   -0.0014,
   -0.001373333,
   -0.001346667,
   -0.00132,
   -0.001293333,
   -0.001266667,
   -0.00124,
   -0.001213333,
   -0.001186667,
   -0.00116,
   -0.001133333,
   -0.001106667,
   -0.00108,
   -0.001053333,
   -0.001026667,
   -0.001,
   -0.0009733334,
   -0.0009466667,
   -0.00092,
   -0.0008933334,
   -0.0008666667,
   -0.0008400001,
   -0.0008133334,
   -0.0007866667,
   -0.00076,
   -0.0007333333,
   -0.0007066667,
   -0.0006800001,
   -0.0006533334,
   -0.0006266667,
   -0.0006,
   -0.0005733334,
   -0.0005466667,
   -0.00052,
   -0.0004933334,
   -0.0004666667,
   -0.00044,
   -0.0004133334,
   -0.0003866667,
   -0.00036,
   -0.0003333334,
   -0.0003066667,
   -0.00028,
   -0.0002533333,
   -0.0002266667,
   -0.0002,
   -0.0001733333,
   -0.0001466667,
   -0.00012,
   -9.333334e-05,
   -6.666667e-05,
   -4e-05,
   -1.333333e-05,
   -2.078731e-08,
   1.333333e-05,
   4e-05,
   6.666667e-05,
   9.333334e-05,
   0.00012,
   0.0001466667,
   0.0001733333,
   0.0002,
   0.0002266667,
   0.0002533333,
   0.00028,
   0.0003066667,
   0.0003333334,
   0.00036,
   0.0003866667,
   0.0004133334,
   0.00044,
   0.0004666667,
   0.0004933334,
   0.00052,
   0.0005466667,
   0.0005733334,
   0.0006,
   0.0006266667,
   0.0006533334,
   0.0006800001,
   0.0007066667,
   0.0007333333,
   0.00076,
   0.0007866667,
   0.0008133334,
   0.0008400001,
   0.0008666667,
   0.0008933334,
   0.00092,
   0.0009466667,
   0.0009733334,
   0.001,
   0.001026667,
   0.001053333,
   0.00108,
   0.001106667,
   0.001133333,
   0.00116,
   0.001186667,
   0.001213333,
   0.00124,
   0.001266667,
   0.001293333,
   0.00132,
   0.001346667,
   0.001373333,
   0.0014,
   0.001426667,
   0.001453333,
   0.00148,
   0.001506667,
   0.001533333,
   0.00156,
   0.001586667,
   0.001613333,
   0.00164,
   0.001666667,
   0.001693333,
   0.00172,
   0.001746667,
   0.001773333,
   0.0018,
   0.001826667,
   0.001853333,
   0.00188,
   0.001906667,
   0.001933333,
   0.00196,
   0.001986667,
   0.002,
   0.002};
   Double_t UID25_fy1[155] = {
   0,
   3.225574e-08,
   3.225574e-08,
   5.121773e-08,
   8.070975e-08,
   1.262263e-07,
   1.959358e-07,
   3.018845e-07,
   4.61694e-07,
   7.009335e-07,
   1.056408e-06,
   1.580673e-06,
   2.348177e-06,
   3.463546e-06,
   5.072659e-06,
   7.37731e-06,
   1.065445e-05,
   1.52812e-05,
   2.176715e-05,
   3.07954e-05,
   4.327466e-05,
   6.040436e-05,
   8.375542e-05,
   0.0001153695,
   0.0001578795,
   0.000214655,
   0.0002899746,
   0.00038923,
   0.0005191629,
   0.0006881383,
   0.0009064552,
   0.001186696,
   0.00154411,
   0.001997047,
   0.002567396,
   0.003281076,
   0.004168532,
   0.005265237,
   0.006612189,
   0.008256386,
   0.01025126,
   0.01265702,
   0.015541,
   0.01897774,
   0.0230491,
   0.02784412,
   0.03345868,
   0.03999507,
   0.04756121,
   0.05626975,
   0.06623687,
   0.07758085,
   0.09042054,
   0.1048734,
   0.1210535,
   0.1390696,
   0.1590224,
   0.1810029,
   0.2050892,
   0.2313455,
   0.2598186,
   0.2905371,
   0.3235089,
   0.3587205,
   0.3961353,
   0.4356937,
   0.4773121,
   0.5208839,
   0.5662796,
   0.6133485,
   0.6619195,
   0.7118043,
   0.7627983,
   0.8146846,
   0.8672359,
   0.9202183,
   0.9733942,
   1,
   0.9734746,
   0.9206235,
   0.868282,
   0.8166708,
   0.7659991,
   0.7164625,
   0.6682404,
   0.6214949,
   0.5763689,
   0.5329854,
   0.4914471,
   0.4518358,
   0.4142132,
   0.3786206,
   0.3450804,
   0.3135968,
   0.2841571,
   0.2567333,
   0.2312833,
   0.207753,
   0.1860778,
   0.1661841,
   0.1479914,
   0.1314137,
   0.1163609,
   0.1027405,
   0.09045878,
   0.07942203,
   0.06953759,
   0.06071473,
   0.05286548,
   0.04590523,
   0.03975324,
   0.03433302,
   0.02957263,
   0.02540477,
   0.02176696,
   0.01860146,
   0.01585525,
   0.0134799,
   0.01143135,
   0.009669775,
   0.008159322,
   0.00686786,
   0.005766729,
   0.004830474,
   0.004036573,
   0.003365195,
   0.002798941,
   0.002322602,
   0.001922937,
   0.00158846,
   0.001309244,
   0.001076736,
   0.0008835985,
   0.0007235513,
   0.000591241,
   0.0004821173,
   0.0003923251,
   0.0003186082,
   0.0002582247,
   0.0002088719,
   0.000168623,
   0.0001358689,
   0.0001092703,
   8.771538e-05,
   7.028345e-05,
   5.621427e-05,
   4.488164e-05,
   3.577106e-05,
   2.846088e-05,
   2.260637e-05,
   1.792634e-05,
   1.419199e-05,
   1.121754e-05,
   1.121754e-05,
   0};
   TGraph *graph = new TGraph(155,UID25_fx1,UID25_fy1);
   graph->SetName("UID25");
   graph->SetTitle("Graph");

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#9999cc");
   graph->SetFillColor(ci);

   ci = TColor::GetColor("#9999cc");
   graph->SetLineColor(ci);
   graph->SetLineWidth(2);
   
   TH1F *Graph_haxesUID261 = new TH1F("Graph_haxesUID261","",100,-0.002,0.002);
   Graph_haxesUID261->SetMinimum(0);
   Graph_haxesUID261->SetMaximum(1.3);
   Graph_haxesUID261->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID261->SetLineColor(ci);
   Graph_haxesUID261->GetXaxis()->SetTitle("#it{#bar{C}_{HW}}");
   Graph_haxesUID261->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID261->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID261->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID261->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID261->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID261->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID261->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID261->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID261->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID261->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID261->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID261->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID261->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID261->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID261->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID261->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID261->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID261->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_haxesUID261);
   
   graph->Draw(" f a");
   
   TH1F *haxesUID32__1 = new TH1F("haxesUID32__1","",100,-0.002,0.002);
   haxesUID32__1->SetMinimum(0);
   haxesUID32__1->SetMaximum(1.3);
   haxesUID32__1->SetStats(0);

   ci = TColor::GetColor("#000099");
   haxesUID32__1->SetLineColor(ci);
   haxesUID32__1->GetXaxis()->SetTitle("#it{#bar{C}_{HW}}");
   haxesUID32__1->GetXaxis()->SetNdivisions(407);
   haxesUID32__1->GetXaxis()->SetLabelFont(133);
   haxesUID32__1->GetXaxis()->SetLabelSize(35);
   haxesUID32__1->GetXaxis()->SetTitleSize(45);
   haxesUID32__1->GetXaxis()->SetTitleOffset(0.9);
   haxesUID32__1->GetXaxis()->SetTitleFont(133);
   haxesUID32__1->GetYaxis()->SetTitle("1-CL");
   haxesUID32__1->GetYaxis()->SetNdivisions(407);
   haxesUID32__1->GetYaxis()->SetLabelFont(133);
   haxesUID32__1->GetYaxis()->SetLabelOffset(0.01);
   haxesUID32__1->GetYaxis()->SetLabelSize(35);
   haxesUID32__1->GetYaxis()->SetTitleSize(45);
   haxesUID32__1->GetYaxis()->SetTitleOffset(0.85);
   haxesUID32__1->GetYaxis()->SetTitleFont(133);
   haxesUID32__1->GetZaxis()->SetLabelFont(42);
   haxesUID32__1->GetZaxis()->SetLabelSize(0.035);
   haxesUID32__1->GetZaxis()->SetTitleSize(0.035);
   haxesUID32__1->GetZaxis()->SetTitleFont(42);
   haxesUID32__1->Draw("axissame");
   
   Double_t UID31_fx2[155] = {
   -0.002,
   -0.002,
   -0.001986667,
   -0.00196,
   -0.001933333,
   -0.001906667,
   -0.00188,
   -0.001853333,
   -0.001826667,
   -0.0018,
   -0.001773333,
   -0.001746667,
   -0.00172,
   -0.001693333,
   -0.001666667,
   -0.00164,
   -0.001613333,
   -0.001586667,
   -0.00156,
   -0.001533333,
   -0.001506667,
   -0.00148,
   -0.001453333,
   -0.001426667,
   -0.0014,
   -0.001373333,
   -0.001346667,
   -0.00132,
   -0.001293333,
   -0.001266667,
   -0.00124,
   -0.001213333,
   -0.001186667,
   -0.00116,
   -0.001133333,
   -0.001106667,
   -0.00108,
   -0.001053333,
   -0.001026667,
   -0.001,
   -0.0009733334,
   -0.0009466667,
   -0.00092,
   -0.0008933334,
   -0.0008666667,
   -0.0008400001,
   -0.0008133334,
   -0.0007866667,
   -0.00076,
   -0.0007333333,
   -0.0007066667,
   -0.0006800001,
   -0.0006533334,
   -0.0006266667,
   -0.0006,
   -0.0005733334,
   -0.0005466667,
   -0.00052,
   -0.0004933334,
   -0.0004666667,
   -0.00044,
   -0.0004133334,
   -0.0003866667,
   -0.00036,
   -0.0003333334,
   -0.0003066667,
   -0.00028,
   -0.0002533333,
   -0.0002266667,
   -0.0002,
   -0.0001733333,
   -0.0001466667,
   -0.00012,
   -9.333334e-05,
   -6.666667e-05,
   -4e-05,
   -1.333333e-05,
   -2.078731e-08,
   1.333333e-05,
   4e-05,
   6.666667e-05,
   9.333334e-05,
   0.00012,
   0.0001466667,
   0.0001733333,
   0.0002,
   0.0002266667,
   0.0002533333,
   0.00028,
   0.0003066667,
   0.0003333334,
   0.00036,
   0.0003866667,
   0.0004133334,
   0.00044,
   0.0004666667,
   0.0004933334,
   0.00052,
   0.0005466667,
   0.0005733334,
   0.0006,
   0.0006266667,
   0.0006533334,
   0.0006800001,
   0.0007066667,
   0.0007333333,
   0.00076,
   0.0007866667,
   0.0008133334,
   0.0008400001,
   0.0008666667,
   0.0008933334,
   0.00092,
   0.0009466667,
   0.0009733334,
   0.001,
   0.001026667,
   0.001053333,
   0.00108,
   0.001106667,
   0.001133333,
   0.00116,
   0.001186667,
   0.001213333,
   0.00124,
   0.001266667,
   0.001293333,
   0.00132,
   0.001346667,
   0.001373333,
   0.0014,
   0.001426667,
   0.001453333,
   0.00148,
   0.001506667,
   0.001533333,
   0.00156,
   0.001586667,
   0.001613333,
   0.00164,
   0.001666667,
   0.001693333,
   0.00172,
   0.001746667,
   0.001773333,
   0.0018,
   0.001826667,
   0.001853333,
   0.00188,
   0.001906667,
   0.001933333,
   0.00196,
   0.001986667,
   0.002,
   0.002};
   Double_t UID31_fy2[155] = {
   0,
   3.225574e-08,
   3.225574e-08,
   5.121773e-08,
   8.070975e-08,
   1.262263e-07,
   1.959358e-07,
   3.018845e-07,
   4.61694e-07,
   7.009335e-07,
   1.056408e-06,
   1.580673e-06,
   2.348177e-06,
   3.463546e-06,
   5.072659e-06,
   7.37731e-06,
   1.065445e-05,
   1.52812e-05,
   2.176715e-05,
   3.07954e-05,
   4.327466e-05,
   6.040436e-05,
   8.375542e-05,
   0.0001153695,
   0.0001578795,
   0.000214655,
   0.0002899746,
   0.00038923,
   0.0005191629,
   0.0006881383,
   0.0009064552,
   0.001186696,
   0.00154411,
   0.001997047,
   0.002567396,
   0.003281076,
   0.004168532,
   0.005265237,
   0.006612189,
   0.008256386,
   0.01025126,
   0.01265702,
   0.015541,
   0.01897774,
   0.0230491,
   0.02784412,
   0.03345868,
   0.03999507,
   0.04756121,
   0.05626975,
   0.06623687,
   0.07758085,
   0.09042054,
   0.1048734,
   0.1210535,
   0.1390696,
   0.1590224,
   0.1810029,
   0.2050892,
   0.2313455,
   0.2598186,
   0.2905371,
   0.3235089,
   0.3587205,
   0.3961353,
   0.4356937,
   0.4773121,
   0.5208839,
   0.5662796,
   0.6133485,
   0.6619195,
   0.7118043,
   0.7627983,
   0.8146846,
   0.8672359,
   0.9202183,
   0.9733942,
   1,
   0.9734746,
   0.9206235,
   0.868282,
   0.8166708,
   0.7659991,
   0.7164625,
   0.6682404,
   0.6214949,
   0.5763689,
   0.5329854,
   0.4914471,
   0.4518358,
   0.4142132,
   0.3786206,
   0.3450804,
   0.3135968,
   0.2841571,
   0.2567333,
   0.2312833,
   0.207753,
   0.1860778,
   0.1661841,
   0.1479914,
   0.1314137,
   0.1163609,
   0.1027405,
   0.09045878,
   0.07942203,
   0.06953759,
   0.06071473,
   0.05286548,
   0.04590523,
   0.03975324,
   0.03433302,
   0.02957263,
   0.02540477,
   0.02176696,
   0.01860146,
   0.01585525,
   0.0134799,
   0.01143135,
   0.009669775,
   0.008159322,
   0.00686786,
   0.005766729,
   0.004830474,
   0.004036573,
   0.003365195,
   0.002798941,
   0.002322602,
   0.001922937,
   0.00158846,
   0.001309244,
   0.001076736,
   0.0008835985,
   0.0007235513,
   0.000591241,
   0.0004821173,
   0.0003923251,
   0.0003186082,
   0.0002582247,
   0.0002088719,
   0.000168623,
   0.0001358689,
   0.0001092703,
   8.771538e-05,
   7.028345e-05,
   5.621427e-05,
   4.488164e-05,
   3.577106e-05,
   2.846088e-05,
   2.260637e-05,
   1.792634e-05,
   1.419199e-05,
   1.121754e-05,
   1.121754e-05,
   0};
   graph = new TGraph(155,UID31_fx2,UID31_fy2);
   graph->SetName("UID31");
   graph->SetTitle("Graph");
   graph->SetFillColor(1);

   ci = TColor::GetColor("#9999cc");
   graph->SetLineColor(ci);
   graph->SetLineStyle(0);
   graph->SetLineWidth(2);
   
   TH1F *Graph_haxesUID322 = new TH1F("Graph_haxesUID322","",100,-0.002,0.002);
   Graph_haxesUID322->SetMinimum(0);
   Graph_haxesUID322->SetMaximum(1.3);
   Graph_haxesUID322->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID322->SetLineColor(ci);
   Graph_haxesUID322->GetXaxis()->SetTitle("#it{#bar{C}_{HW}}");
   Graph_haxesUID322->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID322->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID322->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID322->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID322->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID322->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_haxesUID322);
   
   graph->Draw(" l");
   TGaxis *gaxis = new TGaxis(-0.002,1,0.002,1,-0.002,0.002,407,"-U");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0.04);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axist");
   gaxis->Draw();
   gaxis = new TGaxis(0.002,0,0.002,1.3,0,1.3,407,"+");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(0);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisr");
   gaxis->SetLabelColor(0);
   gaxis->Draw();
   
   TH1F *Graph_haxesUID322 = new TH1F("Graph_haxesUID322","",100,-0.002,0.002);
   Graph_haxesUID322->SetMinimum(0);
   Graph_haxesUID322->SetMaximum(1.3);
   Graph_haxesUID322->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID322->SetLineColor(ci);
   Graph_haxesUID322->GetXaxis()->SetTitle("#it{#bar{C}_{HW}}");
   Graph_haxesUID322->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID322->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID322->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID322->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID322->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID322->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleFont(42);
   Graph_haxesUID322->Draw("axissame");
   TLine *line = new TLine(-2.078731e-08,0,-2.078731e-08,1);
   line->Draw();
   line = new TLine(-0.000391551,0,-0.000391551,1);
   line->SetLineStyle(2);
   line->Draw();
   line = new TLine(0.0004101024,0,0.0004101024,1);
   line->SetLineStyle(2);
   line->Draw();
   
   TPaveText *pt = new TPaveText(-2.078731e-08,0.6,0.0007999793,0.7,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(13);
   pt->SetTextFont(133);
   TText *AText = pt->AddText("-0.00000^{+0.00041}_{#font[122]{-}0.00039}");
   AText->SetTextSize(35);
   pt->Draw();
   
   TLegend *leg = new TLegend(0.19,0.78,0.5,0.9440559,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(133);
   leg->SetTextSize(21.75);
   leg->SetLineColor(0);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("UID25","SMEFT Run 2 Scan of cHW (Prob)","lf");

   ci = TColor::GetColor("#9999cc");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#9999cc");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(133);
   leg->Draw();
   
   pt = new TPaveText(-0.0016,0.3373,0.0004,0.3873,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextFont(133);
   pt->SetTextSize(35);
   AText = pt->AddText("68.3%");
   pt->Draw();
   line = new TLine(-0.002,0.3173,0.002,0.3173);
   line->SetLineStyle(3);
   line->Draw();
   
   pt = new TPaveText(-0.0016,0.0655,0.0004,0.1155,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextFont(133);
   pt->SetTextSize(35);
   AText = pt->AddText("95.5%");
   pt->Draw();
   line = new TLine(-0.002,0.0455,0.002,0.0455);
   line->SetLineStyle(3);
   line->Draw();
   
   pt = new TPaveText(0.65,0.6,0.875,0.725,"BRNDC");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(32);
   pt->SetTextFont(133);
   pt->SetTextSize(45);
   AText = pt->AddText("GammaCombo");
   pt->Draw();
   HGamEFTScanner_linear_quad_exp_cHW_cHWUID20->Modified();
   HGamEFTScanner_linear_quad_exp_cHW_cHWUID20->cd();
   HGamEFTScanner_linear_quad_exp_cHW_cHWUID20->SetSelected(HGamEFTScanner_linear_quad_exp_cHW_cHWUID20);
}
