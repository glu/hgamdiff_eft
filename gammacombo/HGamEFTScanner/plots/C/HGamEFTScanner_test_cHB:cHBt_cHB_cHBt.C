void HGamEFTScanner_test_cHB:cHBt_cHB_cHBt()
{
//=========Macro generated from canvas: HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38/SMEFT Run2
//=========  (Fri Jul  9 11:38:53 2021) by ROOT version6.08/02
   TCanvas *HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38 = new TCanvas("HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38", "SMEFT Run2",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->SetHighLightColor(2);
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->Range(-0.0005620253,-143.5897,0.0004506329,112.8205);
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->SetFillColor(0);
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->SetBorderMode(0);
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->SetBorderSize(2);
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->SetLeftMargin(0.16);
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->SetRightMargin(0.05);
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->SetTopMargin(0.05);
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->SetBottomMargin(0.17);
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->SetFrameBorderMode(0);
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->SetFrameBorderMode(0);
   
   TH2F *haxesUID39__1 = new TH2F("haxesUID39__1","haxes",100,-0.0004,0.0004,100,-100,100);
   haxesUID39__1->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   haxesUID39__1->SetLineColor(ci);
   haxesUID39__1->GetXaxis()->SetTitle("#it{#bar{C}_{HB}}");
   haxesUID39__1->GetXaxis()->SetNdivisions(0);
   haxesUID39__1->GetXaxis()->SetLabelFont(133);
   haxesUID39__1->GetXaxis()->SetLabelSize(35);
   haxesUID39__1->GetXaxis()->SetTitleSize(45);
   haxesUID39__1->GetXaxis()->SetTitleOffset(0.85);
   haxesUID39__1->GetXaxis()->SetTitleFont(133);
   haxesUID39__1->GetYaxis()->SetTitle("#it{#tilde{C}_{HB}}");
   haxesUID39__1->GetYaxis()->SetNdivisions(0);
   haxesUID39__1->GetYaxis()->SetLabelFont(133);
   haxesUID39__1->GetYaxis()->SetLabelOffset(0.01);
   haxesUID39__1->GetYaxis()->SetLabelSize(35);
   haxesUID39__1->GetYaxis()->SetTitleSize(45);
   haxesUID39__1->GetYaxis()->SetTitleOffset(0.95);
   haxesUID39__1->GetYaxis()->SetTitleFont(133);
   haxesUID39__1->GetZaxis()->SetLabelFont(42);
   haxesUID39__1->GetZaxis()->SetLabelSize(0.035);
   haxesUID39__1->GetZaxis()->SetTitleSize(0.035);
   haxesUID39__1->GetZaxis()->SetTitleFont(42);
   haxesUID39__1->Draw("");
   
   Double_t Graph0_fx1[5] = {
   -0.0004,
   -0.0004,
   0.0004,
   0.0004,
   -0.0004};
   Double_t Graph0_fy1[5] = {
   -100,
   100,
   100,
   -100,
   -100};
   TGraph *graph = new TGraph(5,Graph0_fx1,Graph0_fy1);
   graph->SetName("Graph0");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#bee7fd");
   graph->SetFillColor(ci);
   
   TH1F *Graph_Graph1 = new TH1F("Graph_Graph1","Graph",100,-0.00048,0.00048);
   Graph_Graph1->SetMinimum(-120);
   Graph_Graph1->SetMaximum(120);
   Graph_Graph1->SetDirectory(0);
   Graph_Graph1->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph1->SetLineColor(ci);
   Graph_Graph1->GetXaxis()->SetLabelFont(42);
   Graph_Graph1->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph1->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph1->GetXaxis()->SetTitleFont(42);
   Graph_Graph1->GetYaxis()->SetLabelFont(42);
   Graph_Graph1->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph1->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph1->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph1->GetYaxis()->SetTitleFont(42);
   Graph_Graph1->GetZaxis()->SetLabelFont(42);
   Graph_Graph1->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph1->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph1->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph1);
   
   graph->Draw("f");
   
   Double_t Graph1_fx2[5] = {
   -0.0004,
   -0.0004,
   0.0004,
   0.0004,
   -0.0004};
   Double_t Graph1_fy2[5] = {
   -100,
   100,
   100,
   -100,
   -100};
   graph = new TGraph(5,Graph1_fx2,Graph1_fy2);
   graph->SetName("Graph1");
   graph->SetTitle("Graph");

   ci = TColor::GetColor("#18aae7");
   graph->SetFillColor(ci);
   graph->SetFillStyle(3005);
   
   TH1F *Graph_Graph2 = new TH1F("Graph_Graph2","Graph",100,-0.00048,0.00048);
   Graph_Graph2->SetMinimum(-120);
   Graph_Graph2->SetMaximum(120);
   Graph_Graph2->SetDirectory(0);
   Graph_Graph2->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph2->SetLineColor(ci);
   Graph_Graph2->GetXaxis()->SetLabelFont(42);
   Graph_Graph2->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph2->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph2->GetXaxis()->SetTitleFont(42);
   Graph_Graph2->GetYaxis()->SetLabelFont(42);
   Graph_Graph2->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph2->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph2->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph2->GetYaxis()->SetTitleFont(42);
   Graph_Graph2->GetZaxis()->SetLabelFont(42);
   Graph_Graph2->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph2->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph2->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph2);
   
   graph->Draw("f");
   
   Double_t Graph2_fx3[5] = {
   -0.0004,
   -0.0004,
   0.0004,
   0.0004,
   -0.0004};
   Double_t Graph2_fy3[5] = {
   -100,
   100,
   100,
   -100,
   -100};
   graph = new TGraph(5,Graph2_fx3,Graph2_fy3);
   graph->SetName("Graph2");
   graph->SetTitle("Graph");
   graph->SetFillColor(1);
   graph->SetFillStyle(0);

   ci = TColor::GetColor("#18aae7");
   graph->SetLineColor(ci);
   graph->SetLineWidth(2);
   
   TH1F *Graph_Graph3 = new TH1F("Graph_Graph3","Graph",100,-0.00048,0.00048);
   Graph_Graph3->SetMinimum(-120);
   Graph_Graph3->SetMaximum(120);
   Graph_Graph3->SetDirectory(0);
   Graph_Graph3->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph3->SetLineColor(ci);
   Graph_Graph3->GetXaxis()->SetLabelFont(42);
   Graph_Graph3->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph3->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph3->GetXaxis()->SetTitleFont(42);
   Graph_Graph3->GetYaxis()->SetLabelFont(42);
   Graph_Graph3->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph3->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph3->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph3->GetYaxis()->SetTitleFont(42);
   Graph_Graph3->GetZaxis()->SetLabelFont(42);
   Graph_Graph3->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph3->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph3->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph3);
   
   graph->Draw("l");
   
   Double_t Graph3_fx4[5] = {
   -0.0004,
   -0.0004,
   0.0004,
   0.0004,
   -0.0004};
   Double_t Graph3_fy4[5] = {
   -100,
   100,
   100,
   -100,
   -100};
   graph = new TGraph(5,Graph3_fx4,Graph3_fy4);
   graph->SetName("Graph3");
   graph->SetTitle("Graph");
   graph->SetFillColor(1);
   graph->SetFillStyle(0);

   ci = TColor::GetColor("#18aae7");
   graph->SetLineColor(ci);
   graph->SetLineStyle(2);
   graph->SetLineWidth(2);
   
   TH1F *Graph_Graph4 = new TH1F("Graph_Graph4","Graph",100,-0.00048,0.00048);
   Graph_Graph4->SetMinimum(-120);
   Graph_Graph4->SetMaximum(120);
   Graph_Graph4->SetDirectory(0);
   Graph_Graph4->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_Graph4->SetLineColor(ci);
   Graph_Graph4->GetXaxis()->SetLabelFont(42);
   Graph_Graph4->GetXaxis()->SetLabelSize(0.035);
   Graph_Graph4->GetXaxis()->SetTitleSize(0.035);
   Graph_Graph4->GetXaxis()->SetTitleFont(42);
   Graph_Graph4->GetYaxis()->SetLabelFont(42);
   Graph_Graph4->GetYaxis()->SetLabelOffset(0.01);
   Graph_Graph4->GetYaxis()->SetLabelSize(0.035);
   Graph_Graph4->GetYaxis()->SetTitleSize(0.035);
   Graph_Graph4->GetYaxis()->SetTitleFont(42);
   Graph_Graph4->GetZaxis()->SetLabelFont(42);
   Graph_Graph4->GetZaxis()->SetLabelSize(0.035);
   Graph_Graph4->GetZaxis()->SetTitleSize(0.035);
   Graph_Graph4->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_Graph4);
   
   graph->Draw("l");
   TGaxis *gaxis = new TGaxis(-0.0004,100,0.0004,100,-0.0004,0.0004,407,"-U");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0.04);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axist");
   gaxis->Draw();
   gaxis = new TGaxis(-0.0004,-100,0.0004,-100,-0.0004,0.0004,407,"");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(35);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisb");
   gaxis->SetLabelFont(133);
   gaxis->Draw();
   gaxis = new TGaxis(-0.0004,-100,-0.0004,100,-100,100,407,"");
   gaxis->SetLabelOffset(0.01);
   gaxis->SetLabelSize(35);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisl");
   gaxis->SetLabelFont(133);
   gaxis->Draw();
   gaxis = new TGaxis(0.0004,-100,0.0004,100,-100,100,407,"+U");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0.04);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisr");
   gaxis->Draw();
   
   TLegend *leg = new TLegend(0.17,0.75,0.55,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(133);
   leg->SetTextSize(29);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","SMEFT Run2","");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(133);
   leg->Draw();
   
   TPaveText *pt = new TPaveText(0.65,0.775,0.875,0.9,"BRNDC");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(32);
   pt->SetTextFont(133);
   pt->SetTextSize(45);
   TText *AText = pt->AddText("GammaCombo");
   pt->Draw();
   
   pt = new TPaveText(0.17,0.15,0.37,0.275,"BRNDC");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);

   ci = TColor::GetColor("#999999");
   pt->SetTextColor(ci);
   pt->SetTextFont(133);
   pt->SetTextSize(21);
   AText = pt->AddText("contours hold 68%, 95% CL");
   pt->Draw();
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->Modified();
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->cd();
   HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38->SetSelected(HGamEFTScanner_test_cHB:cHBt_cHB_cHBtUID38);
}
