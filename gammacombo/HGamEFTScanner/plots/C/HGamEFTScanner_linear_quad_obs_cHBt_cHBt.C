void HGamEFTScanner_linear_quad_obs_cHBt_cHBt()
{
//=========Macro generated from canvas: HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20/p-value curves
//=========  (Thu Jul 22 06:03:10 2021) by ROOT version6.08/02
   TCanvas *HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20 = new TCanvas("HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20", "p-value curves",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20->SetHighLightColor(2);
   HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20->Range(-0.007162162,-0.2833333,0.006351351,1.383333);
   HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20->SetFillColor(0);
   HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20->SetBorderMode(0);
   HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20->SetBorderSize(2);
   HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20->SetLeftMargin(0.16);
   HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20->SetTopMargin(0.05);
   HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20->SetBottomMargin(0.17);
   HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20->SetFrameBorderMode(0);
   HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20->SetFrameBorderMode(0);
   
   Double_t UID25_fx1[155] = {
   -0.005,
   -0.005,
   -0.004966666,
   -0.0049,
   -0.004833333,
   -0.004766666,
   -0.0047,
   -0.004633333,
   -0.004566667,
   -0.0045,
   -0.004433333,
   -0.004366667,
   -0.0043,
   -0.004233333,
   -0.004166666,
   -0.0041,
   -0.004033333,
   -0.003966667,
   -0.0039,
   -0.003833333,
   -0.003766667,
   -0.0037,
   -0.003633333,
   -0.003566667,
   -0.0035,
   -0.003433333,
   -0.003366667,
   -0.0033,
   -0.003233333,
   -0.003166666,
   -0.0031,
   -0.003033333,
   -0.002966667,
   -0.0029,
   -0.002833333,
   -0.002766667,
   -0.0027,
   -0.002633333,
   -0.002566667,
   -0.0025,
   -0.002433333,
   -0.002366667,
   -0.0023,
   -0.002233333,
   -0.002166667,
   -0.0021,
   -0.002033333,
   -0.001966667,
   -0.0019,
   -0.001833333,
   -0.001766667,
   -0.0017,
   -0.001633333,
   -0.001566667,
   -0.0015,
   -0.001433333,
   -0.001366667,
   -0.0013,
   -0.001233333,
   -0.001166667,
   -0.0011,
   -0.001033333,
   -0.0009666667,
   -0.0009,
   -0.0008333333,
   -0.0007666667,
   -0.0007,
   -0.0006333333,
   -0.0005666667,
   -0.0005,
   -0.0004333333,
   -0.0003666666,
   -0.0003,
   -0.0002333333,
   -0.0001666667,
   -0.0001,
   -3.333333e-05,
   3.333333e-05,
   0.0001,
   0.0001666667,
   0.0002333333,
   0.0003,
   0.0003666666,
   0.0004333333,
   0.0004568246,
   0.0005,
   0.0005666667,
   0.0006333333,
   0.0007,
   0.0007666667,
   0.0008333333,
   0.0009,
   0.0009666667,
   0.001033333,
   0.0011,
   0.001166667,
   0.001233333,
   0.0013,
   0.001366667,
   0.001433333,
   0.0015,
   0.001566667,
   0.001633333,
   0.0017,
   0.001766667,
   0.001833333,
   0.0019,
   0.001966667,
   0.002033333,
   0.0021,
   0.002166667,
   0.002233333,
   0.0023,
   0.002366667,
   0.002433333,
   0.0025,
   0.002566667,
   0.002633333,
   0.0027,
   0.002766667,
   0.002833333,
   0.0029,
   0.002966667,
   0.003033333,
   0.0031,
   0.003166666,
   0.003233333,
   0.0033,
   0.003366667,
   0.003433333,
   0.0035,
   0.003566667,
   0.003633333,
   0.0037,
   0.003766667,
   0.003833333,
   0.0039,
   0.003966667,
   0.004033333,
   0.0041,
   0.004166666,
   0.004233333,
   0.0043,
   0.004366667,
   0.004433333,
   0.0045,
   0.004566667,
   0.004633333,
   0.0047,
   0.004766666,
   0.004833333,
   0.0049,
   0.004966666,
   0.005,
   0.005};
   Double_t UID25_fy1[155] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   2.564376e-43,
   1.060161e-39,
   2.534796e-36,
   3.593472e-33,
   3.094068e-30,
   1.656539e-27,
   5.642517e-25,
   1.250473e-22,
   1.842834e-20,
   1.844794e-18,
   1.28073e-16,
   6.291781e-15,
   2.23058e-13,
   5.81642e-12,
   1.136393e-10,
   1.693677e-09,
   1.959362e-08,
   1.789343e-07,
   1.311092e-06,
   7.82998e-06,
   3.869532e-05,
   0.0001605682,
   0.0005673525,
   0.001730118,
   0.004612245,
   0.01088147,
   0.0229862,
   0.04395906,
   0.07690509,
   0.1242891,
   0.1872588,
   0.2652482,
   0.3559907,
   0.4559095,
   0.5607324,
   0.6661407,
   0.7683172,
   0.8644283,
   0.9546563,
   0.9925047,
   0.9131207,
   0.8580691,
   0.8150121,
   0.7831919,
   0.7622092,
   0.7517926,
   0.751801,
   0.7622336,
   0.7832373,
   0.8150882,
   0.8582031,
   0.9133952,
   1,
   1,
   0.9553873,
   0.8647038,
   0.7684941,
   0.6662705,
   0.5608324,
   0.4559875,
   0.356051,
   0.2652937,
   0.1872917,
   0.1243118,
   0.07691979,
   0.04396792,
   0.0229911,
   0.01088394,
   0.004613357,
   0.001730563,
   0.0005675082,
   0.0001606153,
   3.870744e-05,
   7.832601e-06,
   1.311561e-06,
   1.790027e-07,
   1.960161e-08,
   1.694414e-09,
   1.13692e-10,
   5.819296e-12,
   2.231755e-13,
   6.295307e-15,
   1.281493e-16,
   1.845962e-18,
   1.844073e-20,
   1.251365e-22,
   5.64678e-25,
   1.657865e-27,
   3.096688e-30,
   3.59669e-33,
   2.537194e-36,
   1.06122e-39,
   2.564376e-43,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   TGraph *graph = new TGraph(155,UID25_fx1,UID25_fy1);
   graph->SetName("UID25");
   graph->SetTitle("Graph");

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#9999cc");
   graph->SetFillColor(ci);

   ci = TColor::GetColor("#9999cc");
   graph->SetLineColor(ci);
   graph->SetLineWidth(2);
   
   TH1F *Graph_haxesUID261 = new TH1F("Graph_haxesUID261","",100,-0.005,0.005);
   Graph_haxesUID261->SetMinimum(0);
   Graph_haxesUID261->SetMaximum(1.3);
   Graph_haxesUID261->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID261->SetLineColor(ci);
   Graph_haxesUID261->GetXaxis()->SetTitle("#it{#tilde{C}_{HB}}");
   Graph_haxesUID261->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID261->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID261->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID261->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID261->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID261->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID261->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID261->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID261->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID261->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID261->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID261->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID261->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID261->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID261->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID261->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID261->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID261->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_haxesUID261);
   
   graph->Draw(" f a");
   
   TH1F *haxesUID32__1 = new TH1F("haxesUID32__1","",100,-0.005,0.005);
   haxesUID32__1->SetMinimum(0);
   haxesUID32__1->SetMaximum(1.3);
   haxesUID32__1->SetStats(0);

   ci = TColor::GetColor("#000099");
   haxesUID32__1->SetLineColor(ci);
   haxesUID32__1->GetXaxis()->SetTitle("#it{#tilde{C}_{HB}}");
   haxesUID32__1->GetXaxis()->SetNdivisions(407);
   haxesUID32__1->GetXaxis()->SetLabelFont(133);
   haxesUID32__1->GetXaxis()->SetLabelSize(35);
   haxesUID32__1->GetXaxis()->SetTitleSize(45);
   haxesUID32__1->GetXaxis()->SetTitleOffset(0.9);
   haxesUID32__1->GetXaxis()->SetTitleFont(133);
   haxesUID32__1->GetYaxis()->SetTitle("1-CL");
   haxesUID32__1->GetYaxis()->SetNdivisions(407);
   haxesUID32__1->GetYaxis()->SetLabelFont(133);
   haxesUID32__1->GetYaxis()->SetLabelOffset(0.01);
   haxesUID32__1->GetYaxis()->SetLabelSize(35);
   haxesUID32__1->GetYaxis()->SetTitleSize(45);
   haxesUID32__1->GetYaxis()->SetTitleOffset(0.85);
   haxesUID32__1->GetYaxis()->SetTitleFont(133);
   haxesUID32__1->GetZaxis()->SetLabelFont(42);
   haxesUID32__1->GetZaxis()->SetLabelSize(0.035);
   haxesUID32__1->GetZaxis()->SetTitleSize(0.035);
   haxesUID32__1->GetZaxis()->SetTitleFont(42);
   haxesUID32__1->Draw("axissame");
   
   Double_t UID31_fx2[155] = {
   -0.005,
   -0.005,
   -0.004966666,
   -0.0049,
   -0.004833333,
   -0.004766666,
   -0.0047,
   -0.004633333,
   -0.004566667,
   -0.0045,
   -0.004433333,
   -0.004366667,
   -0.0043,
   -0.004233333,
   -0.004166666,
   -0.0041,
   -0.004033333,
   -0.003966667,
   -0.0039,
   -0.003833333,
   -0.003766667,
   -0.0037,
   -0.003633333,
   -0.003566667,
   -0.0035,
   -0.003433333,
   -0.003366667,
   -0.0033,
   -0.003233333,
   -0.003166666,
   -0.0031,
   -0.003033333,
   -0.002966667,
   -0.0029,
   -0.002833333,
   -0.002766667,
   -0.0027,
   -0.002633333,
   -0.002566667,
   -0.0025,
   -0.002433333,
   -0.002366667,
   -0.0023,
   -0.002233333,
   -0.002166667,
   -0.0021,
   -0.002033333,
   -0.001966667,
   -0.0019,
   -0.001833333,
   -0.001766667,
   -0.0017,
   -0.001633333,
   -0.001566667,
   -0.0015,
   -0.001433333,
   -0.001366667,
   -0.0013,
   -0.001233333,
   -0.001166667,
   -0.0011,
   -0.001033333,
   -0.0009666667,
   -0.0009,
   -0.0008333333,
   -0.0007666667,
   -0.0007,
   -0.0006333333,
   -0.0005666667,
   -0.0005,
   -0.0004333333,
   -0.0003666666,
   -0.0003,
   -0.0002333333,
   -0.0001666667,
   -0.0001,
   -3.333333e-05,
   3.333333e-05,
   0.0001,
   0.0001666667,
   0.0002333333,
   0.0003,
   0.0003666666,
   0.0004333333,
   0.0004568246,
   0.0005,
   0.0005666667,
   0.0006333333,
   0.0007,
   0.0007666667,
   0.0008333333,
   0.0009,
   0.0009666667,
   0.001033333,
   0.0011,
   0.001166667,
   0.001233333,
   0.0013,
   0.001366667,
   0.001433333,
   0.0015,
   0.001566667,
   0.001633333,
   0.0017,
   0.001766667,
   0.001833333,
   0.0019,
   0.001966667,
   0.002033333,
   0.0021,
   0.002166667,
   0.002233333,
   0.0023,
   0.002366667,
   0.002433333,
   0.0025,
   0.002566667,
   0.002633333,
   0.0027,
   0.002766667,
   0.002833333,
   0.0029,
   0.002966667,
   0.003033333,
   0.0031,
   0.003166666,
   0.003233333,
   0.0033,
   0.003366667,
   0.003433333,
   0.0035,
   0.003566667,
   0.003633333,
   0.0037,
   0.003766667,
   0.003833333,
   0.0039,
   0.003966667,
   0.004033333,
   0.0041,
   0.004166666,
   0.004233333,
   0.0043,
   0.004366667,
   0.004433333,
   0.0045,
   0.004566667,
   0.004633333,
   0.0047,
   0.004766666,
   0.004833333,
   0.0049,
   0.004966666,
   0.005,
   0.005};
   Double_t UID31_fy2[155] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   2.564376e-43,
   1.060161e-39,
   2.534796e-36,
   3.593472e-33,
   3.094068e-30,
   1.656539e-27,
   5.642517e-25,
   1.250473e-22,
   1.842834e-20,
   1.844794e-18,
   1.28073e-16,
   6.291781e-15,
   2.23058e-13,
   5.81642e-12,
   1.136393e-10,
   1.693677e-09,
   1.959362e-08,
   1.789343e-07,
   1.311092e-06,
   7.82998e-06,
   3.869532e-05,
   0.0001605682,
   0.0005673525,
   0.001730118,
   0.004612245,
   0.01088147,
   0.0229862,
   0.04395906,
   0.07690509,
   0.1242891,
   0.1872588,
   0.2652482,
   0.3559907,
   0.4559095,
   0.5607324,
   0.6661407,
   0.7683172,
   0.8644283,
   0.9546563,
   0.9925047,
   0.9131207,
   0.8580691,
   0.8150121,
   0.7831919,
   0.7622092,
   0.7517926,
   0.751801,
   0.7622336,
   0.7832373,
   0.8150882,
   0.8582031,
   0.9133952,
   1,
   1,
   0.9553873,
   0.8647038,
   0.7684941,
   0.6662705,
   0.5608324,
   0.4559875,
   0.356051,
   0.2652937,
   0.1872917,
   0.1243118,
   0.07691979,
   0.04396792,
   0.0229911,
   0.01088394,
   0.004613357,
   0.001730563,
   0.0005675082,
   0.0001606153,
   3.870744e-05,
   7.832601e-06,
   1.311561e-06,
   1.790027e-07,
   1.960161e-08,
   1.694414e-09,
   1.13692e-10,
   5.819296e-12,
   2.231755e-13,
   6.295307e-15,
   1.281493e-16,
   1.845962e-18,
   1.844073e-20,
   1.251365e-22,
   5.64678e-25,
   1.657865e-27,
   3.096688e-30,
   3.59669e-33,
   2.537194e-36,
   1.06122e-39,
   2.564376e-43,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   graph = new TGraph(155,UID31_fx2,UID31_fy2);
   graph->SetName("UID31");
   graph->SetTitle("Graph");
   graph->SetFillColor(1);

   ci = TColor::GetColor("#9999cc");
   graph->SetLineColor(ci);
   graph->SetLineStyle(0);
   graph->SetLineWidth(2);
   
   TH1F *Graph_haxesUID322 = new TH1F("Graph_haxesUID322","",100,-0.005,0.005);
   Graph_haxesUID322->SetMinimum(0);
   Graph_haxesUID322->SetMaximum(1.3);
   Graph_haxesUID322->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID322->SetLineColor(ci);
   Graph_haxesUID322->GetXaxis()->SetTitle("#it{#tilde{C}_{HB}}");
   Graph_haxesUID322->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID322->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID322->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID322->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID322->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID322->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_haxesUID322);
   
   graph->Draw(" l");
   TGaxis *gaxis = new TGaxis(-0.005,1,0.005,1,-0.005,0.005,407,"-U");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0.04);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axist");
   gaxis->Draw();
   gaxis = new TGaxis(0.005,0,0.005,1.3,0,1.3,407,"+");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(0);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisr");
   gaxis->SetLabelColor(0);
   gaxis->Draw();
   
   TH1F *Graph_haxesUID322 = new TH1F("Graph_haxesUID322","",100,-0.005,0.005);
   Graph_haxesUID322->SetMinimum(0);
   Graph_haxesUID322->SetMaximum(1.3);
   Graph_haxesUID322->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID322->SetLineColor(ci);
   Graph_haxesUID322->GetXaxis()->SetTitle("#it{#tilde{C}_{HB}}");
   Graph_haxesUID322->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID322->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID322->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID322->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID322->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID322->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID322->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID322->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID322->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID322->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID322->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID322->GetZaxis()->SetTitleFont(42);
   Graph_haxesUID322->Draw("axissame");
   TLine *line = new TLine(0.0004568246,0,0.0004568246,1);
   line->Draw();
   line = new TLine(-0.0009268188,0,-0.0009268188,1);
   line->SetLineStyle(2);
   line->Draw();
   line = new TLine(0.0009281598,0,0.0009281598,1);
   line->SetLineStyle(2);
   line->Draw();
   
   TPaveText *pt = new TPaveText(0.0004568246,0.6,0.002456825,0.7,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(13);
   pt->SetTextFont(133);
   TText *AText = pt->AddText("0.00046^{+0.00047}_{#font[122]{-}0.00139}");
   AText->SetTextSize(35);
   pt->Draw();
   
   TLegend *leg = new TLegend(0.19,0.78,0.5,0.9440559,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(133);
   leg->SetTextSize(21.75);
   leg->SetLineColor(0);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("UID25","SMEFT Run 2 Scan of ~cHB (Prob)","lf");

   ci = TColor::GetColor("#9999cc");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#9999cc");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(133);
   leg->Draw();
   
   pt = new TPaveText(-0.004,0.3373,0.001,0.3873,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextFont(133);
   pt->SetTextSize(35);
   AText = pt->AddText("68.3%");
   pt->Draw();
   line = new TLine(-0.005,0.3173,0.005,0.3173);
   line->SetLineStyle(3);
   line->Draw();
   
   pt = new TPaveText(-0.004,0.0655,0.001,0.1155,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextFont(133);
   pt->SetTextSize(35);
   AText = pt->AddText("95.5%");
   pt->Draw();
   line = new TLine(-0.005,0.0455,0.005,0.0455);
   line->SetLineStyle(3);
   line->Draw();
   
   pt = new TPaveText(0.65,0.6,0.875,0.725,"BRNDC");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(32);
   pt->SetTextFont(133);
   pt->SetTextSize(45);
   AText = pt->AddText("GammaCombo");
   pt->Draw();
   HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20->Modified();
   HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20->cd();
   HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20->SetSelected(HGamEFTScanner_linear_quad_obs_cHBt_cHBtUID20);
}
