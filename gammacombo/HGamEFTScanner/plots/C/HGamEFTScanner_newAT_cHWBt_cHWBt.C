void HGamEFTScanner_newAT_cHWBt_cHWBt()
{
//=========Macro generated from canvas: HGamEFTScanner_newAT_cHWBt_cHWBtUID13/p-value curves
//=========  (Fri Mar  5 07:40:41 2021) by ROOT version6.08/02
   TCanvas *HGamEFTScanner_newAT_cHWBt_cHWBtUID13 = new TCanvas("HGamEFTScanner_newAT_cHWBt_cHWBtUID13", "p-value curves",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   HGamEFTScanner_newAT_cHWBt_cHWBtUID13->SetHighLightColor(2);
   HGamEFTScanner_newAT_cHWBt_cHWBtUID13->Range(-0.007162162,-0.2833333,0.006351351,1.383333);
   HGamEFTScanner_newAT_cHWBt_cHWBtUID13->SetFillColor(0);
   HGamEFTScanner_newAT_cHWBt_cHWBtUID13->SetBorderMode(0);
   HGamEFTScanner_newAT_cHWBt_cHWBtUID13->SetBorderSize(2);
   HGamEFTScanner_newAT_cHWBt_cHWBtUID13->SetLeftMargin(0.16);
   HGamEFTScanner_newAT_cHWBt_cHWBtUID13->SetTopMargin(0.05);
   HGamEFTScanner_newAT_cHWBt_cHWBtUID13->SetBottomMargin(0.17);
   HGamEFTScanner_newAT_cHWBt_cHWBtUID13->SetFrameBorderMode(0);
   HGamEFTScanner_newAT_cHWBt_cHWBtUID13->SetFrameBorderMode(0);
   
   Double_t UID18_fx1[155] = {
   -0.005,
   -0.005,
   -0.004966666,
   -0.0049,
   -0.004833333,
   -0.004766666,
   -0.0047,
   -0.004633333,
   -0.004566667,
   -0.0045,
   -0.004433333,
   -0.004366667,
   -0.0043,
   -0.004233333,
   -0.004166666,
   -0.0041,
   -0.004033333,
   -0.003966667,
   -0.0039,
   -0.003833333,
   -0.003766667,
   -0.0037,
   -0.003633333,
   -0.003566667,
   -0.0035,
   -0.003433333,
   -0.003366667,
   -0.0033,
   -0.003233333,
   -0.003166666,
   -0.0031,
   -0.003033333,
   -0.002966667,
   -0.0029,
   -0.002833333,
   -0.002766667,
   -0.0027,
   -0.002633333,
   -0.002566667,
   -0.0025,
   -0.002433333,
   -0.002366667,
   -0.0023,
   -0.002233333,
   -0.002166667,
   -0.0021,
   -0.002033333,
   -0.001966667,
   -0.0019,
   -0.001833333,
   -0.001766667,
   -0.0017,
   -0.001633333,
   -0.001566667,
   -0.0015,
   -0.001433333,
   -0.001366667,
   -0.0013,
   -0.001233333,
   -0.001166667,
   -0.0011,
   -0.001033333,
   -0.0009666667,
   -0.0009,
   -0.0008333333,
   -0.0007666667,
   -0.0007,
   -0.0006333333,
   -0.0005666667,
   -0.0005,
   -0.0004333333,
   -0.0003666666,
   -0.0003,
   -0.0002333333,
   -0.0001666667,
   -0.0001,
   -3.333333e-05,
   7.08246e-06,
   3.333333e-05,
   0.0001,
   0.0001666667,
   0.0002333333,
   0.0003,
   0.0003666666,
   0.0004333333,
   0.0005,
   0.0005666667,
   0.0006333333,
   0.0007,
   0.0007666667,
   0.0008333333,
   0.0009,
   0.0009666667,
   0.001033333,
   0.0011,
   0.001166667,
   0.001233333,
   0.0013,
   0.001366667,
   0.001433333,
   0.0015,
   0.001566667,
   0.001633333,
   0.0017,
   0.001766667,
   0.001833333,
   0.0019,
   0.001966667,
   0.002033333,
   0.0021,
   0.002166667,
   0.002233333,
   0.0023,
   0.002366667,
   0.002433333,
   0.0025,
   0.002566667,
   0.002633333,
   0.0027,
   0.002766667,
   0.002833333,
   0.0029,
   0.002966667,
   0.003033333,
   0.0031,
   0.003166666,
   0.003233333,
   0.0033,
   0.003366667,
   0.003433333,
   0.0035,
   0.003566667,
   0.003633333,
   0.0037,
   0.003766667,
   0.003833333,
   0.0039,
   0.003966667,
   0.004033333,
   0.0041,
   0.004166666,
   0.004233333,
   0.0043,
   0.004366667,
   0.004433333,
   0.0045,
   0.004566667,
   0.004633333,
   0.0047,
   0.004766666,
   0.004833333,
   0.0049,
   0.004966666,
   0.005,
   0.005};
   Double_t UID18_fy1[155] = {
   0,
   0,
   0,
   0,
   1.401298e-45,
   2.087935e-43,
   3.989637e-41,
   6.149455e-39,
   7.6733e-37,
   7.798792e-35,
   6.494365e-33,
   4.457505e-31,
   2.536272e-29,
   1.20314e-27,
   4.785385e-26,
   1.604645e-24,
   4.561303e-23,
   1.104978e-21,
   2.293409e-20,
   4.099304e-19,
   6.342458e-18,
   8.536889e-17,
   1.004564e-15,
   1.038445e-14,
   9.47509e-14,
   7.666566e-13,
   5.526142e-12,
   3.564468e-11,
   2.066468e-10,
   1.08143e-09,
   5.130176e-09,
   2.215279e-08,
   8.742674e-08,
   3.16591e-07,
   1.056019e-06,
   3.256911e-06,
   9.321925e-06,
   2.485034e-05,
   6.191742e-05,
   0.0001446884,
   0.000318157,
   0.0006604551,
   0.001298393,
   0.0024247,
   0.004314027,
   0.007333765,
   0.01194521,
   0.01869156,
   0.02817096,
   0.04099569,
   0.05774166,
   0.07889414,
   0.1047973,
   0.1356147,
   0.1713057,
   0.2116203,
   0.2561117,
   0.3041659,
   0.3550407,
   0.4079105,
   0.4619129,
   0.5161902,
   0.5699248,
   0.6223662,
   0.672849,
   0.7208019,
   0.7657509,
   0.8073144,
   0.8451965,
   0.8791757,
   0.9090937,
   0.9348427,
   0.9563538,
   0.9735869,
   0.9865206,
   0.995146,
   0.9994599,
   1,
   0.9994619,
   0.995152,
   0.9865306,
   0.9736009,
   0.9563719,
   0.9348648,
   0.9091198,
   0.8792057,
   0.8452303,
   0.8073519,
   0.765792,
   0.7208463,
   0.6728962,
   0.6224159,
   0.5699763,
   0.5162429,
   0.4619661,
   0.4079634,
   0.3550924,
   0.3042156,
   0.2561585,
   0.2116635,
   0.1713448,
   0.1356492,
   0.1048269,
   0.07891891,
   0.05776176,
   0.0410115,
   0.02818297,
   0.01870036,
   0.01195141,
   0.007337953,
   0.004316735,
   0.00242637,
   0.001299373,
   0.0006610006,
   0.0003184442,
   0.0001448309,
   6.198389e-05,
   2.487937e-05,
   9.333765e-06,
   3.261402e-06,
   1.057598e-06,
   3.171037e-07,
   8.757992e-08,
   2.219474e-08,
   5.140661e-09,
   1.083813e-09,
   2.071374e-10,
   3.573574e-11,
   5.54132e-12,
   7.689183e-13,
   9.505086e-14,
   1.04197e-14,
   1.008216e-15,
   8.570107e-17,
   6.368849e-18,
   4.11753e-19,
   2.304296e-20,
   1.110574e-21,
   4.585929e-23,
   1.613874e-24,
   4.814682e-26,
   1.210975e-27,
   2.553832e-29,
   4.490291e-31,
   6.545078e-33,
   7.863405e-35,
   7.740708e-37,
   6.206703e-39,
   4.029013e-41,
   2.101948e-43,
   1.401298e-45,
   0,
   0,
   0,
   0};
   TGraph *graph = new TGraph(155,UID18_fx1,UID18_fy1);
   graph->SetName("UID18");
   graph->SetTitle("Graph");

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#9999cc");
   graph->SetFillColor(ci);

   ci = TColor::GetColor("#9999cc");
   graph->SetLineColor(ci);
   graph->SetLineWidth(2);
   
   TH1F *Graph_haxesUID191 = new TH1F("Graph_haxesUID191","",100,-0.005,0.005);
   Graph_haxesUID191->SetMinimum(0);
   Graph_haxesUID191->SetMaximum(1.3);
   Graph_haxesUID191->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID191->SetLineColor(ci);
   Graph_haxesUID191->GetXaxis()->SetTitle("#tilde{C}_{HWB}");
   Graph_haxesUID191->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID191->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID191->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID191->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID191->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID191->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID191->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID191->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID191->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID191->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID191->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID191->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID191->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID191->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID191->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID191->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID191->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID191->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_haxesUID191);
   
   graph->Draw(" f a");
   
   TH1F *haxesUID25__1 = new TH1F("haxesUID25__1","",100,-0.005,0.005);
   haxesUID25__1->SetMinimum(0);
   haxesUID25__1->SetMaximum(1.3);
   haxesUID25__1->SetStats(0);

   ci = TColor::GetColor("#000099");
   haxesUID25__1->SetLineColor(ci);
   haxesUID25__1->GetXaxis()->SetTitle("#tilde{C}_{HWB}");
   haxesUID25__1->GetXaxis()->SetNdivisions(407);
   haxesUID25__1->GetXaxis()->SetLabelFont(133);
   haxesUID25__1->GetXaxis()->SetLabelSize(35);
   haxesUID25__1->GetXaxis()->SetTitleSize(45);
   haxesUID25__1->GetXaxis()->SetTitleOffset(0.9);
   haxesUID25__1->GetXaxis()->SetTitleFont(133);
   haxesUID25__1->GetYaxis()->SetTitle("1-CL");
   haxesUID25__1->GetYaxis()->SetNdivisions(407);
   haxesUID25__1->GetYaxis()->SetLabelFont(133);
   haxesUID25__1->GetYaxis()->SetLabelOffset(0.01);
   haxesUID25__1->GetYaxis()->SetLabelSize(35);
   haxesUID25__1->GetYaxis()->SetTitleSize(45);
   haxesUID25__1->GetYaxis()->SetTitleOffset(0.85);
   haxesUID25__1->GetYaxis()->SetTitleFont(133);
   haxesUID25__1->GetZaxis()->SetLabelFont(42);
   haxesUID25__1->GetZaxis()->SetLabelSize(0.035);
   haxesUID25__1->GetZaxis()->SetTitleSize(0.035);
   haxesUID25__1->GetZaxis()->SetTitleFont(42);
   haxesUID25__1->Draw("axissame");
   
   Double_t UID24_fx2[155] = {
   -0.005,
   -0.005,
   -0.004966666,
   -0.0049,
   -0.004833333,
   -0.004766666,
   -0.0047,
   -0.004633333,
   -0.004566667,
   -0.0045,
   -0.004433333,
   -0.004366667,
   -0.0043,
   -0.004233333,
   -0.004166666,
   -0.0041,
   -0.004033333,
   -0.003966667,
   -0.0039,
   -0.003833333,
   -0.003766667,
   -0.0037,
   -0.003633333,
   -0.003566667,
   -0.0035,
   -0.003433333,
   -0.003366667,
   -0.0033,
   -0.003233333,
   -0.003166666,
   -0.0031,
   -0.003033333,
   -0.002966667,
   -0.0029,
   -0.002833333,
   -0.002766667,
   -0.0027,
   -0.002633333,
   -0.002566667,
   -0.0025,
   -0.002433333,
   -0.002366667,
   -0.0023,
   -0.002233333,
   -0.002166667,
   -0.0021,
   -0.002033333,
   -0.001966667,
   -0.0019,
   -0.001833333,
   -0.001766667,
   -0.0017,
   -0.001633333,
   -0.001566667,
   -0.0015,
   -0.001433333,
   -0.001366667,
   -0.0013,
   -0.001233333,
   -0.001166667,
   -0.0011,
   -0.001033333,
   -0.0009666667,
   -0.0009,
   -0.0008333333,
   -0.0007666667,
   -0.0007,
   -0.0006333333,
   -0.0005666667,
   -0.0005,
   -0.0004333333,
   -0.0003666666,
   -0.0003,
   -0.0002333333,
   -0.0001666667,
   -0.0001,
   -3.333333e-05,
   7.08246e-06,
   3.333333e-05,
   0.0001,
   0.0001666667,
   0.0002333333,
   0.0003,
   0.0003666666,
   0.0004333333,
   0.0005,
   0.0005666667,
   0.0006333333,
   0.0007,
   0.0007666667,
   0.0008333333,
   0.0009,
   0.0009666667,
   0.001033333,
   0.0011,
   0.001166667,
   0.001233333,
   0.0013,
   0.001366667,
   0.001433333,
   0.0015,
   0.001566667,
   0.001633333,
   0.0017,
   0.001766667,
   0.001833333,
   0.0019,
   0.001966667,
   0.002033333,
   0.0021,
   0.002166667,
   0.002233333,
   0.0023,
   0.002366667,
   0.002433333,
   0.0025,
   0.002566667,
   0.002633333,
   0.0027,
   0.002766667,
   0.002833333,
   0.0029,
   0.002966667,
   0.003033333,
   0.0031,
   0.003166666,
   0.003233333,
   0.0033,
   0.003366667,
   0.003433333,
   0.0035,
   0.003566667,
   0.003633333,
   0.0037,
   0.003766667,
   0.003833333,
   0.0039,
   0.003966667,
   0.004033333,
   0.0041,
   0.004166666,
   0.004233333,
   0.0043,
   0.004366667,
   0.004433333,
   0.0045,
   0.004566667,
   0.004633333,
   0.0047,
   0.004766666,
   0.004833333,
   0.0049,
   0.004966666,
   0.005,
   0.005};
   Double_t UID24_fy2[155] = {
   0,
   0,
   0,
   0,
   1.401298e-45,
   2.087935e-43,
   3.989637e-41,
   6.149455e-39,
   7.6733e-37,
   7.798792e-35,
   6.494365e-33,
   4.457505e-31,
   2.536272e-29,
   1.20314e-27,
   4.785385e-26,
   1.604645e-24,
   4.561303e-23,
   1.104978e-21,
   2.293409e-20,
   4.099304e-19,
   6.342458e-18,
   8.536889e-17,
   1.004564e-15,
   1.038445e-14,
   9.47509e-14,
   7.666566e-13,
   5.526142e-12,
   3.564468e-11,
   2.066468e-10,
   1.08143e-09,
   5.130176e-09,
   2.215279e-08,
   8.742674e-08,
   3.16591e-07,
   1.056019e-06,
   3.256911e-06,
   9.321925e-06,
   2.485034e-05,
   6.191742e-05,
   0.0001446884,
   0.000318157,
   0.0006604551,
   0.001298393,
   0.0024247,
   0.004314027,
   0.007333765,
   0.01194521,
   0.01869156,
   0.02817096,
   0.04099569,
   0.05774166,
   0.07889414,
   0.1047973,
   0.1356147,
   0.1713057,
   0.2116203,
   0.2561117,
   0.3041659,
   0.3550407,
   0.4079105,
   0.4619129,
   0.5161902,
   0.5699248,
   0.6223662,
   0.672849,
   0.7208019,
   0.7657509,
   0.8073144,
   0.8451965,
   0.8791757,
   0.9090937,
   0.9348427,
   0.9563538,
   0.9735869,
   0.9865206,
   0.995146,
   0.9994599,
   1,
   0.9994619,
   0.995152,
   0.9865306,
   0.9736009,
   0.9563719,
   0.9348648,
   0.9091198,
   0.8792057,
   0.8452303,
   0.8073519,
   0.765792,
   0.7208463,
   0.6728962,
   0.6224159,
   0.5699763,
   0.5162429,
   0.4619661,
   0.4079634,
   0.3550924,
   0.3042156,
   0.2561585,
   0.2116635,
   0.1713448,
   0.1356492,
   0.1048269,
   0.07891891,
   0.05776176,
   0.0410115,
   0.02818297,
   0.01870036,
   0.01195141,
   0.007337953,
   0.004316735,
   0.00242637,
   0.001299373,
   0.0006610006,
   0.0003184442,
   0.0001448309,
   6.198389e-05,
   2.487937e-05,
   9.333765e-06,
   3.261402e-06,
   1.057598e-06,
   3.171037e-07,
   8.757992e-08,
   2.219474e-08,
   5.140661e-09,
   1.083813e-09,
   2.071374e-10,
   3.573574e-11,
   5.54132e-12,
   7.689183e-13,
   9.505086e-14,
   1.04197e-14,
   1.008216e-15,
   8.570107e-17,
   6.368849e-18,
   4.11753e-19,
   2.304296e-20,
   1.110574e-21,
   4.585929e-23,
   1.613874e-24,
   4.814682e-26,
   1.210975e-27,
   2.553832e-29,
   4.490291e-31,
   6.545078e-33,
   7.863405e-35,
   7.740708e-37,
   6.206703e-39,
   4.029013e-41,
   2.101948e-43,
   1.401298e-45,
   0,
   0,
   0,
   0};
   graph = new TGraph(155,UID24_fx2,UID24_fy2);
   graph->SetName("UID24");
   graph->SetTitle("Graph");
   graph->SetFillColor(1);

   ci = TColor::GetColor("#9999cc");
   graph->SetLineColor(ci);
   graph->SetLineStyle(0);
   graph->SetLineWidth(2);
   
   TH1F *Graph_haxesUID252 = new TH1F("Graph_haxesUID252","",100,-0.005,0.005);
   Graph_haxesUID252->SetMinimum(0);
   Graph_haxesUID252->SetMaximum(1.3);
   Graph_haxesUID252->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID252->SetLineColor(ci);
   Graph_haxesUID252->GetXaxis()->SetTitle("#tilde{C}_{HWB}");
   Graph_haxesUID252->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID252->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID252->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID252->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID252->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID252->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID252->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID252->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID252->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID252->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID252->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID252->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID252->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID252->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID252->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID252->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID252->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID252->GetZaxis()->SetTitleFont(42);
   graph->SetHistogram(Graph_haxesUID252);
   
   graph->Draw(" l");
   TGaxis *gaxis = new TGaxis(-0.005,1,0.005,1,-0.005,0.005,407,"-U");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0.04);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axist");
   gaxis->Draw();
   gaxis = new TGaxis(0.005,0,0.005,1.3,0,1.3,407,"+");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(1);
   gaxis->SetTitleSize(0.04);
   gaxis->SetTitleColor(0);
   gaxis->SetTitleFont(62);
   gaxis->SetName("axisr");
   gaxis->SetLabelColor(0);
   gaxis->Draw();
   
   TH1F *Graph_haxesUID252 = new TH1F("Graph_haxesUID252","",100,-0.005,0.005);
   Graph_haxesUID252->SetMinimum(0);
   Graph_haxesUID252->SetMaximum(1.3);
   Graph_haxesUID252->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_haxesUID252->SetLineColor(ci);
   Graph_haxesUID252->GetXaxis()->SetTitle("#tilde{C}_{HWB}");
   Graph_haxesUID252->GetXaxis()->SetNdivisions(407);
   Graph_haxesUID252->GetXaxis()->SetLabelFont(133);
   Graph_haxesUID252->GetXaxis()->SetLabelSize(35);
   Graph_haxesUID252->GetXaxis()->SetTitleSize(45);
   Graph_haxesUID252->GetXaxis()->SetTitleOffset(0.9);
   Graph_haxesUID252->GetXaxis()->SetTitleFont(133);
   Graph_haxesUID252->GetYaxis()->SetTitle("1-CL");
   Graph_haxesUID252->GetYaxis()->SetNdivisions(407);
   Graph_haxesUID252->GetYaxis()->SetLabelFont(133);
   Graph_haxesUID252->GetYaxis()->SetLabelOffset(0.01);
   Graph_haxesUID252->GetYaxis()->SetLabelSize(35);
   Graph_haxesUID252->GetYaxis()->SetTitleSize(45);
   Graph_haxesUID252->GetYaxis()->SetTitleOffset(0.85);
   Graph_haxesUID252->GetYaxis()->SetTitleFont(133);
   Graph_haxesUID252->GetZaxis()->SetLabelFont(42);
   Graph_haxesUID252->GetZaxis()->SetLabelSize(0.035);
   Graph_haxesUID252->GetZaxis()->SetTitleSize(0.035);
   Graph_haxesUID252->GetZaxis()->SetTitleFont(42);
   Graph_haxesUID252->Draw("axissame");
   TLine *line = new TLine(7.08246e-06,0,7.08246e-06,1);
   line->Draw();
   line = new TLine(-0.001282328,0,-0.001282328,1);
   line->SetLineStyle(2);
   line->Draw();
   line = new TLine(0.001282935,0,0.001282935,1);
   line->SetLineStyle(2);
   line->Draw();
   
   TPaveText *pt = new TPaveText(7.08246e-06,0.6,0.002007082,0.7,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(13);
   pt->SetTextFont(133);
   TText *AText = pt->AddText("0.0000^{+0.0013}_{#font[122]{-}0.0013}");
   AText->SetTextSize(35);
   pt->Draw();
   
   TLegend *leg = new TLegend(0.19,0.78,0.5,0.9440559,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(133);
   leg->SetTextSize(21.75);
   leg->SetLineColor(0);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("UID18","SMEFT Run 2 Scan of ~cHWB (Prob)","lf");

   ci = TColor::GetColor("#9999cc");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#9999cc");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(133);
   leg->Draw();
   
   pt = new TPaveText(-0.004,0.3373,0.001,0.3873,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextFont(133);
   pt->SetTextSize(35);
   AText = pt->AddText("68.3%");
   pt->Draw();
   line = new TLine(-0.005,0.3173,0.005,0.3173);
   line->SetLineStyle(3);
   line->Draw();
   
   pt = new TPaveText(-0.004,0.0655,0.001,0.1155,"BR");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(12);
   pt->SetTextFont(133);
   pt->SetTextSize(35);
   AText = pt->AddText("95.5%");
   pt->Draw();
   line = new TLine(-0.005,0.0455,0.005,0.0455);
   line->SetLineStyle(3);
   line->Draw();
   
   pt = new TPaveText(0.65,0.6,0.875,0.725,"BRNDC");
   pt->SetBorderSize(0);
   pt->SetFillStyle(0);
   pt->SetTextAlign(32);
   pt->SetTextFont(133);
   pt->SetTextSize(45);
   AText = pt->AddText("GammaCombo");
   pt->Draw();
   HGamEFTScanner_newAT_cHWBt_cHWBtUID13->Modified();
   HGamEFTScanner_newAT_cHWBt_cHWBtUID13->cd();
   HGamEFTScanner_newAT_cHWBt_cHWBtUID13->SetSelected(HGamEFTScanner_newAT_cHWBt_cHWBtUID13);
}
