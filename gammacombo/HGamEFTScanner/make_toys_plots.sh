
##########################################################################################
##
##  Make PLUGIN Plots for cHW, cHWt, cg, cgt
##
##########################################################################################

bin/HGamEFTScanner --var cHW \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHWt=0,cHB=0,cHBt=0'\
    -c 1\
    --scanrange -0.1:0.1\
    -a plugin -j 1-10


bin/HGamEFTScanner --var cHWt \
    --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHB=0,cHBt=0'\
    -c 2\
    --scanrange -0.5:0.5\
    -a plugin -j 1-10

bin/HGamEFTScanner --var cg \
    --fix 'ca=0,cat=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0'\
    -c 6\
    --scanrange -0.00055:0.00015\
     -a plugin -j 1-5 

bin/HGamEFTScanner --var cgt \
    --fix 'ca=0,cat=0,cg=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0'\
    -c 7\
    --scanrange -0.001:0.001\
    -a plugin -j 1

##########################################################################################
##
##  Make PLUGIN Plots for cHW:cHWt
##
##########################################################################################


# bin/HGamEFTScanner --var cHW --var cHWt \
#     --fix 'ca=0,cat=0,cg=0,cgt=0,cH=0,cT=0,cB=0,cW=0,cHB=0,cHBt=0'\
#     -c 3\
#     --2dcl 1\
#     --scanrange -0.25:0.25\
#     --scanrangey -0.5:0.49\
#     --npoints 30 --npointstoy 30\
#     --group "#scale[0.8]{}"\
#     -a plugin -j 1-10 --smooth2d

bin/HGamEFTScanner --var cHW --var cHWt \
  -c 3  --color 10 --title " "\
  --2dcl 1 --ncontours 2\
  --scanrange -0.2:0.2 --scanrangey -0.4:0.39\
  --group "#bf{#it{ATLAS}} Internal"\
  -a plugin -j 1-10 --smooth2d  

bin/HGamEFTScanner --var cHW --var cHWt \
  -c 3  --color 10 --title " "\
  --2dcl 1 --ncontours 2\
  --scanrange -0.2:0.2 --scanrangey -0.4:0.39\
  --group "#bf{#it{ATLAS}} Preliminary"\
  -a plugin -j 1-10 --smooth2d  --po  
  

# bin/HGamEFTScanner --var cg --var cgt \
#     --fix 'ca=0,cat=0,cH=0,cT=0,cB=0,cW=0,cHW=0,cHWt=0,cHB=0,cHBt=0'\
#     -c 8\
#     --2dcl 1\
#     --scanrange -0.00075:0.001\
#     --scanrangey -0.00075:0.001\
#     --npoints 30 --npointstoy 30\
#     --group "#scale[0.8]{}"\
#     -a plugin -j 1-5 --smooth2d  
#     