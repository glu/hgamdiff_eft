#include "EFT_Parameters.h"

EFT_Parameters::EFT_Parameters()
{
  defineParameters();
}

///
/// Define all (nuisance) parameters.
///
///  scan:      defines scan range (for Prob and Plugin methods)
///  phys:      physically allowed range (needs to be set!)
///  bboos:     Ranges for Berger-Boos method
///  force:     min, max used by the force fit method
///  free:	range applied when no boundary is required - set this far away from any possible value
///

void EFT_Parameters::defineParameters()
{

  Parameter *p = 0;

  p = newParameter("cg");
  p->title = "#bar{c}_{g}";
  p->startvalue = 0.0;
  p->unit = "";
  p->scan = range(-1.,1.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);

  p = newParameter("cgt");
  p->title = "#tilde{c}_{g}";
  p->startvalue = 0.0;
  p->unit = "";
  p->scan = range(-1.,1.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);
  
  p = newParameter("ca");
  p->title = "#bar{c}_{#gamma}";
  p->startvalue = 0.0;
  p->unit = "";
  p->scan = range(-5.,5.);
  p->phys = range(-5e1,5e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-5e1,5e1);
  p->bboos = range(-5e1,5e1);
  p->free = range(-5e1,5e1);

  p = newParameter("cat");
  p->title = "#tilde{c}_{#gamma}";
  p->startvalue = 0.0;
  p->unit = "";
  p->scan = range(-5.,5.);
  p->phys = range(-5e1,5e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-5e1,5e1);
  p->bboos = range(-5e1,5e1);
  p->free = range(-5e1,5e1);  
  
  p = newParameter("cH");
  p->title = "#bar{c}_{H}";
  p->startvalue = 0.0;
  p->unit = "";
  p->scan = range(-1.,1.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);

  p = newParameter("cT");
  p->title = "#bar{c}_{T}";
  p->startvalue = 0.0;
  p->unit = "";
  p->scan = range(-1.,1.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);  
  
  p = newParameter("cB");
  p->title = "#bar{c}_{B}";
  p->startvalue = 0.0;
  p->unit = "";
  p->scan = range(-1.,1.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);  
    
  p = newParameter("cW");
  p->title = "#bar{c}_{W}";
  p->startvalue = 0.0;
  p->unit = "";
  p->scan = range(-1.,1.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);  
      
  p = newParameter("cHW");
  p->title = "#bar{c}_{HW}";
  p->startvalue = 0.0;
  p->unit = "";
  p->scan = range(-1.,1.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);

  p = newParameter("cHWt");
  p->title = "#tilde{c}_{HW}";
  p->startvalue = 0.0;
  p->unit = "";
  p->scan = range(-1.,1.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);    

  p = newParameter("cHB");
  p->title = "#bar{c}_{HB}";
  p->startvalue = 0.0;
  p->unit = "";
  p->scan = range(-1.,1.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);

  p = newParameter("cHBt");
  p->title = "#tilde{c}_{HB}";
  p->startvalue = 0.0;
  p->unit = "";
  p->scan = range(-1.,1.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);
  
  p = newParameter("mu_ggF_ttH");
  p->title = "#mu_{ggF+ttH}";
  p->startvalue = 1.0;
  p->unit = "";
  p->scan = range(-10.,10.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);
  
  p = newParameter("mu_vbf_vh");
  p->title = "#mu_{VBF+VH}";
  p->startvalue = 1.0;
  p->unit = "";
  p->scan = range(-10.,10.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);  
  
  p = newParameter("mu_ggF");
  p->title = "#mu_{ggF}";
  p->startvalue = 1.0;
  p->unit = "";
  p->scan = range(-10.,10.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);
  
  p = newParameter("mu_vbf");
  p->title = "#mu_{VBF}";
  p->startvalue = 1.0;
  p->unit = "";
  p->scan = range(-10.,10.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);    

  p = newParameter("mu_vh");
  p->title = "#mu_{VH}";
  p->startvalue = 1.0;
  p->unit = "";
  p->scan = range(-10.,10.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);    
  
  p = newParameter("mu_ttH");
  p->title = "#mu_{ttH}";
  p->startvalue = 1.0;
  p->unit = "";
  p->scan = range(-10.,10.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);    

  p = newParameter("mu");
  p->title = "#Gamma_{H}(#vec{c})/ #Gamma_{H}";
  p->startvalue = 1.0;
  p->unit = "";
  p->scan = range(-10.,10.);
  p->phys = range(-1e1,1e1); // to implement a Feldman-Cousins like forbidden region, set the allowed region here and use --pr
  p->force = range(-1e1,1e1);
  p->bboos = range(-1e1,1e1);
  p->free = range(-1e1,1e1);      
  
}

