/**
 * EFT module for Gamma Combination code
 * 
 * Authors: Florian Urs Bernlochner, florian.bernlochner@cern.ch,
 *          Christopher Meyer, chris.mejer@cern.ch,
 *          Jim Lacey, jim.lacey@cern.ch,
 *          Yanping Huang, yanping.huang@cern.ch,
 *          Maria Josefina Alconada Verzini, josefina.alconada@cern.ch
 * 
 * Date: October 2016
 *
 **/

#include "PDF_HGam_Meas_Run2_Couplings.h"
#include "PDF_HGam_Meas_Run2_Couplings.h"

// ---------------------------------------------------------------------------------------
// Constructor

PDF_HGam_Meas_Run2_Couplings::PDF_HGam_Meas_Run2_Couplings(Str SettingsFileName) : PDF_Abs(1) 
{
    
    // Open Settings file 
    set = OpenSettingsFile(SettingsFileName);  

    // Fetch HepDataFile and Keys
    Str HepDataFile = set->GetValue("HepDataFile","");    
    HepDataKeys = Vectorize(set->GetValue("HepDataKeys","")," ");
    MeasurementReader = new HGMR::HGamMeasurementReader(set);

    // Read in measurements         
    MeasurementReader->loadMeasurements(HepDataFile,HepDataKeys);
    MeasurementReader->setFullCov( HepDataKeys );

    // Determine number of Bins and set Keys
    nObs = 0;
    for(auto HepDataKey : HepDataKeys)
     nObs += MeasurementReader->getXS(HepDataKey).size();
    this->ResizeTo(nObs);
    
    cout << " nObs = " << nObs << endl;
 
    // Name of the PDF
	name = "HGam_Meas_Run2"; 
	
	// Initialize the Parameters, Relations and Observables
	initParameters();
	initRelations();
	initObservables();

	Str ScanType = set->GetValue("ScanType","");
	
	if(ScanType.Contains("SM"))
	 cout << "\n |--> Warning: Running not over data, but ScanType = " << ScanType << endl;
	
	// Setup the Observables, Uncertainties and Correlations
	setObservables(ScanType);
	setUncertainties("");
	setCorrelations("");
	
	// Build the Covariance matrix and eventual PDF
	buildCov();
	buildPdf();
	
}

// ---------------------------------------------------------------------------------------
// Destructor

PDF_HGam_Meas_Run2_Couplings::~PDF_HGam_Meas_Run2_Couplings() {}

// ---------------------------------------------------------------------------------------
// Init free parameters of the scan

void PDF_HGam_Meas_Run2_Couplings::initParameters() 
{

	EFT_Parameters p;
	
	parameters = new RooArgList("parameters");
	
	parameters->add(*(p.get("mu_ggF")));
	parameters->add(*(p.get("mu_vbf")));
	parameters->add(*(p.get("mu_vh")));
	parameters->add(*(p.get("mu_ttH")));

	
}

// ---------------------------------------------------------------------------------------
// Init relations that relate the free parameters with the measured observables

void PDF_HGam_Meas_Run2_Couplings::initRelations() 
{
   
   // Setup theory list
   theory = new RooArgList("theory");
   
   // Add observables
   int nKey(0);
   
   for(auto HepDataKey: HepDataKeys) {

     VecD XSecs = MeasurementReader->getXS(HepDataKey);
     
     int nBin(0);
     
     for(auto XSec : XSecs) {
      Str Tag = Form("%s:%d_th",HepDataKey.Data(),nBin);     
       
      // Get the name of the variable
      Str XSecName = HepDataKey; XSecName.ReplaceAll(set->GetValue("HepDataBase",""),"");
      
      // Fetch the corresponding SM cross sections    
      VecD ggf_XSecSM = VectorizeD(set->GetValue(Form("%s.ggf.XSec",XSecName.Data()),"")," "),
           vbf_XSecSM = VectorizeD(set->GetValue(Form("%s.vbf.XSec",XSecName.Data()),"")," "),
           vh_XSecSM = VectorizeD(set->GetValue(Form("%s.vh.XSec",XSecName.Data()),"")," "),
           tth_XSecSM = VectorizeD(set->GetValue(Form("%s.ttH.XSec",XSecName.Data()),"")," ");
                 
      // Now add the Relation in coupling strengths
	  theory->add(*(new RooFormulaVar(Tag, Tag, Form("(mu_ggF * %f + mu_vbf * %f + mu_vh * %f  + mu_ttH * %f)", ggf_XSecSM[nBin], vbf_XSecSM[nBin], vh_XSecSM[nBin], tth_XSecSM[nBin] ), *(RooArgSet*)parameters)));
      
      nBin++; nKey++;
     }
    

   }

   
}

// ---------------------------------------------------------------------------------------
// Initalize Observables 

void PDF_HGam_Meas_Run2_Couplings::initObservables() 
{

   // Setup observable list
   observables = new RooArgList("observables");
	
   
   for(auto HepDataKey: HepDataKeys) {

     int nBin(0);

     VecD XSecs = MeasurementReader->getXS(HepDataKey);
     for(auto XSec : XSecs) {
      Str Tag = Form("%s:%d_obs",HepDataKey.Data(),nBin);
      observables->add(*(new RooRealVar(Tag,Tag, 0, -1e4, 1e4)));  
      nBin++;
     }

   }
   

	
}

// ---------------------------------------------------------------------------------------
// Init Observables 

void PDF_HGam_Meas_Run2_Couplings::setObservables(TString c) 
{

   if(c.EqualTo("SMAsimov"))
	  setObservablesTruth();
   else if(c.EqualTo("SMToy")) {    
	  setObservablesToy();
	  // pdf->Delete();
	 } 
   else {
	  
   // Add observables      
    for(auto HepDataKey: HepDataKeys) {

     VecD XSecs = MeasurementReader->getXS(HepDataKey);

     int nBin(0);
       
     for(auto XSec : XSecs) {

      Str Tag = Form("%s:%d_obs",HepDataKey.Data(),nBin);
	  setObservable(Tag, XSec);
	  cout << Tag << "\t XSec = " << XSec << endl;
      nBin++;
     }

    }
   
   }

}

// ---------------------------------------------------------------------------------------
// Init Observable Uncertainties 

void PDF_HGam_Meas_Run2_Couplings::setUncertainties(TString c) 
{

   // The first error is stat. and the second systematical
   // The second will be assumed to be a 100% correlated by default
   // unless we intervene, but since it's set to zero it does not matter

   
   for(auto HepDataKey: HepDataKeys) {
   
     int nBin(0);   

     VecD XSecTotErrors = MeasurementReader->getXS_TotErr(HepDataKey);
     for(auto XSecTotError : XSecTotErrors) {
      Str Tag = Form("%s:%d_obs",HepDataKey.Data(),nBin);
	  setUncertainty(Tag, XSecTotError, 0.);	
	  cout << Tag << "\t XSecTotError = " << XSecTotError << endl;
	    
      nBin++;
     }

   } 

}

// ---------------------------------------------------------------------------------------
// Set Correlations

void PDF_HGam_Meas_Run2_Couplings::setCorrelations(TString c) 
{
	resetCorrelations();

    TMatrixD CovTot(nObs,nObs);
    CovTot = MeasurementReader->getFullCov();
    
    for(int xi = 0; xi < nObs; xi++)
     for(int yi = 0; yi < nObs; yi++) 
        corStatMatrix[xi][yi] = CovTot[xi][yi]/sqrt(CovTot[xi][xi])/sqrt(CovTot[yi][yi]);
      
     

	return;
}


// ---------------------------------------------------------------------------------------
// Build final PDF -- we model the observables / measurements with a multivariate Gaussian

void PDF_HGam_Meas_Run2_Couplings::buildPdf() 
{

    covMatrix.Print("all");

	pdf = new RooMultiVarGaussian("pdf_"+name, "pdf_"+name, *(RooArgSet*)observables, *(RooArgSet*)theory, covMatrix);
				
}

// ---------------------------------------------------------------------------------------

