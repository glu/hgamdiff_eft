/**
 * EFT module for Gamma Combination code
 * 
 * Authors: Florian Urs Bernlochner, florian.bernlochner@cern.ch,
 *          Christopher Meyer, chris.mejer@cern.ch,
 *          Jim Lacey, jim.lacey@cern.ch,
 *          Yanping Huang, yanping.huang@cern.ch,
 *          Maria Josefina Alconada Verzini, josefina.alconada@cern.ch
 * 
 * Date: October 2016
 *
 **/

#include "PDF_HGam_Meas_Run2.h"
#include "PDF_HGam_Meas_Run2.h"

// ---------------------------------------------------------------------------------------
// Constructor

PDF_HGam_Meas_Run2::PDF_HGam_Meas_Run2(Str SettingsFileName, Str IpolFileName) : PDF_Abs(1) 
{
    
    // Open Settings file 
    set = OpenSettingsFile(SettingsFileName);  
    ipol = OpenSettingsFile(IpolFileName);

    // Fetch HepDataFile and Keys
    Str HepDataFile = set->GetValue("HepDataFile","");    
    HepDataKeys = Vectorize(set->GetValue("HepDataKeys","")," ");
    MeasurementReader = new HGMR::HGamMeasurementReader(set);
    
    for(auto HepDataKey : HepDataKeys)
     cout << " HepDataKey = " << HepDataKey << endl;

    // Read in measurements         
    MeasurementReader->loadMeasurements(HepDataFile,HepDataKeys);
    MeasurementReader->setFullCov( HepDataKeys );

    // Determine number of Bins and set Keys
    nObs = 0;
    for(auto HepDataKey : HepDataKeys)
     nObs += MeasurementReader->getXS(HepDataKey).size();
    this->ResizeTo(nObs);
    
    cout << " nObs = " << nObs << endl;
 
    // Name of the PDF
	name = "HGam_Meas_Run2"; 
	
	// Initialize the Parameters, Relations and Observables
	initParameters();
	initRelations();
	initObservables();

	Str ScanType = set->GetValue("ScanType","");
	
	if(ScanType.Contains("SM"))
	 cout << "\n |--> Warning: Running not over data, but ScanType = " << ScanType << endl;
	
	// Setup the Observables, Uncertainties and Correlations
	setObservables(ScanType);
	setUncertainties("");
	setCorrelations("");
	
	// Build the Covariance matrix and eventual PDF
	buildCov();
	buildPdf();
	
}

// ---------------------------------------------------------------------------------------
// Destructor

PDF_HGam_Meas_Run2::~PDF_HGam_Meas_Run2() {}

// ---------------------------------------------------------------------------------------
// Init free parameters of the scan

void PDF_HGam_Meas_Run2::initParameters() 
{

	EFT_Parameters p;
	
	parameters = new RooArgList("parameters");
	
	parameters->add(*(p.get("cg")));
	parameters->add(*(p.get("cgt")));
	parameters->add(*(p.get("ca")));
	parameters->add(*(p.get("cat")));
	parameters->add(*(p.get("cH")));
	parameters->add(*(p.get("cT")));
	parameters->add(*(p.get("cB")));
	parameters->add(*(p.get("cW")));
	parameters->add(*(p.get("cHW")));
	parameters->add(*(p.get("cHWt")));
	parameters->add(*(p.get("cHB")));
	parameters->add(*(p.get("cHBt")));
	parameters->add(*(p.get("mu")));

	
}

// ---------------------------------------------------------------------------------------
// Init relations that relate the free parameters with the measured observables

void PDF_HGam_Meas_Run2::initRelations() 
{
   
   // Fetch all Wilson coefficients
   RooRealVar *cg   = (RooRealVar*)parameters->find("cg");
   RooRealVar *cgt  = (RooRealVar*)parameters->find("cgt");
   RooRealVar *ca   = (RooRealVar*)parameters->find("ca");
   RooRealVar *cat  = (RooRealVar*)parameters->find("cat");
   RooRealVar *cH   = (RooRealVar*)parameters->find("cH");
   RooRealVar *cT   = (RooRealVar*)parameters->find("cT");
   RooRealVar *cB   = (RooRealVar*)parameters->find("cB");
   RooRealVar *cW   = (RooRealVar*)parameters->find("cW");
   RooRealVar *cHW  = (RooRealVar*)parameters->find("cHW");
   RooRealVar *cHWt = (RooRealVar*)parameters->find("cHWt");
   RooRealVar *cHB  = (RooRealVar*)parameters->find("cHB");
   RooRealVar *cHBt = (RooRealVar*)parameters->find("cHBt");
   RooRealVar *mu   = (RooRealVar*)parameters->find("mu");

   // Setup theory list
   theory = new RooArgList("theory");
   
   // Add observables
   int nKey(0);
   
   for(auto HepDataKey: HepDataKeys) {

     VecD XSecs = MeasurementReader->getXS(HepDataKey);
     
     int nBin(0);
     
     for(auto XSec : XSecs) {
      Str Tag   = Form("%s:%d_th",HepDataKey.Data(),nBin); Tag.ReplaceAll("/","");
      Str Label = Form("%s:%d_th",HepDataKey.Data(),nBin);
      StrV PDIFiles = Vectorize(ipol->GetValue("ProfDriverIpolFiles","")," ");
      StrVV PDIVars =  Vectorize(ipol->GetValue("ProfDriverIpolVars",""),":"," ");
 //added by glu     
     cout<<"Test"<<endl;
     for (int i = 0 ; i < PDIFiles.size(); i ++) cout<<PDIFiles[i]<<endl;
	cout<<"Vars"<<endl;
  for (int i = 0 ; i < PDIVars.size(); i++)
{
cout<<"i="<<i<<endl;
 for (int j = 0 ; j < PDIVars[i].size(); j++)
{cout<<"j="<<j<<endl;
 cout<<PDIVars[i][j]<<endl;
}
}	      

      // Is this a normalized scan? 
      bool NormalizedScan = set->GetValue("NormalizedScan",false);
       
      // Get the name of the variable
      Str XSecName = HepDataKey; XSecName.ReplaceAll(set->GetValue("HepDataBase",""),"");
      
      // Fetch the corresponding SM cross sections    
      VecD ggf_XSecSM = VectorizeD(set->GetValue(Form("%s.ggf.XSec",XSecName.Data()),"")," "),
           vbf_vh_XSecSM = VectorizeD(set->GetValue(Form("%s.vbf_vh.XSec",XSecName.Data()),"")," "),
           tth_XSecSM = VectorizeD(set->GetValue(Form("%s.ttH.XSec",XSecName.Data()),"")," ");
           
       cout << "Found the following SM Cross sections (ggf,vbf_vh,ttH): " <<  ggf_XSecSM[nBin] << "\t" <<   vbf_vh_XSecSM[nBin] << "\t" <<   tth_XSecSM[nBin] << endl; 

      // Now add the RooProfDriverWrapper to the theory object      
      theory->add( *(new RooProfDriverWrapper(Tag, Label, PDIFiles, PDIVars, ggf_XSecSM[nBin], vbf_vh_XSecSM[nBin], tth_XSecSM[nBin], NormalizedScan, vbf_vh_XSecSM, *cg, *cgt, *ca, *cat, *cH, *cT, *cB, *cW, *cHW, *cHWt, *cHB, *cHBt, *mu)) );                  
      
      nBin++; nKey++;
     }
    

   }

   
}

// ---------------------------------------------------------------------------------------
// Initalize Observables 

void PDF_HGam_Meas_Run2::initObservables() 
{

   // Setup observable list
   observables = new RooArgList("observables");
	
   
   for(auto HepDataKey: HepDataKeys) {

     int nBin(0);

     VecD XSecs = MeasurementReader->getXS(HepDataKey);
     for(auto XSec : XSecs) {
      Str Tag = Form("%s:%d_obs",HepDataKey.Data(),nBin); Tag.ReplaceAll("/","");
      observables->add(*(new RooRealVar(Tag,Tag, 0, -1e4, 1e4)));  
      nBin++;
     }

   }
   

	
}

// ---------------------------------------------------------------------------------------
// Init Observables 

void PDF_HGam_Meas_Run2::setObservables(TString c) 
{

   if(c.EqualTo("SMAsimov")) {
      cout << " Setting Scan central values to correspond to a SM Asimov" << endl;
	  setObservablesTruth();  
   }	  
   else if(c.EqualTo("SMToy")) {    
	  setObservablesToy();
	  // pdf->Delete();
	 } 
   else {
	  
   // Add observables      
    for(auto HepDataKey: HepDataKeys) {

     VecD XSecs = MeasurementReader->getXS(HepDataKey);

     int nBin(0);
       
     for(auto XSec : XSecs) {

      Str Tag = Form("%s:%d_obs",HepDataKey.Data(),nBin); Tag.ReplaceAll("/","");
	  setObservable(Tag, XSec);
	  cout << Tag << "\t XSec = " << XSec << endl;
      nBin++;
     }

    }
   
   }

}

// ---------------------------------------------------------------------------------------
// Init Observable Uncertainties 

void PDF_HGam_Meas_Run2::setUncertainties(TString c) 
{

   // The first error is stat. and the second systematical
   // The second will be assumed to be a 100% correlated by default
   // unless we intervene, but since it's set to zero it does not matter

   
   for(auto HepDataKey: HepDataKeys) {
   
     int nBin(0);   

     VecD XSecTotErrors = MeasurementReader->getXS_TotErr(HepDataKey);
     for(auto XSecTotError : XSecTotErrors) {
      Str Tag = Form("%s:%d_obs",HepDataKey.Data(),nBin); Tag.ReplaceAll("/","");
	  setUncertainty(Tag, XSecTotError, 0.);	
	  cout << Tag << "\t XSecTotError = " << XSecTotError << endl;
	    
      nBin++;
     }

   } 

}

// ---------------------------------------------------------------------------------------
// Set Correlations

void PDF_HGam_Meas_Run2::setCorrelations(TString c) 
{
	resetCorrelations();

    TMatrixD CovTot(nObs,nObs);
    CovTot = MeasurementReader->getFullCov();
    
    for(int xi = 0; xi < nObs; xi++)
     for(int yi = 0; yi < nObs; yi++) 
        corStatMatrix[xi][yi] = CovTot[xi][yi]/sqrt(CovTot[xi][xi])/sqrt(CovTot[yi][yi]);     

	return;
}


// ---------------------------------------------------------------------------------------
// Build final PDF -- we model the observables / measurements with a multivariate Gaussian

void PDF_HGam_Meas_Run2::buildPdf() 
{
	
	// Use adaptive covariance? 
    bool AdaptiveTheoryCovariance = set->GetValue("AdaptiveTheoryCovariance",false);
    
    pdf = AdaptiveTheoryCovariance ? (RooAbsPdf*) new RooHGamEFTMultiVarGaussian("pdf_"+name, "pdf_"+name, *(RooArgSet*)observables, *(RooArgSet*)theory, covMatrix, MeasurementReader) 
                                   : (RooAbsPdf*) new RooMultiVarGaussian("pdf_"+name, "pdf_"+name, *(RooArgSet*)observables, *(RooArgSet*)theory, covMatrix);

    // Old implementation
	// pdf = new RooMultiVarGaussian("pdf_"+name, "pdf_"+name, *(RooArgSet*)observables, *(RooArgSet*)theory, covMatrix);

				
}

// ---------------------------------------------------------------------------------------

