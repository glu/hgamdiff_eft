/**
 * Gamma Combination
 * Authors: Till Moritz Karbach, moritz.karbach@cern.ch
 *          Matthew Kenzie, matthew.kenzie@cern.ch
 *
 * HGam EFT Scanner
 * 
 * Authors: Florian Urs Bernlochner, florian.bernlochner@cern.ch,
 *          Christopher Meyer, chris.mejer@cern.ch,
 *          Jim Lacey, jim.lacey@cern.ch,
 *          Yanping Huang, yanping.huang@cern.ch,
 *          Maria Josefina Alconada Verzini, josefina.alconada@cern.ch
 * 
 * Date: Oct 2016
 *
 **/

#include <stdlib.h>
#include "GammaComboEngine.h"
#include "PDF_HGam_Meas_Run1.h"
#include "PDF_HGam_Meas_Run2.h"
#include "PDF_HGam_Meas_Run2_Couplings.h"

#include "HGamUtils.h"
 
using namespace std;
using namespace RooFit;
using namespace Utils;

typedef TString Str;
typedef vector<TString> StrV;

TEnv* OpenSettingsFile(Str FileName);

//-------------------------------------------------------------------------------------------

int main(int argc, char* argv[])
{

	GammaComboEngine gc("HGamEFTScanner", argc, argv);

	///////////////////////////////////////////////////
	//
	// define PDFs
	//
	///////////////////////////////////////////////////
        
           
	gc.addPdf(1, new PDF_HGam_Meas_Run2("./scan_run2_incl.conf","./cHW_cHWt.conf"), "Run2_incl_cHW_cHWt");
	gc.addPdf(2, new PDF_HGam_Meas_Run2("./scan_run2_Njets.conf","./cHW_cHWt.conf"), "Run2_Njets_cHW_cHWt");
	gc.addPdf(3, new PDF_HGam_Meas_Run2("./scan_run2_pTH.conf","./cHW_cHWt.conf"), "Run2_pTH_cHW_cHWt");

	gc.addPdf(4, new PDF_HGam_Meas_Run2("./scan_run2_incl.conf","./cg_cgt.conf"), "Run2_incl_cg_cgt");
	gc.addPdf(5, new PDF_HGam_Meas_Run2("./scan_run2_Njets.conf","./cg_cgt.conf"), "Run2_Njets_cg_cgt");
	gc.addPdf(6, new PDF_HGam_Meas_Run2("./scan_run2_pTH.conf","./cg_cgt.conf"), "Run2_pTH_cg_cgt");
		
	gc.addPdf(7, new PDF_HGam_Meas_Run2_Couplings("./scan_run2_incl_mu.conf"), "Run2_incl_mu_scan");
		
		
	// gc.addPdf(2, new PDF_HGam_Meas_Run1("Alternative","Alternative","Alternative"), "Run2_Alternative");

	///////////////////////////////////////////////////
	//
	// Define combinations
	//
	///////////////////////////////////////////////////

	gc.newCombiner(0, "empty", "empty");

    // Combiners for 1D Scans
	gc.newCombiner(1, "cHW", "EFT Run 2 Scan of cHW",  1);
	gc.newCombiner(2, "cHWt", "EFT Run 2 Scan of ~cHW",  1);
	
    // Combiners for 2D Scans	
	gc.newCombiner(3, "cHW:cHWt", "EFT Run 2 Scan of cHW:~cHW",  1);
	gc.newCombiner(4, "cHW:cHWt_Njets", "EFT Run 2 Scan of cHW:~cHW: Njets only",  2);
	gc.newCombiner(5, "cHW:cHWt_pTH", "EFT Run 2 Scan of cHW:~cHW: pTH only",  3);

    // Combiners for 1D Scans
	gc.newCombiner(6, "cg", "EFT Run 2 Scan of cg",  4);
	gc.newCombiner(7, "cgt", "EFT Run 2 Scan of ~cg",  4);

    // Combiners for 2D Scans	
	gc.newCombiner(8, "cg:cgt", "EFT Run 2 Scan of cg:~cg",  4);
	gc.newCombiner(9, "cg:cgt_Njets", "EFT Run 2 Scan of cg:~cg: Njets only",  5);
	gc.newCombiner(10, "cg:cgt_pTH", "EFT Run 2 Scan of cg:~cg: pTH only",  6);

    // Combiners for 1D Scans
	gc.newCombiner(11, "ca", "EFT Run 2 Scan of ca",  4);
	gc.newCombiner(12, "cat", "EFT Run 2 Scan of ~ca",  4);

    // Combiners for 2D Scans	
	gc.newCombiner(13, "ca:cg", "EFT Run 2 Scan of ca:cg",  4);
	gc.newCombiner(14, "ca:cg_Njets", "EFT Run 2 Scan of ca:cg: Njets only",  5);
	gc.newCombiner(15, "ca:cg_pTH", "EFT Run 2 Scan of ca:cg: pTH only",  6);
	
	///////////////////////////////////////////////////
	//
	// Coupling Strength Scan
	//
	///////////////////////////////////////////////////	
	
	gc.newCombiner(16, "mus", "Coupling Strength Scan",  7);
	
	///////////////////////////////////////////////////
	//
	// Run
	//
	///////////////////////////////////////////////////

	gc.run();
	
}

//-------------------------------------------------------------------------------------------
