
## Modified README (AT - sep. 2018)
setupATLAS
localSetupROOT 6.08.02-x86_64-slc6-gcc49-opt

mkdir EFT_scan
cd EFT_scan
export CC=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Gcc/gcc493_x86_64_slc6/slc6/gcc49/bin/gcc
export CXX=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Gcc/gcc493_x86_64_slc6/slc6/gcc49/bin/g++


## Checkout the package
git clone https://github.com/gammacombo/gammacombo.git

cd gammacombo
mkdir build

AT : need to updated the CMakeList file to include latest C++ :
L109 :   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y")

cd build
cmake ..
make -j4
make install

cd ..
git clone HGamEFTScanner (from the EFT directory)


cd cmake
vi combiners.cmake

## Add HGamEFTScanner

>> Download SCONS
wget http://prdownloads.sourceforge.net/scons/scons-2.5.1.tar.gz
Untar it and execute
python setup.py install --prefix=MYPATHTOSCONS
export PATH=$PATH:MYPATHTOSCONS

>> Download ProfDriver
wget http://www.hepforge.org/archive/professor/prof-driver-0.4.0-for-2.3.1.tar.gz
untar
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/sft.cern.ch/lcg/releases/LCG_87/MCGenerators/professor/2.2.1/x86_64-slc6-gcc49-opt/lib
scons --with-professor=/cvmfs/sft.cern.ch/lcg/releases/LCG_87/MCGenerators/professor/2.2.1/x86_64-slc6-gcc49-opt/ --prefix=/afs/cern.ch/user/#/##/EFT_scan/gammacombo/prof-driver-0.4.0-for-2.3.1

>> Edit CMakeList of HGamEFTScanner to point to bin and lib directories of professor driver


cd ../build
cmake ..

make -j4
make install


Structure:
----------

Scanner:

vi main/HGamEFTScanner.cpp

PDFs and Combiners
------------------

|| PDFs --> Our measurement - prediction

Can be several, e.g. Run 1 v Run 2 or several (uncorrelated inputs), such as GamGam and 4lep

|| Combiner --> Different combination scenarios

Different scenarios of what you want to combine; need to specify what PDFs goe in plus a unique tag


Parameters
----------

vi src/EFT_Parameters.cpp

List of all parameters we want to scan, fit or profile

unique tag needed, starting values plus ranges can be specified

vi include/EFT_Parameters.cpp

PDFs
----

vi include/PDF_HGam_Meas_Run2.h

inherits from PDF_Abs

  buildPdf(); --> Builds PDF

  initObservables(); --> Creates and initializes measurements
  initParameters();  --> Initializes the free parameters we want to scan, fit or profile
  initRelations();   --> Connects the two measurements:parameters

  setCorrelations(TString c); --> Sets the correlation between measurements
  setObservables(TString c);  --> Sets the central values of measurements
  setUncertainties(TString c); --> Sets the errors of measurements


vi src/PDF_HGam_Meas_Run2.cpp

        PDF_HGam_Meas_Run2::PDF_HGam_Meas_Run2(TString cObs, TString cErr, TString cCor)
: PDF_Abs(2) <-- 2: # of measurements we want to include / dimensionality of our PDF

* Constructor: Sets up parameters, relations, etc., calls inherited function to buildCov and then sets up the final PDF.

* initParameters: fetches the EFT_Parameters we created earlier and puts them into a list of parameters inherited from PDF_Abs

* initRelations: fetches the parameters from the 'parameters' object and sets up the relation between them and the observables (measurements)

 two examples: RooFormulaVar and an external Class (RooProfDriverWrapper)

 Notice the 'NAMe_th' at the end of the name of the FormulaVar and Wrapper. GammaCombo uses this to relate a pair of observable:theory to each other (so each prediction needs this in its TName)

* initObservables: sets up the measurements

* setObservables & setUncertainties and setCorrelatoins

* buildPdf: builds the PDF, here as a multivariate Gaussian (notice covMatrix, observables and theory)



External class as a predictor
-----------------------------

vi CMakeLists.txt

# Define the custom ROOT objects
IF(HAS_CUSTOMROOTOBJECTS)
        SET(COMBINER_DICTIONARY_LINKDEF
                HGamEFTScannerLinkDef.h
        )
        SET(COMBINER_DICTIONARY_SOURCES
                RooProfDriverWrapper.h
        )
ENDIF()


vi include/HGamEFTScannerLinkDef.h

#pragma link C++ class RooProfDriverWrapper+;

vi include/RooProfDriverWrapper.h

* Structure given, especially the clone and evalaute function are fixed (this is how RooFit will query the object; the second constructor is needed as RooFit creates a copy of the original object when importing it into a workspace)


vi src/RooProfDriverWrapper.cpp

Structure should be clear; here we would include the ProfDriver from Holger, keeping in mind that we would want to initialize it not in each fit step, but in the constructor (and then pass the pointer of the driver over in the second constructor, otherwise it won't be initialized). Then we will load and create the interpolation objects only once


// ---------------------------------------------------------------------------------------
// Evaluate Function

Double_t RooProfDriverWrapper::evaluate() const
{
  double value(0); ;

  if(WrapperName.Contains("bin1"))
   value = Double_t(cg);

  if(WrapperName.Contains("bin2"))
   value = Double_t(cgt);

  return value;

}


Ok, let's have some fun

Scans:

bin/HGamEFTScanner

#bin/HGamEFTScanner --var cg --fix 'cgt=0' -c 1 --scanrange -1:1 --npoints 100

../build/bin/HGamEFTScanner --var cg --fix 'cgt=0' -c 1 --scanrange -1:1 --npoints 100


bin/HGamEFTScanner --var cgt\
    --fix 'cg=0'\
    -c 2\
    --scanrange -1:1\
    --npoints 100



bin/HGamEFTScanner --var cg --var cgt\
    --fix ''\
    -c 3\
    --scanrange -1:1 --scanrangey -1:1\
    --npoints 100



bin/HGamEFTScanner --var cg --var cgt\
    --fix ''\
    -c 4\
    --scanrange -1:1 --scanrangey -1:1\
    --npoints 100



bin/HGamEFTScanner --var cg --var cgt\
    --fix ''\
    -c 5\
    --scanrange -1:1 --scanrangey -1:1\
    --npoints 100
