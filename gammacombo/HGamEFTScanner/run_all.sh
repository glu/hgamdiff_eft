#ext=newAT
#ext=newGL
#ext=fixTheoCov
#ext=Marginalized
#ext=linearOnly2D
#ext=LinearOnly
#ext=test
#ext=final_linear_data
#ext=final_linear_quad_data
#ext=linear_marginal
#ext=2dscan_linear_obs_v2
#ext=2dscan_linear_obs_final
#ext=marginal_linear_quad_obs
#ext=linear_quad_exp
ext=2dscan_linear_quad_obs_final
#ext=raw_2dscan_linear_obs
#ext=linear_quad_obs

#for eff in cHWt cHBt cHWBt;do
#for eff in cg cgt;do
#for eff in cHW cHWt cHWBt;do
#for eff in cg cgt cHW cHB cHWB cHWt cHBt cHWBt;do
#for eff in cHWt cHBt cHWBt;do
#for eff in cHW;do
#for eff in cHWt cHBt cHWBt;do
#for eff in cHW cHB cHWB cHWt cHBt cHWBt;do
#for eff in cg_vs_cgt;do
for eff in cg_vs_cgt cHW_vs_cHWt cHB_vs_cHBt cHWB_vs_cHWBt;do
#for eff in cHWBt;do
#for eff in cgt cHWt cHBt cHWBt;do
#for eff in cg cgt cHW cHWt cHB cHBt cHWB cHWBt;do
#for eff in cHW_vs_cHWt cHB_vs_cHBt cHWB_vs_cHWBt cHW_cHWB cHB_cHWB;do
#source run_linear_quad.sh ${eff} ${ext}
source run_linear_quad_data.sh ${eff} ${ext}
#source run_linear.sh ${eff} ${ext}
#source run_linear_data.sh ${eff} ${ext}
done
