#ifndef _HGamMeasurementReader_h
#define _HGamMeasurementReader_h

#include <iostream>
#include <vector>
#include <map>
#include <time.h>
#include <string>
#include <cmath>
#include <fstream>

#include <TEnv.h>
#include <TString.h>
#include <TObjString.h>
#include <TObjArray.h>
#include <TSystem.h>
#include <TMath.h>
#include <TMatrixD.h>

#include "HGamUtils.h"

using namespace std;

namespace HGMR {


  
  /// /brief HGamMeasurementReader:
  ///   class for reading HepData input and returning measurements and errors in a useful format
  ///   most/all of the functions taken from Hype framework (some small modifications as required)
  /// /authors: J. Lacey, F. Bernlochner, D. Gillberg, H. Schulz, R. Kowalewski, A. Pilkington, M. Queitsch-Maitland)
  
  
  // could be relocated to a common file used in the GammaCombo package
  // as I assume these typedrfs will be common to other classes
  // ...but I need them for the current class ;-)
  //! \brief TString
  typedef TString Str;
  //! \brief unsigned integer
  typedef unsigned int uint;
  //! \brief string vector
  typedef vector<TString> StrV;
  //! \brief vector of StrV
  typedef vector<StrV> StrVV;
  //! \brief vector of integers
  typedef vector<int> VecI;
  //! \brief vector of doubles
  typedef vector<double> VecD;
  //! \brief vector of TMatrices
  typedef vector<TMatrixD> TMatrixVec;
  
  class HGamMeasurementReader {
    
  public:
    
    /// @name Standard constructors and destructors.
    //@{
    
    //! \brief constructor
    //! \param TString hepdata_file string of the full path to the input HepData file
    //! \param vector<TString> var_keys set of unique identiifers (keys) for measurement variable to be read 
    //HGamMeasurementReader( Str measurements_name );
    //HGamMeasurementReader( Str hepdata_file, StrV var_keys );
    HGamMeasurementReader(TEnv *set);
    //@}
    
    //! \brief specify input HepData file and list of measurements to load
    void loadMeasurements( Str hepdata_file, StrV hepdata_keys );

    //! \brief Read in HepData multiple table
    void readHepDataTable(Str infile, StrV keys,int column = 1);

    //! \brief Read in HepData
    void readHepDataTable(Str infile, Str key, StrV &err_keys, int column = 1);
    void readHepDataTable( Str file, Str key,
			   VecD &bins,
			   TMatrixD &xsec, TMatrixD &cov, TMatrixD &errs,
			   StrV &err_keys,
			   int column);
    //! \brief Add two matrices or vectors by resizing the first vector
    void AddMatrices(TMatrixD &vec1, TMatrixD &vec2, bool isvector = false);


    //! \brief Function for getting the measurement binning given the var key
    inline VecD getBins(Str key) { return _bins[key]; }

    //! \brief Function for getting/returning the measured cross sections for a specified variable (key)
    inline VecD getXS(Str key) {
      VecD Xsecs;
      for(int j = 0; j < _fullkeys.size(); j++)
	if(_fullkeys[j].Contains(key))
	  Xsecs.push_back(_xsec(0,j));
      return Xsecs;
    }

    //! \brief Function for retrieving the measured cross section total errors for a specified variable (key)
    inline VecD getXS_TotErr(Str key) {
      VecD XSTotErrs;
      for(int j = 0; j < _fullkeys.size(); j++)
	if(_fullkeys[j].Contains(key))
	  XSTotErrs.push_back(sqrt(_totalCov(j,j)) );
      return XSTotErrs;
    }

    //! \brief Function for retrieving the measured cross section stat errors (from HepData file) for a specified variable (key)
    inline VecD getXS_StatErr(Str key) { 
      VecD XSstatErrs;
      for(int i = 0; i < _keys.size(); i++)
	if(_keys[i] == key)
	  for(int j = 0; j < _errV[i].GetNrows(); j++) 
	    XSstatErrs.push_back( _errV[i](j,0) );          
      return XSstatErrs;
    }
 
    //! \brief Function for getting/returning the full covariance of specified keys
    inline TMatrixD getFullCov() {
      return _totalCov;
    } 
    
    inline TMatrixD getFull_th_ggf_Cov() {
      return _th_ggf_CovSub;
    }     

    inline TMatrixD getFull_th_vbf_vh_Cov() {
      return _th_vbf_vh_CovSub;
    }    
    
    inline TMatrixD getFull_th_ttH_Cov() {
      return _th_ttH_CovSub;
    }          
    
    //! \brief Function for setting the full theory corr matrix for all keys
    void initFullTheoryCovMatrix( );
    //! \brief Function for setting the full stat corr matrix for all keys
    void initFullMatrixStatCorr( );
    //! \brief Function for setting the stat cov sub matrix for specified keys
    void setStatCovMatrix(StrV keys);
    //! \brief Function for setting the corr sub matrix for specified keys
    void setSubMatrices(StrV keys);
    //! \brief Function for setting the stat corr sub matrix for specified keys
    void setSubMatrix(StrV keys, bool doStat);
    //! \brief Function for building the full covariance after all components have been set
    void setFullCov(StrV keys);

    //! \brief Function for getting the stat corr sub matrix for specified keys
    //inline TMatrixD getStatCorrSubMatrix(StrV keys) {
    //return _statCorrSub;
    //}
    
  private:

    TEnv *_set;
    map<Str,VecD> _bins, _binsAll;
    map<Str,int > _blockIndexCorr; 
    map<Str,Str> _var_key_map;
    StrV _binKeysCorr;
    TMatrixD _statCorr, _statCovSub;
    TMatrixD _thCov, _thCovSub;
    TMatrixD _th_ggf_Cov, _th_ggf_CovSub;
    TMatrixD _th_vbf_vh_Cov, _th_vbf_vh_CovSub;
    TMatrixD _th_ttH_Cov, _th_ttH_CovSub;
    TMatrixD _totalCov;

    StrV _keys;
    StrV _fullkeys, _binkeys;
    TMatrixD _xsec, _cov, _cov_syst, _errs;
    TMatrixD _corr;
    TMatrixVec _errV;
    
    void printTime();
    void fatal(Str msg);
    bool fileExists(Str fn);
    StrV vectorize(Str str, Str sep);
    Str stringSection(Str str, Str sep, int index);
    VecD vectorizeD(Str str, Str sep);
    
  };

}


#endif


