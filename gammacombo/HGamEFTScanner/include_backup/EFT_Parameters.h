/**
 * Author: Till Moritz Karbach, moritz.karbach@cern.ch
 * Date: April 2013
 *
 * Abstract class to define the (nuisance) parameters:
 * Gamma Combination
 *
 **/

#ifndef EFT_Parameters_h
#define EFT_Parameters_h

#include "ParametersAbs.h"
#include "OptParser.h"
#include "Utils.h"

using namespace std;
using namespace Utils;

class EFT_Parameters : public ParametersAbs
{
public:
    EFT_Parameters();
    inline ~EFT_Parameters(){};
    
protected:
    void defineParameters();
};

#endif
