/**
* Gamma Combination
* Authors: Till Moritz Karbach, moritz.karbach@cern.ch
*          Matthew Kenzie, matthew.kenzie@cern.ch
*
* HGam EFT Scanner
*
* Authors: Florian Urs Bernlochner, florian.bernlochner@cern.ch,
*          Christopher Meyer, chris.mejer@cern.ch,
*          Jim Lacey, jim.lacey@cern.ch,
*          Yanping Huang, yanping.huang@cern.ch,
*          Maria Josefina Alconada Verzini, josefina.alconada@cern.ch
*
* Date: Oct 2016
*
**/

#include <stdlib.h>
#include "GammaComboEngine.h"
#include "PDF_HGam_Meas_Run1.h"
#include "PDF_HGam_Meas_Run2.h"
#include "PDF_HGam_Meas_Run2_Couplings.h"
#include "HGamUtils.h"
#include "PDF_HGam_Meas_Run2_linear.h"


using namespace std;
using namespace RooFit;
using namespace Utils;

typedef TString Str;
typedef vector<TString> StrV;

TEnv* OpenSettingsFile(Str FileName);

//-------------------------------------------------------------------------------------------

int main(int argc, char* argv[])
{

	GammaComboEngine gc("HGamEFTScanner", argc, argv);

	///////////////////////////////////////////////////
	//
	// define PDFs
	//
	///////////////////////////////////////////////////



	//gc.addPdf(2, new PDF_HGam_Meas_Run2("./scan_run2_Njets.conf","./cHW_cHWt.conf"), "Run2_Njets_cHW_cHWt");
	// gc.addPdf(3, new PDF_HGam_Meas_Run2("./scan_run2_pTH.conf","./cHW_cHWt.conf"), "Run2_pTH_cHW_cHWt");
	//
	// gc.addPdf(1, new PDF_HGam_Meas_Run2("./scan_run2_incl.conf","./cg_cgt.conf"), "Run2_incl_cg_cgt");
	//gc.addPdf(1, new PDF_HGam_Meas_Run2("./scan_run2_incl_80.conf","./cg_cgt.conf"), "Run2_incl_cg_cgt_80");
	// gc.addPdf(3, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./cg_cgt.conf"), "Run2_incl_cg_cgt_140");
	// gc.addPdf(1, new PDF_HGam_Meas_Run2("./scan_run2_incl_80.conf","./cHW_cHWt.conf"), "Run2_incl_cHW");
	// gc.addPdf(1, new PDF_HGam_Meas_Run2_linear("./scan_run2_incl_80.conf","./cHW_cHWt.conf"), "Run2_linear_cHW_80");
	// gc.addPdf(2, new PDF_HGam_Meas_Run2_linear("./scan_run2_incl_140.conf","./cHW_cHWt.conf"), "Run2_linear_cHW_140");


	// gc.addPdf(1, new PDF_HGam_Meas_Run2_linear("./scan_run2_incl_80.conf","./cHW_cHWt.conf"), "Run2_80");
	// gc.addPdf(1, new PDF_HGam_Meas_Run2_linear("./scan_run2_incl.conf","./cHW_cHWt.conf"), "Run2_140");

	// gc.addPdf(1, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHW.conf"), "SMEFT_cHW");
	// gc.addPdf(2, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHWt.conf"), "SMEFT_cHWt");
	//
	// gc.addPdf(1, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHG.conf"), "SMEFT_140_cHG");
	// gc.addPdf(2, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHGt.conf"), "SMEFT_140_cHG");
	// gc.addPdf(3, new PDF_HGam_Meas_Run2_linear("./scan_run2_incl_140.conf","./SMEFT_cHW.conf"), "SMEFT_140_cHG");
	// gc.addPdf(4, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHWt.conf"), "SMEFT_140_cHG");
	// gc.addPdf(5, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHB.conf"), "SMEFT_140_cHG");
	// // gc.addPdf(6, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHBt.conf"), "SMEFT_140_cHG");
	// gc.addPdf(7, new PDF_HGam_Meas_Run2_linear("./scan_run2_incl_140.conf","./SMEFT_cHWB.conf"), "SMEFT_140_cHG");
	// gc.addPdf(8, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHWBt.conf"), "SMEFT_140_cHG");

		// gc.addPdf(1, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHG.conf"), "SMEFT_140_cHG");
		// gc.addPdf(2, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHGt.conf"), "SMEFT_140_cHG");
		// gc.addPdf(3, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHW.conf"), "SMEFT_140_cHG");
		// gc.addPdf(4, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHWt.conf"), "SMEFT_140_cHG");
		// gc.addPdf(5, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHB.conf"), "SMEFT_140_cHG");
		// gc.addPdf(6, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHBt.conf"), "SMEFT_140_cHG");
		// gc.addPdf(7, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHWB.conf"), "SMEFT_140_cHG");
		// gc.addPdf(8, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SMEFT_cHWBt.conf"), "SMEFT_140_cHG");
		//
		// gc.addPdf(9, new PDF_HGam_Meas_Run2_linear("./scan_run2_incl_140.conf","./SMEFT_cHG_cHW.conf"), "SMEFT_140_cHG");
		// gc.addPdf(10, new PDF_HGam_Meas_Run2_linear("./scan_run2_incl_140.conf","./SMEFT_cHW_cHWt.conf"), "SMEFT_140_cHG");
		// gc.addPdf(11, new PDF_HGam_Meas_Run2_linear("./scan_run2_incl_140.conf","./SMEFT_cHB_cHBt.conf"), "SMEFT_140_cHG");
		// gc.addPdf(12, new PDF_HGam_Meas_Run2_linear("./scan_run2_incl_140.conf","./SMEFT_cHWB_cHWBt.conf"), "SMEFT_140_cHG");
		// gc.addPdf(13, new PDF_HGam_Meas_Run2_linear("./scan_run2_incl_140.conf","./SMEFT_cHG_cHGt.conf"), "SMEFT_140_cHG");
		gc.addPdf(1, new PDF_HGam_Meas_Run2("./scan_run2_incl_140.conf","./SILH_cat.conf"), "SMEFT_140_cHG");
		// gc.addPdf(15, new PDF_HGam_Meas_Run2_linear("./scan_run2_incl_140.conf","./SMEFT_cHW_cHB.conf"), "SMEFT_140_cHG");
		// gc.addPdf(16, new PDF_HGam_Meas_Run2_linear("./scan_run2_incl_140.conf","./SMEFT_cHW_cHWB.conf"), "SMEFT_140_cHG");


	///////////////////////////////////////////////////
	//
	// Define combinations
	//
	///////////////////////////////////////////////////

	gc.newCombiner(0, "empty", "empty");

	// Combiners for 1D Scans


	// gc.newCombiner(1, "cHG_final_summ2019_prof_data", "SMEFT : SMEFT Asimov Scan of #bar{C}_{HG} (linear)",  1);
	// gc.newCombiner(2, "cHGt_final_summ2019_prof_data", "SMEFT : SMEFT Asimov Scan of #tilde{C}_{HG} (linear)",  2);
	// gc.newCombiner(3, "cHW_final_summ2019_prof_data", "SMEFT : SMEFT data Scan of #bar{C}_{HW} (linear)",  3);
	// gc.newCombiner(4, "cHWt_final_summ2019_prof_data", "SMEFT : SMEFT data Scan of #tilde{C}_{HW} (linear)",  4);
	// gc.newCombiner(5, "cHB_final_summ2019_prof_data", "SMEFT : SMEFT data Scan of #bar{C}_{HB} (linear)",  5);
	// gc.newCombiner(6, "cHBt_final_summ2019_prof_data", "SMEFT : SMEFT data Scan of #tilde{C}_{HB} (linear)",  6);
	// gc.newCombiner(7, "cHWB_final_summ2019_prof_data", "SMEFT : SMEFT data Scan of #bar{C}_{HWB} (linear)",  7);
	// gc.newCombiner(8, "cHWBt_final_summ2019_prof_data", "SMEFT : SMEFT data Scan of #tilde{C}_{HWB} (linear)",  8);

	// gc.newCombiner(9, "cHG_cHW_final_summ2019_test", "SMEFT : SMEFT Asimov Scan of #bar{C}_{HG} (linear)",  9);
	// gc.newCombiner(10, "cHW_cHWt_final_summ2019_data", "SMEFT : SMEFT Asimov Scan of #bar{C}_{HG} (linear)",  10);
	// gc.newCombiner(11, "cHB_cHBt_final_summ2019_data", "SMEFT : SMEFT Asimov Scan of #bar{C}_{HG} (linear)",  11);
	// gc.newCombiner(12, "cHWB_cHWBt_final_summ2019_data", "SMEFT : SMEFT Asimov Scan of #bar{C}_{HG} (linear)",  12);
	// gc.newCombiner(13, "cHG_cHGt_final_summ2019_data", "SMEFT : SMEFT Asimov Scan of #bar{C}_{HG} (linear)",  13);
	//
	// gc.newCombiner(14, "cHW_marginalized_final_summ2019_test", "SMEFT : SMEFT Asimov Scan of #bar{C}_{HG} (linear)",  14);
	// gc.newCombiner(15, "cHG_marginalized_final_summ2019_data", "SMEFT : SMEFT Asimov Scan of #bar{C}_{HG} (linear)",  14);
	// gc.newCombiner(16, "cHB_marginalized_final_summ2019_data", "SMEFT : SMEFT Asimov Scan of #bar{C}_{HG} (linear)",  14);
	// gc.newCombiner(17, "cHWB_marginalized_final_summ2019_data", "SMEFT : SMEFT Asimov Scan of #bar{C}_{HG} (linear)",  14);
		// gc.newCombiner(17, "cHW_marginalized_final_summ2019_3d", "SMEFT : SMEFT Asimov Scan of #bar{C}_{HG} (linear)",  14);
		gc.newCombiner(1, "SILH_summer2019_asimov_pT", "SMEFT : SMEFT Asimov Scan of #tilde{c}_{g} (linear)",  1);
		gc.newCombiner(2, "SILH_summer2019_asimov_Njet", "SMEFT : SMEFT Asimov Scan of #tilde{c}_{g} (linear)",  1);
                gc.newCombiner(3, "SILH_summer2019_asimov_pTj1", "SMEFT : SMEFT Asimov Scan of #tilde{c}_{g} (linear)",  1);
                gc.newCombiner(4, "SILH_summer2019_asimov_mjj", "SMEFT : SMEFT Asimov Scan of #tilde{c}_{g} (linear)",  1);
                gc.newCombiner(5, "SILH_summer2019_asimov_dphijj", "SMEFT : SMEFT Asimov Scan of #tilde{c}_{g} (linear)",  1);


		// //
// gc.newCombiner(18, "cHW_cHB_marginalized_final_summ2019_asimov", "SMEFT : SMEFT Asimov Scan of #bar{C}_{HG} (linear)",  15);



	gc.run();

}

//-------------------------------------------------------------------------------------------
