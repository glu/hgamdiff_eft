/**
 * Linear Interpolator
      follows closely the ProfDriverWrapper
      Author : Ahmed TAREK (ahabouel@cern.ch)
 * EFT ProfDriver Wrapper
 *
 * Authors: Holger Schulz, holger.schulz@cern.ch,
 *          Florian Urs Bernlochner, florian.bernlochner@cern.ch,
 *          Christopher Meyer, chris.mejer@cern.ch,
 *          Jim Lacey, jim.lacey@cern.ch,
 *          Yanping Huang, yanping.huang@cern.ch,
 *          Maria Josefina Alconada Verzini, josefina.alconada@cern.ch
 *
 * Date: October 2016
 *
 **/

#ifndef _LinearInterpolator
#define _LinearInterpolator

#include "RooRealProxy.h"
#include "RooCategoryProxy.h"
#include "RooAbsReal.h"
#include "RooAbsCategory.h"

#include "HGamUtils.h"


using namespace std;
using namespace RooFit;


// class LinearInterpolatorFile{
// public:
//   LinearInterpolatorFile(){};
//   TH1F * h_variation ; // histogram of the varied wilson coiefficient
//   TH1F * h_SM; // histogram of the nominal (SM)
//   double GenerationParameter; // value of the genrated wislon coiefficient
//   bool has_ggf_pd, has_vbf_vh_pd, has_tth_pd;
//   TString WC; // wilson
// };
//
// typedef std::map<Str , LinearInterpolatorFile*> MapLI;
class LinearInterpolator : public RooAbsReal
{

public:

  LinearInterpolator() {};
  LinearInterpolator(const char *name, const char *title, StrV _rootfilename, StrV _LinearInterpolatorVariables, double xs_ggh, double xs_vbf_vh, double xs_tth, bool NormalizedScan, VecD xs_vbf_vh_tot, RooAbsReal& cg, RooAbsReal& cgt, RooAbsReal& ca, RooAbsReal& cat, RooAbsReal& cH, RooAbsReal& cT, RooAbsReal& cB, RooAbsReal& cW, RooAbsReal& cHW, RooAbsReal& cHWt, RooAbsReal& cHB, RooAbsReal& cHBt, RooAbsReal& cHWB, RooAbsReal& cHWBt, RooAbsReal& lambda);

  LinearInterpolator(const LinearInterpolator& other, const char* name=0) ;
  virtual ~LinearInterpolator();
  virtual TObject* clone(const char* newname) const { return new LinearInterpolator(*this,newname); }
  Double_t evaluate() const;
  Double_t calculateHiggsDecayWidthRatio() const;
  Double_t calculateHiggstoDiphotonDecayWidthRatio() const;

  // ggF, VBF+VH, ttH predictions
  Double_t evaluate_ratio_ggF() const;
  Double_t evaluate_ratio_VBF_VH() const;
  Double_t evaluate_ratio_ttH() const;

protected:

 RooArgSet *cPars;
 RooRealProxy cg, cgt, ca, cat, cH, cT, cB, cW, cHW, cHWt, cHB, cHBt, cHWB, cHWBt, lambda;

 double xs_ggh, xs_vbf_vh, xs_tth;

 // For normalized scan
 VecD xs_vbf_vh_tot;

 bool has_ggf_pd, has_vbf_vh_pd, has_tth_pd;

private:

 Str WrapperName, HistName;
 StrV rootfilename;
 StrV LinearInterpolatorVariables;
 MapLI LinearInterpolatorFiles;

 // TH1F * h_variation ; // histogram of the varied wilson coiefficient
 // TH1F * h_SM; // histogram of the nominal (SM)
 // double GenerationParameter; // value of the genrated wislon coiefficient

 int BinNumber; bool NormalizedScan;


 ClassDef(LinearInterpolator, 1);

};

#endif
