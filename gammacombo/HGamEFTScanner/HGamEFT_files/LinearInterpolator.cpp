/*
A simple linear Interpolator
Ahmed TAREK ahabouel@cern.ch
follows closely RooProfDriverWrapper
*/

#include "Riostream.h"

#include "LinearInterpolator.h"

#include "RooAbsReal.h"
#include "RooAbsCategory.h"
#include <math.h>
#include "TMath.h"

// ---------------------------------------------------------------------------------------
// Constructors

//vector <ProfDriver> LinearInterpolator::v_pd;

LinearInterpolator::LinearInterpolator(const char *name, const char *title, StrV _rootfilename, StrV _LinearInterpolatorVariables, double _xs_ggh, double _xs_vbf_vh, double _xs_tth, bool _NormalizedScan, VecD _xs_vbf_vh_tot,
  RooAbsReal& _cg,
  RooAbsReal& _cgt,
  RooAbsReal& _ca,
  RooAbsReal& _cat,
  RooAbsReal& _cH,
  RooAbsReal& _cT,
  RooAbsReal& _cB,
  RooAbsReal& _cW,
  RooAbsReal& _cHW,
  RooAbsReal& _cHWt,
  RooAbsReal& _cHB,
  RooAbsReal& _cHBt,
  RooAbsReal& _cHWB,
  RooAbsReal& _cHWBt,
  RooAbsReal& _lambda
) :
RooAbsReal(name,title),
xs_ggh(_xs_ggh),
xs_vbf_vh(_xs_vbf_vh),
xs_tth(_xs_tth),
NormalizedScan(_NormalizedScan),
xs_vbf_vh_tot(_xs_vbf_vh_tot),
cg("cg","cg",this,_cg),
cgt("cgt","cgt",this,_cgt),
ca("ca","ca",this,_ca),
cat("cat","act",this,_cat),
cH("cH","cH",this,_cH),
cT("cT","cT",this,_cT),
cB("cB","cB",this,_cB),
cW("cW","cW",this,_cW),
cHW("cHW","cHW",this,_cHW),
cHWt("cHWt","cHWt",this,_cHWt),
cHB("cHB","cHB",this,_cHB),
cHBt("cHBt","cHBt",this,_cHBt),
cHWB("cHWB","cHWB",this,_cHWB),
cHWBt("cHWBt","cHWBt",this,_cHWBt),
lambda("lambda","lambda",this,_lambda)
{

  WrapperName = (Str) title;

  HistName = Vectorize(WrapperName,":").front();

  Str Bin = Vectorize(WrapperName.Data(),":").back().ReplaceAll("_th","");
  BinNumber = atoi( Bin.Data() );

  LinearInterpolatorVariables = _LinearInterpolatorVariables;
  rootfilename = _rootfilename;
  if(LinearInterpolatorVariables.size() != rootfilename.size())
  {
    cout << "Number of root files is not equal to number of variables to be scaned! exiting .." << endl;
    exit(0);
  }

  int iter = 0;
  for(auto filename : rootfilename)
  {

    Str WilsonCoff = LinearInterpolatorVariables[iter];
    cout << "Now checkig : " << filename << " for variable : " << WilsonCoff << endl;
    if(WilsonCoff == "lambda") continue;
    TFile *f = TFile::Open(filename);
    cout << HistName << endl;
    Str HistName_input = HistName;
    HistName_input.ReplaceAll("/HiggsDiphotonDifferentialCrossSection2016/","");
    LinearInterpolatorFiles[WilsonCoff] = new LinearInterpolatorFile();
    LinearInterpolatorFiles[WilsonCoff]->h_SM = (TH1F*) f->Get("SM_"+HistName_input);   // MG5 SM
    LinearInterpolatorFiles[WilsonCoff]->h_variation = (TH1F*) f->Get("BSM_"+HistName_input); // NP^2==1 component
    TH1F* h_genration_par = (TH1F*) f->Get("GenerationParameter");
    LinearInterpolatorFiles[WilsonCoff]->GenerationParameter = h_genration_par->GetBinContent(1)* 0.06; // NOTE : SMEFT *0.06;// * 0.06; // should * 0.06;
    LinearInterpolatorFiles[WilsonCoff]-> has_ggf_pd = false;
    LinearInterpolatorFiles[WilsonCoff]-> has_vbf_vh_pd = false;
    LinearInterpolatorFiles[WilsonCoff]-> has_tth_pd = false;
    cout << LinearInterpolatorFiles[WilsonCoff]-> has_ggf_pd << "\t" << LinearInterpolatorFiles[WilsonCoff]-> has_vbf_vh_pd << endl;

    if(filename.Contains("ggF")) LinearInterpolatorFiles[WilsonCoff]-> has_ggf_pd = true;
    if(filename.Contains("VBFVH")) LinearInterpolatorFiles[WilsonCoff]-> has_vbf_vh_pd = true;

    cout << LinearInterpolatorFiles[WilsonCoff]-> has_ggf_pd << "\t" << LinearInterpolatorFiles[WilsonCoff]-> has_vbf_vh_pd << endl;
    iter++;
    cout << filename << endl;

    // cout << "In zzz : " << filename << endl;
    // ofstream ofile;
    // Str outputfiletext = WrapperName+"_production.txt";
    // outputfiletext.ReplaceAll("/HiggsDiphotonDifferentialCrossSection2016/","");
    // outputfiletext.ReplaceAll(":","_");
    // cout << "Writing to " << outputfiletext.Data() << endl;
    // ofile.open(outputfiletext.Data(),std::ofstream::out | std::ofstream::app);
    // int ni = 0 ;
    // double VariedValue = LinearInterpolatorFiles.at(WilsonCoff)-> h_variation -> GetBinContent(BinNumber+1);
    // cout << "VariedValue " << VariedValue << endl;
    // VariedValue*=0.005/LinearInterpolatorFiles.at(WilsonCoff)->GenerationParameter;
    // cout << "VariedValue after scaling : " << VariedValue << endl;
    // double SMValue = LinearInterpolatorFiles.at(WilsonCoff)-> h_SM -> GetBinContent(BinNumber+1);
    // cout << "SMValue " << SMValue << endl;
    // VariedValue+=SMValue;
    //
    //
    //   ofile << "0.00086" << "\t" << VariedValue << endl;
    //   ofile << 0.0 << "\t" << SMValue << endl;
    //   ofile.close();
    //
    //   ofstream ofile_2;
    //   Str outputfiletext_2 = WrapperName+"_productiondecay.txt";
    //   outputfiletext_2.ReplaceAll("/HiggsDiphotonDifferentialCrossSection2016/","");
    //   outputfiletext_2.ReplaceAll(":","_");
    //   cout << "Writing to " << outputfiletext_2.Data() << endl;
    //   ofile_2.open(outputfiletext_2.Data(),std::ofstream::out | std::ofstream::app);
    //   double DeltaBR = (1-2.24e02*(0.005))/(1 - 1.1*(0.005)) - 1.;
    //
    //   SMValue = LinearInterpolatorFiles.at(WilsonCoff)-> h_SM -> GetBinContent(BinNumber+1);
    //   double variation = VariedValue / SMValue + DeltaBR;
    //   cout << "variation with decay : " << variation*SMValue << endl;
    //   ofile_2 << "0.0004" << "\t" << variation << endl;
    //   ofile_2 << 0.0 << "\t" << SMValue << endl;
    //   ofile_2.close();

  }



  // has_ggf_pd = has_vbf_vh_pd = has_tth_pd = false;
  // if(rootfilename.Contains("ggF")) has_ggf_pd = true;
  // if(rootfilename.Contains("VBFVH")) has_vbf_vh_pd = true;
  // cout << "has_ggf_pd = " << has_ggf_pd << "\t" << "has_vbf_vh_pd = " << has_vbf_vh_pd << "\t" << "has_tth_pd = " << has_tth_pd << endl;


}


LinearInterpolator::LinearInterpolator(const LinearInterpolator& other, const char* name) :
RooAbsReal(other,name),
xs_ggh(other.xs_ggh),
xs_vbf_vh(other.xs_vbf_vh),
xs_tth(other.xs_tth),
NormalizedScan(other.NormalizedScan),
xs_vbf_vh_tot(other.xs_vbf_vh_tot),
cg("cg",this,other.cg),
cgt("cgt",this,other.cgt),
ca("ca",this,other.ca),
cat("cat",this,other.cat),
cH("cH",this,other.cH),
cT("cT",this,other.cT),
cB("cB",this,other.cB),
cW("cW",this,other.cW),
cHW("cHW",this,other.cHW),
cHWt("cHWt",this,other.cHWt),
cHB("cHB",this,other.cHB),
cHBt("cHBt",this,other.cHBt),
cHWB("cHWB",this,other.cHWB),
cHWBt("cHWBt",this,other.cHWBt),
lambda("lambda",this,other.lambda)
{

  WrapperName = other.WrapperName;
  HistName = other.HistName;
  BinNumber = other.BinNumber;
  rootfilename = other.rootfilename;
  LinearInterpolatorVariables = other.LinearInterpolatorVariables;
  LinearInterpolatorFiles = other.LinearInterpolatorFiles;


}

// ---------------------------------------------------------------------------------------
// Destructors

LinearInterpolator::~LinearInterpolator() { }


// ---------------------------------------------------------------------------------------
// Evaluate Function

Double_t LinearInterpolator::evaluate() const
{
  double value(0);

  map <Str,RooRealProxy> ParMap;

  ParMap["cg"]   = cg;  ParMap["cgt"]  = cgt;
  ParMap["ca"]   = ca;  ParMap["cat"]  = cat;
  ParMap["cH"]   = cH;  ParMap["cT"]   = cT;
  ParMap["cB"]   = cB;  ParMap["cW"]   = cW;
  ParMap["cHW"]  = cHW; ParMap["cHWt"] = cHWt;
  ParMap["cHB"]  = cHB; ParMap["cHBt"] = cHBt;
  ParMap["cHWB"]  = cHWB; ParMap["cHWBt"] = cHWBt;
  ParMap["lambda"]   = lambda;

  // Loop over all LinearInterpolator Variables and produce a prediction

  bool hadggFvariation = false;
  bool hadVBFvariation = false;
  value = xs_ggh + xs_vbf_vh + xs_tth;
  double DeltaBR = calculateHiggstoDiphotonDecayWidthRatio()/calculateHiggsDecayWidthRatio() - 1.;
    double lambda_denom = ParMap["lambda"]*ParMap["lambda"];
    double lambda_variation = 1.e06/lambda_denom;
  value*=(1+DeltaBR);

  for(auto Var : LinearInterpolatorVariables ) {
    if(LinearInterpolatorFiles.at(Var)->has_ggf_pd)
    {
      double VariedValue = LinearInterpolatorFiles.at(Var)-> h_variation -> GetBinContent(BinNumber+1);
      double SMValue = LinearInterpolatorFiles.at(Var)-> h_SM -> GetBinContent(BinNumber+1);
      double BSMValue = Double_t(ParMap[Var])* VariedValue / (lambda_variation * LinearInterpolatorFiles.at(Var)-> GenerationParameter );
      double K_xs =  BSMValue/SMValue; //MG interference/SM
      value+=xs_ggh*K_xs;
    }
    else if(LinearInterpolatorFiles.at(Var)->has_vbf_vh_pd)
    {
      double VariedValue = LinearInterpolatorFiles.at(Var)-> h_variation -> GetBinContent(BinNumber+1);
      double SMValue = LinearInterpolatorFiles.at(Var)-> h_SM -> GetBinContent(BinNumber+1);
      double BSMValue = Double_t(ParMap[Var])* VariedValue / (lambda_variation * LinearInterpolatorFiles.at(Var)-> GenerationParameter );
      double K_xs =  BSMValue/SMValue; //MG interference/SM
      value+=xs_vbf_vh*K_xs;
    }
    //
    //
    //
    // // ggf Interpolator:
    // if(LinearInterpolatorFiles.at(Var)->has_ggf_pd) { // ggF variation
    //   // cout << "In Evaluate : xs_ggh=" << xs_ggh << " xs_vbf_vh=" << xs_vbf_vh << endl;
    //   // cout << "value=" << value << endl;
    //   double VariedValue = LinearInterpolatorFiles.at(Var)-> h_variation -> GetBinContent(BinNumber+1);
    //   double SMValue = LinearInterpolatorFiles.at(Var)-> h_SM -> GetBinContent(BinNumber+1);
    //
    //   double lambda_denom = ParMap["lambda"]*ParMap["lambda"];
    //   double lambda_variation = 1.e06/lambda_denom;
    //   double BSMValue = Double_t(ParMap[Var])* VariedValue / (lambda_variation * LinearInterpolatorFiles.at(Var)-> GenerationParameter );
    //   double VariedXS = SMValue +  BSMValue; //MG varied XS + BSM
    //   double DeltaBR = calculateHiggstoDiphotonDecayWidthRatio()/calculateHiggsDecayWidthRatio() - 1.;
    //   double variation = VariedXS / SMValue + DeltaBR;
    //   // cout << "value_vbvh=" << value << endl;
    //   value += xs_ggh * variation;
    //
    //   // if(value < 0) {  cout << "Warning: negative cross section encountered: xs_ggh = " << value << "\t" << Double_t(ParMap[Var]) << endl;  value = 0.;}
    //
    //   // Add SM VBFVH + ttH
    //   // value += xs_vbf_vh*(1+DeltaBR);
    //   value += xs_tth*(1+DeltaBR);
    //   hadggFvariation = true;
    //
    //
    //   // vbf_vh ProvDriver:
    // } else if (LinearInterpolatorFiles.at(Var)->has_vbf_vh_pd) { //VBF variation
    //   if(hadggFvariation){
    //     double VariedValue = LinearInterpolatorFiles.at(Var)-> h_variation -> GetBinContent(BinNumber+1);
    //     double SMValue = LinearInterpolatorFiles.at(Var)-> h_SM -> GetBinContent(BinNumber+1);
    //     double lambda_denom = ParMap["lambda"]*ParMap["lambda"];
    //     double lambda_variation = 1.e06/lambda_denom;
    //     double BSMValue = Double_t(ParMap[Var])* VariedValue / (lambda_variation * LinearInterpolatorFiles.at(Var)-> GenerationParameter );
    //     double VariedXS = SMValue +  BSMValue; //MG varied XS + BSM
    //     double DeltaBR = calculateHiggstoDiphotonDecayWidthRatio()/calculateHiggsDecayWidthRatio() - 1.;
    //     double variation = VariedXS / SMValue + DeltaBR;
    //     value += xs_vbf_vh*variation;
    //     // value += xs_vbf_vh*(1+DeltaBR);
    //
    //   }if(hadVBFvariation)
    //   {
    //     double VariedValue = LinearInterpolatorFiles.at(Var)-> h_variation -> GetBinContent(BinNumber+1);
    //     double SMValue = LinearInterpolatorFiles.at(Var)-> h_SM -> GetBinContent(BinNumber+1);
    //     // cout << "Bin : " << BinNumber+1 << endl;
    //     // cout << "VariedValue: " << VariedValue << endl;
    //     // cout << "SMValue: " << SMValue << endl;
    //     double lambda_denom = ParMap["lambda"]*ParMap["lambda"];
    //     double lambda_variation = 1.e06/lambda_denom;
    //     double BSMValue = Double_t(ParMap[Var])* VariedValue / (lambda_variation * LinearInterpolatorFiles.at(Var)-> GenerationParameter );
    //     double VariedXS = SMValue +  BSMValue; //MG varied XS + BSM
    //     double DeltaBR = calculateHiggstoDiphotonDecayWidthRatio()/calculateHiggsDecayWidthRatio() - 1.;
    //     double variation = VariedXS / SMValue + DeltaBR;
    //     value += xs_vbf_vh*variation;
    //   }
    //   else{
    //     // {cout << "In Evaluate : xs_ggh=" << xs_ggh << " xs_vbf_vh=" << xs_vbf_vh << endl;
    //     // cout << "value=" << value << endl;
    //
    //     double VariedValue = LinearInterpolatorFiles.at(Var)-> h_variation -> GetBinContent(BinNumber+1);
    //     double SMValue = LinearInterpolatorFiles.at(Var)-> h_SM -> GetBinContent(BinNumber+1);
    //     // cout << "Bin : " << BinNumber+1 << endl;
    //     // cout << "VariedValue: " << VariedValue << endl;
    //     // cout << "SMValue: " << SMValue << endl;
    //     double lambda_denom = ParMap["lambda"]*ParMap["lambda"];
    //     double lambda_variation = 1.e06/lambda_denom;
    //     double BSMValue = Double_t(ParMap[Var])* VariedValue / (lambda_variation * LinearInterpolatorFiles.at(Var)-> GenerationParameter );
    //     double VariedXS = SMValue +  BSMValue; //MG varied XS + BSM
    //     double DeltaBR = calculateHiggstoDiphotonDecayWidthRatio()/calculateHiggsDecayWidthRatio() - 1.;
    //     double variation = VariedXS / SMValue;// + DeltaBR;
    //     value += xs_vbf_vh*variation;
    //     // cout << "value_vbvh=" << value << endl;
    //     if(value < 0) {  }
    //
    //     // Add SM ggH + ttH
    //     value += xs_ggh*(1+DeltaBR);
    //     // cout << "value_ggH=" << value << endl;
    //     value += xs_tth*(1+DeltaBR);
    //
    //     hadVBFvariation = true;
    //   }
    //
    // } // end of VBFVH
  }
  // end of LinearInterpolatorVariables
  // cout << "value_end=" << value << endl;

  return value;

}


// ---------------------------------------------------------------------------------------
// Function that calculates the total Higgs Decay width for a given set of Wilson coefficients
// TODO : to be corrected with the values
Double_t LinearInterpolator::calculateHiggsDecayWidthRatio() const
{

  // Higgs boson width at mH = 125.09 GeV
  double sm_width = 4.074;

  // These values were obtained from MG5
  // double new_width = sm_width +
  // //  1934.7*Double_t(ca)*Double_t(ca) - 8.4655*Double_t(ca)     //increase in partial width to photons due to cA
  // // + 1937.9* Double_t(cat)* Double_t(cat)                           //increase in partial width to photons due to tcA
  // // + 6810354* Double_t(cg)*Double_t(cg)
  // + 3057.12*Double_t(cg)      //increase in partial width to gluons due to cG
  // // + 6806751*Double_t(cgt)*Double_t(cgt)                            //increase in partial width to gluons due to tcG
  // // + 3.3611*Double_t(cHW)*Double_t(cHW)
  // + 3.368*Double_t(cHW)       //increase in partial width to Ws due to cHW
  // // + 0.271* Double_t(cHWt)* Double_t(cHWt)                          //increase in partial width to Ws due to tcHW
  // // + 0.3872* Double_t(cHW)* Double_t(cHW) +
  // +0.4462* Double_t(cHW);  //increase in partial width to Zs due to cHW (cHB=cHW)
  //
  // return new_width/sm_width;
  // double lambda_nom = Double_t(lambda)*Double_t(lambda);
  // double higgs_vev_squard = 246.*246.;
  //
  double lambda_variation = 1 ; //higgs_vev_squard/lambda_nom;
  //

  double ratio = 1  - 1.5*lambda_variation*Double_t(cHB)   + 50.6*lambda_variation*Double_t(cg)   - 1.21*lambda_variation*Double_t(cHW) + 1.21* Double_t(cHWB);
  return ratio ;

}

// ---------------------------------------------------------------------------------------
// Function that calculates the total Higgs Decay width for a given set of Wilson coefficients
// TODO : To be corrected with the correct width
Double_t LinearInterpolator::calculateHiggstoDiphotonDecayWidthRatio() const
{

  double ratio;
  //
  // // Higgs boson width to yy at mH = 125.09 GeV
  // double sm_yy_width = 0.00924798;
  // double sm_yy_width = 0.00928872;
  //
  // // These values were obtained from MG5
  // double new_yy_width = sm_yy_width + 1934.7*Double_t(ca)*Double_t(ca) - 8.4655*Double_t(ca)     //increase in partial width to photons due to cA
  // + 1937.9* Double_t(cat)* Double_t(cat);                          //increase in partial width to photons due to tcA
  //
  //
  // ratio = new_yy_width / sm_yy_width;
  //
  // if(ratio < 0) cout << "Warning: negative Higgs to diphoton width encountered: width ratio to SM = " << ratio << "\t" << ca << endl;
  // double lambda_nom = Double_t(lambda)*Double_t(lambda);
  // double higgs_vev_squard = 246.*246.;

  double lambda_variation = 1 ; //higgs_vev_squard/lambda_nom;
  ratio = 1
  -2.4e02*lambda_variation*Double_t(cHW) // Interfernce cHW
  // + 143.1*Double_t(cHW)*Double_t(cHW) // BSM^2 cHW
  // + 143.1*Double_t(cHWt)*Double_t(cHWt) // BSM^2 cHWt (Interfernce =0)
  -8e02*lambda_variation*Double_t(cHB)  //Interfernce cHB
  // + 1541*Double_t(cHB)*Double_t(cHB) // BSM^2 cHB
  // + 1541*Double_t(cHBt)*Double_t(cHBt) // BSM^2 cHBt
  + 4.4e02*Double_t(cHWB); // Interfernce cHWB

  return ratio; // > 0 ? ratio : 0.;


}


// ---------------------------------------------------------------------------------------



// ---------------------------------------------------------------------------------------
ClassImp(LinearInterpolator)
// ---------------------------------------------------------------------------------------
