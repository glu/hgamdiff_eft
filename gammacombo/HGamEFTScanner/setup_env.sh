# Setup ATLAS local ROOT base if necessary
[ "`compgen -a | grep localSetupROOT`x" == "x" ] \
   && echo "Going to set up ATLAS local ROOT base from cvmfs" \
   && export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase \
   && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet 1>/dev/null 2>&1

# Setup more recent Git version
lsetup git

# Setup Gcc
gcc='gcc493_x86_64_slc6'
localSetupGcc $gcc

# Setup FAX if not in PATH
[ "${ROOTSYS}x" == "x" ] \
   && echo "Going to set up ROOT-`showVersions fax | grep default | awk '{print $2}'` from cvmfs" \
   && localSetupROOT --quiet 1>/dev/null 2>&1

export CC=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Gcc/gcc493_x86_64_slc6/slc6/gcc49/bin/gcc
export CXX=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Gcc/gcc493_x86_64_slc6/slc6/gcc49/bin/g++
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/sft.cern.ch/lcg/releases/LCG_85b/MCGenerators/professor/2.1.3/x86_64-slc6-gcc49-opt/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/prof-driver-0.4.0-for-2.3.1/local/lib

export PATH=$PATH:/cvmfs/sft.cern.ch/lcg/releases/LCG_85b/MCGenerators/professor/2.1.3/x86_64-slc6-gcc49-opt/bin
export PYTHONPATH=$PYTHONPATH:/cvmfs/sft.cern.ch/lcg/releases/LCG_85b/MCGenerators/professor/2.1.3/x86_64-slc6-gcc49-opt/lib/python2.7/site-packages
