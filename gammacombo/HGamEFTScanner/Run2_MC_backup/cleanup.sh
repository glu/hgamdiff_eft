#!/bin/bash

DIR=$(find . -name yoda* -type d)

if [ -n "$DIR" ]; then
  echo "Cleaning up $DIR"

  for dir in $DIR/*; do
    echo $dir
    for f in $(ls $dir | grep -v 'param.dat\|Higgs-scaled.yoda'); do
      rm -rf $dir/$f
    done
  done

else
  echo "Cannot find directory to clean up"
fi
