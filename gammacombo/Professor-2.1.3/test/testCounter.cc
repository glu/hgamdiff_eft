// Test for Ipol class

#include "Professor/Counter.h"
#include <iostream>


int main(int argc, char *argv[]) {

  using namespace std;

  int N=atoi(argv[1]);
  int O=atoi(argv[2]);

  cout << N << "-dimensional polynomial with max order " << O << endl;

  for (unsigned int i=0;i<=O;++i) {
    Professor::Counter c(N,i);
    while (c.next(N-1)) {
      if (c.sum() == i) {
        c.print();
      }
    }
  }

  return 0;
}
