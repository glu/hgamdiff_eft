// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME gammacomboCoreDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooBinned2DBicubicBase.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooCrossCorPdf.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooHistPdfAngleVar.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooHistPdfVar.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooPoly3Var.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooPoly4Var.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooSlimFitResult.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *SharedArrayImplEdoublegR_Dictionary();
   static void SharedArrayImplEdoublegR_TClassManip(TClass*);
   static void *new_SharedArrayImplEdoublegR(void *p = 0);
   static void *newArray_SharedArrayImplEdoublegR(Long_t size, void *p);
   static void delete_SharedArrayImplEdoublegR(void *p);
   static void deleteArray_SharedArrayImplEdoublegR(void *p);
   static void destruct_SharedArrayImplEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArrayImp<double>*)
   {
      ::SharedArrayImp<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArrayImp<double> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArrayImp<double>", ::SharedArrayImp<double>::Class_Version(), "SharedArray.h", 20,
                  typeid(::SharedArrayImp<double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArrayImplEdoublegR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArrayImp<double>) );
      instance.SetNew(&new_SharedArrayImplEdoublegR);
      instance.SetNewArray(&newArray_SharedArrayImplEdoublegR);
      instance.SetDelete(&delete_SharedArrayImplEdoublegR);
      instance.SetDeleteArray(&deleteArray_SharedArrayImplEdoublegR);
      instance.SetDestructor(&destruct_SharedArrayImplEdoublegR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArrayImp<double>*)
   {
      return GenerateInitInstanceLocal((::SharedArrayImp<double>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArrayImp<double>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArrayImplEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<double>*)0x0)->GetClass();
      SharedArrayImplEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArrayImplEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArrayImplEULong64_tgR_Dictionary();
   static void SharedArrayImplEULong64_tgR_TClassManip(TClass*);
   static void *new_SharedArrayImplEULong64_tgR(void *p = 0);
   static void *newArray_SharedArrayImplEULong64_tgR(Long_t size, void *p);
   static void delete_SharedArrayImplEULong64_tgR(void *p);
   static void deleteArray_SharedArrayImplEULong64_tgR(void *p);
   static void destruct_SharedArrayImplEULong64_tgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArrayImp<ULong64_t>*)
   {
      ::SharedArrayImp<ULong64_t> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArrayImp<ULong64_t> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArrayImp<ULong64_t>", ::SharedArrayImp<ULong64_t>::Class_Version(), "SharedArray.h", 20,
                  typeid(::SharedArrayImp<ULong64_t>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArrayImplEULong64_tgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArrayImp<ULong64_t>) );
      instance.SetNew(&new_SharedArrayImplEULong64_tgR);
      instance.SetNewArray(&newArray_SharedArrayImplEULong64_tgR);
      instance.SetDelete(&delete_SharedArrayImplEULong64_tgR);
      instance.SetDeleteArray(&deleteArray_SharedArrayImplEULong64_tgR);
      instance.SetDestructor(&destruct_SharedArrayImplEULong64_tgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArrayImp<ULong64_t>*)
   {
      return GenerateInitInstanceLocal((::SharedArrayImp<ULong64_t>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArrayImp<ULong64_t>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArrayImplEULong64_tgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<ULong64_t>*)0x0)->GetClass();
      SharedArrayImplEULong64_tgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArrayImplEULong64_tgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArrayImplEunsignedsPlonggR_Dictionary();
   static void SharedArrayImplEunsignedsPlonggR_TClassManip(TClass*);
   static void *new_SharedArrayImplEunsignedsPlonggR(void *p = 0);
   static void *newArray_SharedArrayImplEunsignedsPlonggR(Long_t size, void *p);
   static void delete_SharedArrayImplEunsignedsPlonggR(void *p);
   static void deleteArray_SharedArrayImplEunsignedsPlonggR(void *p);
   static void destruct_SharedArrayImplEunsignedsPlonggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArrayImp<unsigned long>*)
   {
      ::SharedArrayImp<unsigned long> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArrayImp<unsigned long> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArrayImp<unsigned long>", ::SharedArrayImp<unsigned long>::Class_Version(), "SharedArray.h", 20,
                  typeid(::SharedArrayImp<unsigned long>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArrayImplEunsignedsPlonggR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArrayImp<unsigned long>) );
      instance.SetNew(&new_SharedArrayImplEunsignedsPlonggR);
      instance.SetNewArray(&newArray_SharedArrayImplEunsignedsPlonggR);
      instance.SetDelete(&delete_SharedArrayImplEunsignedsPlonggR);
      instance.SetDeleteArray(&deleteArray_SharedArrayImplEunsignedsPlonggR);
      instance.SetDestructor(&destruct_SharedArrayImplEunsignedsPlonggR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArrayImp<unsigned long>*)
   {
      return GenerateInitInstanceLocal((::SharedArrayImp<unsigned long>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned long>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArrayImplEunsignedsPlonggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned long>*)0x0)->GetClass();
      SharedArrayImplEunsignedsPlonggR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArrayImplEunsignedsPlonggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArrayImplEunsignedsPintgR_Dictionary();
   static void SharedArrayImplEunsignedsPintgR_TClassManip(TClass*);
   static void *new_SharedArrayImplEunsignedsPintgR(void *p = 0);
   static void *newArray_SharedArrayImplEunsignedsPintgR(Long_t size, void *p);
   static void delete_SharedArrayImplEunsignedsPintgR(void *p);
   static void deleteArray_SharedArrayImplEunsignedsPintgR(void *p);
   static void destruct_SharedArrayImplEunsignedsPintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArrayImp<unsigned int>*)
   {
      ::SharedArrayImp<unsigned int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArrayImp<unsigned int> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArrayImp<unsigned int>", ::SharedArrayImp<unsigned int>::Class_Version(), "SharedArray.h", 20,
                  typeid(::SharedArrayImp<unsigned int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArrayImplEunsignedsPintgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArrayImp<unsigned int>) );
      instance.SetNew(&new_SharedArrayImplEunsignedsPintgR);
      instance.SetNewArray(&newArray_SharedArrayImplEunsignedsPintgR);
      instance.SetDelete(&delete_SharedArrayImplEunsignedsPintgR);
      instance.SetDeleteArray(&deleteArray_SharedArrayImplEunsignedsPintgR);
      instance.SetDestructor(&destruct_SharedArrayImplEunsignedsPintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArrayImp<unsigned int>*)
   {
      return GenerateInitInstanceLocal((::SharedArrayImp<unsigned int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArrayImplEunsignedsPintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned int>*)0x0)->GetClass();
      SharedArrayImplEunsignedsPintgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArrayImplEunsignedsPintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArrayImplEunsignedsPshortgR_Dictionary();
   static void SharedArrayImplEunsignedsPshortgR_TClassManip(TClass*);
   static void *new_SharedArrayImplEunsignedsPshortgR(void *p = 0);
   static void *newArray_SharedArrayImplEunsignedsPshortgR(Long_t size, void *p);
   static void delete_SharedArrayImplEunsignedsPshortgR(void *p);
   static void deleteArray_SharedArrayImplEunsignedsPshortgR(void *p);
   static void destruct_SharedArrayImplEunsignedsPshortgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArrayImp<unsigned short>*)
   {
      ::SharedArrayImp<unsigned short> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArrayImp<unsigned short> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArrayImp<unsigned short>", ::SharedArrayImp<unsigned short>::Class_Version(), "SharedArray.h", 20,
                  typeid(::SharedArrayImp<unsigned short>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArrayImplEunsignedsPshortgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArrayImp<unsigned short>) );
      instance.SetNew(&new_SharedArrayImplEunsignedsPshortgR);
      instance.SetNewArray(&newArray_SharedArrayImplEunsignedsPshortgR);
      instance.SetDelete(&delete_SharedArrayImplEunsignedsPshortgR);
      instance.SetDeleteArray(&deleteArray_SharedArrayImplEunsignedsPshortgR);
      instance.SetDestructor(&destruct_SharedArrayImplEunsignedsPshortgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArrayImp<unsigned short>*)
   {
      return GenerateInitInstanceLocal((::SharedArrayImp<unsigned short>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned short>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArrayImplEunsignedsPshortgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned short>*)0x0)->GetClass();
      SharedArrayImplEunsignedsPshortgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArrayImplEunsignedsPshortgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArrayImplEunsignedsPchargR_Dictionary();
   static void SharedArrayImplEunsignedsPchargR_TClassManip(TClass*);
   static void *new_SharedArrayImplEunsignedsPchargR(void *p = 0);
   static void *newArray_SharedArrayImplEunsignedsPchargR(Long_t size, void *p);
   static void delete_SharedArrayImplEunsignedsPchargR(void *p);
   static void deleteArray_SharedArrayImplEunsignedsPchargR(void *p);
   static void destruct_SharedArrayImplEunsignedsPchargR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArrayImp<unsigned char>*)
   {
      ::SharedArrayImp<unsigned char> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArrayImp<unsigned char> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArrayImp<unsigned char>", ::SharedArrayImp<unsigned char>::Class_Version(), "SharedArray.h", 20,
                  typeid(::SharedArrayImp<unsigned char>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArrayImplEunsignedsPchargR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArrayImp<unsigned char>) );
      instance.SetNew(&new_SharedArrayImplEunsignedsPchargR);
      instance.SetNewArray(&newArray_SharedArrayImplEunsignedsPchargR);
      instance.SetDelete(&delete_SharedArrayImplEunsignedsPchargR);
      instance.SetDeleteArray(&deleteArray_SharedArrayImplEunsignedsPchargR);
      instance.SetDestructor(&destruct_SharedArrayImplEunsignedsPchargR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArrayImp<unsigned char>*)
   {
      return GenerateInitInstanceLocal((::SharedArrayImp<unsigned char>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned char>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArrayImplEunsignedsPchargR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned char>*)0x0)->GetClass();
      SharedArrayImplEunsignedsPchargR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArrayImplEunsignedsPchargR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArrayImplEfloatgR_Dictionary();
   static void SharedArrayImplEfloatgR_TClassManip(TClass*);
   static void *new_SharedArrayImplEfloatgR(void *p = 0);
   static void *newArray_SharedArrayImplEfloatgR(Long_t size, void *p);
   static void delete_SharedArrayImplEfloatgR(void *p);
   static void deleteArray_SharedArrayImplEfloatgR(void *p);
   static void destruct_SharedArrayImplEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArrayImp<float>*)
   {
      ::SharedArrayImp<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArrayImp<float> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArrayImp<float>", ::SharedArrayImp<float>::Class_Version(), "SharedArray.h", 20,
                  typeid(::SharedArrayImp<float>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArrayImplEfloatgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArrayImp<float>) );
      instance.SetNew(&new_SharedArrayImplEfloatgR);
      instance.SetNewArray(&newArray_SharedArrayImplEfloatgR);
      instance.SetDelete(&delete_SharedArrayImplEfloatgR);
      instance.SetDeleteArray(&deleteArray_SharedArrayImplEfloatgR);
      instance.SetDestructor(&destruct_SharedArrayImplEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArrayImp<float>*)
   {
      return GenerateInitInstanceLocal((::SharedArrayImp<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArrayImp<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArrayImplEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<float>*)0x0)->GetClass();
      SharedArrayImplEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArrayImplEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArrayImplELong64_tgR_Dictionary();
   static void SharedArrayImplELong64_tgR_TClassManip(TClass*);
   static void *new_SharedArrayImplELong64_tgR(void *p = 0);
   static void *newArray_SharedArrayImplELong64_tgR(Long_t size, void *p);
   static void delete_SharedArrayImplELong64_tgR(void *p);
   static void deleteArray_SharedArrayImplELong64_tgR(void *p);
   static void destruct_SharedArrayImplELong64_tgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArrayImp<Long64_t>*)
   {
      ::SharedArrayImp<Long64_t> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArrayImp<Long64_t> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArrayImp<Long64_t>", ::SharedArrayImp<Long64_t>::Class_Version(), "SharedArray.h", 20,
                  typeid(::SharedArrayImp<Long64_t>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArrayImplELong64_tgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArrayImp<Long64_t>) );
      instance.SetNew(&new_SharedArrayImplELong64_tgR);
      instance.SetNewArray(&newArray_SharedArrayImplELong64_tgR);
      instance.SetDelete(&delete_SharedArrayImplELong64_tgR);
      instance.SetDeleteArray(&deleteArray_SharedArrayImplELong64_tgR);
      instance.SetDestructor(&destruct_SharedArrayImplELong64_tgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArrayImp<Long64_t>*)
   {
      return GenerateInitInstanceLocal((::SharedArrayImp<Long64_t>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArrayImp<Long64_t>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArrayImplELong64_tgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<Long64_t>*)0x0)->GetClass();
      SharedArrayImplELong64_tgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArrayImplELong64_tgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArrayImplElonggR_Dictionary();
   static void SharedArrayImplElonggR_TClassManip(TClass*);
   static void *new_SharedArrayImplElonggR(void *p = 0);
   static void *newArray_SharedArrayImplElonggR(Long_t size, void *p);
   static void delete_SharedArrayImplElonggR(void *p);
   static void deleteArray_SharedArrayImplElonggR(void *p);
   static void destruct_SharedArrayImplElonggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArrayImp<long>*)
   {
      ::SharedArrayImp<long> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArrayImp<long> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArrayImp<long>", ::SharedArrayImp<long>::Class_Version(), "SharedArray.h", 20,
                  typeid(::SharedArrayImp<long>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArrayImplElonggR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArrayImp<long>) );
      instance.SetNew(&new_SharedArrayImplElonggR);
      instance.SetNewArray(&newArray_SharedArrayImplElonggR);
      instance.SetDelete(&delete_SharedArrayImplElonggR);
      instance.SetDeleteArray(&deleteArray_SharedArrayImplElonggR);
      instance.SetDestructor(&destruct_SharedArrayImplElonggR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArrayImp<long>*)
   {
      return GenerateInitInstanceLocal((::SharedArrayImp<long>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArrayImp<long>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArrayImplElonggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<long>*)0x0)->GetClass();
      SharedArrayImplElonggR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArrayImplElonggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArrayImplEintgR_Dictionary();
   static void SharedArrayImplEintgR_TClassManip(TClass*);
   static void *new_SharedArrayImplEintgR(void *p = 0);
   static void *newArray_SharedArrayImplEintgR(Long_t size, void *p);
   static void delete_SharedArrayImplEintgR(void *p);
   static void deleteArray_SharedArrayImplEintgR(void *p);
   static void destruct_SharedArrayImplEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArrayImp<int>*)
   {
      ::SharedArrayImp<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArrayImp<int> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArrayImp<int>", ::SharedArrayImp<int>::Class_Version(), "SharedArray.h", 20,
                  typeid(::SharedArrayImp<int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArrayImplEintgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArrayImp<int>) );
      instance.SetNew(&new_SharedArrayImplEintgR);
      instance.SetNewArray(&newArray_SharedArrayImplEintgR);
      instance.SetDelete(&delete_SharedArrayImplEintgR);
      instance.SetDeleteArray(&deleteArray_SharedArrayImplEintgR);
      instance.SetDestructor(&destruct_SharedArrayImplEintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArrayImp<int>*)
   {
      return GenerateInitInstanceLocal((::SharedArrayImp<int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArrayImp<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArrayImplEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<int>*)0x0)->GetClass();
      SharedArrayImplEintgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArrayImplEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArrayImplEshortgR_Dictionary();
   static void SharedArrayImplEshortgR_TClassManip(TClass*);
   static void *new_SharedArrayImplEshortgR(void *p = 0);
   static void *newArray_SharedArrayImplEshortgR(Long_t size, void *p);
   static void delete_SharedArrayImplEshortgR(void *p);
   static void deleteArray_SharedArrayImplEshortgR(void *p);
   static void destruct_SharedArrayImplEshortgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArrayImp<short>*)
   {
      ::SharedArrayImp<short> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArrayImp<short> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArrayImp<short>", ::SharedArrayImp<short>::Class_Version(), "SharedArray.h", 20,
                  typeid(::SharedArrayImp<short>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArrayImplEshortgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArrayImp<short>) );
      instance.SetNew(&new_SharedArrayImplEshortgR);
      instance.SetNewArray(&newArray_SharedArrayImplEshortgR);
      instance.SetDelete(&delete_SharedArrayImplEshortgR);
      instance.SetDeleteArray(&deleteArray_SharedArrayImplEshortgR);
      instance.SetDestructor(&destruct_SharedArrayImplEshortgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArrayImp<short>*)
   {
      return GenerateInitInstanceLocal((::SharedArrayImp<short>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArrayImp<short>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArrayImplEshortgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<short>*)0x0)->GetClass();
      SharedArrayImplEshortgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArrayImplEshortgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArrayImplEchargR_Dictionary();
   static void SharedArrayImplEchargR_TClassManip(TClass*);
   static void *new_SharedArrayImplEchargR(void *p = 0);
   static void *newArray_SharedArrayImplEchargR(Long_t size, void *p);
   static void delete_SharedArrayImplEchargR(void *p);
   static void deleteArray_SharedArrayImplEchargR(void *p);
   static void destruct_SharedArrayImplEchargR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArrayImp<char>*)
   {
      ::SharedArrayImp<char> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArrayImp<char> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArrayImp<char>", ::SharedArrayImp<char>::Class_Version(), "SharedArray.h", 20,
                  typeid(::SharedArrayImp<char>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArrayImplEchargR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArrayImp<char>) );
      instance.SetNew(&new_SharedArrayImplEchargR);
      instance.SetNewArray(&newArray_SharedArrayImplEchargR);
      instance.SetDelete(&delete_SharedArrayImplEchargR);
      instance.SetDeleteArray(&deleteArray_SharedArrayImplEchargR);
      instance.SetDestructor(&destruct_SharedArrayImplEchargR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArrayImp<char>*)
   {
      return GenerateInitInstanceLocal((::SharedArrayImp<char>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArrayImp<char>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArrayImplEchargR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<char>*)0x0)->GetClass();
      SharedArrayImplEchargR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArrayImplEchargR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArraylEdoublegR_Dictionary();
   static void SharedArraylEdoublegR_TClassManip(TClass*);
   static void delete_SharedArraylEdoublegR(void *p);
   static void deleteArray_SharedArraylEdoublegR(void *p);
   static void destruct_SharedArraylEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArray<double>*)
   {
      ::SharedArray<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArray<double> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArray<double>", ::SharedArray<double>::Class_Version(), "SharedArray.h", 67,
                  typeid(::SharedArray<double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArraylEdoublegR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArray<double>) );
      instance.SetDelete(&delete_SharedArraylEdoublegR);
      instance.SetDeleteArray(&deleteArray_SharedArraylEdoublegR);
      instance.SetDestructor(&destruct_SharedArraylEdoublegR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArray<double>*)
   {
      return GenerateInitInstanceLocal((::SharedArray<double>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArray<double>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArraylEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArray<double>*)0x0)->GetClass();
      SharedArraylEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArraylEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArraylEULong64_tgR_Dictionary();
   static void SharedArraylEULong64_tgR_TClassManip(TClass*);
   static void delete_SharedArraylEULong64_tgR(void *p);
   static void deleteArray_SharedArraylEULong64_tgR(void *p);
   static void destruct_SharedArraylEULong64_tgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArray<ULong64_t>*)
   {
      ::SharedArray<ULong64_t> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArray<ULong64_t> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArray<ULong64_t>", ::SharedArray<ULong64_t>::Class_Version(), "SharedArray.h", 67,
                  typeid(::SharedArray<ULong64_t>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArraylEULong64_tgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArray<ULong64_t>) );
      instance.SetDelete(&delete_SharedArraylEULong64_tgR);
      instance.SetDeleteArray(&deleteArray_SharedArraylEULong64_tgR);
      instance.SetDestructor(&destruct_SharedArraylEULong64_tgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArray<ULong64_t>*)
   {
      return GenerateInitInstanceLocal((::SharedArray<ULong64_t>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArray<ULong64_t>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArraylEULong64_tgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArray<ULong64_t>*)0x0)->GetClass();
      SharedArraylEULong64_tgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArraylEULong64_tgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArraylEunsignedsPlonggR_Dictionary();
   static void SharedArraylEunsignedsPlonggR_TClassManip(TClass*);
   static void delete_SharedArraylEunsignedsPlonggR(void *p);
   static void deleteArray_SharedArraylEunsignedsPlonggR(void *p);
   static void destruct_SharedArraylEunsignedsPlonggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArray<unsigned long>*)
   {
      ::SharedArray<unsigned long> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArray<unsigned long> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArray<unsigned long>", ::SharedArray<unsigned long>::Class_Version(), "SharedArray.h", 67,
                  typeid(::SharedArray<unsigned long>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArraylEunsignedsPlonggR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArray<unsigned long>) );
      instance.SetDelete(&delete_SharedArraylEunsignedsPlonggR);
      instance.SetDeleteArray(&deleteArray_SharedArraylEunsignedsPlonggR);
      instance.SetDestructor(&destruct_SharedArraylEunsignedsPlonggR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArray<unsigned long>*)
   {
      return GenerateInitInstanceLocal((::SharedArray<unsigned long>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArray<unsigned long>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArraylEunsignedsPlonggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned long>*)0x0)->GetClass();
      SharedArraylEunsignedsPlonggR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArraylEunsignedsPlonggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArraylEunsignedsPintgR_Dictionary();
   static void SharedArraylEunsignedsPintgR_TClassManip(TClass*);
   static void delete_SharedArraylEunsignedsPintgR(void *p);
   static void deleteArray_SharedArraylEunsignedsPintgR(void *p);
   static void destruct_SharedArraylEunsignedsPintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArray<unsigned int>*)
   {
      ::SharedArray<unsigned int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArray<unsigned int> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArray<unsigned int>", ::SharedArray<unsigned int>::Class_Version(), "SharedArray.h", 67,
                  typeid(::SharedArray<unsigned int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArraylEunsignedsPintgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArray<unsigned int>) );
      instance.SetDelete(&delete_SharedArraylEunsignedsPintgR);
      instance.SetDeleteArray(&deleteArray_SharedArraylEunsignedsPintgR);
      instance.SetDestructor(&destruct_SharedArraylEunsignedsPintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArray<unsigned int>*)
   {
      return GenerateInitInstanceLocal((::SharedArray<unsigned int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArray<unsigned int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArraylEunsignedsPintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned int>*)0x0)->GetClass();
      SharedArraylEunsignedsPintgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArraylEunsignedsPintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArraylEunsignedsPshortgR_Dictionary();
   static void SharedArraylEunsignedsPshortgR_TClassManip(TClass*);
   static void delete_SharedArraylEunsignedsPshortgR(void *p);
   static void deleteArray_SharedArraylEunsignedsPshortgR(void *p);
   static void destruct_SharedArraylEunsignedsPshortgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArray<unsigned short>*)
   {
      ::SharedArray<unsigned short> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArray<unsigned short> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArray<unsigned short>", ::SharedArray<unsigned short>::Class_Version(), "SharedArray.h", 67,
                  typeid(::SharedArray<unsigned short>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArraylEunsignedsPshortgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArray<unsigned short>) );
      instance.SetDelete(&delete_SharedArraylEunsignedsPshortgR);
      instance.SetDeleteArray(&deleteArray_SharedArraylEunsignedsPshortgR);
      instance.SetDestructor(&destruct_SharedArraylEunsignedsPshortgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArray<unsigned short>*)
   {
      return GenerateInitInstanceLocal((::SharedArray<unsigned short>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArray<unsigned short>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArraylEunsignedsPshortgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned short>*)0x0)->GetClass();
      SharedArraylEunsignedsPshortgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArraylEunsignedsPshortgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArraylEunsignedsPchargR_Dictionary();
   static void SharedArraylEunsignedsPchargR_TClassManip(TClass*);
   static void delete_SharedArraylEunsignedsPchargR(void *p);
   static void deleteArray_SharedArraylEunsignedsPchargR(void *p);
   static void destruct_SharedArraylEunsignedsPchargR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArray<unsigned char>*)
   {
      ::SharedArray<unsigned char> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArray<unsigned char> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArray<unsigned char>", ::SharedArray<unsigned char>::Class_Version(), "SharedArray.h", 67,
                  typeid(::SharedArray<unsigned char>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArraylEunsignedsPchargR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArray<unsigned char>) );
      instance.SetDelete(&delete_SharedArraylEunsignedsPchargR);
      instance.SetDeleteArray(&deleteArray_SharedArraylEunsignedsPchargR);
      instance.SetDestructor(&destruct_SharedArraylEunsignedsPchargR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArray<unsigned char>*)
   {
      return GenerateInitInstanceLocal((::SharedArray<unsigned char>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArray<unsigned char>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArraylEunsignedsPchargR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned char>*)0x0)->GetClass();
      SharedArraylEunsignedsPchargR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArraylEunsignedsPchargR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArraylEfloatgR_Dictionary();
   static void SharedArraylEfloatgR_TClassManip(TClass*);
   static void delete_SharedArraylEfloatgR(void *p);
   static void deleteArray_SharedArraylEfloatgR(void *p);
   static void destruct_SharedArraylEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArray<float>*)
   {
      ::SharedArray<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArray<float> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArray<float>", ::SharedArray<float>::Class_Version(), "SharedArray.h", 67,
                  typeid(::SharedArray<float>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArraylEfloatgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArray<float>) );
      instance.SetDelete(&delete_SharedArraylEfloatgR);
      instance.SetDeleteArray(&deleteArray_SharedArraylEfloatgR);
      instance.SetDestructor(&destruct_SharedArraylEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArray<float>*)
   {
      return GenerateInitInstanceLocal((::SharedArray<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArray<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArraylEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArray<float>*)0x0)->GetClass();
      SharedArraylEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArraylEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArraylELong64_tgR_Dictionary();
   static void SharedArraylELong64_tgR_TClassManip(TClass*);
   static void delete_SharedArraylELong64_tgR(void *p);
   static void deleteArray_SharedArraylELong64_tgR(void *p);
   static void destruct_SharedArraylELong64_tgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArray<Long64_t>*)
   {
      ::SharedArray<Long64_t> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArray<Long64_t> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArray<Long64_t>", ::SharedArray<Long64_t>::Class_Version(), "SharedArray.h", 67,
                  typeid(::SharedArray<Long64_t>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArraylELong64_tgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArray<Long64_t>) );
      instance.SetDelete(&delete_SharedArraylELong64_tgR);
      instance.SetDeleteArray(&deleteArray_SharedArraylELong64_tgR);
      instance.SetDestructor(&destruct_SharedArraylELong64_tgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArray<Long64_t>*)
   {
      return GenerateInitInstanceLocal((::SharedArray<Long64_t>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArray<Long64_t>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArraylELong64_tgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArray<Long64_t>*)0x0)->GetClass();
      SharedArraylELong64_tgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArraylELong64_tgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArraylElonggR_Dictionary();
   static void SharedArraylElonggR_TClassManip(TClass*);
   static void delete_SharedArraylElonggR(void *p);
   static void deleteArray_SharedArraylElonggR(void *p);
   static void destruct_SharedArraylElonggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArray<long>*)
   {
      ::SharedArray<long> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArray<long> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArray<long>", ::SharedArray<long>::Class_Version(), "SharedArray.h", 67,
                  typeid(::SharedArray<long>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArraylElonggR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArray<long>) );
      instance.SetDelete(&delete_SharedArraylElonggR);
      instance.SetDeleteArray(&deleteArray_SharedArraylElonggR);
      instance.SetDestructor(&destruct_SharedArraylElonggR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArray<long>*)
   {
      return GenerateInitInstanceLocal((::SharedArray<long>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArray<long>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArraylElonggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArray<long>*)0x0)->GetClass();
      SharedArraylElonggR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArraylElonggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArraylEintgR_Dictionary();
   static void SharedArraylEintgR_TClassManip(TClass*);
   static void delete_SharedArraylEintgR(void *p);
   static void deleteArray_SharedArraylEintgR(void *p);
   static void destruct_SharedArraylEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArray<int>*)
   {
      ::SharedArray<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArray<int> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArray<int>", ::SharedArray<int>::Class_Version(), "SharedArray.h", 67,
                  typeid(::SharedArray<int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArraylEintgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArray<int>) );
      instance.SetDelete(&delete_SharedArraylEintgR);
      instance.SetDeleteArray(&deleteArray_SharedArraylEintgR);
      instance.SetDestructor(&destruct_SharedArraylEintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArray<int>*)
   {
      return GenerateInitInstanceLocal((::SharedArray<int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArray<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArraylEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArray<int>*)0x0)->GetClass();
      SharedArraylEintgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArraylEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArraylEshortgR_Dictionary();
   static void SharedArraylEshortgR_TClassManip(TClass*);
   static void delete_SharedArraylEshortgR(void *p);
   static void deleteArray_SharedArraylEshortgR(void *p);
   static void destruct_SharedArraylEshortgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArray<short>*)
   {
      ::SharedArray<short> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArray<short> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArray<short>", ::SharedArray<short>::Class_Version(), "SharedArray.h", 67,
                  typeid(::SharedArray<short>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArraylEshortgR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArray<short>) );
      instance.SetDelete(&delete_SharedArraylEshortgR);
      instance.SetDeleteArray(&deleteArray_SharedArraylEshortgR);
      instance.SetDestructor(&destruct_SharedArraylEshortgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArray<short>*)
   {
      return GenerateInitInstanceLocal((::SharedArray<short>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArray<short>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArraylEshortgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArray<short>*)0x0)->GetClass();
      SharedArraylEshortgR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArraylEshortgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SharedArraylEchargR_Dictionary();
   static void SharedArraylEchargR_TClassManip(TClass*);
   static void delete_SharedArraylEchargR(void *p);
   static void deleteArray_SharedArraylEchargR(void *p);
   static void destruct_SharedArraylEchargR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SharedArray<char>*)
   {
      ::SharedArray<char> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SharedArray<char> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SharedArray<char>", ::SharedArray<char>::Class_Version(), "SharedArray.h", 67,
                  typeid(::SharedArray<char>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SharedArraylEchargR_Dictionary, isa_proxy, 4,
                  sizeof(::SharedArray<char>) );
      instance.SetDelete(&delete_SharedArraylEchargR);
      instance.SetDeleteArray(&deleteArray_SharedArraylEchargR);
      instance.SetDestructor(&destruct_SharedArraylEchargR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SharedArray<char>*)
   {
      return GenerateInitInstanceLocal((::SharedArray<char>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SharedArray<char>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SharedArraylEchargR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SharedArray<char>*)0x0)->GetClass();
      SharedArraylEchargR_TClassManip(theClass);
   return theClass;
   }

   static void SharedArraylEchargR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RooBinned2DBicubicBaselERooAbsRealgR_Dictionary();
   static void RooBinned2DBicubicBaselERooAbsRealgR_TClassManip(TClass*);
   static void *new_RooBinned2DBicubicBaselERooAbsRealgR(void *p = 0);
   static void *newArray_RooBinned2DBicubicBaselERooAbsRealgR(Long_t size, void *p);
   static void delete_RooBinned2DBicubicBaselERooAbsRealgR(void *p);
   static void deleteArray_RooBinned2DBicubicBaselERooAbsRealgR(void *p);
   static void destruct_RooBinned2DBicubicBaselERooAbsRealgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooBinned2DBicubicBase<RooAbsReal>*)
   {
      ::RooBinned2DBicubicBase<RooAbsReal> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooBinned2DBicubicBase<RooAbsReal> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooBinned2DBicubicBase<RooAbsReal>", ::RooBinned2DBicubicBase<RooAbsReal>::Class_Version(), "RooBinned2DBicubicBase.h", 47,
                  typeid(::RooBinned2DBicubicBase<RooAbsReal>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &RooBinned2DBicubicBaselERooAbsRealgR_Dictionary, isa_proxy, 4,
                  sizeof(::RooBinned2DBicubicBase<RooAbsReal>) );
      instance.SetNew(&new_RooBinned2DBicubicBaselERooAbsRealgR);
      instance.SetNewArray(&newArray_RooBinned2DBicubicBaselERooAbsRealgR);
      instance.SetDelete(&delete_RooBinned2DBicubicBaselERooAbsRealgR);
      instance.SetDeleteArray(&deleteArray_RooBinned2DBicubicBaselERooAbsRealgR);
      instance.SetDestructor(&destruct_RooBinned2DBicubicBaselERooAbsRealgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooBinned2DBicubicBase<RooAbsReal>*)
   {
      return GenerateInitInstanceLocal((::RooBinned2DBicubicBase<RooAbsReal>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooBinned2DBicubicBase<RooAbsReal>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RooBinned2DBicubicBaselERooAbsRealgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RooBinned2DBicubicBase<RooAbsReal>*)0x0)->GetClass();
      RooBinned2DBicubicBaselERooAbsRealgR_TClassManip(theClass);
   return theClass;
   }

   static void RooBinned2DBicubicBaselERooAbsRealgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RooBinned2DBicubicBaselERooAbsPdfgR_Dictionary();
   static void RooBinned2DBicubicBaselERooAbsPdfgR_TClassManip(TClass*);
   static void *new_RooBinned2DBicubicBaselERooAbsPdfgR(void *p = 0);
   static void *newArray_RooBinned2DBicubicBaselERooAbsPdfgR(Long_t size, void *p);
   static void delete_RooBinned2DBicubicBaselERooAbsPdfgR(void *p);
   static void deleteArray_RooBinned2DBicubicBaselERooAbsPdfgR(void *p);
   static void destruct_RooBinned2DBicubicBaselERooAbsPdfgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooBinned2DBicubicBase<RooAbsPdf>*)
   {
      ::RooBinned2DBicubicBase<RooAbsPdf> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooBinned2DBicubicBase<RooAbsPdf> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooBinned2DBicubicBase<RooAbsPdf>", ::RooBinned2DBicubicBase<RooAbsPdf>::Class_Version(), "RooBinned2DBicubicBase.h", 47,
                  typeid(::RooBinned2DBicubicBase<RooAbsPdf>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &RooBinned2DBicubicBaselERooAbsPdfgR_Dictionary, isa_proxy, 4,
                  sizeof(::RooBinned2DBicubicBase<RooAbsPdf>) );
      instance.SetNew(&new_RooBinned2DBicubicBaselERooAbsPdfgR);
      instance.SetNewArray(&newArray_RooBinned2DBicubicBaselERooAbsPdfgR);
      instance.SetDelete(&delete_RooBinned2DBicubicBaselERooAbsPdfgR);
      instance.SetDeleteArray(&deleteArray_RooBinned2DBicubicBaselERooAbsPdfgR);
      instance.SetDestructor(&destruct_RooBinned2DBicubicBaselERooAbsPdfgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooBinned2DBicubicBase<RooAbsPdf>*)
   {
      return GenerateInitInstanceLocal((::RooBinned2DBicubicBase<RooAbsPdf>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooBinned2DBicubicBase<RooAbsPdf>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RooBinned2DBicubicBaselERooAbsPdfgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RooBinned2DBicubicBase<RooAbsPdf>*)0x0)->GetClass();
      RooBinned2DBicubicBaselERooAbsPdfgR_TClassManip(theClass);
   return theClass;
   }

   static void RooBinned2DBicubicBaselERooAbsPdfgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void delete_RooCrossCorPdf(void *p);
   static void deleteArray_RooCrossCorPdf(void *p);
   static void destruct_RooCrossCorPdf(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooCrossCorPdf*)
   {
      ::RooCrossCorPdf *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooCrossCorPdf >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooCrossCorPdf", ::RooCrossCorPdf::Class_Version(), "RooCrossCorPdf.h", 14,
                  typeid(::RooCrossCorPdf), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooCrossCorPdf::Dictionary, isa_proxy, 4,
                  sizeof(::RooCrossCorPdf) );
      instance.SetDelete(&delete_RooCrossCorPdf);
      instance.SetDeleteArray(&deleteArray_RooCrossCorPdf);
      instance.SetDestructor(&destruct_RooCrossCorPdf);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooCrossCorPdf*)
   {
      return GenerateInitInstanceLocal((::RooCrossCorPdf*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooCrossCorPdf*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooHistPdfAngleVar(void *p = 0);
   static void *newArray_RooHistPdfAngleVar(Long_t size, void *p);
   static void delete_RooHistPdfAngleVar(void *p);
   static void deleteArray_RooHistPdfAngleVar(void *p);
   static void destruct_RooHistPdfAngleVar(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooHistPdfAngleVar*)
   {
      ::RooHistPdfAngleVar *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooHistPdfAngleVar >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooHistPdfAngleVar", ::RooHistPdfAngleVar::Class_Version(), "RooHistPdfAngleVar.h", 16,
                  typeid(::RooHistPdfAngleVar), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooHistPdfAngleVar::Dictionary, isa_proxy, 4,
                  sizeof(::RooHistPdfAngleVar) );
      instance.SetNew(&new_RooHistPdfAngleVar);
      instance.SetNewArray(&newArray_RooHistPdfAngleVar);
      instance.SetDelete(&delete_RooHistPdfAngleVar);
      instance.SetDeleteArray(&deleteArray_RooHistPdfAngleVar);
      instance.SetDestructor(&destruct_RooHistPdfAngleVar);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooHistPdfAngleVar*)
   {
      return GenerateInitInstanceLocal((::RooHistPdfAngleVar*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooHistPdfAngleVar*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooHistPdfVar(void *p = 0);
   static void *newArray_RooHistPdfVar(Long_t size, void *p);
   static void delete_RooHistPdfVar(void *p);
   static void deleteArray_RooHistPdfVar(void *p);
   static void destruct_RooHistPdfVar(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooHistPdfVar*)
   {
      ::RooHistPdfVar *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooHistPdfVar >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooHistPdfVar", ::RooHistPdfVar::Class_Version(), "RooHistPdfVar.h", 16,
                  typeid(::RooHistPdfVar), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooHistPdfVar::Dictionary, isa_proxy, 4,
                  sizeof(::RooHistPdfVar) );
      instance.SetNew(&new_RooHistPdfVar);
      instance.SetNewArray(&newArray_RooHistPdfVar);
      instance.SetDelete(&delete_RooHistPdfVar);
      instance.SetDeleteArray(&deleteArray_RooHistPdfVar);
      instance.SetDestructor(&destruct_RooHistPdfVar);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooHistPdfVar*)
   {
      return GenerateInitInstanceLocal((::RooHistPdfVar*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooHistPdfVar*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooPoly3Var(void *p = 0);
   static void *newArray_RooPoly3Var(Long_t size, void *p);
   static void delete_RooPoly3Var(void *p);
   static void deleteArray_RooPoly3Var(void *p);
   static void destruct_RooPoly3Var(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooPoly3Var*)
   {
      ::RooPoly3Var *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooPoly3Var >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooPoly3Var", ::RooPoly3Var::Class_Version(), "RooPoly3Var.h", 16,
                  typeid(::RooPoly3Var), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooPoly3Var::Dictionary, isa_proxy, 4,
                  sizeof(::RooPoly3Var) );
      instance.SetNew(&new_RooPoly3Var);
      instance.SetNewArray(&newArray_RooPoly3Var);
      instance.SetDelete(&delete_RooPoly3Var);
      instance.SetDeleteArray(&deleteArray_RooPoly3Var);
      instance.SetDestructor(&destruct_RooPoly3Var);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooPoly3Var*)
   {
      return GenerateInitInstanceLocal((::RooPoly3Var*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooPoly3Var*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooPoly4Var(void *p = 0);
   static void *newArray_RooPoly4Var(Long_t size, void *p);
   static void delete_RooPoly4Var(void *p);
   static void deleteArray_RooPoly4Var(void *p);
   static void destruct_RooPoly4Var(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooPoly4Var*)
   {
      ::RooPoly4Var *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooPoly4Var >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooPoly4Var", ::RooPoly4Var::Class_Version(), "RooPoly4Var.h", 16,
                  typeid(::RooPoly4Var), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooPoly4Var::Dictionary, isa_proxy, 4,
                  sizeof(::RooPoly4Var) );
      instance.SetNew(&new_RooPoly4Var);
      instance.SetNewArray(&newArray_RooPoly4Var);
      instance.SetDelete(&delete_RooPoly4Var);
      instance.SetDeleteArray(&deleteArray_RooPoly4Var);
      instance.SetDestructor(&destruct_RooPoly4Var);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooPoly4Var*)
   {
      return GenerateInitInstanceLocal((::RooPoly4Var*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooPoly4Var*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooSlimFitResult(void *p = 0);
   static void *newArray_RooSlimFitResult(Long_t size, void *p);
   static void delete_RooSlimFitResult(void *p);
   static void deleteArray_RooSlimFitResult(void *p);
   static void destruct_RooSlimFitResult(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooSlimFitResult*)
   {
      ::RooSlimFitResult *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooSlimFitResult >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooSlimFitResult", ::RooSlimFitResult::Class_Version(), "RooSlimFitResult.h", 31,
                  typeid(::RooSlimFitResult), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooSlimFitResult::Dictionary, isa_proxy, 4,
                  sizeof(::RooSlimFitResult) );
      instance.SetNew(&new_RooSlimFitResult);
      instance.SetNewArray(&newArray_RooSlimFitResult);
      instance.SetDelete(&delete_RooSlimFitResult);
      instance.SetDeleteArray(&deleteArray_RooSlimFitResult);
      instance.SetDestructor(&destruct_RooSlimFitResult);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooSlimFitResult*)
   {
      return GenerateInitInstanceLocal((::RooSlimFitResult*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooSlimFitResult*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArrayImp<double>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArrayImp<double>::Class_Name()
{
   return "SharedArrayImp<double>";
}

//______________________________________________________________________________
template <> const char *SharedArrayImp<double>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<double>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArrayImp<double>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<double>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<double>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<double>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<double>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<double>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArrayImp<ULong64_t>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArrayImp<ULong64_t>::Class_Name()
{
   return "SharedArrayImp<ULong64_t>";
}

//______________________________________________________________________________
template <> const char *SharedArrayImp<ULong64_t>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<ULong64_t>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArrayImp<ULong64_t>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<ULong64_t>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<ULong64_t>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<ULong64_t>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<ULong64_t>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<ULong64_t>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArrayImp<unsigned long>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArrayImp<unsigned long>::Class_Name()
{
   return "SharedArrayImp<unsigned long>";
}

//______________________________________________________________________________
template <> const char *SharedArrayImp<unsigned long>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned long>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArrayImp<unsigned long>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned long>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<unsigned long>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned long>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<unsigned long>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned long>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArrayImp<unsigned int>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArrayImp<unsigned int>::Class_Name()
{
   return "SharedArrayImp<unsigned int>";
}

//______________________________________________________________________________
template <> const char *SharedArrayImp<unsigned int>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned int>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArrayImp<unsigned int>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned int>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<unsigned int>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned int>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<unsigned int>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned int>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArrayImp<unsigned short>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArrayImp<unsigned short>::Class_Name()
{
   return "SharedArrayImp<unsigned short>";
}

//______________________________________________________________________________
template <> const char *SharedArrayImp<unsigned short>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned short>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArrayImp<unsigned short>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned short>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<unsigned short>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned short>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<unsigned short>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned short>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArrayImp<unsigned char>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArrayImp<unsigned char>::Class_Name()
{
   return "SharedArrayImp<unsigned char>";
}

//______________________________________________________________________________
template <> const char *SharedArrayImp<unsigned char>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned char>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArrayImp<unsigned char>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned char>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<unsigned char>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned char>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<unsigned char>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<unsigned char>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArrayImp<float>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArrayImp<float>::Class_Name()
{
   return "SharedArrayImp<float>";
}

//______________________________________________________________________________
template <> const char *SharedArrayImp<float>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<float>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArrayImp<float>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<float>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<float>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<float>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<float>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<float>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArrayImp<Long64_t>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArrayImp<Long64_t>::Class_Name()
{
   return "SharedArrayImp<Long64_t>";
}

//______________________________________________________________________________
template <> const char *SharedArrayImp<Long64_t>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<Long64_t>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArrayImp<Long64_t>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<Long64_t>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<Long64_t>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<Long64_t>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<Long64_t>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<Long64_t>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArrayImp<long>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArrayImp<long>::Class_Name()
{
   return "SharedArrayImp<long>";
}

//______________________________________________________________________________
template <> const char *SharedArrayImp<long>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<long>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArrayImp<long>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<long>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<long>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<long>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<long>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<long>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArrayImp<int>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArrayImp<int>::Class_Name()
{
   return "SharedArrayImp<int>";
}

//______________________________________________________________________________
template <> const char *SharedArrayImp<int>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<int>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArrayImp<int>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<int>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<int>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<int>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<int>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<int>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArrayImp<short>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArrayImp<short>::Class_Name()
{
   return "SharedArrayImp<short>";
}

//______________________________________________________________________________
template <> const char *SharedArrayImp<short>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<short>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArrayImp<short>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<short>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<short>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<short>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<short>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<short>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArrayImp<char>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArrayImp<char>::Class_Name()
{
   return "SharedArrayImp<char>";
}

//______________________________________________________________________________
template <> const char *SharedArrayImp<char>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<char>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArrayImp<char>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<char>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<char>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<char>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArrayImp<char>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArrayImp<char>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArray<double>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArray<double>::Class_Name()
{
   return "SharedArray<double>";
}

//______________________________________________________________________________
template <> const char *SharedArray<double>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<double>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArray<double>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<double>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArray<double>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<double>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArray<double>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<double>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArray<ULong64_t>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArray<ULong64_t>::Class_Name()
{
   return "SharedArray<ULong64_t>";
}

//______________________________________________________________________________
template <> const char *SharedArray<ULong64_t>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<ULong64_t>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArray<ULong64_t>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<ULong64_t>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArray<ULong64_t>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<ULong64_t>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArray<ULong64_t>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<ULong64_t>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArray<unsigned long>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArray<unsigned long>::Class_Name()
{
   return "SharedArray<unsigned long>";
}

//______________________________________________________________________________
template <> const char *SharedArray<unsigned long>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned long>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArray<unsigned long>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned long>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArray<unsigned long>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned long>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArray<unsigned long>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned long>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArray<unsigned int>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArray<unsigned int>::Class_Name()
{
   return "SharedArray<unsigned int>";
}

//______________________________________________________________________________
template <> const char *SharedArray<unsigned int>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned int>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArray<unsigned int>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned int>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArray<unsigned int>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned int>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArray<unsigned int>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned int>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArray<unsigned short>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArray<unsigned short>::Class_Name()
{
   return "SharedArray<unsigned short>";
}

//______________________________________________________________________________
template <> const char *SharedArray<unsigned short>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned short>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArray<unsigned short>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned short>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArray<unsigned short>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned short>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArray<unsigned short>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned short>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArray<unsigned char>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArray<unsigned char>::Class_Name()
{
   return "SharedArray<unsigned char>";
}

//______________________________________________________________________________
template <> const char *SharedArray<unsigned char>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned char>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArray<unsigned char>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned char>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArray<unsigned char>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned char>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArray<unsigned char>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<unsigned char>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArray<float>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArray<float>::Class_Name()
{
   return "SharedArray<float>";
}

//______________________________________________________________________________
template <> const char *SharedArray<float>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<float>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArray<float>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<float>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArray<float>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<float>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArray<float>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<float>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArray<Long64_t>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArray<Long64_t>::Class_Name()
{
   return "SharedArray<Long64_t>";
}

//______________________________________________________________________________
template <> const char *SharedArray<Long64_t>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<Long64_t>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArray<Long64_t>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<Long64_t>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArray<Long64_t>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<Long64_t>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArray<Long64_t>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<Long64_t>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArray<long>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArray<long>::Class_Name()
{
   return "SharedArray<long>";
}

//______________________________________________________________________________
template <> const char *SharedArray<long>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<long>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArray<long>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<long>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArray<long>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<long>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArray<long>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<long>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArray<int>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArray<int>::Class_Name()
{
   return "SharedArray<int>";
}

//______________________________________________________________________________
template <> const char *SharedArray<int>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<int>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArray<int>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<int>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArray<int>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<int>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArray<int>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<int>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArray<short>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArray<short>::Class_Name()
{
   return "SharedArray<short>";
}

//______________________________________________________________________________
template <> const char *SharedArray<short>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<short>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArray<short>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<short>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArray<short>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<short>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArray<short>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<short>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr SharedArray<char>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *SharedArray<char>::Class_Name()
{
   return "SharedArray<char>";
}

//______________________________________________________________________________
template <> const char *SharedArray<char>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<char>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int SharedArray<char>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<char>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *SharedArray<char>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<char>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *SharedArray<char>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SharedArray<char>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr RooBinned2DBicubicBase<RooAbsReal>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *RooBinned2DBicubicBase<RooAbsReal>::Class_Name()
{
   return "RooBinned2DBicubicBase<RooAbsReal>";
}

//______________________________________________________________________________
template <> const char *RooBinned2DBicubicBase<RooAbsReal>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooBinned2DBicubicBase<RooAbsReal>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int RooBinned2DBicubicBase<RooAbsReal>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooBinned2DBicubicBase<RooAbsReal>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *RooBinned2DBicubicBase<RooAbsReal>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooBinned2DBicubicBase<RooAbsReal>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *RooBinned2DBicubicBase<RooAbsReal>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooBinned2DBicubicBase<RooAbsReal>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> atomic_TClass_ptr RooBinned2DBicubicBase<RooAbsPdf>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *RooBinned2DBicubicBase<RooAbsPdf>::Class_Name()
{
   return "RooBinned2DBicubicBase<RooAbsPdf>";
}

//______________________________________________________________________________
template <> const char *RooBinned2DBicubicBase<RooAbsPdf>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooBinned2DBicubicBase<RooAbsPdf>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int RooBinned2DBicubicBase<RooAbsPdf>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooBinned2DBicubicBase<RooAbsPdf>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *RooBinned2DBicubicBase<RooAbsPdf>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooBinned2DBicubicBase<RooAbsPdf>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *RooBinned2DBicubicBase<RooAbsPdf>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooBinned2DBicubicBase<RooAbsPdf>*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooCrossCorPdf::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooCrossCorPdf::Class_Name()
{
   return "RooCrossCorPdf";
}

//______________________________________________________________________________
const char *RooCrossCorPdf::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooCrossCorPdf*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooCrossCorPdf::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooCrossCorPdf*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooCrossCorPdf::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooCrossCorPdf*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooCrossCorPdf::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooCrossCorPdf*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooHistPdfAngleVar::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooHistPdfAngleVar::Class_Name()
{
   return "RooHistPdfAngleVar";
}

//______________________________________________________________________________
const char *RooHistPdfAngleVar::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooHistPdfAngleVar*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooHistPdfAngleVar::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooHistPdfAngleVar*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooHistPdfAngleVar::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooHistPdfAngleVar*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooHistPdfAngleVar::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooHistPdfAngleVar*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooHistPdfVar::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooHistPdfVar::Class_Name()
{
   return "RooHistPdfVar";
}

//______________________________________________________________________________
const char *RooHistPdfVar::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooHistPdfVar*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooHistPdfVar::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooHistPdfVar*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooHistPdfVar::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooHistPdfVar*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooHistPdfVar::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooHistPdfVar*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooPoly3Var::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooPoly3Var::Class_Name()
{
   return "RooPoly3Var";
}

//______________________________________________________________________________
const char *RooPoly3Var::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooPoly3Var*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooPoly3Var::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooPoly3Var*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooPoly3Var::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooPoly3Var*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooPoly3Var::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooPoly3Var*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooPoly4Var::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooPoly4Var::Class_Name()
{
   return "RooPoly4Var";
}

//______________________________________________________________________________
const char *RooPoly4Var::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooPoly4Var*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooPoly4Var::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooPoly4Var*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooPoly4Var::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooPoly4Var*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooPoly4Var::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooPoly4Var*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooSlimFitResult::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooSlimFitResult::Class_Name()
{
   return "RooSlimFitResult";
}

//______________________________________________________________________________
const char *RooSlimFitResult::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooSlimFitResult*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooSlimFitResult::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooSlimFitResult*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooSlimFitResult::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooSlimFitResult*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooSlimFitResult::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooSlimFitResult*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
template <> void SharedArrayImp<double>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArrayImp<double>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArrayImp<double>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArrayImp<double>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SharedArrayImplEdoublegR(void *p) {
      return  p ? new(p) ::SharedArrayImp<double> : new ::SharedArrayImp<double>;
   }
   static void *newArray_SharedArrayImplEdoublegR(Long_t nElements, void *p) {
      return p ? new(p) ::SharedArrayImp<double>[nElements] : new ::SharedArrayImp<double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SharedArrayImplEdoublegR(void *p) {
      delete ((::SharedArrayImp<double>*)p);
   }
   static void deleteArray_SharedArrayImplEdoublegR(void *p) {
      delete [] ((::SharedArrayImp<double>*)p);
   }
   static void destruct_SharedArrayImplEdoublegR(void *p) {
      typedef ::SharedArrayImp<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArrayImp<double>

//______________________________________________________________________________
template <> void SharedArrayImp<ULong64_t>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArrayImp<ULong64_t>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArrayImp<ULong64_t>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArrayImp<ULong64_t>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SharedArrayImplEULong64_tgR(void *p) {
      return  p ? new(p) ::SharedArrayImp<ULong64_t> : new ::SharedArrayImp<ULong64_t>;
   }
   static void *newArray_SharedArrayImplEULong64_tgR(Long_t nElements, void *p) {
      return p ? new(p) ::SharedArrayImp<ULong64_t>[nElements] : new ::SharedArrayImp<ULong64_t>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SharedArrayImplEULong64_tgR(void *p) {
      delete ((::SharedArrayImp<ULong64_t>*)p);
   }
   static void deleteArray_SharedArrayImplEULong64_tgR(void *p) {
      delete [] ((::SharedArrayImp<ULong64_t>*)p);
   }
   static void destruct_SharedArrayImplEULong64_tgR(void *p) {
      typedef ::SharedArrayImp<ULong64_t> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArrayImp<ULong64_t>

//______________________________________________________________________________
template <> void SharedArrayImp<unsigned long>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArrayImp<unsigned long>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArrayImp<unsigned long>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArrayImp<unsigned long>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SharedArrayImplEunsignedsPlonggR(void *p) {
      return  p ? new(p) ::SharedArrayImp<unsigned long> : new ::SharedArrayImp<unsigned long>;
   }
   static void *newArray_SharedArrayImplEunsignedsPlonggR(Long_t nElements, void *p) {
      return p ? new(p) ::SharedArrayImp<unsigned long>[nElements] : new ::SharedArrayImp<unsigned long>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SharedArrayImplEunsignedsPlonggR(void *p) {
      delete ((::SharedArrayImp<unsigned long>*)p);
   }
   static void deleteArray_SharedArrayImplEunsignedsPlonggR(void *p) {
      delete [] ((::SharedArrayImp<unsigned long>*)p);
   }
   static void destruct_SharedArrayImplEunsignedsPlonggR(void *p) {
      typedef ::SharedArrayImp<unsigned long> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArrayImp<unsigned long>

//______________________________________________________________________________
template <> void SharedArrayImp<unsigned int>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArrayImp<unsigned int>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArrayImp<unsigned int>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArrayImp<unsigned int>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SharedArrayImplEunsignedsPintgR(void *p) {
      return  p ? new(p) ::SharedArrayImp<unsigned int> : new ::SharedArrayImp<unsigned int>;
   }
   static void *newArray_SharedArrayImplEunsignedsPintgR(Long_t nElements, void *p) {
      return p ? new(p) ::SharedArrayImp<unsigned int>[nElements] : new ::SharedArrayImp<unsigned int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SharedArrayImplEunsignedsPintgR(void *p) {
      delete ((::SharedArrayImp<unsigned int>*)p);
   }
   static void deleteArray_SharedArrayImplEunsignedsPintgR(void *p) {
      delete [] ((::SharedArrayImp<unsigned int>*)p);
   }
   static void destruct_SharedArrayImplEunsignedsPintgR(void *p) {
      typedef ::SharedArrayImp<unsigned int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArrayImp<unsigned int>

//______________________________________________________________________________
template <> void SharedArrayImp<unsigned short>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArrayImp<unsigned short>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArrayImp<unsigned short>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArrayImp<unsigned short>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SharedArrayImplEunsignedsPshortgR(void *p) {
      return  p ? new(p) ::SharedArrayImp<unsigned short> : new ::SharedArrayImp<unsigned short>;
   }
   static void *newArray_SharedArrayImplEunsignedsPshortgR(Long_t nElements, void *p) {
      return p ? new(p) ::SharedArrayImp<unsigned short>[nElements] : new ::SharedArrayImp<unsigned short>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SharedArrayImplEunsignedsPshortgR(void *p) {
      delete ((::SharedArrayImp<unsigned short>*)p);
   }
   static void deleteArray_SharedArrayImplEunsignedsPshortgR(void *p) {
      delete [] ((::SharedArrayImp<unsigned short>*)p);
   }
   static void destruct_SharedArrayImplEunsignedsPshortgR(void *p) {
      typedef ::SharedArrayImp<unsigned short> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArrayImp<unsigned short>

//______________________________________________________________________________
template <> void SharedArrayImp<unsigned char>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArrayImp<unsigned char>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArrayImp<unsigned char>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArrayImp<unsigned char>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SharedArrayImplEunsignedsPchargR(void *p) {
      return  p ? new(p) ::SharedArrayImp<unsigned char> : new ::SharedArrayImp<unsigned char>;
   }
   static void *newArray_SharedArrayImplEunsignedsPchargR(Long_t nElements, void *p) {
      return p ? new(p) ::SharedArrayImp<unsigned char>[nElements] : new ::SharedArrayImp<unsigned char>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SharedArrayImplEunsignedsPchargR(void *p) {
      delete ((::SharedArrayImp<unsigned char>*)p);
   }
   static void deleteArray_SharedArrayImplEunsignedsPchargR(void *p) {
      delete [] ((::SharedArrayImp<unsigned char>*)p);
   }
   static void destruct_SharedArrayImplEunsignedsPchargR(void *p) {
      typedef ::SharedArrayImp<unsigned char> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArrayImp<unsigned char>

//______________________________________________________________________________
template <> void SharedArrayImp<float>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArrayImp<float>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArrayImp<float>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArrayImp<float>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SharedArrayImplEfloatgR(void *p) {
      return  p ? new(p) ::SharedArrayImp<float> : new ::SharedArrayImp<float>;
   }
   static void *newArray_SharedArrayImplEfloatgR(Long_t nElements, void *p) {
      return p ? new(p) ::SharedArrayImp<float>[nElements] : new ::SharedArrayImp<float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SharedArrayImplEfloatgR(void *p) {
      delete ((::SharedArrayImp<float>*)p);
   }
   static void deleteArray_SharedArrayImplEfloatgR(void *p) {
      delete [] ((::SharedArrayImp<float>*)p);
   }
   static void destruct_SharedArrayImplEfloatgR(void *p) {
      typedef ::SharedArrayImp<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArrayImp<float>

//______________________________________________________________________________
template <> void SharedArrayImp<Long64_t>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArrayImp<Long64_t>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArrayImp<Long64_t>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArrayImp<Long64_t>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SharedArrayImplELong64_tgR(void *p) {
      return  p ? new(p) ::SharedArrayImp<Long64_t> : new ::SharedArrayImp<Long64_t>;
   }
   static void *newArray_SharedArrayImplELong64_tgR(Long_t nElements, void *p) {
      return p ? new(p) ::SharedArrayImp<Long64_t>[nElements] : new ::SharedArrayImp<Long64_t>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SharedArrayImplELong64_tgR(void *p) {
      delete ((::SharedArrayImp<Long64_t>*)p);
   }
   static void deleteArray_SharedArrayImplELong64_tgR(void *p) {
      delete [] ((::SharedArrayImp<Long64_t>*)p);
   }
   static void destruct_SharedArrayImplELong64_tgR(void *p) {
      typedef ::SharedArrayImp<Long64_t> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArrayImp<Long64_t>

//______________________________________________________________________________
template <> void SharedArrayImp<long>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArrayImp<long>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArrayImp<long>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArrayImp<long>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SharedArrayImplElonggR(void *p) {
      return  p ? new(p) ::SharedArrayImp<long> : new ::SharedArrayImp<long>;
   }
   static void *newArray_SharedArrayImplElonggR(Long_t nElements, void *p) {
      return p ? new(p) ::SharedArrayImp<long>[nElements] : new ::SharedArrayImp<long>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SharedArrayImplElonggR(void *p) {
      delete ((::SharedArrayImp<long>*)p);
   }
   static void deleteArray_SharedArrayImplElonggR(void *p) {
      delete [] ((::SharedArrayImp<long>*)p);
   }
   static void destruct_SharedArrayImplElonggR(void *p) {
      typedef ::SharedArrayImp<long> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArrayImp<long>

//______________________________________________________________________________
template <> void SharedArrayImp<int>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArrayImp<int>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArrayImp<int>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArrayImp<int>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SharedArrayImplEintgR(void *p) {
      return  p ? new(p) ::SharedArrayImp<int> : new ::SharedArrayImp<int>;
   }
   static void *newArray_SharedArrayImplEintgR(Long_t nElements, void *p) {
      return p ? new(p) ::SharedArrayImp<int>[nElements] : new ::SharedArrayImp<int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SharedArrayImplEintgR(void *p) {
      delete ((::SharedArrayImp<int>*)p);
   }
   static void deleteArray_SharedArrayImplEintgR(void *p) {
      delete [] ((::SharedArrayImp<int>*)p);
   }
   static void destruct_SharedArrayImplEintgR(void *p) {
      typedef ::SharedArrayImp<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArrayImp<int>

//______________________________________________________________________________
template <> void SharedArrayImp<short>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArrayImp<short>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArrayImp<short>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArrayImp<short>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SharedArrayImplEshortgR(void *p) {
      return  p ? new(p) ::SharedArrayImp<short> : new ::SharedArrayImp<short>;
   }
   static void *newArray_SharedArrayImplEshortgR(Long_t nElements, void *p) {
      return p ? new(p) ::SharedArrayImp<short>[nElements] : new ::SharedArrayImp<short>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SharedArrayImplEshortgR(void *p) {
      delete ((::SharedArrayImp<short>*)p);
   }
   static void deleteArray_SharedArrayImplEshortgR(void *p) {
      delete [] ((::SharedArrayImp<short>*)p);
   }
   static void destruct_SharedArrayImplEshortgR(void *p) {
      typedef ::SharedArrayImp<short> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArrayImp<short>

//______________________________________________________________________________
template <> void SharedArrayImp<char>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArrayImp<char>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArrayImp<char>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArrayImp<char>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SharedArrayImplEchargR(void *p) {
      return  p ? new(p) ::SharedArrayImp<char> : new ::SharedArrayImp<char>;
   }
   static void *newArray_SharedArrayImplEchargR(Long_t nElements, void *p) {
      return p ? new(p) ::SharedArrayImp<char>[nElements] : new ::SharedArrayImp<char>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SharedArrayImplEchargR(void *p) {
      delete ((::SharedArrayImp<char>*)p);
   }
   static void deleteArray_SharedArrayImplEchargR(void *p) {
      delete [] ((::SharedArrayImp<char>*)p);
   }
   static void destruct_SharedArrayImplEchargR(void *p) {
      typedef ::SharedArrayImp<char> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArrayImp<char>

//______________________________________________________________________________
template <> void SharedArray<double>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArray<double>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArray<double>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArray<double>::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SharedArraylEdoublegR(void *p) {
      delete ((::SharedArray<double>*)p);
   }
   static void deleteArray_SharedArraylEdoublegR(void *p) {
      delete [] ((::SharedArray<double>*)p);
   }
   static void destruct_SharedArraylEdoublegR(void *p) {
      typedef ::SharedArray<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArray<double>

//______________________________________________________________________________
template <> void SharedArray<ULong64_t>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArray<ULong64_t>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArray<ULong64_t>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArray<ULong64_t>::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SharedArraylEULong64_tgR(void *p) {
      delete ((::SharedArray<ULong64_t>*)p);
   }
   static void deleteArray_SharedArraylEULong64_tgR(void *p) {
      delete [] ((::SharedArray<ULong64_t>*)p);
   }
   static void destruct_SharedArraylEULong64_tgR(void *p) {
      typedef ::SharedArray<ULong64_t> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArray<ULong64_t>

//______________________________________________________________________________
template <> void SharedArray<unsigned long>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArray<unsigned long>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArray<unsigned long>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArray<unsigned long>::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SharedArraylEunsignedsPlonggR(void *p) {
      delete ((::SharedArray<unsigned long>*)p);
   }
   static void deleteArray_SharedArraylEunsignedsPlonggR(void *p) {
      delete [] ((::SharedArray<unsigned long>*)p);
   }
   static void destruct_SharedArraylEunsignedsPlonggR(void *p) {
      typedef ::SharedArray<unsigned long> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArray<unsigned long>

//______________________________________________________________________________
template <> void SharedArray<unsigned int>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArray<unsigned int>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArray<unsigned int>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArray<unsigned int>::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SharedArraylEunsignedsPintgR(void *p) {
      delete ((::SharedArray<unsigned int>*)p);
   }
   static void deleteArray_SharedArraylEunsignedsPintgR(void *p) {
      delete [] ((::SharedArray<unsigned int>*)p);
   }
   static void destruct_SharedArraylEunsignedsPintgR(void *p) {
      typedef ::SharedArray<unsigned int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArray<unsigned int>

//______________________________________________________________________________
template <> void SharedArray<unsigned short>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArray<unsigned short>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArray<unsigned short>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArray<unsigned short>::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SharedArraylEunsignedsPshortgR(void *p) {
      delete ((::SharedArray<unsigned short>*)p);
   }
   static void deleteArray_SharedArraylEunsignedsPshortgR(void *p) {
      delete [] ((::SharedArray<unsigned short>*)p);
   }
   static void destruct_SharedArraylEunsignedsPshortgR(void *p) {
      typedef ::SharedArray<unsigned short> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArray<unsigned short>

//______________________________________________________________________________
template <> void SharedArray<unsigned char>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArray<unsigned char>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArray<unsigned char>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArray<unsigned char>::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SharedArraylEunsignedsPchargR(void *p) {
      delete ((::SharedArray<unsigned char>*)p);
   }
   static void deleteArray_SharedArraylEunsignedsPchargR(void *p) {
      delete [] ((::SharedArray<unsigned char>*)p);
   }
   static void destruct_SharedArraylEunsignedsPchargR(void *p) {
      typedef ::SharedArray<unsigned char> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArray<unsigned char>

//______________________________________________________________________________
template <> void SharedArray<float>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArray<float>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArray<float>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArray<float>::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SharedArraylEfloatgR(void *p) {
      delete ((::SharedArray<float>*)p);
   }
   static void deleteArray_SharedArraylEfloatgR(void *p) {
      delete [] ((::SharedArray<float>*)p);
   }
   static void destruct_SharedArraylEfloatgR(void *p) {
      typedef ::SharedArray<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArray<float>

//______________________________________________________________________________
template <> void SharedArray<Long64_t>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArray<Long64_t>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArray<Long64_t>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArray<Long64_t>::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SharedArraylELong64_tgR(void *p) {
      delete ((::SharedArray<Long64_t>*)p);
   }
   static void deleteArray_SharedArraylELong64_tgR(void *p) {
      delete [] ((::SharedArray<Long64_t>*)p);
   }
   static void destruct_SharedArraylELong64_tgR(void *p) {
      typedef ::SharedArray<Long64_t> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArray<Long64_t>

//______________________________________________________________________________
template <> void SharedArray<long>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArray<long>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArray<long>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArray<long>::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SharedArraylElonggR(void *p) {
      delete ((::SharedArray<long>*)p);
   }
   static void deleteArray_SharedArraylElonggR(void *p) {
      delete [] ((::SharedArray<long>*)p);
   }
   static void destruct_SharedArraylElonggR(void *p) {
      typedef ::SharedArray<long> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArray<long>

//______________________________________________________________________________
template <> void SharedArray<int>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArray<int>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArray<int>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArray<int>::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SharedArraylEintgR(void *p) {
      delete ((::SharedArray<int>*)p);
   }
   static void deleteArray_SharedArraylEintgR(void *p) {
      delete [] ((::SharedArray<int>*)p);
   }
   static void destruct_SharedArraylEintgR(void *p) {
      typedef ::SharedArray<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArray<int>

//______________________________________________________________________________
template <> void SharedArray<short>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArray<short>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArray<short>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArray<short>::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SharedArraylEshortgR(void *p) {
      delete ((::SharedArray<short>*)p);
   }
   static void deleteArray_SharedArraylEshortgR(void *p) {
      delete [] ((::SharedArray<short>*)p);
   }
   static void destruct_SharedArraylEshortgR(void *p) {
      typedef ::SharedArray<short> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArray<short>

//______________________________________________________________________________
template <> void SharedArray<char>::Streamer(TBuffer &R__b)
{
   // Stream an object of class SharedArray<char>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SharedArray<char>::Class(),this);
   } else {
      R__b.WriteClassBuffer(SharedArray<char>::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SharedArraylEchargR(void *p) {
      delete ((::SharedArray<char>*)p);
   }
   static void deleteArray_SharedArraylEchargR(void *p) {
      delete [] ((::SharedArray<char>*)p);
   }
   static void destruct_SharedArraylEchargR(void *p) {
      typedef ::SharedArray<char> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SharedArray<char>

//______________________________________________________________________________
template <> void RooBinned2DBicubicBase<RooAbsReal>::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooBinned2DBicubicBase<RooAbsReal>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooBinned2DBicubicBase<RooAbsReal>::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooBinned2DBicubicBase<RooAbsReal>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooBinned2DBicubicBaselERooAbsRealgR(void *p) {
      return  p ? new(p) ::RooBinned2DBicubicBase<RooAbsReal> : new ::RooBinned2DBicubicBase<RooAbsReal>;
   }
   static void *newArray_RooBinned2DBicubicBaselERooAbsRealgR(Long_t nElements, void *p) {
      return p ? new(p) ::RooBinned2DBicubicBase<RooAbsReal>[nElements] : new ::RooBinned2DBicubicBase<RooAbsReal>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooBinned2DBicubicBaselERooAbsRealgR(void *p) {
      delete ((::RooBinned2DBicubicBase<RooAbsReal>*)p);
   }
   static void deleteArray_RooBinned2DBicubicBaselERooAbsRealgR(void *p) {
      delete [] ((::RooBinned2DBicubicBase<RooAbsReal>*)p);
   }
   static void destruct_RooBinned2DBicubicBaselERooAbsRealgR(void *p) {
      typedef ::RooBinned2DBicubicBase<RooAbsReal> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooBinned2DBicubicBase<RooAbsReal>

//______________________________________________________________________________
template <> void RooBinned2DBicubicBase<RooAbsPdf>::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooBinned2DBicubicBase<RooAbsPdf>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooBinned2DBicubicBase<RooAbsPdf>::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooBinned2DBicubicBase<RooAbsPdf>::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooBinned2DBicubicBaselERooAbsPdfgR(void *p) {
      return  p ? new(p) ::RooBinned2DBicubicBase<RooAbsPdf> : new ::RooBinned2DBicubicBase<RooAbsPdf>;
   }
   static void *newArray_RooBinned2DBicubicBaselERooAbsPdfgR(Long_t nElements, void *p) {
      return p ? new(p) ::RooBinned2DBicubicBase<RooAbsPdf>[nElements] : new ::RooBinned2DBicubicBase<RooAbsPdf>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooBinned2DBicubicBaselERooAbsPdfgR(void *p) {
      delete ((::RooBinned2DBicubicBase<RooAbsPdf>*)p);
   }
   static void deleteArray_RooBinned2DBicubicBaselERooAbsPdfgR(void *p) {
      delete [] ((::RooBinned2DBicubicBase<RooAbsPdf>*)p);
   }
   static void destruct_RooBinned2DBicubicBaselERooAbsPdfgR(void *p) {
      typedef ::RooBinned2DBicubicBase<RooAbsPdf> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooBinned2DBicubicBase<RooAbsPdf>

//______________________________________________________________________________
void RooCrossCorPdf::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooCrossCorPdf.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooCrossCorPdf::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooCrossCorPdf::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RooCrossCorPdf(void *p) {
      delete ((::RooCrossCorPdf*)p);
   }
   static void deleteArray_RooCrossCorPdf(void *p) {
      delete [] ((::RooCrossCorPdf*)p);
   }
   static void destruct_RooCrossCorPdf(void *p) {
      typedef ::RooCrossCorPdf current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooCrossCorPdf

//______________________________________________________________________________
void RooHistPdfAngleVar::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooHistPdfAngleVar.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooHistPdfAngleVar::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooHistPdfAngleVar::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooHistPdfAngleVar(void *p) {
      return  p ? new(p) ::RooHistPdfAngleVar : new ::RooHistPdfAngleVar;
   }
   static void *newArray_RooHistPdfAngleVar(Long_t nElements, void *p) {
      return p ? new(p) ::RooHistPdfAngleVar[nElements] : new ::RooHistPdfAngleVar[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooHistPdfAngleVar(void *p) {
      delete ((::RooHistPdfAngleVar*)p);
   }
   static void deleteArray_RooHistPdfAngleVar(void *p) {
      delete [] ((::RooHistPdfAngleVar*)p);
   }
   static void destruct_RooHistPdfAngleVar(void *p) {
      typedef ::RooHistPdfAngleVar current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooHistPdfAngleVar

//______________________________________________________________________________
void RooHistPdfVar::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooHistPdfVar.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooHistPdfVar::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooHistPdfVar::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooHistPdfVar(void *p) {
      return  p ? new(p) ::RooHistPdfVar : new ::RooHistPdfVar;
   }
   static void *newArray_RooHistPdfVar(Long_t nElements, void *p) {
      return p ? new(p) ::RooHistPdfVar[nElements] : new ::RooHistPdfVar[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooHistPdfVar(void *p) {
      delete ((::RooHistPdfVar*)p);
   }
   static void deleteArray_RooHistPdfVar(void *p) {
      delete [] ((::RooHistPdfVar*)p);
   }
   static void destruct_RooHistPdfVar(void *p) {
      typedef ::RooHistPdfVar current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooHistPdfVar

//______________________________________________________________________________
void RooPoly3Var::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooPoly3Var.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooPoly3Var::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooPoly3Var::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooPoly3Var(void *p) {
      return  p ? new(p) ::RooPoly3Var : new ::RooPoly3Var;
   }
   static void *newArray_RooPoly3Var(Long_t nElements, void *p) {
      return p ? new(p) ::RooPoly3Var[nElements] : new ::RooPoly3Var[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooPoly3Var(void *p) {
      delete ((::RooPoly3Var*)p);
   }
   static void deleteArray_RooPoly3Var(void *p) {
      delete [] ((::RooPoly3Var*)p);
   }
   static void destruct_RooPoly3Var(void *p) {
      typedef ::RooPoly3Var current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooPoly3Var

//______________________________________________________________________________
void RooPoly4Var::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooPoly4Var.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooPoly4Var::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooPoly4Var::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooPoly4Var(void *p) {
      return  p ? new(p) ::RooPoly4Var : new ::RooPoly4Var;
   }
   static void *newArray_RooPoly4Var(Long_t nElements, void *p) {
      return p ? new(p) ::RooPoly4Var[nElements] : new ::RooPoly4Var[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooPoly4Var(void *p) {
      delete ((::RooPoly4Var*)p);
   }
   static void deleteArray_RooPoly4Var(void *p) {
      delete [] ((::RooPoly4Var*)p);
   }
   static void destruct_RooPoly4Var(void *p) {
      typedef ::RooPoly4Var current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooPoly4Var

//______________________________________________________________________________
void RooSlimFitResult::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooSlimFitResult.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooSlimFitResult::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooSlimFitResult::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooSlimFitResult(void *p) {
      return  p ? new(p) ::RooSlimFitResult : new ::RooSlimFitResult;
   }
   static void *newArray_RooSlimFitResult(Long_t nElements, void *p) {
      return p ? new(p) ::RooSlimFitResult[nElements] : new ::RooSlimFitResult[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooSlimFitResult(void *p) {
      delete ((::RooSlimFitResult*)p);
   }
   static void deleteArray_RooSlimFitResult(void *p) {
      delete [] ((::RooSlimFitResult*)p);
   }
   static void destruct_RooSlimFitResult(void *p) {
      typedef ::RooSlimFitResult current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooSlimFitResult

namespace ROOT {
   static TClass *vectorlEunsignedsPshortgR_Dictionary();
   static void vectorlEunsignedsPshortgR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPshortgR(void *p = 0);
   static void *newArray_vectorlEunsignedsPshortgR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPshortgR(void *p);
   static void deleteArray_vectorlEunsignedsPshortgR(void *p);
   static void destruct_vectorlEunsignedsPshortgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned short>*)
   {
      vector<unsigned short> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned short>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned short>", -2, "vector", 214,
                  typeid(vector<unsigned short>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPshortgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<unsigned short>) );
      instance.SetNew(&new_vectorlEunsignedsPshortgR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPshortgR);
      instance.SetDelete(&delete_vectorlEunsignedsPshortgR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPshortgR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPshortgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned short> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<unsigned short>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPshortgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned short>*)0x0)->GetClass();
      vectorlEunsignedsPshortgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPshortgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPshortgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned short> : new vector<unsigned short>;
   }
   static void *newArray_vectorlEunsignedsPshortgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned short>[nElements] : new vector<unsigned short>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPshortgR(void *p) {
      delete ((vector<unsigned short>*)p);
   }
   static void deleteArray_vectorlEunsignedsPshortgR(void *p) {
      delete [] ((vector<unsigned short>*)p);
   }
   static void destruct_vectorlEunsignedsPshortgR(void *p) {
      typedef vector<unsigned short> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned short>

namespace ROOT {
   static TClass *vectorlEunsignedsPlonggR_Dictionary();
   static void vectorlEunsignedsPlonggR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPlonggR(void *p = 0);
   static void *newArray_vectorlEunsignedsPlonggR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPlonggR(void *p);
   static void deleteArray_vectorlEunsignedsPlonggR(void *p);
   static void destruct_vectorlEunsignedsPlonggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned long>*)
   {
      vector<unsigned long> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned long>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned long>", -2, "vector", 214,
                  typeid(vector<unsigned long>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPlonggR_Dictionary, isa_proxy, 0,
                  sizeof(vector<unsigned long>) );
      instance.SetNew(&new_vectorlEunsignedsPlonggR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPlonggR);
      instance.SetDelete(&delete_vectorlEunsignedsPlonggR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPlonggR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPlonggR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned long> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<unsigned long>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPlonggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned long>*)0x0)->GetClass();
      vectorlEunsignedsPlonggR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPlonggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPlonggR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned long> : new vector<unsigned long>;
   }
   static void *newArray_vectorlEunsignedsPlonggR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned long>[nElements] : new vector<unsigned long>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPlonggR(void *p) {
      delete ((vector<unsigned long>*)p);
   }
   static void deleteArray_vectorlEunsignedsPlonggR(void *p) {
      delete [] ((vector<unsigned long>*)p);
   }
   static void destruct_vectorlEunsignedsPlonggR(void *p) {
      typedef vector<unsigned long> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned long>

namespace ROOT {
   static TClass *vectorlEunsignedsPintgR_Dictionary();
   static void vectorlEunsignedsPintgR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPintgR(void *p = 0);
   static void *newArray_vectorlEunsignedsPintgR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPintgR(void *p);
   static void deleteArray_vectorlEunsignedsPintgR(void *p);
   static void destruct_vectorlEunsignedsPintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned int>*)
   {
      vector<unsigned int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned int>", -2, "vector", 214,
                  typeid(vector<unsigned int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPintgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<unsigned int>) );
      instance.SetNew(&new_vectorlEunsignedsPintgR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPintgR);
      instance.SetDelete(&delete_vectorlEunsignedsPintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPintgR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<unsigned int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned int>*)0x0)->GetClass();
      vectorlEunsignedsPintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned int> : new vector<unsigned int>;
   }
   static void *newArray_vectorlEunsignedsPintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned int>[nElements] : new vector<unsigned int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPintgR(void *p) {
      delete ((vector<unsigned int>*)p);
   }
   static void deleteArray_vectorlEunsignedsPintgR(void *p) {
      delete [] ((vector<unsigned int>*)p);
   }
   static void destruct_vectorlEunsignedsPintgR(void *p) {
      typedef vector<unsigned int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned int>

namespace ROOT {
   static TClass *vectorlEunsignedsPchargR_Dictionary();
   static void vectorlEunsignedsPchargR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPchargR(void *p = 0);
   static void *newArray_vectorlEunsignedsPchargR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPchargR(void *p);
   static void deleteArray_vectorlEunsignedsPchargR(void *p);
   static void destruct_vectorlEunsignedsPchargR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned char>*)
   {
      vector<unsigned char> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned char>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned char>", -2, "vector", 214,
                  typeid(vector<unsigned char>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPchargR_Dictionary, isa_proxy, 0,
                  sizeof(vector<unsigned char>) );
      instance.SetNew(&new_vectorlEunsignedsPchargR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPchargR);
      instance.SetDelete(&delete_vectorlEunsignedsPchargR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPchargR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPchargR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned char> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<unsigned char>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPchargR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned char>*)0x0)->GetClass();
      vectorlEunsignedsPchargR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPchargR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPchargR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned char> : new vector<unsigned char>;
   }
   static void *newArray_vectorlEunsignedsPchargR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned char>[nElements] : new vector<unsigned char>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPchargR(void *p) {
      delete ((vector<unsigned char>*)p);
   }
   static void deleteArray_vectorlEunsignedsPchargR(void *p) {
      delete [] ((vector<unsigned char>*)p);
   }
   static void destruct_vectorlEunsignedsPchargR(void *p) {
      typedef vector<unsigned char> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned char>

namespace ROOT {
   static TClass *vectorlEstringgR_Dictionary();
   static void vectorlEstringgR_TClassManip(TClass*);
   static void *new_vectorlEstringgR(void *p = 0);
   static void *newArray_vectorlEstringgR(Long_t size, void *p);
   static void delete_vectorlEstringgR(void *p);
   static void deleteArray_vectorlEstringgR(void *p);
   static void destruct_vectorlEstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<string>*)
   {
      vector<string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<string>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<string>", -2, "vector", 214,
                  typeid(vector<string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEstringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<string>) );
      instance.SetNew(&new_vectorlEstringgR);
      instance.SetNewArray(&newArray_vectorlEstringgR);
      instance.SetDelete(&delete_vectorlEstringgR);
      instance.SetDeleteArray(&deleteArray_vectorlEstringgR);
      instance.SetDestructor(&destruct_vectorlEstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<string>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<string>*)0x0)->GetClass();
      vectorlEstringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string> : new vector<string>;
   }
   static void *newArray_vectorlEstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string>[nElements] : new vector<string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEstringgR(void *p) {
      delete ((vector<string>*)p);
   }
   static void deleteArray_vectorlEstringgR(void *p) {
      delete [] ((vector<string>*)p);
   }
   static void destruct_vectorlEstringgR(void *p) {
      typedef vector<string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<string>

namespace ROOT {
   static TClass *vectorlEshortgR_Dictionary();
   static void vectorlEshortgR_TClassManip(TClass*);
   static void *new_vectorlEshortgR(void *p = 0);
   static void *newArray_vectorlEshortgR(Long_t size, void *p);
   static void delete_vectorlEshortgR(void *p);
   static void deleteArray_vectorlEshortgR(void *p);
   static void destruct_vectorlEshortgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<short>*)
   {
      vector<short> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<short>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<short>", -2, "vector", 214,
                  typeid(vector<short>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEshortgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<short>) );
      instance.SetNew(&new_vectorlEshortgR);
      instance.SetNewArray(&newArray_vectorlEshortgR);
      instance.SetDelete(&delete_vectorlEshortgR);
      instance.SetDeleteArray(&deleteArray_vectorlEshortgR);
      instance.SetDestructor(&destruct_vectorlEshortgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<short> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<short>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEshortgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<short>*)0x0)->GetClass();
      vectorlEshortgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEshortgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEshortgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<short> : new vector<short>;
   }
   static void *newArray_vectorlEshortgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<short>[nElements] : new vector<short>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEshortgR(void *p) {
      delete ((vector<short>*)p);
   }
   static void deleteArray_vectorlEshortgR(void *p) {
      delete [] ((vector<short>*)p);
   }
   static void destruct_vectorlEshortgR(void *p) {
      typedef vector<short> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<short>

namespace ROOT {
   static TClass *vectorlElonggR_Dictionary();
   static void vectorlElonggR_TClassManip(TClass*);
   static void *new_vectorlElonggR(void *p = 0);
   static void *newArray_vectorlElonggR(Long_t size, void *p);
   static void delete_vectorlElonggR(void *p);
   static void deleteArray_vectorlElonggR(void *p);
   static void destruct_vectorlElonggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<long>*)
   {
      vector<long> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<long>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<long>", -2, "vector", 214,
                  typeid(vector<long>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlElonggR_Dictionary, isa_proxy, 0,
                  sizeof(vector<long>) );
      instance.SetNew(&new_vectorlElonggR);
      instance.SetNewArray(&newArray_vectorlElonggR);
      instance.SetDelete(&delete_vectorlElonggR);
      instance.SetDeleteArray(&deleteArray_vectorlElonggR);
      instance.SetDestructor(&destruct_vectorlElonggR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<long> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<long>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlElonggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<long>*)0x0)->GetClass();
      vectorlElonggR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlElonggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlElonggR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<long> : new vector<long>;
   }
   static void *newArray_vectorlElonggR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<long>[nElements] : new vector<long>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlElonggR(void *p) {
      delete ((vector<long>*)p);
   }
   static void deleteArray_vectorlElonggR(void *p) {
      delete [] ((vector<long>*)p);
   }
   static void destruct_vectorlElonggR(void *p) {
      typedef vector<long> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<long>

namespace ROOT {
   static TClass *vectorlEintgR_Dictionary();
   static void vectorlEintgR_TClassManip(TClass*);
   static void *new_vectorlEintgR(void *p = 0);
   static void *newArray_vectorlEintgR(Long_t size, void *p);
   static void delete_vectorlEintgR(void *p);
   static void deleteArray_vectorlEintgR(void *p);
   static void destruct_vectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<int>*)
   {
      vector<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<int>", -2, "vector", 214,
                  typeid(vector<int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<int>) );
      instance.SetNew(&new_vectorlEintgR);
      instance.SetNewArray(&newArray_vectorlEintgR);
      instance.SetDelete(&delete_vectorlEintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEintgR);
      instance.SetDestructor(&destruct_vectorlEintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<int>*)0x0)->GetClass();
      vectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<int> : new vector<int>;
   }
   static void *newArray_vectorlEintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<int>[nElements] : new vector<int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEintgR(void *p) {
      delete ((vector<int>*)p);
   }
   static void deleteArray_vectorlEintgR(void *p) {
      delete [] ((vector<int>*)p);
   }
   static void destruct_vectorlEintgR(void *p) {
      typedef vector<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<int>

namespace ROOT {
   static TClass *vectorlEfloatgR_Dictionary();
   static void vectorlEfloatgR_TClassManip(TClass*);
   static void *new_vectorlEfloatgR(void *p = 0);
   static void *newArray_vectorlEfloatgR(Long_t size, void *p);
   static void delete_vectorlEfloatgR(void *p);
   static void deleteArray_vectorlEfloatgR(void *p);
   static void destruct_vectorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<float>*)
   {
      vector<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<float>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<float>", -2, "vector", 214,
                  typeid(vector<float>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<float>) );
      instance.SetNew(&new_vectorlEfloatgR);
      instance.SetNewArray(&newArray_vectorlEfloatgR);
      instance.SetDelete(&delete_vectorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfloatgR);
      instance.SetDestructor(&destruct_vectorlEfloatgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<float> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<float>*)0x0)->GetClass();
      vectorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfloatgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<float> : new vector<float>;
   }
   static void *newArray_vectorlEfloatgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<float>[nElements] : new vector<float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfloatgR(void *p) {
      delete ((vector<float>*)p);
   }
   static void deleteArray_vectorlEfloatgR(void *p) {
      delete [] ((vector<float>*)p);
   }
   static void destruct_vectorlEfloatgR(void *p) {
      typedef vector<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<float>

namespace ROOT {
   static TClass *vectorlEdoublegR_Dictionary();
   static void vectorlEdoublegR_TClassManip(TClass*);
   static void *new_vectorlEdoublegR(void *p = 0);
   static void *newArray_vectorlEdoublegR(Long_t size, void *p);
   static void delete_vectorlEdoublegR(void *p);
   static void deleteArray_vectorlEdoublegR(void *p);
   static void destruct_vectorlEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<double>*)
   {
      vector<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<double>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<double>", -2, "vector", 214,
                  typeid(vector<double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEdoublegR_Dictionary, isa_proxy, 0,
                  sizeof(vector<double>) );
      instance.SetNew(&new_vectorlEdoublegR);
      instance.SetNewArray(&newArray_vectorlEdoublegR);
      instance.SetDelete(&delete_vectorlEdoublegR);
      instance.SetDeleteArray(&deleteArray_vectorlEdoublegR);
      instance.SetDestructor(&destruct_vectorlEdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<double>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<double>*)0x0)->GetClass();
      vectorlEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double> : new vector<double>;
   }
   static void *newArray_vectorlEdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double>[nElements] : new vector<double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEdoublegR(void *p) {
      delete ((vector<double>*)p);
   }
   static void deleteArray_vectorlEdoublegR(void *p) {
      delete [] ((vector<double>*)p);
   }
   static void destruct_vectorlEdoublegR(void *p) {
      typedef vector<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<double>

namespace ROOT {
   static TClass *vectorlEchargR_Dictionary();
   static void vectorlEchargR_TClassManip(TClass*);
   static void *new_vectorlEchargR(void *p = 0);
   static void *newArray_vectorlEchargR(Long_t size, void *p);
   static void delete_vectorlEchargR(void *p);
   static void deleteArray_vectorlEchargR(void *p);
   static void destruct_vectorlEchargR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<char>*)
   {
      vector<char> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<char>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<char>", -2, "vector", 214,
                  typeid(vector<char>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEchargR_Dictionary, isa_proxy, 0,
                  sizeof(vector<char>) );
      instance.SetNew(&new_vectorlEchargR);
      instance.SetNewArray(&newArray_vectorlEchargR);
      instance.SetDelete(&delete_vectorlEchargR);
      instance.SetDeleteArray(&deleteArray_vectorlEchargR);
      instance.SetDestructor(&destruct_vectorlEchargR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<char> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<char>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEchargR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<char>*)0x0)->GetClass();
      vectorlEchargR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEchargR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEchargR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<char> : new vector<char>;
   }
   static void *newArray_vectorlEchargR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<char>[nElements] : new vector<char>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEchargR(void *p) {
      delete ((vector<char>*)p);
   }
   static void deleteArray_vectorlEchargR(void *p) {
      delete [] ((vector<char>*)p);
   }
   static void destruct_vectorlEchargR(void *p) {
      typedef vector<char> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<char>

namespace ROOT {
   static TClass *vectorlEboolgR_Dictionary();
   static void vectorlEboolgR_TClassManip(TClass*);
   static void *new_vectorlEboolgR(void *p = 0);
   static void *newArray_vectorlEboolgR(Long_t size, void *p);
   static void delete_vectorlEboolgR(void *p);
   static void deleteArray_vectorlEboolgR(void *p);
   static void destruct_vectorlEboolgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<bool>*)
   {
      vector<bool> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<bool>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<bool>", -2, "vector", 526,
                  typeid(vector<bool>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEboolgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<bool>) );
      instance.SetNew(&new_vectorlEboolgR);
      instance.SetNewArray(&newArray_vectorlEboolgR);
      instance.SetDelete(&delete_vectorlEboolgR);
      instance.SetDeleteArray(&deleteArray_vectorlEboolgR);
      instance.SetDestructor(&destruct_vectorlEboolgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<bool> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<bool>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEboolgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<bool>*)0x0)->GetClass();
      vectorlEboolgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEboolgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEboolgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<bool> : new vector<bool>;
   }
   static void *newArray_vectorlEboolgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<bool>[nElements] : new vector<bool>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEboolgR(void *p) {
      delete ((vector<bool>*)p);
   }
   static void deleteArray_vectorlEboolgR(void *p) {
      delete [] ((vector<bool>*)p);
   }
   static void destruct_vectorlEboolgR(void *p) {
      typedef vector<bool> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<bool>

namespace ROOT {
   static TClass *vectorlEULong64_tgR_Dictionary();
   static void vectorlEULong64_tgR_TClassManip(TClass*);
   static void *new_vectorlEULong64_tgR(void *p = 0);
   static void *newArray_vectorlEULong64_tgR(Long_t size, void *p);
   static void delete_vectorlEULong64_tgR(void *p);
   static void deleteArray_vectorlEULong64_tgR(void *p);
   static void destruct_vectorlEULong64_tgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ULong64_t>*)
   {
      vector<ULong64_t> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ULong64_t>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ULong64_t>", -2, "vector", 214,
                  typeid(vector<ULong64_t>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEULong64_tgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<ULong64_t>) );
      instance.SetNew(&new_vectorlEULong64_tgR);
      instance.SetNewArray(&newArray_vectorlEULong64_tgR);
      instance.SetDelete(&delete_vectorlEULong64_tgR);
      instance.SetDeleteArray(&deleteArray_vectorlEULong64_tgR);
      instance.SetDestructor(&destruct_vectorlEULong64_tgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ULong64_t> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ULong64_t>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEULong64_tgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ULong64_t>*)0x0)->GetClass();
      vectorlEULong64_tgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEULong64_tgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEULong64_tgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ULong64_t> : new vector<ULong64_t>;
   }
   static void *newArray_vectorlEULong64_tgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ULong64_t>[nElements] : new vector<ULong64_t>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEULong64_tgR(void *p) {
      delete ((vector<ULong64_t>*)p);
   }
   static void deleteArray_vectorlEULong64_tgR(void *p) {
      delete [] ((vector<ULong64_t>*)p);
   }
   static void destruct_vectorlEULong64_tgR(void *p) {
      typedef vector<ULong64_t> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ULong64_t>

namespace ROOT {
   static TClass *vectorlELong64_tgR_Dictionary();
   static void vectorlELong64_tgR_TClassManip(TClass*);
   static void *new_vectorlELong64_tgR(void *p = 0);
   static void *newArray_vectorlELong64_tgR(Long_t size, void *p);
   static void delete_vectorlELong64_tgR(void *p);
   static void deleteArray_vectorlELong64_tgR(void *p);
   static void destruct_vectorlELong64_tgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Long64_t>*)
   {
      vector<Long64_t> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Long64_t>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Long64_t>", -2, "vector", 214,
                  typeid(vector<Long64_t>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlELong64_tgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Long64_t>) );
      instance.SetNew(&new_vectorlELong64_tgR);
      instance.SetNewArray(&newArray_vectorlELong64_tgR);
      instance.SetDelete(&delete_vectorlELong64_tgR);
      instance.SetDeleteArray(&deleteArray_vectorlELong64_tgR);
      instance.SetDestructor(&destruct_vectorlELong64_tgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Long64_t> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<Long64_t>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlELong64_tgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Long64_t>*)0x0)->GetClass();
      vectorlELong64_tgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlELong64_tgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlELong64_tgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Long64_t> : new vector<Long64_t>;
   }
   static void *newArray_vectorlELong64_tgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Long64_t>[nElements] : new vector<Long64_t>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlELong64_tgR(void *p) {
      delete ((vector<Long64_t>*)p);
   }
   static void deleteArray_vectorlELong64_tgR(void *p) {
      delete [] ((vector<Long64_t>*)p);
   }
   static void destruct_vectorlELong64_tgR(void *p) {
      typedef vector<Long64_t> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Long64_t>

namespace {
  void TriggerDictionaryInitialization_gammacomboCoreDict_Impl() {
    static const char* headers[] = {
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooBinned2DBicubicBase.h",
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooCrossCorPdf.h",
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooHistPdfAngleVar.h",
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooHistPdfVar.h",
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooPoly3Var.h",
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooPoly4Var.h",
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooSlimFitResult.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.08.02-x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/./core/include",
"/usr/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.08.02-x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/build/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "gammacomboCoreDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
template <class TYPE> class __attribute__((annotate("$clingAutoload$SharedArray.h")))  __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooBinned2DBicubicBase.h")))  SharedArrayImp;

template <class TYPE> class __attribute__((annotate("$clingAutoload$SharedArray.h")))  __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooBinned2DBicubicBase.h")))  SharedArray;

class __attribute__((annotate("$clingAutoload$RooAbsReal.h")))  __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooBinned2DBicubicBase.h")))  RooAbsReal;
template <class BASE> class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooBinned2DBicubicBase.h")))  RooBinned2DBicubicBase;

class __attribute__((annotate("$clingAutoload$RooAbsPdf.h")))  __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooBinned2DBicubicBase.h")))  RooAbsPdf;
class __attribute__((annotate(R"ATTRDUMP(RooCrossCorPdf function PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooCrossCorPdf.h")))  RooCrossCorPdf;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooHistPdfAngleVar.h")))  RooHistPdfAngleVar;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooHistPdfVar.h")))  RooHistPdfVar;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooPoly3Var.h")))  RooPoly3Var;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooPoly4Var.h")))  RooPoly4Var;
class __attribute__((annotate(R"ATTRDUMP(defines version number, ClassDef is a macro)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooSlimFitResult.h")))  RooSlimFitResult;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "gammacomboCoreDict dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooBinned2DBicubicBase.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooCrossCorPdf.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooHistPdfAngleVar.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooHistPdfVar.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooPoly3Var.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooPoly4Var.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/core/include/RooSlimFitResult.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"RooBinned2DBicubicBase<RooAbsPdf>", payloadCode, "@",
"RooBinned2DBicubicBase<RooAbsReal>", payloadCode, "@",
"RooCrossCorPdf", payloadCode, "@",
"RooHistPdfAngleVar", payloadCode, "@",
"RooHistPdfVar", payloadCode, "@",
"RooPoly3Var", payloadCode, "@",
"RooPoly4Var", payloadCode, "@",
"RooSlimFitResult", payloadCode, "@",
"SharedArray<Long64_t>", payloadCode, "@",
"SharedArray<ULong64_t>", payloadCode, "@",
"SharedArray<char>", payloadCode, "@",
"SharedArray<double>", payloadCode, "@",
"SharedArray<float>", payloadCode, "@",
"SharedArray<int>", payloadCode, "@",
"SharedArray<long>", payloadCode, "@",
"SharedArray<short>", payloadCode, "@",
"SharedArray<unsigned char>", payloadCode, "@",
"SharedArray<unsigned int>", payloadCode, "@",
"SharedArray<unsigned long>", payloadCode, "@",
"SharedArray<unsigned short>", payloadCode, "@",
"SharedArrayImp<Long64_t>", payloadCode, "@",
"SharedArrayImp<ULong64_t>", payloadCode, "@",
"SharedArrayImp<char>", payloadCode, "@",
"SharedArrayImp<double>", payloadCode, "@",
"SharedArrayImp<float>", payloadCode, "@",
"SharedArrayImp<int>", payloadCode, "@",
"SharedArrayImp<long>", payloadCode, "@",
"SharedArrayImp<short>", payloadCode, "@",
"SharedArrayImp<unsigned char>", payloadCode, "@",
"SharedArrayImp<unsigned int>", payloadCode, "@",
"SharedArrayImp<unsigned long>", payloadCode, "@",
"SharedArrayImp<unsigned short>", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("gammacomboCoreDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_gammacomboCoreDict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_gammacomboCoreDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_gammacomboCoreDict() {
  TriggerDictionaryInitialization_gammacomboCoreDict_Impl();
}
