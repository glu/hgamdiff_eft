# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/HGamEFTScanner/main/HGamEFTScanner.cpp" "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/build/HGamEFTScanner/CMakeFiles/HGamEFTScanner.dir/main/HGamEFTScanner.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/build/HGamEFTScanner/CMakeFiles/HGamEFTScannerComponents.dir/DependInfo.cmake"
  "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/build/CMakeFiles/gammacomboCoreComponents.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../prof-driver-0.4.0-for-2.3.1/include/ProfDriver"
  "/cvmfs/sft.cern.ch/lcg/releases/LCG_87/MCGenerators/professor/2.2.1/x86_64-slc6-gcc49-opt/include"
  "../HGamEFTScanner/./include"
  "../HGamEFTScanner/./core/include"
  "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.08.02-x86_64-slc6-gcc49-opt/include"
  ".././core/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
