// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME HGamEFTScannerDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/HGamEFTScanner/include/RooProfDriverWrapper.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/HGamEFTScanner/include/RooHGamEFTMultiVarGaussian.h"

// Header files passed via #pragma extra_include

namespace Professor {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *Professor_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("Professor", 0 /*version*/, "Professor/ParamPoints.h", 11,
                     ::ROOT::Internal::DefineBehavior((void*)0,(void*)0),
                     &Professor_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *Professor_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static TClass *ProfDriver_Dictionary();
   static void ProfDriver_TClassManip(TClass*);
   static void *new_ProfDriver(void *p = 0);
   static void *newArray_ProfDriver(Long_t size, void *p);
   static void delete_ProfDriver(void *p);
   static void deleteArray_ProfDriver(void *p);
   static void destruct_ProfDriver(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ProfDriver*)
   {
      ::ProfDriver *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ProfDriver));
      static ::ROOT::TGenericClassInfo 
         instance("ProfDriver", "ProfDriver.h", 9,
                  typeid(::ProfDriver), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ProfDriver_Dictionary, isa_proxy, 0,
                  sizeof(::ProfDriver) );
      instance.SetNew(&new_ProfDriver);
      instance.SetNewArray(&newArray_ProfDriver);
      instance.SetDelete(&delete_ProfDriver);
      instance.SetDeleteArray(&deleteArray_ProfDriver);
      instance.SetDestructor(&destruct_ProfDriver);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ProfDriver*)
   {
      return GenerateInitInstanceLocal((::ProfDriver*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ProfDriver*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ProfDriver_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ProfDriver*)0x0)->GetClass();
      ProfDriver_TClassManip(theClass);
   return theClass;
   }

   static void ProfDriver_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_RooProfDriverWrapper(void *p = 0);
   static void *newArray_RooProfDriverWrapper(Long_t size, void *p);
   static void delete_RooProfDriverWrapper(void *p);
   static void deleteArray_RooProfDriverWrapper(void *p);
   static void destruct_RooProfDriverWrapper(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooProfDriverWrapper*)
   {
      ::RooProfDriverWrapper *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooProfDriverWrapper >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooProfDriverWrapper", ::RooProfDriverWrapper::Class_Version(), "RooProfDriverWrapper.h", 30,
                  typeid(::RooProfDriverWrapper), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooProfDriverWrapper::Dictionary, isa_proxy, 4,
                  sizeof(::RooProfDriverWrapper) );
      instance.SetNew(&new_RooProfDriverWrapper);
      instance.SetNewArray(&newArray_RooProfDriverWrapper);
      instance.SetDelete(&delete_RooProfDriverWrapper);
      instance.SetDeleteArray(&deleteArray_RooProfDriverWrapper);
      instance.SetDestructor(&destruct_RooProfDriverWrapper);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooProfDriverWrapper*)
   {
      return GenerateInitInstanceLocal((::RooProfDriverWrapper*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooProfDriverWrapper*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooHGamEFTMultiVarGaussian(void *p = 0);
   static void *newArray_RooHGamEFTMultiVarGaussian(Long_t size, void *p);
   static void delete_RooHGamEFTMultiVarGaussian(void *p);
   static void deleteArray_RooHGamEFTMultiVarGaussian(void *p);
   static void destruct_RooHGamEFTMultiVarGaussian(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooHGamEFTMultiVarGaussian*)
   {
      ::RooHGamEFTMultiVarGaussian *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooHGamEFTMultiVarGaussian >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooHGamEFTMultiVarGaussian", ::RooHGamEFTMultiVarGaussian::Class_Version(), "RooHGamEFTMultiVarGaussian.h", 20,
                  typeid(::RooHGamEFTMultiVarGaussian), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooHGamEFTMultiVarGaussian::Dictionary, isa_proxy, 4,
                  sizeof(::RooHGamEFTMultiVarGaussian) );
      instance.SetNew(&new_RooHGamEFTMultiVarGaussian);
      instance.SetNewArray(&newArray_RooHGamEFTMultiVarGaussian);
      instance.SetDelete(&delete_RooHGamEFTMultiVarGaussian);
      instance.SetDeleteArray(&deleteArray_RooHGamEFTMultiVarGaussian);
      instance.SetDestructor(&destruct_RooHGamEFTMultiVarGaussian);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooHGamEFTMultiVarGaussian*)
   {
      return GenerateInitInstanceLocal((::RooHGamEFTMultiVarGaussian*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooHGamEFTMultiVarGaussian*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr RooProfDriverWrapper::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooProfDriverWrapper::Class_Name()
{
   return "RooProfDriverWrapper";
}

//______________________________________________________________________________
const char *RooProfDriverWrapper::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooProfDriverWrapper*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooProfDriverWrapper::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooProfDriverWrapper*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooProfDriverWrapper::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooProfDriverWrapper*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooProfDriverWrapper::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooProfDriverWrapper*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooHGamEFTMultiVarGaussian::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooHGamEFTMultiVarGaussian::Class_Name()
{
   return "RooHGamEFTMultiVarGaussian";
}

//______________________________________________________________________________
const char *RooHGamEFTMultiVarGaussian::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooHGamEFTMultiVarGaussian*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooHGamEFTMultiVarGaussian::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooHGamEFTMultiVarGaussian*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooHGamEFTMultiVarGaussian::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooHGamEFTMultiVarGaussian*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooHGamEFTMultiVarGaussian::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooHGamEFTMultiVarGaussian*)0x0)->GetClass(); }
   return fgIsA;
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ProfDriver(void *p) {
      return  p ? new(p) ::ProfDriver : new ::ProfDriver;
   }
   static void *newArray_ProfDriver(Long_t nElements, void *p) {
      return p ? new(p) ::ProfDriver[nElements] : new ::ProfDriver[nElements];
   }
   // Wrapper around operator delete
   static void delete_ProfDriver(void *p) {
      delete ((::ProfDriver*)p);
   }
   static void deleteArray_ProfDriver(void *p) {
      delete [] ((::ProfDriver*)p);
   }
   static void destruct_ProfDriver(void *p) {
      typedef ::ProfDriver current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ProfDriver

//______________________________________________________________________________
void RooProfDriverWrapper::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooProfDriverWrapper.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooProfDriverWrapper::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooProfDriverWrapper::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooProfDriverWrapper(void *p) {
      return  p ? new(p) ::RooProfDriverWrapper : new ::RooProfDriverWrapper;
   }
   static void *newArray_RooProfDriverWrapper(Long_t nElements, void *p) {
      return p ? new(p) ::RooProfDriverWrapper[nElements] : new ::RooProfDriverWrapper[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooProfDriverWrapper(void *p) {
      delete ((::RooProfDriverWrapper*)p);
   }
   static void deleteArray_RooProfDriverWrapper(void *p) {
      delete [] ((::RooProfDriverWrapper*)p);
   }
   static void destruct_RooProfDriverWrapper(void *p) {
      typedef ::RooProfDriverWrapper current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooProfDriverWrapper

//______________________________________________________________________________
void RooHGamEFTMultiVarGaussian::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooHGamEFTMultiVarGaussian.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooHGamEFTMultiVarGaussian::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooHGamEFTMultiVarGaussian::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooHGamEFTMultiVarGaussian(void *p) {
      return  p ? new(p) ::RooHGamEFTMultiVarGaussian : new ::RooHGamEFTMultiVarGaussian;
   }
   static void *newArray_RooHGamEFTMultiVarGaussian(Long_t nElements, void *p) {
      return p ? new(p) ::RooHGamEFTMultiVarGaussian[nElements] : new ::RooHGamEFTMultiVarGaussian[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooHGamEFTMultiVarGaussian(void *p) {
      delete ((::RooHGamEFTMultiVarGaussian*)p);
   }
   static void deleteArray_RooHGamEFTMultiVarGaussian(void *p) {
      delete [] ((::RooHGamEFTMultiVarGaussian*)p);
   }
   static void destruct_RooHGamEFTMultiVarGaussian(void *p) {
      typedef ::RooHGamEFTMultiVarGaussian current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooHGamEFTMultiVarGaussian

namespace ROOT {
   static TClass *vectorlEvectorlETStringgRsPgR_Dictionary();
   static void vectorlEvectorlETStringgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlETStringgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlETStringgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlETStringgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlETStringgRsPgR(void *p);
   static void destruct_vectorlEvectorlETStringgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<TString> >*)
   {
      vector<vector<TString> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<TString> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<TString> >", -2, "vector", 214,
                  typeid(vector<vector<TString> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlETStringgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<vector<TString> >) );
      instance.SetNew(&new_vectorlEvectorlETStringgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlETStringgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlETStringgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlETStringgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlETStringgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<TString> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<TString> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlETStringgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<TString> >*)0x0)->GetClass();
      vectorlEvectorlETStringgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlETStringgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlETStringgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<TString> > : new vector<vector<TString> >;
   }
   static void *newArray_vectorlEvectorlETStringgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<TString> >[nElements] : new vector<vector<TString> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlETStringgRsPgR(void *p) {
      delete ((vector<vector<TString> >*)p);
   }
   static void deleteArray_vectorlEvectorlETStringgRsPgR(void *p) {
      delete [] ((vector<vector<TString> >*)p);
   }
   static void destruct_vectorlEvectorlETStringgRsPgR(void *p) {
      typedef vector<vector<TString> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<TString> >

namespace ROOT {
   static TClass *vectorlEdoublegR_Dictionary();
   static void vectorlEdoublegR_TClassManip(TClass*);
   static void *new_vectorlEdoublegR(void *p = 0);
   static void *newArray_vectorlEdoublegR(Long_t size, void *p);
   static void delete_vectorlEdoublegR(void *p);
   static void deleteArray_vectorlEdoublegR(void *p);
   static void destruct_vectorlEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<double>*)
   {
      vector<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<double>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<double>", -2, "vector", 214,
                  typeid(vector<double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEdoublegR_Dictionary, isa_proxy, 0,
                  sizeof(vector<double>) );
      instance.SetNew(&new_vectorlEdoublegR);
      instance.SetNewArray(&newArray_vectorlEdoublegR);
      instance.SetDelete(&delete_vectorlEdoublegR);
      instance.SetDeleteArray(&deleteArray_vectorlEdoublegR);
      instance.SetDestructor(&destruct_vectorlEdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<double>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<double>*)0x0)->GetClass();
      vectorlEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double> : new vector<double>;
   }
   static void *newArray_vectorlEdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double>[nElements] : new vector<double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEdoublegR(void *p) {
      delete ((vector<double>*)p);
   }
   static void deleteArray_vectorlEdoublegR(void *p) {
      delete [] ((vector<double>*)p);
   }
   static void destruct_vectorlEdoublegR(void *p) {
      typedef vector<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<double>

namespace ROOT {
   static TClass *vectorlETStringgR_Dictionary();
   static void vectorlETStringgR_TClassManip(TClass*);
   static void *new_vectorlETStringgR(void *p = 0);
   static void *newArray_vectorlETStringgR(Long_t size, void *p);
   static void delete_vectorlETStringgR(void *p);
   static void deleteArray_vectorlETStringgR(void *p);
   static void destruct_vectorlETStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TString>*)
   {
      vector<TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TString>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TString>", -2, "vector", 214,
                  typeid(vector<TString>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETStringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<TString>) );
      instance.SetNew(&new_vectorlETStringgR);
      instance.SetNewArray(&newArray_vectorlETStringgR);
      instance.SetDelete(&delete_vectorlETStringgR);
      instance.SetDeleteArray(&deleteArray_vectorlETStringgR);
      instance.SetDestructor(&destruct_vectorlETStringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TString> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<TString>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TString>*)0x0)->GetClass();
      vectorlETStringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETStringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TString> : new vector<TString>;
   }
   static void *newArray_vectorlETStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TString>[nElements] : new vector<TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETStringgR(void *p) {
      delete ((vector<TString>*)p);
   }
   static void deleteArray_vectorlETStringgR(void *p) {
      delete [] ((vector<TString>*)p);
   }
   static void destruct_vectorlETStringgR(void *p) {
      typedef vector<TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TString>

namespace ROOT {
   static TClass *vectorlERooProfDriverWrappergR_Dictionary();
   static void vectorlERooProfDriverWrappergR_TClassManip(TClass*);
   static void *new_vectorlERooProfDriverWrappergR(void *p = 0);
   static void *newArray_vectorlERooProfDriverWrappergR(Long_t size, void *p);
   static void delete_vectorlERooProfDriverWrappergR(void *p);
   static void deleteArray_vectorlERooProfDriverWrappergR(void *p);
   static void destruct_vectorlERooProfDriverWrappergR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<RooProfDriverWrapper>*)
   {
      vector<RooProfDriverWrapper> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<RooProfDriverWrapper>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<RooProfDriverWrapper>", -2, "vector", 214,
                  typeid(vector<RooProfDriverWrapper>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlERooProfDriverWrappergR_Dictionary, isa_proxy, 0,
                  sizeof(vector<RooProfDriverWrapper>) );
      instance.SetNew(&new_vectorlERooProfDriverWrappergR);
      instance.SetNewArray(&newArray_vectorlERooProfDriverWrappergR);
      instance.SetDelete(&delete_vectorlERooProfDriverWrappergR);
      instance.SetDeleteArray(&deleteArray_vectorlERooProfDriverWrappergR);
      instance.SetDestructor(&destruct_vectorlERooProfDriverWrappergR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<RooProfDriverWrapper> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<RooProfDriverWrapper>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlERooProfDriverWrappergR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<RooProfDriverWrapper>*)0x0)->GetClass();
      vectorlERooProfDriverWrappergR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlERooProfDriverWrappergR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlERooProfDriverWrappergR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<RooProfDriverWrapper> : new vector<RooProfDriverWrapper>;
   }
   static void *newArray_vectorlERooProfDriverWrappergR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<RooProfDriverWrapper>[nElements] : new vector<RooProfDriverWrapper>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlERooProfDriverWrappergR(void *p) {
      delete ((vector<RooProfDriverWrapper>*)p);
   }
   static void deleteArray_vectorlERooProfDriverWrappergR(void *p) {
      delete [] ((vector<RooProfDriverWrapper>*)p);
   }
   static void destruct_vectorlERooProfDriverWrappergR(void *p) {
      typedef vector<RooProfDriverWrapper> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<RooProfDriverWrapper>

namespace ROOT {
   static TClass *vectorlEProfDrivergR_Dictionary();
   static void vectorlEProfDrivergR_TClassManip(TClass*);
   static void *new_vectorlEProfDrivergR(void *p = 0);
   static void *newArray_vectorlEProfDrivergR(Long_t size, void *p);
   static void delete_vectorlEProfDrivergR(void *p);
   static void deleteArray_vectorlEProfDrivergR(void *p);
   static void destruct_vectorlEProfDrivergR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ProfDriver>*)
   {
      vector<ProfDriver> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ProfDriver>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ProfDriver>", -2, "vector", 214,
                  typeid(vector<ProfDriver>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEProfDrivergR_Dictionary, isa_proxy, 0,
                  sizeof(vector<ProfDriver>) );
      instance.SetNew(&new_vectorlEProfDrivergR);
      instance.SetNewArray(&newArray_vectorlEProfDrivergR);
      instance.SetDelete(&delete_vectorlEProfDrivergR);
      instance.SetDeleteArray(&deleteArray_vectorlEProfDrivergR);
      instance.SetDestructor(&destruct_vectorlEProfDrivergR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ProfDriver> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ProfDriver>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEProfDrivergR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ProfDriver>*)0x0)->GetClass();
      vectorlEProfDrivergR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEProfDrivergR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEProfDrivergR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ProfDriver> : new vector<ProfDriver>;
   }
   static void *newArray_vectorlEProfDrivergR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ProfDriver>[nElements] : new vector<ProfDriver>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEProfDrivergR(void *p) {
      delete ((vector<ProfDriver>*)p);
   }
   static void deleteArray_vectorlEProfDrivergR(void *p) {
      delete [] ((vector<ProfDriver>*)p);
   }
   static void destruct_vectorlEProfDrivergR(void *p) {
      typedef vector<ProfDriver> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ProfDriver>

namespace ROOT {
   static TClass *maplETStringcOTStringgR_Dictionary();
   static void maplETStringcOTStringgR_TClassManip(TClass*);
   static void *new_maplETStringcOTStringgR(void *p = 0);
   static void *newArray_maplETStringcOTStringgR(Long_t size, void *p);
   static void delete_maplETStringcOTStringgR(void *p);
   static void deleteArray_maplETStringcOTStringgR(void *p);
   static void destruct_maplETStringcOTStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,TString>*)
   {
      map<TString,TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,TString>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,TString>", -2, "map", 96,
                  typeid(map<TString,TString>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOTStringgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,TString>) );
      instance.SetNew(&new_maplETStringcOTStringgR);
      instance.SetNewArray(&newArray_maplETStringcOTStringgR);
      instance.SetDelete(&delete_maplETStringcOTStringgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOTStringgR);
      instance.SetDestructor(&destruct_maplETStringcOTStringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,TString> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<TString,TString>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOTStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,TString>*)0x0)->GetClass();
      maplETStringcOTStringgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOTStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOTStringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,TString> : new map<TString,TString>;
   }
   static void *newArray_maplETStringcOTStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,TString>[nElements] : new map<TString,TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOTStringgR(void *p) {
      delete ((map<TString,TString>*)p);
   }
   static void deleteArray_maplETStringcOTStringgR(void *p) {
      delete [] ((map<TString,TString>*)p);
   }
   static void destruct_maplETStringcOTStringgR(void *p) {
      typedef map<TString,TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,TString>

namespace {
  void TriggerDictionaryInitialization_HGamEFTScannerDict_Impl() {
    static const char* headers[] = {
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/HGamEFTScanner/include/RooProfDriverWrapper.h",
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/HGamEFTScanner/include/RooHGamEFTMultiVarGaussian.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/prof-driver-0.4.0-for-2.3.1/include/ProfDriver",
"/cvmfs/sft.cern.ch/lcg/releases/LCG_87/MCGenerators/professor/2.2.1/x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/HGamEFTScanner/./include",
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/HGamEFTScanner/./core/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.08.02-x86_64-slc6-gcc49-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.08.02-x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/./core/include",
"/usr/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.08.02-x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/build/HGamEFTScanner/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "HGamEFTScannerDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$ProfDriver.h")))  __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/HGamEFTScanner/include/RooProfDriverWrapper.h")))  ProfDriver;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/HGamEFTScanner/include/RooProfDriverWrapper.h")))  RooProfDriverWrapper;
class __attribute__((annotate(R"ATTRDUMP(Multivariate Gaussian PDF with correlations)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/HGamEFTScanner/include/RooHGamEFTMultiVarGaussian.h")))  RooHGamEFTMultiVarGaussian;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "HGamEFTScannerDict dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/HGamEFTScanner/include/RooProfDriverWrapper.h"
#include "/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/HGamEFTScanner/include/RooHGamEFTMultiVarGaussian.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"ProfDriver", payloadCode, "@",
"RooHGamEFTMultiVarGaussian", payloadCode, "@",
"RooProfDriverWrapper", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("HGamEFTScannerDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_HGamEFTScannerDict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_HGamEFTScannerDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_HGamEFTScannerDict() {
  TriggerDictionaryInitialization_HGamEFTScannerDict_Impl();
}
