:mod:`professor.params`
=======================

.. automodule:: professor.params
   :members:
   :undoc-members:
   :show-inheritance:
