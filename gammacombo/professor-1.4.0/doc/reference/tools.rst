:mod:`professor.tools`
======================

This module contains helper functions for common tasks in the Professor
system. Some may also be useful for user scripts, such as logging and
file/directory handling.


:mod:`professor.tools.io`
-------------------------
.. automodule:: professor.tools.io
   :members:
   :undoc-members:
   :show-inheritance:

:mod:`professor.tools.decorators`
---------------------------------
.. automodule:: professor.tools.decorators
   :members:
   :undoc-members:
   :show-inheritance:

:mod:`professor.tools.stats`
----------------------------
.. automodule:: professor.tools.stats
   :members:
   :undoc-members:
   :show-inheritance:

:mod:`professor.tools.permut`
-----------------------------
.. automodule:: professor.tools.permut
   :members:
   :undoc-members:
   :show-inheritance:

:mod:`professor.tools.eigen`
----------------------------
.. automodule:: professor.tools.eigen
   :members:
   :undoc-members:
   :show-inheritance:

:mod:`professor.tools.pointsampling`
------------------------------------
.. automodule:: professor.tools.pointsampling
   :members:
   :undoc-members:
   :show-inheritance:

:mod:`professor.tools.config`
-----------------------------
.. automodule:: professor.tools.config
   :members:
   :undoc-members:
   :show-inheritance:
