:mod:`professor.histo`
======================

This module contains simple containers for histograms
(:class:`~professor.histo.Histo`) and bins
(:class:`~professor.histo.Bin`) and a
parser for rivet's plot info files
(:class:`~professor.histo.PlotParser`).

.. automodule:: professor.histo
   :members:
   :undoc-members:
   :show-inheritance:
