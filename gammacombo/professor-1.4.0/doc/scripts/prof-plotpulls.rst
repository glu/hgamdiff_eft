Visualize deviations between MC and reference plots -- :program:`prof-plotpulls`
--------------------------------------------------------------------------------

.. create some short-cuts to link to other documents
.. |prof-tune| replace:: :doc:`prof-tune <prof-tune>`

.. program:: prof-plotpulls

Create PDF pull plots for a MC data file relative to a reference data file. The
:option:`--weights` / :option:`--obsfile` argument may be used to restrict the
calculated pulls to a subset of the observables in the MC data file.

Example
^^^^^^^

Plot pull distributions for all the observables in `mc.aida`:

    prof-plotpulls mc.aida

Plot pull distributions for a subset of the observables in `mc.aida`, using the
reference data in the `DATADIR`:

    prof-plotpulls --datadir=DATADIR --obsfile weightsfile mc.aida
