Viewing parameterisation details -- :program:`prof-showipol`
------------------------------------------------------------

.. program:: prof-showipol

The :program:`prof-showipol` script is a very simple one to let you view the
definitions of ipol pickle files in `DATADIR/ipol/`.


Examples
^^^^^^^^
::

    prof-showipol mydata/ipol/profipol_cubic_f8f1fd804b25a3e610aeb1d18b282385.pkl

These will write out the MC runs in this hashed ipol. Multiple pickle files may be specified.


Command-line options
^^^^^^^^^^^^^^^^^^^^

.. cmdoption:: -r, --runs

   Show the list of MC runs (default).

.. cmdoption:: -R, --no-runs

   Do not show the MC-runs.

.. cmdoption::  -n, --numruns

   Show the number of MC-runs (default).

.. cmdoption::  -N, --no-numruns

   Do not show the number of MC-runs.

.. cmdoption::  -m, --method

   Show the used interpolation method.

.. cmdoption::  -M, --no-method

   Do not show the used interpolation method (default).

.. cmdoption::  -o, --observables

   Show the interpolated observables.

.. cmdoption::  -O, --no-observables

   Do not show the interpolated observables (default).

.. .. cmdoption::  -b BINIDS, --binids=BINIDS
..                         available binids for observable
..   -c, --center          show the center used for interpolating
..   -C, --no-center       do not show the center used for interpolatin
