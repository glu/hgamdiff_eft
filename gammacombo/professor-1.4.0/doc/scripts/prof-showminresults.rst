Viewing tune result details -- :program:`prof-showminresults`
-------------------------------------------------------------

.. program:: prof-showminresults

The :program:`prof-showminresults` script is a very simple one to let you view
the details of a tune from a results pickle file, including a small summary in
terms of weighted means of all the results.


Examples
^^^^^^^^
::

    prof-showminresults mydata/tunes/mytune/results.pkl

These will write out the MC runs in this results file. Multiple pickle files may be specified.


Command-line options
^^^^^^^^^^^^^^^^^^^^

None! Just the pickle file arguments, please.
