Interactive parameterisation GUI -- :program:`prof-I`
-----------------------------------------------------

.. |prof-I| replace:: :program:`prof-I`
.. |prof-interpolate| replace:: :doc:`prof-interpolate <prof-interpolate>`
.. |prof-tune| replace:: :doc:`prof-tune <prof-tune>`

.. program:: prof-I

|prof-I| is a graphical user interface which reads reference data and
|prof-interpolate| interpolation files from a data directory and allows you to
interactively explore how variations of parameters will affect different
observables. Two observables can be viewed at once, and as many parameters can
be varied as you can fit on screen!

|prof-I| can be very useful in the early stages of a tune for parameter space
 exploration, for debugging if the minimiser or parameterisations seem to be
 failing, or for investigation if the physics model itself is struggling.

The :option:`--datadir` `DATADIR` and related options are used as normal to
specify the reference data, MC runs, and interpolation objects: see the :doc:`path
options <options-paths>` page.

More usage details will be provided later, but we hope that the user interface
is reasonably clear. To run it, just use the same command line options as you
would with |prof-tune|.

.. todo::
    Document the parameter name -> description translation and any non-obvious
    GUI details.
