The Professor toolkit
=====================

Professor is a tool which creates parameterisations of very CPU-expensive
functions, and uses them for tasks which require a lot of sampling -- such as
optimising input parameters or scanning a large parameter volume. The system was
developed for (and is still mostly used in) tuning of Monte Carlo event
generators for particle physics, but the method is very generally useful.


Contents:

.. toctree::
    :maxdepth: 2

    install.rst
    tutorial.rst
    userguide.rst
    refdoc.rst
    todo.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Tools for plotting
------------------

See docs in Rivet for

- `compare-histos <compare_histos.html>`_
- `make-plots <make_plot.html>`_
