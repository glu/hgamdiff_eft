Professor TODO
==============

For 1.4.0:
----------

* Reading MC and reference histos in YODA _or_ AIDA format (SIMONE, ANDY)

   Done?

* Write out ipolhistos in YODA format rather than (or as well as?) AIDA.

   Done by Holger.

* Check correlation implementation, steering format and behaviour (SIMONE)

   Simone... results?


For 2.0.0:
----------

* Rewrite much more minimally -- fewer scripts, simple API

   Redesign to reflect experience: weight/obs file important, points/bins are
   fundamental and should be efficiently parallelisable. Lightweight groupings
   of bins/points into histograms and analyses for plotting and correlated
   error & chi2 calculation. Keep generic polynomial interpolator and
   eigentunes. Greatly reduce number of scripts.

   Cleverer handling of null bins, i.e. detect, warn, and skip (or whatever)
   bins with nan values or zero errors (undefined errs can be dealt with better
   via YODA -- 100% err would be more appropriate than zero err? Or really set a
   nan error value.)

   Build in normalisation strategies, etc. such as first-bin/all-bin/whole
   dbn/none in the weight file (already done?)  Histo to store overall norm from
   YODA histo conversion? Weight file to allow specifying ranges by bin number
   as well as physical variable and to allow use of regexes. Weight file to
   allow specifying transforms to variables, the obvious one being a log-space
   ipol.

   Sampling: sampling script to be able to explicitly devote a sampling fraction
   to walls and corners. Syntax for sampling tied variables and for log
   sampling. Tuning/ipol scripts to distinguish between requested sampling
   ranges and achieved sampling ranges.


**********
OLD PLANS!
**********

For 1.3.1:
----------

* Test correlated systematic errors and remove 'EXPERIMENTAL' declaration (DANIEL, HOLGER)

* Fix treatment of param sorting: use internal alphabetical sorting, not least
  for much-needed simplicity, but make the output of param sets to terminal/file
  from prof-tune etc. use the ordering from the first run's used_params
  file. (HENDRIK and ANDY)

* Allow multi-observable weight specifications in WeightManager, using a
  regex. Requires a rewrite of the WM and Weights objects, not least to
  internally store a list rather than a dict and to permit overlaps, so it's
  not something to be done lightly but I think it will end up being both simpler
  and more powerful. (ANDY)

* Optional parameterising of log(observable) -- specify in weights file. (ANDY)

* Sphinx docs completion -- just make sure that we have at least a very minimal
  doc page for prof-checkspace and prof-terror.

* Allow prof-plotcorrelations to operate at any parameter point, using the same
  options flags as prof-sensitivities, and rename as prof-correlations (we don't
  call the other ``prof-plotsensitivities``, and simplicity is good). (DANIEL)

* Add a radio-button toggle to prof-I to allow viewing the plot as a ratio -- if
  there is ref data. (HOLGER)

* Partially sanitize TeX -> text strings in prof-I, e.g. getting rid of markup like
  ``\,``, ``\mathrm``, ``\text``, etc. (ANDY)

* Remove the mandatory "licensing" message from the prof-* scripts other than
  prof-interpolate and prof-tune... it's just annoying, particularly with the
  prof-show* scripts.

* prof-I: show overall (weighted, via CL option) chi2 as well as unweighted
  per-plot chi2. Speed problem in applying the weights? Ignore correlations for
  now? (DANIEL)

* scanchi2 should be able to plot chi2s from ipol (ANDY)

* prof-interpolate/prof-tune: Include the number of runs in the name of the
  pickle file, for easier identification. Backward compatibility? (ANDY)

* prof-ipol: Don't remake already-done ipols unless --force'd (extend to
  prof-tune?) (ANDY)

* prof-plotresultsscatter: Allow use of params files as inputs in place of
  pickle files. Rename --ranges -> --limits, with a behaviour as for the
  --limits option to prof-tune. Draw the limits for *all* the plotted runcombs,
  or the most restricted set?

* Display options in prof-envelopes: show individual MC run lines, show
  density-shaded envelopes, show multiple overlaid CLs.

* prof-I: display a background envelope calculated from the anchor points. Are
  the max and min stored in the bin ipol object? We should do so.

* prof-tune: make sure that the start point is inside the limits, i.e. if "center"
  is used together with --limits, we should center within the limits. And if a
  parameter is fixed, we should use that value as starting point.


For 1.4.0:
----------

* Parallelising: get threaded Minuit to work (requires bug report to ROOT?);
  explicit multithreading of many-bin ipol building and many-runcomb minimising;
  try using GPU computation in e.g. GSL linear algebra. PyCUDA looks very neat:
  this would be a nice (part of a) MSc or summer student project.

* Check Python 2to3 compatibility.

* Finish error bands study! 3-param JIMMY tune -> 100 "stat" smearings around
  each of 100 "sys" minimisation points, using many different interpolations.

* Investigate smearing by sampling mock ref data from its errors -- instead of /
  as well as run-combinations. This is the method used for replica building by NNPDF.

* WeightManager should provide a wm.getValue("/path/to/MYHIST:42") if at all possible.

* Histo loading from multiple formats via/with YODA.

* Parameterising the MC errors? Any use-case or does the median anchor point
  error do a good enough job?

* Think about generalising the weights file to allow more things than just
  weights to be specified: the value could be a dict to also use different
  parameterisations for different histos/regions. In this model, the epsilon
  errors and so-on would also live in this one file rather than a separate one
  with the scalar format used for weights.

* Boundary sampling: generate samplings on the walls and corners of the space to
  constrain away parameterisation deviations outside the sampled region. We
  would need to keep these runs separate, so that they could all be used in all
  ipol buildings, with the runcomb sampling only happening to the "bulk" points.

* Use new style string formatting and "with ... as foo:" file handling: requires
  Python >= what version? Use from __future__ import ... mechanism or require
  Python 2.6... I guess the former for now.

* Clean ResultList interface.

* Modify prof-plotpulls to plot observable comparison plots comparing ipols to
  anchor points, as a way of checking ipol performance without having to do new
  line scans.

* Use clearer vetoing for inputs to prof-sensitivities: should we exclude bins
  with few stats or leave that up to the user via the prof-sensitivities interface?
