*******************************
Library Reference Documentation
*******************************

.. note::
    The professor package contains a meta-module :mod:`professor.user` that
    exports all data-structures and functions intended to be used outside of
    the framework. It should be imported with

        >>> import professor.user as prof

    All examples use this as silent convention.


.. toctree::
    :maxdepth: 1

    reference/data.rst
    reference/params.rst
    reference/fitfunctions.rst
    reference/interpolation.rst
    reference/minimize.rst
    reference/histo.rst
    reference/tools.rst
