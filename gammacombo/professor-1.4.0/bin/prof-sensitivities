#! /usr/bin/env python

"""\
%prog [--datadir DATADIR|--ipoldir IPOLDIR] --obsfile OBSFILE --runs RUNSFILE
      [--plotmode (extremal|extrandom|colormap|colormaplim|colormapobslim)]

E.g:

    %prog  --datadir mydata/ --weights weights --runs myruns

    %prog  --datadir mydata/ --weights weights --runs myruns --plotmode colormaplim

Produce sensitivity plots. The plot files are put in OUTDIR/sensitivities.
If --outdir is not specified DATADIR is used as output base directory.

To calculate the sensitivities the interpolation corresponding to the first
run combination in RUNSFILE is used.

Except for the `extrandom` plot mode, the sensitivities are calculated along
the axes of a N-dim cross that is centered at PARAMETERPOINT given by
--paramsvector or --paramsfile . Default is to use the center of the
hyper-cube spanned by the anchor points of the run combination. The axes of
the cross are parallel to the parameter axes. For the `extremal` plot mode
only the extremal value along each axis is plotted.

If one wants to study the dependence of the sensitivity on the respective
parameter one of the colormap... plot modes can be used. However, these
modes do not scale well with large numbers of parameters/observables.

A downside of the above "cross-construction" is that only a small portion of
the parameter space is covered and, in principle, correlations between
parameters are disregarded. To include parameter-parameter correlations the
`extrandom` plot mode can be used. Instead of sampling along a N-dim cross,
the points where the sensitivity is evaluated are sampled randomly. By
default the hyper-cube spanned by the anchor points is used,
--parameter-range RANGEFILE can be used to sample from a different parameter
range.

Supported plot modes:
    extremal
        Plot the extremal sensitivities. Produces one plot per observable.

        To get the extremal values the sensitivity is evaluated along a
        N-dim cross that is centered at PARAMETERPOINT

    extrandom
        Plot the extremal sensitivities. Produces one plot per observable.

        To get the extremal values the sensitivity is evaluated at randomly
        located points from the hyper-cube given in RANGEFILE or the
        hyper-cube spanned by the anchor points of the interpolation.

    colormap
        Create colormaps of the sensitivity in the observable-parameter
        plane. Produces one plot for each observable and parameter.

    colormaplim
        Create colormaps of the sensitivity in the observable-parameter
        plane. Produces one plot for each observable and parameter.

        The limits for the colors are the same for all plots, i.e. colors
        can be compared between plots.

    colormapobslim
        Create colormaps of the sensitivity in the observable-parameter
        plane. Produces one plot for each observable and parameter.

        The limits for the colors are the same for all plots about the same
        observable, i.e. colors can be compared between plots.

Implemented sensitivity definitions:

    new (default)
        This definition uses the numerically computed slope and scales the
        result by a typical MC/p ratio::

            S_i = [dMC_i/(eps*MC_width + MC(p_center))] / [dp/(eps*p_width + p_center)]

    slope
        This definition uses the numerically computed slope of the MC
        response::

            S_i = dMC_i/dp

    relslope
        Adefinition that yields a sensitivity that is proportional to
        1/MC(p) if the slope dMC(p)/dp is constant::

            S_i = ( dMC_i/MC )/ dp

    old
        The definition used in the old code, i.e.::

            S_i = ( dMC_i/MC )/( (p_i + dp)/p_i )

        With constant slope dMC(p)/dp this definition yields a sensitivity
        that is proportional to p/MC(p).

    original
        The definition used in the original professor paper
        (DELPHI collab. Z. Phys., C73 (1996) 11-60)::

            S_i = ( dMC_i/MC )/( (dp)/p_i )

where in the above we use::

    dMC_i = MC(p + u_i*dp) - MC(p)

FOR THe numerically computed differential.


TODO
----
    * log-scaled x-axis
"""

import sys, os, itertools
import numpy
from scipy import interpolate
# TODO: is this still necessary?
import matplotlib     # These two lines must be called before import pylab
matplotlib.use('Agg') # to prevent memory leakage
from matplotlib import pyplot
# WARNING: There's a memory leak in matplotlib when saving figures. The
# following seems to circumvent it:
#
# from  matplotlib import pyplot
# fig = pyplot.figure(1)
# sub = fig.add_subplot()
# [do stuff]
# pyplot.savefig(PATH)
# fig.clf()
#
# taken from german forum:
# http://www.python-forum.de/viewtopic.php?p=145135&sid=108b118e841872bd893f0b5a5c171099

from professor import user as prof
from professor.controlplots import StyleGenerator, Sensitivity
from professor.tools.stringtools import safeFilename, latex2mpl
from professor.tools.shell import usePrettyTraceback
usePrettyTraceback()

plotparams = {
    'backend':'pdf',
    'figure.dpi': 300,
    # 'text.fontsize': 16,
    'text.fontsize': 24,
    # 'legend.fontsize': 8,
    'legend.fontsize': 16,
    # 'axes.titlesize': 16,
    'axes.titlesize': 24,
    # 'axes.labelsize': 24,
    'axes.labelsize': 24,
    # 'xtick.labelsize': 16,
    'xtick.labelsize': 20,
    # 'ytick.labelsize': 16,
    'ytick.labelsize': 20,
    # 'lines.markersize': 7,
    'lines.markersize': 12,
    # 'figure.subplot.left' : 0.14,
    'figure.subplot.left' : 0.16,
    'figure.subplot.right' : 0.96,
    # 'figure.subplot.bottom' : 0.10,
    'figure.subplot.bottom' : 0.14,
    'figure.subplot.top' : 0.98,
    'figure.subplot.wspace' : 0.4,
    # Don't use TeX because it failes with some parameter names.
    'text.usetex': False
    # 'text.latex.preamble' :  ['\usepackage{amsmath}', '\usepackage{mathpazo}', '\usepackage{hepunits}']
    }
matplotlib.rcParams.update(plotparams)

import optparse
parser = optparse.OptionParser(usage=__doc__, version=prof.version)
prof.addRunCombsCLOptions(parser)
parser.add_option("--penalty",
                  type = "float",
                  dest = "PENALTY",
                  default = 0.5,
                  help = "Max. allowed relative data uncertainty for bins. Set to 0.0"
                  " to disable this feature. [default: %default]")
parser.add_option("--plotmode",
                  dest = "PLOTMODE",
                  type = "choice",
                  choices = ["extremal", "extrandom", "median", "colormap", "colormaplim", "colormapobslim"],
                  default= "extremal",
                  help = "The type of plots. [default: %default]")
parser.add_option("--splines",
                  dest = "SPLINES",
                  action = "store_true",
                  default = True,
                  help = "Connect points by splines in extremal plot mode [default].")
parser.add_option("--no-splines",
                  dest = "SPLINES",
                  action = "store_false",
                  default = True,
                  help = "Don't connect points by splines in extremal plot mode.")
parser.add_option("--format",
                  dest = "FORMAT",
                  type = "choice",
                  choices = ["makeplots", "matplotlib"],
                  default = "matplotlib",
                  help = "Choose format of output files [default: %default]")
parser.add_option("--legend",
                  dest = "LEGEND",
                  action = "store_true",
                  default = True,
                  help = "Put a legend on each plot. [default]")
parser.add_option("--no-legend",
                  dest = "LEGEND",
                  action = "store_false",
                  default = True,
                  help = "Do not show a legend.")
parser.add_option("--table",
                  dest = "TABLEFILE",
                  default = None,
                  help = "Store a tex-fragment table of sensitivity values for"
                  " one-bin observables in TABLEFILE. [default is off].")
parser.add_option("--logy",
                  dest = "LOGY",
                  action = "store_true",
                  default = False,
                  help = "Log y-axis scaling for extremal plot mode.")
parser.add_option("--definition",
                  dest = "DEFINITION",
                  type = "choice",
                  choices = ("new", "old", "original", "relslope", "slope"),
                  default = "new",
                  help = "Sensitivity definition [default: %default]")
parser.add_option("--pf", "--paramsfile",
                  metavar = "PARAMETERPOINT",
                  dest = "PARAMSFILE",
                  help = "File with the parameter values.")
parser.add_option("--pv", "--paramsvector",
                  metavar = "PARAMETERPOINT",
                  dest = "PARAMSVECTOR",
                  help = "Parameter values as comma-separated list. E.g."
                  " PAR1=42.0,PAR2=0.23 .")
parser.add_option("--pr", "--paramsrange",
                  metavar = "RANGEFILE",
                  dest = "PARAMSRANGE",
                  help = "File with parameter sampling range for 'extrandom' plot"
                  " method.")
prof.addIpolCLOptions(parser)
prof.addDataCLOptions(parser, ref=True, mc=True, ipol=True, scan=False)
prof.addOutputCLOptions(parser)
prof.addLoggingCLOptions(parser)
opts, args = parser.parse_args()
prof.log.setPriority(opts)
prof.writeGuideLine()

## Build DataProxy
try:
    dataproxy = prof.DataProxy.mkFromCLOptions(opts)
except prof.DataProxyError, err:
    prof.log.info("No reference data available.")
    prof.log.debug("Error: " + str(err))
    delattr(opts.REFDIR)
    dataproxy = prof.DataProxy.mkFromCLOptions(opts)


## Check output directory.
outdir = prof.getOutputDirectory(opts,  "sensitivities")
prof.io.makeDir(outdir)
prof.log.info("Output directory for sensitivity plots: %s" % outdir)


## Check the central parameter file/vector and the parameter range.
params = None
ranges = None
if opts.PLOTMODE != "extrandom":
    if opts.PARAMSRANGE is not None:
        # prof.log.error(parser.format_help())
        prof.log.error("Option --paramsrange conflicts with --plotmode extrandom!")
        sys.exit(1)

    if opts.PARAMSVECTOR is not None and opts.PARAMSFILE is not None:
        prof.log.error("Both options --paramsfile and --paramsvector given:"
                       " Cannot decide, please give only one option!")
        sys.exit(1)

    elif opts.PARAMSVECTOR is not None:
        params = prof.ParameterPoint.mkFromString(opts.PARAMSVECTOR)

    elif opts.PARAMSFILE is not None:
        params = prof.ParameterPoint.mkFromFile(opts.PARAMSFILE)

    else:
        prof.log.info("Using center of hyper-cube spanned by anchor points as"
                      " center of cross.")
else:
    if opts.PARAMSVECTOR is not None and opts.PARAMSFILE is not None:
        prof.log.error("Options --paramsfile and --paramsvector conflict"
                       " with --plotmode extrandom!")
        sys.exit(1)

    if opts.PARAMSRANGE is not None:
        ranges = prof.ParameterRange.mkFromFile(opts.PARAMSRANGE)



## Get the configured interpolation class.
try:
    # Note: the (no)weave switch is not important, we only need the 'method'
    # attribute to construct the file name of the interpolation set.
    IpolCls = prof.InterpolationClass
    IpolCls.method = opts.IPOLMETHOD
    prof.log.info("Using %s for parameterisation." % IpolCls.__name__)
except Exception, e:
    prof.log.error("Problem getting parameterisation method: %s" % e)
    prof.log.error("Exiting!")
    sys.exit(1)


## Select the observables for the sensitivity calculations.
obsnames = []
if opts.OBSERVABLEFILE:
    try:
        prof.io.testReadFile(opts.OBSERVABLEFILE)
        wm = prof.WeightManager.mkFromFile(opts.OBSERVABLEFILE)
        obsnames = wm.observables
    except Exception, e:
        prof.log.error("Problem when reading observable file: %s" % e)
        prof.log.error("Exiting!")
        sys.exit(1)
    prof.log.debug("Loaded observable file from %s" % opts.OBSERVABLEFILE)
else:
    prof.log.debug("No observable file given! Using all available observables.")
    wm = prof.WeightManager()
    for obsname in dataproxy.getMCData().getAvailableObservables():
        wm.addBinRangeWeight(obsname)
    obsnames = wm.observables
prof.log.debug("Loaded observables: %s" % obsnames)


## Load AIDA-path => title dictionary.
pp = prof.PlotParser()
histoinfos = {}
for obs in obsnames:
    try:
        histoinfos[obs] = pp.getHeaders(obs)
        for k,v in (("Title", obs), ("XLabel", "x label"), ("YLabel", "y label")):
            if not histoinfos[obs].has_key(k):
                prof.log.warn("Plotting info for observable %s is missing entry %s" % (obs, k))
                prof.log.warn("You might want to check that the *.plot files can be found via $RIVET_INFO_PATH")
                prof.log.warn("Using dummy value for plotting.")
                histoinfos[k] = v
    except:
        pass
prof.log.debug("Histogram informations: %s" % histoinfos)


## Load run combinations
runs = []
if opts.RUNSFILE:
    prof.log.debug("Using %s as runs file" % opts.RUNSFILE)
    try:
        rcm = prof.RunCombManager.mkFromFile(opts.RUNSFILE)
        runs = rcm.firstruncomb
    except Exception, e:
        prof.log.error("Error while opening run combination file %s: %s" % (opts.RUNSFILE, e))
        sys.exit(1)
else:
    prof.log.debug("No run combination file given! Using all available runs.")
    runs = dataproxy.getMCData().availableruns


## Load interpolation set in a SensCalculator.
ipolset = dataproxy.getInterpolationSet(IpolCls, runs)
try:
    tundat = dataproxy.getTuneData(withref=True, useipol=IpolCls,
                                   useobs=obsnames, useruns=runs)
    senscal = Sensitivity(ipolset, 0.01, tundat, opts.PENALTY)
except (prof.DataProxyError, TypeError):
    # This case has already been logged.
    senscal = Sensitivity(ipolset, 0.01)


## Set central parameter point for N-dim cross and parameter ranges for
## random extremal sensitivities.
if params is not None:
    senscal.parampoint = params
    prof.log.debug("Setting central parameter point:\n%s" % params)
if ranges is not None:
    from professor.tools.pointsampling import RandomPointGenerator
    senscal.rndpointgen = RandomPointGenerator(ranges)
    prof.log.debug("Setting sampling ranges:\n%s" % ranges)

## Check that we have all needed interpolations.
ipolhistonames = senscal.getHistogramNames()
for obs in obsnames:
    if obs not in ipolhistonames:
        msg = "Could not find interpolation for histogram %s" % obs
        msg += " in file %s !" % (dataproxy.getIpolFilePath(IpolCls, runs))
        prof.log.error(msg)
        prof.log.error("Please call prof-interpolate with the correct arguments first!")
        prof.log.error("Exiting!")
        sys.exit(1)

prof.log.info("Setting sensitivity definition: %s." % opts.DEFINITION)
senscal.setSensitivityDefinition(opts.DEFINITION)


## Finished parsing options.
## Define some plotting helper functions and then do the plotting.

def getExtremalData(observable):
    """Prepare extremal sensitivitiy data for plotting.

    Depending on `opts.PLOTMODE` either
    :meth:`Sensitivity.extremalSensitivity` or
    :meth:`Sensitivity.extremalSensitivityRandom` is used.

    Returns
    -------
    obsbinxranges : numpy.array
        2D array with the observable bin edges for each bin with
        shape (numbins, 2).
    obsbincenters : numpy.array
        1D array with the observable bin centers.
    sensdata : dict
        Dict mapping parameter name to a numpy.array with sensitivity data
        for this parameter.
    """
    global senscal
    # get the observable bin centers
    obsbincenters = []
    obsbinxranges = []
    for binid in senscal.getObservableBins(observable).sortedBinIDs():
        obsbincenters.append(senscal[binid].getBinCenter())
        obsbinxranges.append(senscal[binid].binrange)
    obsbincenters = numpy.array(obsbincenters)
    obsbinxranges = numpy.array(obsbinxranges)

    sensdata = {}

    if opts.PLOTMODE == "extremal":
        for param in senscal.getParameterNames():
            parbincenters = senscal.getParameterBins(observable, param)[0]
            sensdata[param] = senscal.extremalSensitivity(observable,
                                param, parbincenters)
    elif opts.PLOTMODE == "median":
        for param in senscal.getParameterNames():
            parbincenters = senscal.getParameterBins(observable, param)[0]
            sensdata[param] = senscal.medianSensitivity(observable,
                                param, parbincenters)
    elif opts.PLOTMODE == "extrandom":
        for param in senscal.getParameterNames():
            sensdata[param] = senscal.extremalSensitivityRandom(observable,
                                param)
    else:
        raise ValueError("opts.PLOTMODE contains unexpected value: %s" % opts.PLOTMODE)

    return obsbincenters, obsbinxranges, sensdata

def plotExtremalMPL(subplot, xcenters, sensdata, stylegen, splines):
    """Plot the extremal sensitivities of observable for all parameters.

    Global variables used:

        `senscal`
            A Sensitivity instance.
        `histoinfos`
            A dictionary with histogram meta data from PlotParser

    Parameters
    ----------
    subplot : matplotlib.Axes
    observable : str
        The observable identifier (i.e. AIDA path).
    stylegen : StyleGenerator
        Generate for plot styles.
    splines : bool
        Connect the sensitivity data point with splines.
    """
    for param in sorted(sensdata.keys()):
        # set up plot styles (splinestyle only used if argument `splines` is
        # True)
        dotstyle = stylegen.next()
        splinestyle = dotstyle.copy()

        dotstyle["label"] = param
        dotstyle["linestyle"] = ""

        splinestyle["marker"] = ""
        splinestyle["label"] = "_nolabel_"

        sens = sensdata[param]
        subplot.plot(xcenters, sens, **dotstyle)

        if splines and len(xcenters) > 3:
            # calculate splines (k=order, s=smoothness)
            spline_coeffs = interpolate.splrep(xcenters, sens, k=3, s=0)
            xfine = numpy.linspace(xcenters[0], xcenters[-1], 100,
                                   endpoint=True)
            yfine = interpolate.splev(xfine, spline_coeffs)
            subplot.plot(xfine, yfine, **splinestyle)

def getExtrMakePlotsData(obs, xranges, sensdata, stylegen):
    prof.log.debug("I got %i observable bins" % (len(xranges)))

    ret = ""
    for param in sorted(sensdata.keys()):
        style = stylegen.next()

        numsenspts = len(sensdata[param])

        # axis 0 -> bin index
        # axis 1 -> data values (xlow xhigh yval yerr)
        data = numpy.zeros((numsenspts, 4))
        data[:,0:2] = numpy.array(xranges)
        data[:,2] = sensdata[param]

        ret += "# BEGIN HISTOGRAM sens-%s-%s\n" % (param, obs)
        ret += "Title=%s\n" % (param)
        ret += "LineStyle=none\n"
        ret += "LineColor=%s\n" % (style['color'])
        ret += "PolyMarker=%s\n" % (style['marker'])
        # ret += "PolyMarkerColor=%s\n" % (style['color'])

        for i in xrange(numsenspts):
            ret += "\t".join(["%e" % (v) for v in data[i]]) + "\n"
        ret += "# END HISTOGRAM\n\n"

    return ret

def makeExtremalPlots():
    global obsnames
    global histoinfos
    if opts.TABLEFILE is None:
        tablefile = None
    else:
        tablefile = open(opts.TABLEFILE, "w")
        tablefile.write(opts.DEFINITION + " & "
                        + " & ".join(senscal.getParameterNames())
                        + r"\\" + "\n")

    for i, obs in enumerate(sorted(obsnames)):
        prof.log.info("Starting observable %i/%i: %s." % (i+1,
                      len(obsnames), obs))
        obsbincenters, obsbinxranges, sensdata = getExtremalData(obs)

        # short-cut
        paramnames = sorted(senscal.getParameterNames())

        if tablefile is not None and len(obsbinxranges) == 1:
            # make the obs name column 70 characters wide (70 is an educated
            # guess ;) for easier post-production
            tablefile.write(histoinfos[obs]["Title"].ljust(70))
            for p in paramnames:
                tablefile.write(" & %.3g" % (sensdata[p][0]))
            # add EOL for the LaTeX table and the TeX source code
            tablefile.write(r" \\" + "\n")


        if opts.FORMAT == "makeplots":
            fpath = os.path.join(outdir, safeFilename(obs) + "_%s_"%opts.PLOTMODE + opts.DEFINITION + ".dat")
            stylegen = StyleGenerator(
                    marker = itertools.cycle(['*', 'SolidTriangle',
                                    'SolidPentagon', 'SolidSquare']),
                    color = itertools.cycle(['red!80!green', 'red',
                                    'blue!30!green', 'black', 'blue',
                                    'green!70!black', 'magenta', 'cyan']))
            if opts.PLOTMODE == "median":
                ylabel = "Median sensitivity"
            else:
                ylabel = r"$\mathrm{extr}\," + senscal.getSensFormulaSign() + "$"
            f = open(fpath, "w")
            f.write(getMakePlotsHeader(obs, paramnames, ylabel))
            f.write(getExtrMakePlotsData(obs, obsbinxranges, sensdata,
                                         stylegen))
            f.close()

        elif opts.FORMAT == "matplotlib":
            fpath = os.path.join(outdir, safeFilename(obs) + "_ext_" + opts.DEFINITION + ".pdf")
            stylegen = StyleGenerator(
                    marker = itertools.cycle(['o', 'd', 'v', '*', '^', 'p',
                                              '<']),
                    color = itertools.cycle(['r', 'g', 'b', 'k']),
                    linestyle = itertools.cycle([":", "--", "-."]))
            fig = pyplot.figure(1, facecolor="w")
            sub = fig.add_subplot(1,1,1)

            plotExtremalMPL(sub, obsbincenters, sensdata, stylegen,
                            opts.SPLINES)

            if opts.LEGEND:
                legendbox = sub.legend(loc="best", numpoints=1
                                      # , borderpad=0.0
                                      , borderaxespad=0.0
                                      )
                legendbox.draw_frame(False)

                # labels = [line.get_label() for line in sub.lines]
                # legendbox = fig.legend(sub.lines, labels, "upper right", numpoints=1)
                # alpha does not work :(
                legendbox.set_alpha(0.0)
                # legendbox.set_zorder(-1)

            # draw line at sens=0.0 after the legend is drawn
            sub.axhline(y=0.0, linestyle=":", color="k", label="_nolabel_")

            sub.set_xlabel(latex2mpl(histoinfos[obs]["Title"]))
            sub.set_ylabel(r"$\mathrm{extr}\,"
                           + senscal.getSensFormulaSign() + "$")
            if opts.LOGY:
                sub.set_yscale("log")

            prof.log.info("Saving file %s" % (fpath))
            pyplot.savefig(fpath)
            fig.clf()



def getColormapData(observable, param):
    # get the observable range
    # TODO: this does not account for gaps in the binning!
    obsedges = []
    for binid in senscal.getObservableBins(observable).sortedBinIDs():
        obsedges.append(senscal[binid].binrange[0])
    obsedges.append(senscal[binid].binrange[1])
    prof.log.debug("I got %i observable bin edges: %s" % (len(obsedges), obsedges))
    # calculate parameter bin edges and centers
    parbincenters, parbinedges = senscal.getParameterBins(observable, param)
    sensitivities = senscal.arraySensitivity(observable, param, parbincenters)
    prof.log.debug("parbinedges: %s" % (parbinedges))

    # X, Y = numpy.meshgrid(numpy.array(obsedges), parbinedges)
    return obsedges, parbinedges, sensitivities

def makeColormapPlots(observable, obslim=False):
    global senscal
    paramnames = senscal.getParameterNames()

    # param => (obsedges, parbinedges, sensitivities), (vmin, vmax))
    plotdata = {}
    # param => (vmin, vmax)
    minmax = {}

    for param in paramnames:
        obsedges, parbinedges, sensitivities = getColormapData(obs, param)
        plotdata[param] = (obsedges, parbinedges, sensitivities)
        minmax[param] =  getExtremalLimits(sensitivities.min(),
                                           sensitivities.max())
    # Use the same colormap scale for all plots of this observable
    if obslim:
        min_, max_ = minmax.values()[0]
        for imin, imax in minmax.values():
            min_ = min(min_, imin)
            max_ = max(max_, imax)
        for param in minmax.keys():
            minmax[param] = (min_, max_)

    for param in paramnames:
        obsedges, parbinedges, sensitivities = plotdata[param]
        fpath = os.path.join(outdir,
                             safeFilename(obs)
                             + "_" + safeFilename(param)
                             + "_cm_" + opts.DEFINITION)

        if opts.FORMAT == "makeplots":
            if obslim:
                prof.log.warning("make-plots output does not support a common color scale!")
            data = []
            for i in xrange(len(obsedges)-1):
                for j in xrange(len(parbinedges)-1):
                    z = sensitivities[i][j]
                    data.append((obsedges[i], obsedges[i+1],
                                parbinedges[j], parbinedges[j+1], z))
            fpath += ".dat"
            f = open(fpath, "w")
            f.write(getMakePlotsHeader(obs, [param], ylabel=param,
                                         ymin=min(parbinedges),
                                         ymax=max(parbinedges)))
            f.write("# BEGIN HISTOGRAM sens-%s-%s\n" % (param, observable))
            for row in data:
                f.write("\t".join(["%e" % (v) for v in row]))
                f.write("\t%e\n" % (0.0))
            f.write("# END HISTOGRAM\n\n")
            f.close()

        else:
            fpath += ".pdf"
            fig = pyplot.figure(1, facecolor="w")
            sub = fig.add_subplot(1,1,1)
            X, Y = numpy.meshgrid(numpy.array(obsedges), parbinedges)
            vmin, vmax = minmax[param]
            colplot = sub.pcolor(X, Y, sensitivities.transpose(),
                    cmap = pyplot.cm.RdBu, vmin=vmin, vmax=vmax)
            colbar = fig.colorbar(colplot)

            sub.set_xlabel(latex2mpl(histoinfos[obs]["Title"]))
            sub.set_ylabel(param)
            colbar.set_label("Sensitivity")

            pyplot.savefig(fpath)
            fig.clf()

        prof.log.info("Saving file %s" % (fpath))


def makeColormapPlotsGlobalScale():
    paramnames = senscal.getParameterNames()
    # Note: Caching the plot data for all color map plots is too memory
    # intensive. Therefore we have to loop through all plots 2 times:
    # 1. to get the min/max sensitivities for all plots to get a common
    # sensitivitiy scale,
    # 2. to do the plotting
    # Thus, we don't need a plotdata = {} statement as above.
    maxsens = -1.0e18
    minsens = 1.0e18
    prof.log.info("Calculating extremal sensitivities...")
    for i, obs in enumerate(sorted(obsnames)):
        prof.log.info("  for observable %i/%i: %s" % (i+1, len(obsnames), obs))
        for param in paramnames:
            sensitivities = getColormapData(obs, param)[2]
            # update max-/minsens
            maxsens = max(maxsens, sensitivities.max())
            minsens = min(minsens, sensitivities.min())

    prof.log.debug("Extremal overall sensitivities: max=%f min=%f" % (
                        maxsens, minsens))
    if numpy.sign(maxsens) == numpy.sign(minsens):
        prof.log.warn("Unexpected: all sensitivities have the same sign,"
                " colors may be odd!")
    else:
        minsens, maxsens = getExtremalLimits(minsens, maxsens)

    prof.log.debug("Extremal plotting sensitivities: max=%f min=%f" % (
                        maxsens, minsens))

    prof.log.info("Plotting sensitivities...")
    for i, obs in enumerate(sorted(obsnames)):
        prof.log.info("Starting observable %i/%i: %s." % (i+1,
                    len(obsnames), obs))
        for param in paramnames:
            fpath = os.path.join(outdir,
                                 safeFilename(obs) + "_" + safeFilename(param) +
                                 "_cmlim_" + opts.DEFINITION + ".pdf")
            fig = pyplot.figure(1, facecolor="w")
            sub = fig.add_subplot(1,1,1)

            obsedges, parbinedges, sensitivities = getColormapData(obs, param)
            X, Y = numpy.meshgrid(numpy.array(obsedges), parbinedges)
            colplot = sub.pcolor(X, Y, sensitivities.transpose(),
                    vmin=minsens, vmax=maxsens)
            colbar = fig.colorbar(colplot)

            sub.set_xlabel(latex2mpl(histoinfos[obs]["Title"]))
            sub.set_ylabel(param)
            colbar.set_label("Sensitivity")
            prof.log.info("Saving file %s" % (fpath))
            pyplot.savefig(fpath)
            fig.clf()


def getExtremalLimits(vmin, vmax):
    """Return the interval limits with the greater extremum as tuple."""
    if abs(vmin) > abs(vmax):
        return -abs(vmin), abs(vmin)
    else:
        return -abs(vmax), abs(vmax)

# TODO: put this somewhere either in tools/... or controlplots/...
def getMakePlotsHeader(observable, paramnames, ylabel=None, ymin=False,
                         ymax=False, legend=True):
    header = "# BEGIN PLOT\n"
    header +="DrawOnly="
    for param in paramnames:
        header+="sens-%s-%s "%(param, observable)
    header+="\n"
    header += "RatioPlot=0\n"
    title  = histoinfos[observable]['Title']
    xlabel = histoinfos[observable]['XLabel']
    header += "XLabel=%s\n"%xlabel
    if ylabel is not None:
        header += "YLabel=%s\n"%ylabel
    if ymin:
        header+="YMin=%s\n"%ymin
    if ymax:
        header+="YMax=%s\n"%ymax
    header += "Title=Sensitivities for %s\n"%title
    if legend:
        header += "Legend=1\n"
    header += "# END PLOT\n\n"
    return header


if opts.PLOTMODE == "extremal" or opts.PLOTMODE == "extrandom" or opts.PLOTMODE == "median":
    makeExtremalPlots()

elif opts.PLOTMODE == "colormap":
    prof.log.info("Calculating and plotting sensitivities...")
    for i, obs in enumerate(sorted(obsnames)):
        prof.log.info("Starting observable %i/%i: %s." % (i+1,
                    len(obsnames), obs))
        makeColormapPlots(obs, False)

elif opts.PLOTMODE == "colormapobslim":
    prof.log.info("Calculating and plotting sensitivities...")
    for i, obs in enumerate(sorted(obsnames)):
        prof.log.info("Starting observable %i/%i: %s." % (i+1,
                    len(obsnames), obs))
        makeColormapPlots(obs, True)

elif opts.PLOTMODE == "colormaplim":
    makeColormapPlotsGlobalScale()
