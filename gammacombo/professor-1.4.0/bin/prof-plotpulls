#! /usr/bin/env python

"""
%prog [--obsfile weights.dat] mc.aida

Create PDF pull plots for a MC data file relative to a reference data file. The
--weights/--obsfile argument may be used to restrict the calculated pulls to a
subset of the observables in the MC data file.
"""

import os
import numpy

import professor.user as prof

import optparse
parser = optparse.OptionParser(usage = __doc__, version=prof.version)
prof.addLoggingCLOptions(parser)
prof.addOutputCLOptions(parser)
prof.addDataCLOptions(parser, ref=True, mc=False)
opts, args = parser.parse_args()

## Write usage messages
prof.log.setPriority(opts)
prof.writeGuideLine()

## Check if specified outdir exists and create it otherwise.
outdir = prof.getOutputDirectory(opts, "pulls")
prof.log.debug("Using %s for pulls output." % outdir)
prof.io.makeDir(outdir)


def pull(refbin, mcbin):
    """ The definition for the pull calculation based on a reference
        bin and an MC bin.
    """
    return (refbin.getVal() - mcbin.getVal())/refbin.getErr()


from matplotlib import pyplot
params = {
        'backend':'pdf',
        'axes.labelsize': 14,
        'text.fontsize': 14,
        'legend.fontsize': 14,
        'axes.titlesize': 14,
        'xtick.labelsize': 14,
        'ytick.labelsize': 14,
        'text.usetex': True,
        'figure.dpi': 300,
        'figure.subplot.left' : 0.12,
        'figure.subplot.right' : 0.9,
        'figure.subplot.bottom' : 0.1,
        'figure.subplot.top' : 0.9,
        'lines.markersize':7,
        'text.latex.preamble' :  ['\usepackage{amsmath}', '\usepackage{mathpazo}', '\usepackage{hepunits}']
        }
pyplot.rcParams.update(params)


def plotPulls(observable):
    """
    The plotting routine. This plots the pulls for bins of a certain observable.
    """
    global mchistos
    global dp
    global mctitles


    ## Processing histos to have an easy to handle numpy.array as input only.
    mc_h = mchistos[observable]
    ref_h= dp.getRefHisto(observable)
    data = [(mcbin.getBinCenter(), pull(ref_h.getBins()[n] ,mcbin))
            for n, mcbin in enumerate(mc_h.getBins())
            if not ref_h.getBins()[n].getVal() == 0]
    D = numpy.array(data)

    ## Define a figure and subplot.
    fig = pyplot.figure(facecolor='w')
    sub = fig.add_subplot(1,1,1)

    ## The actual data-plotting command.
    sub.plot(D[:,0], D[:,1], ls=" ", marker="o", color="k")

    ## Three horizontal lines to guide the eye at y = -1, 0 and +1
    sub.axhline(0)
    sub.axhline(1., ls="--")
    sub.axhline(-1., ls="--")

    ## Make the y - plot range symmetric around 0
    ymax = numpy.ceil(max(abs(D[:,1])))
    sub.set_ylim(-1.*ymax, ymax)

    ## Plot and axis titles
    sub.set_title("Pulls for observable %s" % mctitles[observable])
    sub.set_xlabel(mctitles[observable])
    sub.set_ylabel("pull")

    ## Have tickmarks only at the bottom and to the left
    sub.get_xaxis().tick_bottom()
    sub.get_yaxis().tick_left()

    return fig


## Read in ref and MC histos
dp = prof.DataProxy.mkFromCLOptions(opts)
mchistos = prof.Histo.fromAIDA(args[0])

## Read in histo titles
plotparser = prof.PlotParser()
mctitles = dict()
for hp in mchistos.keys():
    headers = plotparser.getHeaders(hp)
    mctitles[hp] = headers.get("Title", hp)


## Read in observable file
if opts.OBSERVABLEFILE is None:
    plot_obs = mchistos.keys()
else:
    wm = prof.WeightManager.mkFromFile(opts.OBSERVABLEFILE)
    prof.log.debug("Loaded observable file from %s: %s" % (opts.OBSERVABLEFILE, wm))
    plot_obs = wm.posWeightObservables


## Process the observables
from professor.tools.progressbar import ForLoopProgressBar as FLPB
bar = FLPB(0, len(plot_obs), 30, 'Plotting... ')
for n, obs in enumerate(plot_obs):
    bar.update(n)
    try:
        ## Make figure and save the plot to a PDF file
        fig = plotPulls(obs)
        outname = obs.strip("/").replace("/", "_") + ".pdf"
        fig.savefig(os.path.join(outdir, outname))
    except:
        prof.log.error("Could not process %s - data file missing?" % mctitles[obs])
