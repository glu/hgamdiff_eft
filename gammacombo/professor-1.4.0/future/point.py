import professor.tools.log as logging

class Point(object):
    """ Most basic class for points. """
    def __init__(self):
        self.X = []        # All kind of fixed coords, i.e. x in case of Bins
        self.Y = None      # The thing that changes with different param values
        self.params = []   # The e.g. MC parameter settings leading to that points value
        self.globID = None # Global unique identifier
        self.ipol = None   # The parametrisation

    def getValue(params=None): # Return value, either from parametrisation or from param point it
                   # belongs to or data?
        if params is None:
            return self.Y
        else:
            if self.ipol is not None:
                return self.ipol.GetValue(params)
            #else:
                #raise shit

    def parametrise():
        # if do magic works:
        #    self.ipol = ipol
        print "void"
