#!/usr/bin/env python

import generator

g=generator.PointGeneratorBase()
g=generator.PointGeneratorBase("sadfsdf")
g=generator.PointGeneratorBase(2023)
g=generator.PointGeneratorBase(-1)
gr = generator.SimpleRandomGenerator()
gr.addRange("PAR1", 0, 1)
gr.addRange("PAR2", 1, 100)
gr.addFunction("PAR1", "10*numpy.random.exponential()")
gr.addFunction("PAR2", "numpy.random.randint(0,300)")
p=[i for i in gr.generate(10)]

gc = generator.ConditionalRandomGenerator()
gc.addRange("PAR1", 0, 1)
gc.addRange("PAR2", 0, 1)
gc.addRange("PAR3", 0, 1)
gc.addCondition("PAR1:<:PAR2")
gc.addCondition("PAR1:>:PAR3")
p=[i for i in gc.generate(10)]

print p
from IPython import embed
embed()
