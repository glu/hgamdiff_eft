#!/usr/bin/env python
"""
Test script of 2D parametrisation.
Generates points, draws them, calculates
parametrisations, draws them.
"""



def function(params):
    import random
    return (1+random.gauss(0,0.05))*params[0]**2 + (1+random.gauss(0,0.05))*params[1]**2

def calcAndDraw(ITP, order, axis):
    # Parametrisation as wireframe
    ITP.polynomial(order)
    from numpy import linspace, asarray, meshgrid, zeros
    X_P=linspace(0,1,100)
    M_X, M_Y = meshgrid(X_P, X_P)
    M_Z = Z=zeros(M_X.shape)
    for i in xrange(M_X.shape[0]):
        for j in xrange(M_X.shape[1]):
            M_Z[i][j] = IP.getValue(asarray([M_X[i][j], M_Y[i][j]]), order)
    axis.plot_wireframe(M_X, M_Y, M_Z,rstride=10, cstride=10)

import ipol, generator, anchor

# Empty ipol thingie
IP = ipol.BaseParameterisation()

gc = generator.ConditionalRandomGenerator()
gc.addRange("PAR1", 0, 1)
gc.addRange("PAR2", 0, 1)
gc.addCondition("PAR1:<:PAR2")
P=[i for i in gc.generate(1000)]
print "%i points generated"%len(P)

# Lets generate some 2D anchors
for p in P:
    a = anchor.Anchor()
    a.params = p
    a.Y = function(p)
    IP.anchors.append(a)

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
# Draw anchors as scatter graph first
X=map(lambda x:x[0], P)
Y=map(lambda x:x[1], P)
Z=map(lambda x: x.Y, IP.anchors)
ax.scatter(X,Y,Z, c="r")

import sys
calcAndDraw(IP, int(sys.argv[1]), ax)

fig.show()
from IPython import embed
embed()
