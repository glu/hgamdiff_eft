import professor.tools.log as logging

class Anchor(object):
    """ A class for parametrisation anchors. """
    def __init__(self):
        self.Y = None        # The thing that changes with different param values
        self.params = None   # The e.g. MC parameter settings leading to that points value

    def Y(self):
        return self.Y

    def params(self):
        import numpy
        return numpy.asarray(self.params)
