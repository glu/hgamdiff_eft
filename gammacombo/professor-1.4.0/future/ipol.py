import professor.tools.log as logging

class BaseParameterisation(object):
    """ Base parameterisation class.
        This thing does not need to know about x values
        and so forth.
    """

    def __init__(self):
        self.anchors = []   # A collection of anchor points
        self.center = None
        self.coeffs = None  # Extend this so that multiple different types of 
                            # parametrisations can be stored/accessed

    def dim(self):
        """
        Short hand to get the dimensionality of the parameter space.
        """
        return len(self.anchors[0].params)


    def getValue(self, P, order):
        """
        Return the value of the parametrisation at coordinates P

        Parameters
        ----------
        p : array_like

        Returns
        -------
        val : float
        """
        DP = self.getLongVector(P - self.center, order)
        import numpy
        val = numpy.dot(DP, self.coeffs)
        return val


    def numOfCoefficients(self, dim, order):
        """The number of coefficients for a `dim` dimensional space.

        Parameters
        ----------
        dim   : int ... dimensionality of parameter space
        order : int ... order of the to be calculated polynomial
        """
        def binomial(n,k):
            # taken from http://www.velocityreviews.com/forums/t502438-combination-function-in-python.html
            ntok = 1
            for t in xrange(min(k,n-k)):
                ntok = ntok*(n-t)//(t+1)
            return ntok
        return binomial(dim+order, order)

    def getFiducialVolume(self):
        """ Return coordinates of hyper cube (?) of anchor points
        """
        minvalues = []
        maxvalues = []
        for i in xrange(self.dim()):
            vl = map(lambda x: x.params[i], self.anchors)
            mn = min(vl)
            mx = max(vl)
            minvalues.append(mn)
            maxvalues.append(mx)
        import numpy
        return numpy.asarray(zip(minvalues, maxvalues))

    def getCenter(self, calc=False):
        if calc:
            FDV = self.getFiducialVolume()
            C = []
            for i in xrange(self.dim()):
                C.append( FDV[i][0] + (FDV[i][1] - FDV[i][0])*0.5 )
            import numpy
            self.center = numpy.asarray(C)
        else:
            return self.center

    def polynomial(self, order):
        """Fill an empty interpolation from a bin distribution.
        Will overwrite previous bin data!

        This calculates the interpolation coefficients.

        Parameters
        ----------
        bd : BinDistribution
            The distribution of the MC response function.
        center : ParameterPoint, optional
            The center of the interpolation. If `None` the center of the
            hyper cube spanned by the anchor points is used.
        """
        if self.center is None:
            self.getCenter(calc=True)
        ### TODO: Maybe we want some safeguards in the population of anchors

        ### Prepare class
        numOfCoeffs = self.numOfCoefficients(self.dim(), order) # Keep this floating

        ### Check the arguments
        #if bd.paramnames != center.names:
            #raise ParameterError("Parameter names of bin distribution and"
                                 #" interpolation center mismatch!")
        #if bd.numberOfRuns() < self.minNumOfRuns(dim):
            #raise ValueError("Not enough runs for this interpolation!")



        import numpy
        DP = numpy.ones(( len(self.anchors),
                          numOfCoeffs ))
        MC = numpy.zeros( len(self.anchors) )

        for num, a in enumerate(self.anchors):
            MC[num] = a.Y
            try:
                DP[num] = self.getLongVectorWeave(a.params - self.center, order)
            except:
                DP[num] = self.getLongVector(a.params - self.center, order)

        # SVD
        import numpy.dual
        DP_inv = numpy.dual.pinv(DP)
        self.coeffs = numpy.dot(DP_inv, MC)

        #self.binid = bd.binid
        #self.binrange = bd.binrange
        #if bd.valrange is not None:
            #self.valrange = bd.valrange

        #if not numpy.isfinite(coeffs).any():
            #raise InterpolationFailedError("Some coefficients are not finite for bin %s!" % self.binid, self.binid)
        #self.coeffs = coeffs

        #if not numpy.isfinite(center).any():
            #raise InterpolationError("Center contains non finite elements for bin %s!" % self.binid, self.binid)
        #self.center = center
        #self.medianmcerr = bd.getMedianMCError()
        #return self


    def getLongVector(self, p, order):
        """Make an extended parameter vector.

        Polynomials are split into parts of same order.
        Generating functions (lambda) for these parts are created once per order
        and stored in a dict.
        In case the same class/object is used multiple times the stored functions
        will be used.

        TODO: Maybe a numpy-equivalent can be found for the by-order-creation task
        which would speed-up the pure-python implementation.
        (Anyway this might not be used because of the C-implementation).

        Parameters
        ----------
        p : numpy.ndarray
        """

        if order < 0:
            return
        npar = len(p)

        def getOrder(p,n):
            if n == 0:
                return (1,)
            return eval("lambda x:(%s for x0 in xrange(%d) %s)" % (
                "*".join("x[x%d]"%j for j in xrange(n)),
                npar,
                " ".join("for x%d in xrange(x%d,%d)" % (k,l,npar) for (l,k) in zip(xrange(n-1),xrange(1,n)))
                ))(p)

        from itertools import chain
        import numpy
        return numpy.fromiter(chain(*(getOrder(p, n) for n in xrange(order+1))),float,count=self.numOfCoefficients(npar, order))

    def getLongVectorWeave(self, p, order):
        """
        Creates C source code from class configuration
        (number of params, order of polynomial) and stores this.

        If cls.code available, run with weave.inline.
        """

        nop = len(p)
        #noc = self.numOfCoeffs #numOfCoefficients(nop,self.order)
        noc = self.numOfCoefficients(nop, order)
                #numOfCoefficients(self, dim, order)
        import numpy
        retvec = numpy.empty(noc)
        retvec[0] = 1

        try:
            code = cls.code[(noc,nop)]
        except:
            code_init = "\n".join("int ind%d = %d;" % (i+1, self.numOfCoefficients(nop, i)) for i in xrange(order))

            code_for0 = """for (int i1 = 0; i1 < nop; i1++) {
                retvec[ind1] = (double) p[i1];
                ind1++; """
            code_forj = """for (int i%d = i%d; i%d < nop; i%d++) {
                retvec[ind%d] = %s;
                ind%d++; """
            code_for = "\n".join(code_forj % (j+1, j, j+1, j+1, j+1, "*".join("(double) p[i%d]" % (i+1) for i in xrange(j+1)), j+1) for j in xrange(1, order))

            code = "\n".join((
                code_init,
                code_for0,
                code_for,
                "\n}"*int(order)
                ))
            try:
                self.code[(noc,nop)] = code
            except AttributeError:
                self.code = {(noc,nop): code}
        from scipy import weave
        weave.inline(code, ['retvec', 'p', 'nop'])
        return retvec

