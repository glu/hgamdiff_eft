#!/usr/bin/env python
"""
Test scripy of 1D parametrisation.
Generates points, draws them, calculates
parametrisations, draws them.
"""


def calcAndDraw(ITP, order):
    # Calculate the parameterisation
    ITP.polynomial(order)
    NIpol=1000
    from numpy import linspace, array
    XI = linspace(XMin, XMax, NIpol)
    YI=array(map(lambda x:ITP.getValue([x], order), XI))
    import pylab
    pylab.plot(XI, YI, label="Parametrisation, order=%i"%order)

def calcAndGof(ITP, order):
    # Chi2 fun
    # What is NDOF here?
    chi2=0.0
    ITP.polynomial(order)
    for a in ITP.anchors:
        chi2+= (a.Y - ITP.getValue([a.params[0]], order) )**2
    return chi2

import ipol, anchor

# Empty ipol thingie
IP = ipol.BaseParameterisation()

# Lets generate some 1D anchors

XMin=-1
XMax=1
NGen=30
import random
from numpy import linspace, sqrt, cos, exp
X = [(i, (1+random.gauss(0,0.05))*i**1 * cos(i)*sqrt(abs(i))*exp(abs(i))) for i in linspace(XMin, XMax, NGen)]

# Populate the ipol
for x in X:
    a = anchor.Anchor()
    a.params = [x[0]]
    a.Y = x[1]
    IP.anchors.append(a)

# Plot anchor points first
from numpy import array
X=array(X)

x=X[:,0]
y=X[:,1]

import pylab
pylab.plot(x,y, "o", label="Anchors")

# Read polynomial order from CL
import sys
if len(sys.argv)>1:
    order=int(sys.argv[1])
    calcAndDraw(IP, order)
else:
    for i in xrange(1, 10):
        calcAndDraw(IP, i)



ch = 99999999999999
gof = []
for i in xrange(1, 10):
    gof.append([i, calcAndGof(IP, i)])
    #print i, temp
    #if temp < ch:
        #print "Chi2 getting lower at order %i, trying next buddy"%i
        #ch = temp
    #else:
        #print "Chi2 getting higher at order %i, trying next buddy"%i
        #ch = temp

for i in gof:
    print i




pylab.legend(loc=2)
pylab.show()
from IPython import embed
embed()
