#! /usr/bin/env python

"""Professor: a parameterisation-based tuning tool for Monte Carlo event generators."""

## Import some info from the professor package itself
import os, sys
scriptdir = os.path.basename(sys.argv[0])
if not scriptdir in sys.path:
    sys.path.insert(0, scriptdir)
from professor import __doc__ as profdescr, __version__ as profversion

## Check for matplotlib (optional, but you'll want it!)
try:
    import matplotlib
    if matplotlib.__version__ < "0.98":
        raise ImportError("installed matplotlib version %s: version 0.98 required!" % matplotlib.__version__)
except ImportError, err:
    print "Failed to import a recent version of matplotlib:", err
    print "Please install matplotlib (>=0.98) by hand if you want to do tune visualisation."

## Check for wx (optional, required for prof-I))
try:
    import wx
except ImportError, err:
    print "Failed to import wx:", err
    print "Please install wx if you want to use the interactive prof-I tool."

## Check for Minuit (optional, but needed for tune optimisation)
try:
    import minuit
except ImportError, err:
    try:
        import minuit2
    except ImportError, err2:
        print "Failed to import minuit or minuit2:" , err
        print "Please install pyminuit or pyminuit2 if you want to use the prof-tune script"
        print "See the PyMinuit homepage for information: http://code.google.com/p/pyminuit/"
        print "and http://projects.hepforge.org/professor/trac/wiki/PyMinuit"

## Check for SciPy (required)
try:
    import scipy
except ImportError, err:
    print "Failed to import scipy:", err
    print "SciPy is required. Please install it to use Professor."
    #sys.exit(1)

## Dynamically add executable files from the bin/ directory to the scripts list
myscripts = []
for it in os.listdir("./bin"):
    it = os.path.join("./bin", it)
    if os.path.isfile(it) and os.access(it, os.X_OK):
        myscripts.append(it)

## Setup definition
setupkwargs = {
    "name" : "professor",
    "version" : profversion,
    "scripts" : myscripts,
    "author" : ['Andy Buckley', 'Hendrick Hoeth', "Holger Schulz", "Eike von Seggern"],
    "author_email" : 'professor@projects.hepforge.org',
    "url" : 'http://projects.hepforge.org/professor/',
    "description" : 'A parameterisation-based tuning tool for Monte Carlo event generators.',
    "long_description" : profdescr,
    "keywords" : 'tuning generator montecarlo data hep physics particle optimisation',
    "license" : 'GPL',
    "classifiers" : ['Development Status :: 4 - Beta',
               'Environment :: Console',
               'Intended Audience :: Science/Research',
               'License :: OSI Approved :: GNU General Public License (GPL)',
               'Operating System :: OS Independent',
               'Programming Language :: Python',
               'Topic :: Scientific/Engineering :: Physics'],
    "data_files" : [("share/professor/scripts", ["contrib/makegallery.py", "contrib/prof-validateipol", "contrib/prof-filltemplates"]),
                    ("share/professor/templates", ["templates/prof-tune.template", "templates/prof-interpolate.template"]),
                    ("etc/bash_completion.d", ["prof-completion"])],
    "packages" : ["professor", "professor.interpolation", "professor.params", "professor.data", "professor.histo",
                  "professor.tools", "professor.minimize", "professor.user", "professor.controlplots"]
    }


if __name__ == "__main__":
    from distutils.core import setup
    setup(**setupkwargs)
