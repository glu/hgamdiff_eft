/***
 *
 * Professor C++ Driver
 *
 * Holger Schulz for the Professor Collaboration
 * email: holger.schulz@durham.ac.uk
 *
 * December 2014
 *
 * Usage:
 *
 *  ProfDriver *pdf = new ProfDriver("coeffs.ProfDF");
 *
 *  pdf->getValue(binid, P); // -> returns double
 *
 *
 */
#include <string>
#include <vector>
#include <map>

using std::string;
using std::vector;
// Coeffiecient map
typedef std::map<string, vector<double> > ProfDF;

class ProfDriver {
  
    private:
        ProfDF m_coeffs;
        int m_dim;
        int m_order;
        vector<double> m_center;
        vector<string> m_params;
        string m_info;

        
        int binomial(int n, int k);
        
        int numOfCoefficients(int dim, int order);
        vector<double> getLongVector1D(vector<double> p);
        vector<double> getLongVector2D(vector<double> p);
        vector<double> getLongVector3D(vector<double> p);
        vector<double> getLongVector(vector<double> p, vector<double> coeffs, int order);
        vector<double> getDP(vector<double> P, vector<double> C);
        
        
        void readProfDF(const char* fname);

    public:
         /// The constructor
         ProfDriver(string const& filename = "");
         /// The destructor
         ~ProfDriver();

         void printMeta();
         void printNbins();
         void printBinIds();
         void printCoeffs();

         vector<double> getCenter() {return m_center;};
        
         double getValue(string key, vector<double> P);
      
};
