// Professor technology demonstrator
#include "ProfDriver.h"
#include <iostream>
#include <cstdlib>
#include <sstream>

using namespace std;

int main(int argc, char** argv)
{
  string profdf = "";
  string binid = "";
  if (argc > 2)
    {
      profdf = argv[1];
      binid =  argv[2];
    }
  else
    {
      cout << "\nusage: ./prof filename binid\n" << endl;
      exit(-1);
    }

  
  // fast option load single grid file
  ProfDriver *pd = new ProfDriver(profdf);

  pd->printMeta();
  pd->printMeta();
  pd->printNbins();
 
  for (int i=0;i<49;i++) {
    std::ostringstream oss;
    oss << i;
    cout << i << " " << pd->getValue(binid+oss.str(), pd->getCenter()) << endl;
  }
  
  delete pd;

  return 0;
}
