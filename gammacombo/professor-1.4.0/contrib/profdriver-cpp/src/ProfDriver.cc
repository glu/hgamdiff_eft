#include "ProfDriver.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <boost/foreach.hpp>
#include<boost/algorithm/string/split.hpp>                                      
#include<boost/algorithm/string.hpp>
using namespace std;
using boost::algorithm::split;
using boost::is_any_of;                                                     

ProfDriver::ProfDriver(string const& filename):
  m_dim(0),
  m_order(0),
  m_center(0),
  m_params(0),
  m_info("")
{
    readProfDF(filename.c_str());
}

ProfDriver::~ProfDriver() {
}


double ProfDriver::getValue(string key, vector<double> P) {
  vector<double> LV = getLongVector(getDP(P, m_center), m_coeffs[key], m_order);
  double v = 0.0;
  for (int i=0; i< LV.size();i++) {
    v += LV[i]*m_coeffs[key][i];
  }
  return v;
}


void ProfDriver::printMeta() {
  cout << "Parametrisation info: " << m_info << endl;
  cout << "Polynomial degree: " << m_order << endl;
  cout << "Number of parameters: " << m_dim << " " <<  endl;
  cout << "Parameters:";
  for (int i=0;i<m_params.size();i++) {
    cout << " " << m_params[i];
  }
  cout << endl;
  cout << "Interpolation center:";
  for (int i=0;i<m_center.size();i++) {
    cout << " " << m_center[i];
  }
  cout << endl;
}


void ProfDriver::printCoeffs() {
  BOOST_FOREACH(const ProfDF::value_type& c, m_coeffs) {
    cout << c.first << " --- ";
    for (int i=0; i<c.second.size();i++) {
      cout << " " << c.second[i];
    }
    cout << endl;
  }
}

void ProfDriver::printBinIds() {
  BOOST_FOREACH(const ProfDF::value_type& c, m_coeffs) {
    cout << c.first << endl;
  }
}

void ProfDriver::printNbins() {
    cout << m_coeffs.size() << " bins available" << endl;
}

void ProfDriver::readProfDF(const char* fname) {
  string line;
  ifstream myfile (fname);
  if (myfile.is_open())
  {
    while ( getline (myfile,line) )
    {
      vector<string> tokens;
      boost::algorithm::split(tokens, line, boost::is_any_of("\t "), boost::token_compress_on);
      if (tokens[0] != "#") {
        vector<double> c;
        for (int i=1; i<tokens.size();i++) c.push_back(atof(tokens[i].c_str()));
        m_coeffs[tokens[0]] = c;
      }
      else {
        if (tokens[1] == "DIM") {
          m_dim = atoi(tokens[2].c_str());
        }
        else if (tokens[1] == "ORDER") {
          m_order = atoi(tokens[2].c_str());
        }
        else if (tokens[1] == "INFO") {
          for (int i=2;i<tokens.size();i++) {
            m_info+=" ";
            m_info+=tokens[i];
          }
        }
        else if (tokens[1] == "PARAMS") {
          for (int i=2;i<tokens.size();i++) {
            m_params.push_back(tokens[i]);
          }
        }
        else if (tokens[1] == "CENTER") {
          for (int i=2;i<tokens.size();i++) {
            m_center.push_back(atof(tokens[i].c_str()));
          }
        }
      }
    }
    myfile.close();
  }
  else cout << "Unable to open file"; 
}

int ProfDriver::binomial(int n, int k) {
    int ntok = 1;
    int r = min(k, n-k);
    for(int i=0; i<r;++i) {
      ntok=ntok*(n-i)/(i+1);
    }
    return ntok;
}

// Tested and working
int ProfDriver::numOfCoefficients(int dim, int order) {
  return binomial(dim+order, order);
}

vector<double> ProfDriver::getLongVector1D(vector<double> p) {
  int nop = p.size();
  vector<double> retvec;
  retvec.push_back(1.0);    // This is the offset, for alpha
  for (int i=0;i<nop;i++) { // Linear coefficients, for beta
    retvec.push_back(p[i]);
  }

  return retvec;
}

vector<double> ProfDriver::getLongVector2D(vector<double> p) {
  int nop = p.size();
  vector<double> retvec;
  retvec.push_back(1.0);    // This is the offset, for alpha
  for (int i=0;i<nop;i++) { // Linear coefficients, for beta
    retvec.push_back(p[i]);
  }
  for (int i=0;i<nop;i++) {
    for (int j=0;j<nop;j++) {
      if (i<=j) {
        retvec.push_back(p[i]*p[j]);
      }
    }
  }

  return retvec;
}

vector<double> ProfDriver::getLongVector3D(vector<double> p) {
  int nop = p.size();
  vector<double> retvec;
  retvec.push_back(1.0);    // This is the offset, for alpha
  for (int i=0;i<nop;i++) { // Linear coefficients, for beta
    retvec.push_back(p[i]);
  }
  for (int i=0;i<nop;i++) {
    for (int j=0;j<nop;j++) {
      if (i<=j) {
        retvec.push_back(p[i]*p[j]);
      }
    }
  }
  for (int i=0;i<nop;i++) {
    for (int j=0;j<nop;j++) {
      for (int k=0;k<nop;k++) {
        if (i<=j && i<=k && j<=k) {
          retvec.push_back(p[i]*p[j]*p[k]);
        }
      }
    }
  }

  return retvec;
}

vector<double> ProfDriver::getLongVector(vector<double> p, vector<double> coeffs, int order) {
  if (order < 1 || order > 3) {
    cout << "ERROR degree " << order << " not implemented, exiting" << endl;
    exit(1);
  }
  if (coeffs.size() != numOfCoefficients(p.size(), order)) {
    cout << "ERROR invalid number of coefficients: " << coeffs.size() << " supplied, " << numOfCoefficients(p.size(), order) << " required, exiting" << endl;
  }
  if (order == 1) return getLongVector1D(p);
  if (order == 2) return getLongVector2D(p);
  if (order == 3) return getLongVector3D(p);

}

vector<double> ProfDriver::getDP(vector<double> P, vector<double> C) {
  vector<double> dp;
  for (int i=0; i<P.size();i++) {
    dp.push_back(P[i] - C[i]);
  }
  return dp;
}
