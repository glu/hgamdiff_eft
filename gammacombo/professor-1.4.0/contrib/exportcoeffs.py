#!/usr/bin/env python

import sys
import os

import professor.user as prof
from professor.tools import shell
ipshell = shell.usePrettyTraceback()

import optparse
parser = optparse.OptionParser(usage=__doc__, version=prof.version)


parser.add_option("-o", "--outfile",
        dest = "OUTFILE",
        help = "Output text file")

## Add standard logging control

opts, args = parser.parse_args()
ipolset = prof.InterpolationSet.mkFromPickle(args[0])

with open(opts.OUTFILE, "w") as f:
    import time
    f.write("# INFO source file %s generated %s\n"%(args[0], time.ctime(os.path.getctime(args[0]))))
    f.write("# DIM %i\n"%len(ipolset.center))
    f.write("# ORDER %i\n"%ipolset.items()[0][1].order)
    P = "# PARAMS"
    for p in ipolset.getParameterNames():
        P+=" %s"%p
    f.write(P+"\n")
    C = "# CENTER"
    for c in ipolset.center:
        C+=" " + format(c, '.16f')
    f.write(C+"\n")
    for k, v in ipolset.iteritems():
        co = "%s"%k
        for c in v.coeffs:
            co +=" " + format(c, '.16f')
        co +="\n"
        f.write(co)
