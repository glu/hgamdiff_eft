#! /usr/bin/env python

"""\
Merge all the single-bin PDG_HADRON_MULTIPLICITIES data point sets
into a single DPS with lots of bins.
"""

from optparse import OptionParser
parser = OptionParser(usage=__doc__)
parser.add_option("-o", "--outfile", dest="OUTFILE", help="filename to write output to", default=None)
opts, args = parser.parse_args()
if not args:
    import sys
    print "You must specify an input file"
    sys.exit(1)
print "Reading data from %s" % args[0]

from xml.etree import ElementTree as ET
tree = ET.parse(args[0])
root = tree.getroot()
merge_dps = ET.Element("dataPointSet", path="/MERGE_PDG_HADRON_MULTIPLICITIES", name="merge", dimension="2")
root.append(merge_dps)
for dps in tree.findall("dataPointSet"):
    if "/PDG_HADRON_MULTIPLICITIES" not in dps.get("path"):
        continue

    name = dps.get("name")
    # print dps.findall("dataPoint/measurement")
    dp = dps.findall("dataPoint/measurement")[-1]
    y = dp.get("value")
    dyplus = dp.get("errorPlus")
    dyminus = dp.get("errorMinus")

    if "y03" in name:
        xval = int(name[1:3]) # e.g. 03 in d03-x...
        merge_dp = ET.Element("dataPoint")
        merge_dp.append(ET.Element("measurement", value=str(xval), errorPlus="0.5", errorMinus="0.5"))
        merge_dp.append(ET.Element("measurement", value=str(y), errorPlus=str(dyplus), errorMinus=str(dyminus)))
        merge_dps.append(merge_dp)

    name_particle = {
        "d01-x01-y03" : "pi+",
        "d02-x01-y03" : "pi0",
        "d03-x01-y03" : "K+",
        "d04-x01-y03" : "K0",
        "d05-x01-y03" : "eta",
        "d06-x01-y03" : "eta'",
        "d07-x01-y03" : "D+",
        "d08-x01-y03" : "D0",
        # "d09-x01-y03" : "D+s",
        "d10-x01-y03" : "B+0",
        "d11-x01-y03" : "B+",
        # "d12-x01-y03" : "B0s",
        # "d18-x01-y03" : "K*+",
        "d19-x01-y03" : "K*0",
        "d38-x01-y03" : "p",
        "d39-x01-y03" : "Lambda",
        "d43-x01-y03" : "Sigma+-"
        }

    if name in name_particle.keys():
        print name_particle[name], y, dyplus, dyminus

print

## Write output
if opts.OUTFILE:
    tree.write(opts.OUTFILE)
else:
    import sys
    tree.write(sys.stdout)


# from xml.etree import ElementLib as ETlib
# from xml.etree import XML
# print
# merge_dps.write(sys.stdout)
# print ETlib.prettyprint(merge_dps)
