import os
import numpy

from professor import histo
from professor.params import ParameterPoint, ParameterRange
from professor.tools.errors import ArgumentError, DataProxyError
from professor.tools.sorting import ParameterCmp
import professor.tools.log as logging

## TODO: Add getRunPath(run) method to MCData and ManualMCData. This should
##       render double-implementations of getParameterCmp, loadRun
##       unneccessary.


class MCData(object):
    """Interface for a directory with MC generated data.

    `MCData` abstracts a directory with MC generated data with a layout
    following::

        base/run1
             run2
             ...

    Data is read from the filesystem only if necessary.

    :ivar basepath: Directory path within which all runs are located (typically `basepath`/mc).
    :ivar availableruns: List of valid run names, based on a scan of valid run dirs found in `basepath`.
    """

    def __init__(self, base, DUMMY_REMOVE=None):
        self.basepath = base

        # {run => ParameterPoint}
        self._params = dict()

        # {run => {histoname => histo}}
        self._histos = dict()

        temp = []
        for run in os.listdir(self.basepath):
            if self.isValidRunDir(runid=run):
                temp.append(run)
        self.availableruns = tuple(sorted(temp))
        if not self.availableruns:
            raise DataProxyError("No valid runs found in '%s'! Check for existence of non-empty files out.{yoda,aida} and used_params." % self.basepath)

    @property
    def runs(self):
        return self._params.keys()

    def isValidRunDir(self, runid=None, runpath=None):
        """Check that the run directory is valid.

        Checks for an AIDA or YODA file and a ``used_params`` file.

        The run can be specified by `runid` or `runpath`.

        Parameters
        ----------
        runid : str
            The ID of the run, i.e. the subdirectory name.
        runpath : str
            The full path to the rundirectory. This is used in the
            ManualMCData class.
        """
        if runid is not None and runpath is not None:
            raise ArgumentError("runid and runpath given: Don't know what to do!")
        if runid is None and runpath is None:
            raise ArgumentError("runid and runpath not given!")

        if runid is not None:
            path = os.path.join(self.basepath, runid)
        else:
            path = runpath

        if not os.path.isdir(path):
            return False

        temp = os.path.join(path, "used_params")
        if not os.path.isfile(temp):
            logging.debug("Could not find file 'used_params' in %s" % path)
            return False

        # TODO: Do dir scanning for histo files in the histo module?
        mcfiles = []
        for x in histo.FORMATS:
            import glob
            mcfiles += glob.glob(os.path.join(path, "*.%s" % x))

        # TODO: Return false / throw exception if no histo files found?

        return True


    ## TODO: add switch to ignore used_params files
    def loadRun(self, run, loadhistos=True):
        """Load data for a run.

        Parameters
        ----------
        run : str
            The run identifier to load.
        loadhistos : bool, optional
            Turn loading histogram data on (default) or off.
        """
        ## Load run only if necessary
        if (not loadhistos or self._histos.has_key(run)) and self._params.has_key(run):
            return

        d = os.path.join(self.basepath, run)
        if not os.path.isdir(d):
            raise DataProxyError("Run '%s' not found in base directory '%s'!" % (run, self.basepath))

        temp = os.path.join(d, "used_params")
        if not os.path.isfile(temp):
            raise DataProxyError("Run '%s': no file 'used_params' found!" % run)
        self._params[run] = ParameterPoint.mkFromFile(temp)

        if loadhistos:
            ## Read in histos from files
            # TODO: reduce histo-file-finding code duplication with isValidRun function above
            hfiles = []
            for x in histo.FORMATS:
                import glob
                hfiles += glob.glob(os.path.join(d, "*.%s" % x))
            # TODO: Check that there are not clashes/overlaps between data files (i.e. two formats with the same data)
            if not hfiles:
                raise DataProxyError("Run '%s': no MC data files found!" % run)
            logging.debug("Loading MC data from %s" % hfiles)
            histodict = {}
            for hfile in sorted(hfiles):
                # TODO: Check that there are not clashes/overlaps between data file contents before updating
                histodict.update(histo.Histo.fromFile(hfile))
            self._histos[run] = histodict


    def loadAllRuns(self, loadhistos=True):
        """Load the data for all available runs.

        Parameters
        ----------
        loadhistos : bool, optional
            Turn loading histogram data on (default) or off.

        See Also
        --------
        loadRun : Load a single run.
        loadAllThreaded : Load all runs threaded, useful if IO lags are
            huge, e.g. with network file storage.
        """
        for run in self.availableruns:
            try:
                self.loadRun(run, loadhistos=loadhistos)
            except DataProxyError:
                logging.error("Could not load data from run directory '%s'!" % run)


    # Try threading to speed things up => To no avail, blast you GIL!
    def loadAllThreaded(self, loadhistos=True, numthreads=8):
        """Load the data for all available runs (multi-threaded).

        This is only useful if IO lags are huge. Otherwise the Python thread
        overhead makes this more time-consuming than `loadAll`.

        Parameters
        ----------
        loadhistos : bool, optional
            Turn loading histogram data on (default) or off.
        numthreads : int, optional
            Number of threads (default: 8).

        See Also
        --------
        loadRun : Load a single run.
        loadAll : Load all runs sequentially.
        """
        import threading
        import Queue
        # Code copied from Python documentation.
        # I think the threads idle after all runs were read. But I'm not
        # sure if it's a problem...
        q = Queue.Queue()
        def worker(idx):
            logging.debug("Starting IO worker thread")
            while True:
                # print "T%i: Trying to get a run..." % (idx)
                run = q.get()
                # print "T%i: loading run %s" % (idx, run)
                self.loadRun(run, loadhistos=loadhistos)
                # print "T%i: loaded  run %s" % (idx, run)
                # time.sleep(1)
                q.task_done()
        # fill Queue and start the IO
        for run in self.availableruns:
            q.put(run)
            # print "added run", run, "to queue"

        # create #(availableruns) worker threads
        for i in range(numthreads):
            t = threading.Thread(target=worker, args=(i,))
            t.setDaemon(True)
            t.start()
        # print "waiting for threads"
        q.join()

    def getRunHistos(self, run, filtered=False):
        """Return the {obsname => Histo} dict for given run.

        Parameters
        ----------
        run : str
            Run ID.
        filtered : bool, optional
            Return only histograms that contain valid (i.e. not NaN) data.
            By default all histograms are returned (for the sake of speed).

        Returns
        -------
        histograms : dict
            Dictionary that map histogram paths to `Histo` instances.
        """
        self.loadRun(run)
        if filtered:
            ret = {}
            for o, h in self._histos[run].iteritems():
                if self._is_valid_histo(h):
                    ret[o] = h
            return ret
        else:
            return self._histos[run]

    def getRunParams(self, run, retall=False):
        """Get the run parameters.

        Parameters
        ----------
        run : str
            Run ID.

        Returns
        -------
        parameters : ParameterPoint
            The parameter values.
        """
        self.loadRun(run, loadhistos=False)
        return self._params[run]

    def getParameterBounds(self, runs=None):
        """Get the extremal parameter bounds of runs.

        Returns
        -------
        bounds : ParameterRange
        """
        # load parameter files
        if not runs:
            runs = self.availableruns
        points = [self.getRunParams(r) for r in runs]
        return ParameterRange.mkFromPoints(points)
        # bounds = {}
        # init = self.getRunParams(runs[0])
        # for name, value in init.items():
            # bounds[name] = [value, value]

        # for run in runs[1:]:
            # for name, value in self.getRunParams(run).items():
                # bounds[name][0] = min(bounds[name][0], value)
                # bounds[name][1] = max(bounds[name][1], value)
        # return bounds

    def getParameterNames(self):
        return self.getRunParams(self.availableruns[0]).names

    def getParameterCmp(self, run=None):
        if run is None:
            run = self.availableruns[0]
        p = os.path.join(self.basepath, run, "used_params")
        return ParameterCmp.mkFromFile(p)

    def getScanParam(self, run):
        self.loadRun(run)
        sp = self._params[run].scanparam
        if sp is None:
            raise DataProxyError("Run %s has no scan parameter attached!" % run)
        return sp

    def getAvailableObservables(self, filtered=True):
        """Get a sorted list with the available observables.

        The observables are taken from the first available MC run data.

        By default only the observables containing valid numerical data
        (i.e. no NaN's) are returned.

        Parameters
        ----------
        filtered : bool, optional
            Return only histograms that contain valid (i.e. not NaN) data
            (default). If set to `False` all available observables are
            returned.
        """
        if len(self._histos) == 0:
            self.loadRun(self.availableruns[0])

        if filtered:
            obs = []
            for o, h in self._histos.values()[0].iteritems():
                if self._is_valid_histo(h):
                    obs.append(o)
        else:
            obs = self._histos.values()[0].keys()
        return sorted(obs)

    availablehistos = property(getAvailableObservables,
                               doc="The available histogram names (sorted).")
    loadedruns = property(lambda s: sorted(s._histos.keys()),
                          doc="The currently loaded run numbers (sorted).")

    # TODO: Move this to lighthisto.py?
    # A simple helper used to define what a "valid" histogram is.
    @staticmethod
    def _is_valid_histo(histogram):
        return not numpy.isnan(histogram.getArea())



class ManualMCData(MCData):
    def __init__(self, runpathmap=None):

        # {run => parameter dict}
        self._params = dict()

        #DEPRECATED:
        # {run => scan parameter}
        # self._scanparam = dict()

        # {run => {histoname => histo}}
        self._histos = dict()

        self._runpaths = {}
        if runpathmap is not None:
            for runid, path in runpathmap.items():
                self.addRunPath(runid, path)

    # overwrite MCData.availableruns variable
    availableruns = property(lambda self: self._runpaths.keys())


    def addRunPath(self, runid, path):
        if not self.isValidRunDir(runpath = path):
            raise DataProxyError("Path '%s' is not a valid MC run"
                                 " directory!" % (path))
        if self._runpaths.has_key(runid):
            raise DataProxyError("MC run '%s' already exists!" % (runid))
        self._runpaths[runid] = path


    ## TODO: Once getRunPath is implemented, remove the following
    ##       implementations.
    def getParameterCmp(self, runid=None):
        if runid is None:
            return ParameterCmp.mkFromFile(self._runpaths.values()[0])
        else:
            return ParameterCmp.mkFromFile(self._runpaths[runid])


    def loadRun(self, runid, loadhistos=True):
        """Load data for `run`."""
        ## Load run only if necessary
        if ((not loadhistos or self._histos.has_key(runid)) and self._params.has_key(runid)):
            return

        d = self._runpaths[runid]

        if not os.path.isdir(d):
            raise DataProxyError("Run '%s' not found in base directory '%s'!" % (runid, self.basepath))

        temp = os.path.join(d, "used_params")
        if not os.path.isfile(temp):
            raise DataProxyError("Run '%s': no file 'used_params' found!" % runid)
        self._params[runid] = ParameterPoint.mkFromFile(temp)

        if loadhistos:
            import glob
            aidafiles = glob.glob(os.path.join(d, "*.aida"))
            if not aidafiles:
                raise DataProxyError("Run '%s': no MC data files found!" % runid)
            logging.debug("Loading MC data from %s" % aidafiles)
            histodict = {}
            for aidafile in sorted(aidafiles):
                histodict.update(histo.Histo.fromAIDA(aidafile))
            self._histos[runid] = histodict
