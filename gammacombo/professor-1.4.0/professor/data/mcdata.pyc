ó
9Tc           @   s¦   d  d l  Z  d  d l Z d  d l m Z d  d l m Z m Z d  d l m Z m	 Z	 d  d l
 m Z d  d l j j Z d e f d     YZ d e f d	     YZ d S(
   iÿÿÿÿN(   t   histo(   t   ParameterPointt   ParameterRange(   t   ArgumentErrort   DataProxyError(   t   ParameterCmpt   MCDatac           B   sã   e  Z d  Z d d  Z e d    Z d d d  Z e d  Z	 e d  Z
 e d d  Z e d  Z e d	  Z d d
  Z d   Z d d  Z d   Z e d  Z e e d d Z e d   d d Z e d    Z RS(   sÈ  Interface for a directory with MC generated data.

    `MCData` abstracts a directory with MC generated data with a layout
    following::

        base/run1
             run2
             ...

    Data is read from the filesystem only if necessary.

    :ivar basepath: Directory path within which all runs are located (typically `basepath`/mc).
    :ivar availableruns: List of valid run names, based on a scan of valid run dirs found in `basepath`.
    c         C   s   | |  _  t   |  _ t   |  _ g  } x< t j |  j   D]( } |  j d |  r: | j |  q: q: Wt t	 |   |  _
 |  j
 s t d |  j    n  d  S(   Nt   runidsd   No valid runs found in '%s'! Check for existence of non-empty files out.{yoda,aida} and used_params.(   t   basepatht   dictt   _paramst   _histost   ost   listdirt   isValidRunDirt   appendt   tuplet   sortedt   availablerunsR   (   t   selft   baset   DUMMY_REMOVEt   tempt   run(    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   __init__   s    		c         C   s   |  j  j   S(   N(   R
   t   keys(   R   (    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   runs0   s    c      	   C   s  | d k	 r' | d k	 r' t d   n  | d k rN | d k rN t d   n  | d k	 ru t j j |  j |  } n | } t j j |  s t St j j | d  } t j j |  sÍ t	 j
 d |  t Sg  } xC t j D]8 } d d l } | | j t j j | d |   7} qÝ Wt S(   s  Check that the run directory is valid.

        Checks for an AIDA or YODA file and a ``used_params`` file.

        The run can be specified by `runid` or `runpath`.

        Parameters
        ----------
        runid : str
            The ID of the run, i.e. the subdirectory name.
        runpath : str
            The full path to the rundirectory. This is used in the
            ManualMCData class.
        s/   runid and runpath given: Don't know what to do!s   runid and runpath not given!t   used_paramss'   Could not find file 'used_params' in %siÿÿÿÿNs   *.%s(   t   NoneR   R   t   patht   joinR   t   isdirt   Falset   isfilet   loggingt   debugR    t   FORMATSt   globt   True(   R   R   t   runpathR   R   t   mcfilest   xR%   (    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyR   4   s$    *c   
   	   C   s  | s |  j  j |  r/ |  j j |  r/ d St j j |  j |  } t j j |  su t d | |  j f   n  t j j | d  } t j j	 |  s¯ t d |   n  t
 j |  |  j | <| rg  } xC t j D]8 } d d l } | | j t j j | d |   7} qÛ W| s0t d |   n  t j d |  i  } x- t |  D] }	 | j t j j |	   qTW| |  j  | <n  d S(	   sß   Load data for a run.

        Parameters
        ----------
        run : str
            The run identifier to load.
        loadhistos : bool, optional
            Turn loading histogram data on (default) or off.
        Ns*   Run '%s' not found in base directory '%s'!R   s&   Run '%s': no file 'used_params' found!iÿÿÿÿs   *.%ss!   Run '%s': no MC data files found!s   Loading MC data from %s(   R   t   has_keyR
   R   R   R   R   R   R   R!   R   t
   mkFromFileR    R$   R%   R"   R#   R   t   updatet   Histot   fromFile(
   R   R   t
   loadhistost   dR   t   hfilesR)   R%   t	   histodictt   hfile(    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   loadRuna   s*    +*c         C   sT   xM |  j  D]B } y |  j | d | Wq
 t k
 rK t j d |  q
 Xq
 Wd S(   sx  Load the data for all available runs.

        Parameters
        ----------
        loadhistos : bool, optional
            Turn loading histogram data on (default) or off.

        See Also
        --------
        loadRun : Load a single run.
        loadAllThreaded : Load all runs threaded, useful if IO lags are
            huge, e.g. with network file storage.
        R/   s,   Could not load data from run directory '%s'!N(   R   R4   R   R"   t   error(   R   R/   R   (    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   loadAllRuns   s
    i   c   	         s±   d d l  } d d l } | j        f d   } x  j D] }  j |  qC WxF t |  D]8 } | j d | d | f  } | j t  | j   qg W j	   d S(   s  Load the data for all available runs (multi-threaded).

        This is only useful if IO lags are huge. Otherwise the Python thread
        overhead makes this more time-consuming than `loadAll`.

        Parameters
        ----------
        loadhistos : bool, optional
            Turn loading histogram data on (default) or off.
        numthreads : int, optional
            Number of threads (default: 8).

        See Also
        --------
        loadRun : Load a single run.
        loadAll : Load all runs sequentially.
        iÿÿÿÿNc            sG   t  j d  x3 t rB  j   }  j | d    j   q Wd  S(   Ns   Starting IO worker threadR/   (   R"   R#   R&   t   getR4   t	   task_done(   t   idxR   (   R/   t   qR   (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   worker¸   s
    	t   targett   args(
   t	   threadingt   QueueR   t   putt   ranget   Threadt	   setDaemonR&   t   startR   (	   R   R/   t
   numthreadsR>   R?   R;   R   t   it   t(    (   R/   R:   R   s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   loadAllThreaded    s    c         C   sl   |  j  |  | r] i  } x= |  j | j   D]( \ } } |  j |  r- | | | <q- q- W| S|  j | Sd S(   sÁ  Return the {obsname => Histo} dict for given run.

        Parameters
        ----------
        run : str
            Run ID.
        filtered : bool, optional
            Return only histograms that contain valid (i.e. not NaN) data.
            By default all histograms are returned (for the sake of speed).

        Returns
        -------
        histograms : dict
            Dictionary that map histogram paths to `Histo` instances.
        N(   R4   R   t	   iteritemst   _is_valid_histo(   R   R   t   filteredt   rett   ot   h(    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   getRunHistosÏ   s     c         C   s   |  j  | d t |  j | S(   sÔ   Get the run parameters.

        Parameters
        ----------
        run : str
            Run ID.

        Returns
        -------
        parameters : ParameterPoint
            The parameter values.
        R/   (   R4   R    R
   (   R   R   t   retall(    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   getRunParamsé   s    c         C   sA   | s |  j  } n  g  | D] } |  j |  ^ q } t j |  S(   st   Get the extremal parameter bounds of runs.

        Returns
        -------
        bounds : ParameterRange
        (   R   RQ   R   t   mkFromPoints(   R   R   t   rt   points(    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   getParameterBoundsù   s    "c         C   s   |  j  |  j d  j S(   Ni    (   RQ   R   t   names(   R   (    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   getParameterNames  s    c         C   sD   | d  k r |  j d } n  t j j |  j | d  } t j |  S(   Ni    R   (   R   R   R   R   R   R   R   R+   (   R   R   t   p(    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   getParameterCmp  s    c         C   s@   |  j  |  |  j | j } | d  k r< t d |   n  | S(   Ns&   Run %s has no scan parameter attached!(   R4   R
   t	   scanparamR   R   (   R   R   t   sp(    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   getScanParam  s
    c         C   s§   t  |  j  d k r, |  j |  j d  n  | r g  } xb |  j j   d j   D]+ \ } } |  j |  rR | j |  qR qR Wn |  j j   d j   } t	 |  S(   sâ  Get a sorted list with the available observables.

        The observables are taken from the first available MC run data.

        By default only the observables containing valid numerical data
        (i.e. no NaN's) are returned.

        Parameters
        ----------
        filtered : bool, optional
            Return only histograms that contain valid (i.e. not NaN) data
            (default). If set to `False` all available observables are
            returned.
        i    (
   t   lenR   R4   R   t   valuesRI   RJ   R   R   R   (   R   RK   t   obsRM   RN   (    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   getAvailableObservables   s    &t   docs'   The available histogram names (sorted).c         C   s   t  |  j j    S(   N(   R   R   R   (   t   s(    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   <lambda>=  s    s*   The currently loaded run numbers (sorted).c         C   s   t  j |  j    S(   N(   t   numpyt   isnant   getArea(   t	   histogram(    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyRJ   B  s    N(   t   __name__t
   __module__t   __doc__R   R   t   propertyR   R   R&   R4   R6   RH   R    RO   RQ   RU   RW   RY   R\   R`   t   availablehistost
   loadedrunst   staticmethodRJ   (    (    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyR      s&   -)/					t   ManualMCDatac           B   sD   e  Z d d   Z e d    Z d   Z d d  Z e d  Z	 RS(   c         C   sa   t    |  _ t    |  _ i  |  _ | d  k	 r] x- | j   D] \ } } |  j | |  q: Wn  d  S(   N(   R	   R
   R   t	   _runpathsR   t   itemst
   addRunPath(   R   t
   runpathmapR   R   (    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyR   I  s    	c         C   s   |  j  j   S(   N(   Rp   R   (   R   (    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyRc   [  s    c         C   s[   |  j  d |  s% t d |   n  |  j j |  rJ t d |   n  | |  j | <d  S(   NR'   s*   Path '%s' is not a valid MC run directory!s   MC run '%s' already exists!(   R   R   Rp   R*   (   R   R   R   (    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyRr   ^  s    c         C   s>   | d  k r& t j |  j j   d  St j |  j |  Sd  S(   Ni    (   R   R   R+   Rp   R^   (   R   R   (    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyRY   i  s    c   	      C   s^  | s |  j  j |  r/ |  j j |  r/ d S|  j | } t j j |  sj t d | |  j f   n  t j j	 | d  } t j j
 |  s¤ t d |   n  t j |  |  j | <| rZd d l } | j t j j	 | d   } | st d |   n  t j d |  i  } x- t |  D] } | j t j j |   q'W| |  j  | <n  d S(	   s   Load data for `run`.Ns*   Run '%s' not found in base directory '%s'!R   s&   Run '%s': no file 'used_params' found!iÿÿÿÿs   *.aidas!   Run '%s': no MC data files found!s   Loading MC data from %s(   R   R*   R
   Rp   R   R   R   R   R   R   R!   R   R+   R%   R"   R#   R   R,   R    R-   t   fromAIDA(	   R   R   R/   R0   R   R%   t	   aidafilesR2   t   aidafile(    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyR4   p  s&    +N(
   Rh   Ri   R   R   Rk   R   Rr   RY   R&   R4   (    (    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyRo   H  s
   	(   R   Rd   t	   professorR    t   professor.paramsR   R   t   professor.tools.errorsR   R   t   professor.tools.sortingR   t   professor.tools.logt   toolst   logR"   t   objectR   Ro   (    (    (    s@   /home/hschulz/src/prof-release/prof-140/professor/data/mcdata.pyt   <module>   s   ÿ :