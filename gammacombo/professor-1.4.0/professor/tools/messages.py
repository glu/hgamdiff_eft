"""
Central collection of guidance messages and logos for use by all official prof-*
scripts.
"""

FARNSWORTH = \
"""
                      -smmy+++oyyyhs/-`
                     .ms.         `:ohmho-
                     dM:               :mmhyd+`
                     mM/         so+o/`    -hhyd+`
                     +M+        `+:-`:mo:/-  :-hdddo
                      Mh`       `//mNhy+++-+sMooydddhsdy.
                      /M+       .Ny/+s .-/s/`ds/ym. :y/ym:
                       +M:     .yN  ++  :/   hd+/syMm:.oMo
                       `sm:+ss+ood: `s/`   `od:    .ohss.
                        oMdyd/    /syydsooos-oh-`.   -N:
                        dhoN+          .d:    :o+yydhohd
                        .mMMy        .yh-           :hMs/-:`
                          +NM+     `oh- .////:..`     `-/dM+
                           hh-    -d/-hNmddhNNNNNmdyyo+sdms`
                           -dNs-  yo:MMMMMMMy
                         `-/dd    /.+dNNMMMMNs:
                       :yhsd+.      `/+o///odMMo           `ymdyd+
                     `hh:/Mhdh -s.       `dy/-.           .oMmshmmys.
                    :d+`-h.sN+sN:      -/ds`              /N:-hNoyoNm.
                ./yyNo  /o  `+yo/./d::NM/                 -dmsoMy  .hM
             :hdy/`.m.   y     .oyhyydyM`                  h.`Nhd+oo+M
           smy/`   +M.   NNdo-       /dMsoyo`              oNyM/ ...Ns
         /y+`      `d`   do-oyhhyoo:ho:ho `:so:`          :+mM+`  `dd`
       +m+`     o:  ++   /m`   `.-//m: /N    .+dh-      +y:yMh-`  yN.
      sd.       hy  :m    N:        .d `N       /mh`  oN/  .NMNm`oMd.
    -sd`       -N:   s:   s:         N. N/        +dmh+`    oMMooMMM/
    my.        sm`   `d   /h         M: md          :`       +NmMdMMM/
  -hd         +N/     y/  .h         M:/Nm-                   .ymNmMN.
 `mM.        .my      .d`  y.        M:/MM:                     .sNy.
`sM-     .+-.yo      `-N+.`h.        M:/Msd:                 .+hh+.
:Ms       dMMd:-/+.+my+/osymo        ys/MNmmd:            .shd+
sd`           -hmMNy-  -//-.y+       +msMys -/s+..    `:odd+-
Ms           .NMd/     ``./hh        `yMN:h`   .yNmydNyo.`
"""


FRINK = \
"""
                                                       +/+`      /+/o-
                                                      /+ `y-    -y   d
                                                      :o  `h.   s-   m
               .///::ss+.                             `h   `y.  h   `d
             .o: .++o::`                               y`   `+/o-   :s
     -///:-:/d/:+s:/+:/y`                              //           o+/+//y
  ://.  `///:`    .:+ho`                               `o:/+//o.     .   `h
-hso/:/h+`           `+/                              ///`    /+       -++
./oo/y+                .o/-                          :o    -/o/       s:
-h+:o-        `-:.       :o///:                     `:+    -`o/      y-
`:s/s      `+o:..-+/  `:y..`  -s                   shso       m     /o
 +- y     `so ::::`:o-.yy :ooo`y                 `s`//:+:     :    /o
    h`    -/+`-::- o-   -+:``-+-                .y`  ./+:/+:   :/:/.
    :s     s/+/:/++-      .yy:/                -s       ://-///os+
     /+  :+- ----           -o+:              :o           -/::o/s
      -ss-                   `o:+/-`         +/                :+
      +:++.          /.     :o.   ./o.      o:               `o:
     `o +/:          `:+++//.       o:    `s.               :s`
        s/:. ``            -::::/y/+-    -s`              `o:
         //:os         -/o+. :///`      /o               :o`
             o:      `+:  :::-s`       o/              `o:
              s:            /o`      -o.              /o`
               y-           d      :+:              .s-
               `h`          s` `/+/.              `+/
                .y       `///:s/                 /o`
               .`o.    `o+`   +o/              -o-
             `h-.-//s//h`    `y `o/          `o/
              s-    s``s-/o- `+s  -s`      `++
              .oy++smNMNMMMMMNMM-  `h     /+`
            .oy-MMMMN/` .+shhhd/   o:   :y-
           +/o `yNh+` +o      :/  /+    -.s
         .oo:-  .+    ``       h   `+/    h`
        :o +.+  /+             /+    .y`  -o
       /+  /-.s o:             `h     o:   h`
      o/    so. o-     h+       s-    /+   -s
    `s-    .s   o-              -s    :+    y.
   :o`     -s   h`               h    :+    `h
 `o:        h`  h     `/`        +:   :+     //
-o`         ++  d     -o-        .y   o/      y.
"""

HOMER = \
"""
                                                   ./sdNd:
                                              `:ohNMMMMMMMy.
m/                                        ./smMMMMMMMMMMMMMNo`
md                                    ./ymMMMMMMMMMMMMMMMMMMMm:
oM-                               `:sdNMMMMMMMMMMMMMMMMMMMMMMMMy`
-My                            `/yNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN/
 mN`                          /mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMy.
 sM+                          sMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm:
 -Mm                          `oNMMMMMMMh+osydNMMMMMMMMMMMMMMMMMMMMMNs`
  mM:                           .hMMMMMy`    ``-+hNMMMMMMMMMMMMMMMMMMMh.
  sMh                            `:hMMy`          ./hNMMMMMMMMMMMMMMMMMm-
  -MM.                             :ss`              .sNMMMMMMMMMMMMMMMMN:
   mMo                            .h///:-`             .sNMMMMMMMMMMMMMMMN-
   oMm`                         `+o-````:s/.-:::.        :dMMMMMMMMMMMMMMNy
   -MM/                         ss `y:   -do/--:+s/`      `yMMMMMMMMNmms/-`
    mMd                         d:  .`  :y`  .`  `+y       oMMMmdhoh:.y
    oMM-                        /s:/+//:d-  `yo    N:    `ohs+-.`  y  s
    -MMs                        `md.  `.y:   `    .m.  ``s/        y  m-
     mMN`                       :soy/.` `s/.`  `.:y-  /dh/.       `N  M.
    `yMNo.                     `os:://+o/:/+oooso/` `s+mdyh`      `N `N
   /++h.-/o/.                 `h/       .-+o/.      `.+d:y-       `N.+y
 .+d  y/` `:y+`               oh`           :y+`    `./s+:         ysh.
 d.oo``/y:   +y`               :+y/:-`.```````/s`   ./::d          `:`
.m: -oo:h-    d/                 +ss+/++/::://o/+  -y++s-
h+ss-`.y+     y+                 `/o.mMMNdmmdmN-s  :y``
/y``:/sh/    `m-                 +o`:NMMMMMMNh:o-  y.
 :ys//y.     /d                 ``/s/-/+++/:``o:   mo
  :d..       +y                -hy--h+:. `-/+/```-+hd/
   yo        -N`              /s`h`+/ `:oss+////:-` `m.
   oo         yo       `/soo+m+.+ysh  /yoo           so
   /h         `m-   `oso-   -yyo. h:/y:  h`       `+sssoos+.
   .N          .d- -m+`     /y.   -h:    :s `-/oody-      .+y+.
    d+          .dsy/:+s+` yo          .-.:/:-` s/           .+h/`
    :d           `.     .ydo          +hoyho:/osd:-`       `/ohh/m.
     h+                  /d          os/``     ``-/sss/. :so.  :hN`
     .m-                -h-        `d:  /:           `-+yd+.     :y:
      -d.             .ss`         os -y: -`             `-oso-   `ss
       -d-          `os.           /yoh`-s/`                `.-     +h`
        .y+`       :y/              `/y/y.                           ss
         `:s+.   `os`                 :m``/++:-.                     `N.
           `-+soyd/                   `y+s/../+os:`                   d-
               :d:                      ``      `/so-`               .N.
              -h-                                  ./oo/.`          .h/
             `d-                                      .:+oyo/::::/+oo-
             so                                           oh.-:/:-.`
"""

CHAOS = \
"""
                        `  `  .           ./    . ``
                   ``         -  `.  `    ``         +`
                   ..            ./`
                   .-. `                               `/`
               `    `                                      :
                                                               `
        . .`  .                                         .`     .`
         `:`                                            `` `-`
  .///:/:..`                                               .:....-::/+/`
  /s+++/o///:-..`                                        `...-//+://:so/
  :/+//o/+:+s/o//::--::-:--...```````````````````...---::/o/+:/s/o+///+:
  -/oso+yos+o/oo+/++/sso+yos++:oo+//+:+o+/s+s/+/oo+//o/oso+yoy+o:oo+//o.
  -+os//yysyo.:soo+++so//ysyyo.:yo++++os//yyyyo.:yo++++ss/:syyyo.:s++++.
  .:+o/-/yso/-o+:o/::+s/-/yso/-o+:+/::+s/:/yso/-o+:+/::+s:::yso/-oo:o/:
 `/os/+s+o++soh+:/o++s/+o+o++soh/:/o+os/+s+oo+soh/:/o+os/+s+oo+soh+:+o+
 -o:oyo+-+yo+-//:++o:oyo+:+yo+-///ssy+sso+-+so+://:+/o:oso+-+yo+:/+:+++`
`+++oy+oooossso+/++++oy+ohdhsssoydmddmdhooooosyddo/++++os+sooososo+/+++.
.sss/+++o/++++://:sss/+++o+shdy/mNo.:dNyo+ssyhyo://:sss/+o/o+/+++://:ss/
-/+/+//o/+:+s/o///+//+//o/+:+ssydNmhdmd+ohyy//s/o////+/+//o/+:/s/o+//++:
:/o/oso/sos//:oo+:/o/sso/sss++:osyssh+oys+soy++:oso/:o/ooo/soy++:oso/:s:`
++++os//yyso......--.--.-+/+o+.:s++/+/oo:/so+/:.-:-...--...+yyyo.:s++/++.
+/::os/-:ys/``````````````````````......```````````````````-ys+:-oo/o/::`
/s+os/+so++-                                               -o++ssh+:+soo.
//o:syo+-+y/                 .o+         :s-               `+yo+::/:++o:`
:+oooy+sooo+                 .yo         /y:               -oossso+/++o+`
.sos+++/o///-.`.`                                          :+/+/+-//:so+
`//:+//o/+-+s/+///:--:-..`                   ````....../::+:+:+s/+/////.
 /o/oso/hoy+o:sso/+o/sso/yos/`             `y+o:sso//o/oss:yoy+o:sso//o`
 `++oo//yysyo.:s+o++oso//yyss`             `sys./y+++++so//yysys.:s+o+-
  `:os:-:ys+::s+:+/::os:-:ys/`             `++::o+:++::+s:-:ys+::so:o:
   .o/os+o++y+y+:/sooo/+s+o+-               :+soh/:/sooo++sooo+yoy+::
    .o++-+yo/-//:+/+:sy++-+y:               /o/://:+/+:oy++-+yo+:/+.
     `.ooosysoo+++oooos+sooo-               -soso+/+oooos+sooosss+.
       .+//+/+://:sos+/++o//`               `//+://:soo+/+/o//++/`
         ::+s/+//////+/+s/+:   .::-...-::.   /s/+/+////+//s/+:odh:
         shso:os+/+o/ss+/yo+   `.--:::--.`   /o/oso/+o/sso/hshddd/
         .-:/./soo/++oo//sy/                 +s.:y++/++os+os:--..
              :s+o/::os:::y-                 -/:s+/+/:/oy+-`
               .+ssooo/+s+ooo+:--.......-::+/osoh/:/sss/.`
      `.-----.``.-::/:///-:-:ymddhhhhhhhhy:/.-:--.../:-.``.-:::-..
     -/+++++++/::-...........-/+++++hs/::-........`....-:+++++++++/`
    `/+++++++++++:...`.````.`......-h+.....`..```.....-/+++++++++++-
    `/++/+//:++++:...`.............-d/.....`......`...-++//++//////-
     -+:o+:+-y//+-.................-d:................-/+//o/+-+o/o.
     :o+o//::+//:-.................-d:.................-/oo+o/:://o/
    `++.`   `.`....................-s-..................--.``    `.s`
    .-      `    ..................-h-..................`   ``     .-`
    :.      ````...................:d-.................--````       :o
   .y-`       `./-.................:h-.................-::`        .+h:
   :sss/`````-//::-----............/y-..........----::/+oss/`````-+syyh-
   -hdydhosoyhmmmdddhhyysssoo++++++oh+++++ooossyyhdddmmmmmdyhyssysohhdmd/
  -hmmhsdo+hshhdyhhdhddddmddhddddso+ssoooyyyyyhdhdhddhyoo:+ods.-sshmmmmmmy-
"""


SMITH = \
"""
                           `:+ssyssoooooossys+
                      `-+shs/-`             `/y+`
                    -yy+.                      om+-
                 `+h+`                      /ooodMMs
                /d:        ``-///.          `....dMh
              `:M/`+oo.  /ssoo+::---+oooosyysss++dMh
             /NNhNmsohd.`s-/oshysy+//.`````      `oh
             N+-------my do-`                     +h
             d:-------yN/M-           /mo.        +h
            +m+:------yMooh           hh:yhs:     dh
            +Ms:-::::NyMy+mo`      `:ds`  `.sdhyyydh
             dm////:yMMMM``+ysyysyhyo.      +NMhmmyd-
             -mh+oyshMMMM      `--.`::.`..+hMd+/+++shsys`
              .NNNsoyMMMM     :sMMdmNMdyssdy+---------md.
               -dy-ys+NMs   /mMh:---/------------------odo`
                oy`.hy-+:`:+d+:-------------------------:mN`
                 m/`yd-`.+y:---------------------------:hM-
                 .d+`..`-------------------------------:+dN`
                  `ohyhMs-------------------------------:my
                     +hMo--------------------------------hd`
                     :/yds------------------------------os+
                      .dh:-----------------------------/N/
                      :syh/--------------------------:/ym`
                        -my--------------------------odN+
                      `oMs------------------------:/yNd-
            -ssyssshyyyhMMmhs+------------------:/odMMmdysso:`
          `sd:`     .ymNMMMMMNyyyy/:-------:/odmNmMMmdMMm. ./sho-
          sd.`        oMMMMMMMMMMMhyyh/osdmNho/ydMMNo+hMMo    `-sdo:
         yd-``         yMMMMMMNMMM:`:mMMMMMN/  .mNMy++dMMN        /yh/.
        yd-``          .NMMMMMdsNMN:/hmMMMMMMy.sMMN++sNMMN          .sms.
       oN-```           sMMMMMNooNMMh`.mMMMMh/mMMMo++oMMMN             /hs.
      -M+```            `MMMMMNo+omMm.-NMMMMN+mMMs+++yMMMy               :d
     `dh````             MMMMMd+++oNMNdMMMMMMMMMy+++++MMMm`
     +N:```              MMMMMNo+++omMMMMMMMMMMh+++++sMMMM+``
     mh````              MMMMMMs++++omMMMMMMMMy++++++oMMMMNs.```
"""



CALCULUS = \
"""
                    `-/osyyyyso:.
               `-+hhddhso+++++ohdh/
            ./hmmho:.`       .--.yMd:
          :hNms:`           /dmds/yhNo
        .yNd/`              /h:``  `+N+
       .yMs`                        `mN+
       /Ny.                    .yho+oMmhd/`
       .yo`   `-.-+/.`        `om..MMMNmMmhs/`
       .odsoydNMMMMMNh:``.-/syhdd/:NNhs+/:/dm:
   .:ohmMMMMMMMMMhosdMNdhhho/:.` .:syooshdmd+`
 `dmMMMMMMMMMMMMy-ydo:-.``        /MMMMMMMd:
 .MMMMMMMMMMMMMMN:-ss`      `.-:` hMMMMMMMMo
  /MMMMMMMMMMMMMMNs-```      ``-  hMMMMMMNh:
  .dMMMMMMMMMMMMMNNhoo+`          .mNMMMMy`
   `+dNMMMNNmmo:.``od+`             .MMMMMs           `/:`
      .//:.       `NNmdo::--.......-sMMMMMmo.         /Nmy`
                  .m/.-:://oyyyyhhhNMddd/+yNm+`      `sNhN+
                  /Ns              +d`./`  .+s-       -NhNo
                 :NMNy.            :h .s`          /o/:hmNs
                :NMMMMms:`         hN/oNy.         mNsNMNMNds-
                +MMNMMMMMdoosoo+ooyMMMMMMm/`       `mmmMMddNMNh-
                `MMNmNMMMMh:`  ````-mm-hMMMy.       +MMMMNmmNNmm+`
                oMMMNh/sNMMNs-      `o+-yMMMd:      dNoyNMNNdhdmN:
               /NMhNMd. .omMMNo.     `:+/hMMMh-     sN: -ymNmNd+h:
              /NMy -hMh.  .+yNMm+`    `/dyNMMM/     -Ns   `:+o:.h/
            `sNMy   `+dd+.  `.hMMd/   `+MdyMMMs.     /N+.      /m/
           .hMMh`  -/.+NNs.   `/dMMy- .yMdyMNNM/     .MMd:   `+mMNo.
          -hMMMMh``/   -hNmmmmd.`/dMNo.+MNmMmNM/     .MMNms:`.hMMMMN+
         .hMN:-+NN+:    ./o+yNm.   /dMmdMMMMmNMo`    sMh.+yNNNNMMMMMM`
        `sMN:    +Md`       oNd.     /dNMMMNmNMm-   .MN-     -+++yNMh`
       `sMMo`    .NMy       /mh-      `/hMMNNNmNo`  .Mm`         :mM/
       +NMN:      hMm.       -sds-      `oNMMdohm: `oMMd.        /mMs
       +MMy.      yMN+         .oyys-.   `/dMy+omhodMMNNms.      hMM:
       +MM+       dMd`            .oymy:.  -hms+hNMymNNy:sys///ymNd/
       .hMy.     :MMs                .+ydho.`sdsomM/-.+/ `-:sdMMN/`
        oMN:     dMm                    `-smdhmNydMm    `-`  oMN/
"""

NUTMEG = \
"""
             cc;                   .''..
            ;: .oc.         ..';;;,..  x.
           ;o    .cc..;c:lkko;.       ,0::::::;.
           o:;.     :x:o:;.           do .   ..;cc;...
              'dkc.  .k.   .';::::;;,,c.        ...lxc:clc.
             ,xNMWk;,o,,lk0:cod'..            .''....c'  .:c;
           ,d:xlko,,:NKKWX:   oNcc           .o..,;;;,.     .c;
         .lc:;,'.....o0..    .00c.         ..o.               .:;
         l:.    .;;;;oxc;;;lk0l..           c;    ',,;l'        .l.
         o,      .  .c0l;:oo:'..          .,c   ;:..'..d          l.
         cc    .'::;'  ll..               .o  .;..;;. .o         ;:.
        .Ol;,,,,.       :x..              'lc:;..:co. :'     .;;;.
        d'               :x.               .. .... ..'l.,,;;;'
       'd                 lo.                   ...':c;O;
       k;                  O.                   .','.,:,;;.
      .N.                  cl                    ..:d'    :l.
      .X                   'k                ..,cc;,;      .d:
      .X.                  .0..       . ..,;:::,   l         :d.
       d;  .,,;:xkcxdlll;. ,x;;::::::;;;,'.       'c          'k.
       .l,'.   ;o  o.    .;l.                     l.           .x;
              ::   o                              l              oc
             l,    o                              ;c.             ll
           .x'     d   .,,,,,.                      ;:.            co
           o;      k  :d....,x;                       ;l.           :l
           oo      O   .'..',c.                        .c:          ,c
            lx.    O                                     .oc      ,o,
             .x:  .O                                      cl    ,o:
               ,l;'O    .'',;:.                           'x  ;:'
                 .oO   c:....'x,                           kc;
                  .x    ',;;;;;                            l;
                  .x                                       ;o
                  .x                                       .k
                  .d                                        k.
                  .d                                        l:
                  .o                                        .x
                  'o                                         x
                  .o                                         l;
                  .d                                         o,
                   k                                      'cl.
                   'oc,.                            .,:ccc,
                      .;:;;;,'..........',,;:codkl::,.
                             .'0MMMMMMMkWMMMMMMWl
                               'WMMMMMMoNMMMMMMo
                                oMMMMMMoXMMMMMk
                                 0MMMMMoKMMMM0
                         ,;'.    'WMMMMdKMMMX.    .';cc.
                        kWMMMNKOkxXMMMMd0MMWl,cdOXWMMMMO
                        kMMMMMMMMMMMMMMxOMMMMMMMMMMMMMWo
                         .:coodxxxkkkxxoldddxxxxxdolc;.
"""

LOGOS = [FARNSWORTH, FRINK, HOMER, CHAOS, SMITH, CALCULUS, NUTMEG]


import random
LOGO = random.choice(LOGOS)


def writeLogo(stream=None, logoindex=None):
    """
    Send the logo to the given stream.

    stream defaults to sys.stdout.

    logoindex can be used to get a specific logo, otherwise a random one will be
    returned.
    """
    if stream is None:
        import sys
        stream = sys.stdout
    logo = ""
    if logoindex is None:
        logo = random.choice(LOGOS)
    else:
        logo = LOGOS[logoindex]
    stream.write(LOGO)
    stream.flush()


import professor
GUIDELINES = \
"""
The Professor %s system for systematic tuning of Monte Carlo generators
is licensed under version 2 of the GPL. See COPYING for details.

Copyright (C) 2007-2014 Andy Buckley, Holger Schulz and Simone Amoroso.

Previous authors: Hendrik Hoeth, Heiko Lacker, Holger Schulz,
                  Jan Eike von Seggern, and Daniel Weyh.

Please respect the MCnet academic usage guidelines, see GUIDELINES:
http://www.montecarlonet.org/GUIDELINES

Please cite doi:10.1140/epjc/s10052-009-1196-7.

""" % professor.__version__


def writeGuideLine(stream=None):
    """
    Send the MCnet guidelines string to the given stream.

    stream defaults to sys.stdout
    """
    if stream is None:
        import sys
        stream = sys.stdout
    stream.write(GUIDELINES)
    stream.flush()
