"""Professor utility functions and classes.

The contents of the professor.tools subpackage are useful coding tools which are
used by several Professor modules or scripts, and which are collected here
because they are more about the practicalities of some data handling, file
access, etc. than about the Professor data parameterisation/optimisation
system itself.

The general rule is that professor.tools utilities should not need to be exposed
to user-written scripts, but if users find anything useful in here they should
feel free to use them! However, stability of the professor.tools API is not
considered as important for other packages, so Caveat Emptor!
"""

__all__ = ['config', 'decorators', 'stats', 'eigen', 'permut', 'io', 'pointsampling']
