"""
.. autoclass:: Histo
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: Bin
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: PlotParser
    :members:
    :undoc-members:
    :show-inheritance:
"""
from lighthisto import *
