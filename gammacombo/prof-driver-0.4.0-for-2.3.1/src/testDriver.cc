#include "ProfDriver/ProfDriver.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;
using namespace Professor;

int main(int argc, char** argv)
{
  if (argc<2) {
    cout << "\nPlease provide parametrisation file as 1st CL argument.\n" << endl; 
    exit(1);
  }

  ProfDriver* p = new ProfDriver(string(argv[1]));

  if (argc<3) {
    cout << "\nPlease provide observable from above as 2nd CL argument.\n" << endl;
    exit(0);
  }


  string obsname = string(argv[2]);

  vector<double> sm;
  for (int i=0;i<p->dim(obsname);i++) sm.push_back(0.0);

  for (int i=0;i<p->nbins(obsname);i++) {
    cout << obsname << " bin " << i  << " ranging " << p->xmin( obsname, i) << " ... " << p->xmax(obsname, i) << " has " << p->value(obsname, i, sm ) << " +/- " <<  p->error(obsname, i, sm ) << endl;
  }

}
