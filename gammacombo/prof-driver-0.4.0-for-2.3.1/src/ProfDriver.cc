#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include "Professor/Ipol.h"
#include "ProfDriver/ProfDriver.h"
#include <algorithm>
#include <iterator>
#include <regex>

ProfDriver::ProfDriver(string const& filename) {
  _version="0.3.0";
  readMeta(filename.c_str()); // Important to get the min max values for the scaling
  readIpol(filename.c_str());
}

ProfDriver::~ProfDriver(){}
void ProfDriver::readIpol(const char* fname) {

    std::cout << "ProfDriver (" << _version << ") Please cite arXiv:0907.2973 [hep-ph]" << std::endl;
    std::cout << "Reading Professor parametrisations from file " << fname << std:: endl;
    std::ifstream infile(fname);
    std::string line;
    bool header_complete = false;
    vector<string> temp;
    string hname("");
    vector<std::string> hnames;
    while (std::getline(infile, line)) {
        std::string first = line.substr(0,1);
        if (first=="#") continue;
        if (!header_complete) {
            temp = tokenize(line, ":");
            if (temp.size()==1 && temp[0]=="---") {
                header_complete = true;
            }
        }
        else {
          // Done with the header
            if (first=="/") { // Found a new obsservable, prepare empty vector<Ipol> to be filled with val: stuff
                hname = tokenize(line, "#")[0];
                vector<Ipol> test, errors;
                if (!has_key(_ipols,hname)) {
                  hnames.push_back(hname);
                    _ipols[hname] = test;
                    _errors[hname] = errors;
                  vector <double> xmins, xmaxs;
                    _xmin[hname] = xmins;
                    _xmax[hname] = xmaxs;
                }
                _xmin[hname].push_back(std::stod(tokenize(line, " ")[1]));
                _xmax[hname].push_back(std::stod(tokenize(line, " ")[2]));
            }
            else {
              if ( (line.substr(2,4)) == "val:") {
                string istring = tokenize(line, ":")[1];
                Ipol myipol(istring);
                myipol.setParamLimits(_pMin, _pMax);
                _ipols[hname].push_back(myipol);
              }
              else if ( (line.substr(2,4)) == "err:") {
                string istring = tokenize(line, ":")[1];
                Ipol myerror(istring);
                myerror.setParamLimits(_pMin, _pMax);
                _errors[hname].push_back(myerror);
              }
            }
        }
    } // End while loop

    int nbins=0;
    for (unsigned int i=0; i< hnames.size();++i) {
      nbins += _ipols[hnames[i]].size();
    }
   
    std::cout << "Successfully read " << nbins << " objects in " << hnames.size()<< " obervables." << std::endl;
    for (unsigned int i=0; i< hnames.size();++i) {
      std::cout << hnames[i] << ": " << _ipols[hnames[i]].size() << " bins" << std::endl;
    }
}

void ProfDriver::readMeta(const char* fname) {
    std::ifstream infile(fname);
    std::string line;
    bool header_complete = false;
    vector<string> temp;
    while (std::getline(infile, line)) {
        std::string first = line.substr(0,1);
        if (first=="#") continue;
        if (!header_complete) {
            temp = tokenize(line, ":");
            if (temp.size()==2) {
                _meta[temp[0]] = temp[1];
            }
            if (temp.size()==1 && temp[0]=="---") {
                header_complete = true;
            }
        }
        else break;
    } // End while loop

   // All this is for setting the correct scaling internally
   vector<string> tMin = tokenize(_meta["MinParamVals"], " "); 
   vector<string> tMax = tokenize(_meta["MaxParamVals"], " ");
   for (auto t : tMin) {
     if (t.length()==0) continue; // ignore empty strings
     _pMin.push_back(std::stod(t));
   }
   for (auto t : tMax) {
     if (t.length()==0) continue;
     _pMax.push_back(std::stod(t));
   }
}

// http://stackoverflow.com/questions/9435385/split-a-string-using-c11
std::vector<std::string> ProfDriver::tokenize(const std::string& input, const string& separator) {
    std::regex re(separator);
    std::sregex_token_iterator
        first{input.begin(), input.end(), re, -1},
        last;

    vector<std::string> ret;
    while(first!=last) {
      ret.push_back(*first);
      first++;
    }
    return ret;
}

bool ProfDriver::has_key(unordered_map< string, vector< Ipol > > mymap, string key) {
    std::unordered_map<std::string, vector< Ipol > >::const_iterator got = mymap.find (key);
    if (got==mymap.end()) return false;
    else return true;
}

double ProfDriver::value(string hname, int binnum, vector<double> P) {
  return _ipols[hname][binnum].value(P);
}

double ProfDriver::xmin(string hname, int binnum) {
  return _xmin[hname][binnum];
}

double ProfDriver::xmax(string hname, int binnum) {
  return _xmax[hname][binnum];
}

vector<double> ProfDriver::values(string hname, vector<double> P) {
  vector<double> temp;
  for (unsigned int i=0; i<_ipols[hname].size();++i) {
    temp.push_back(value(hname, i, P));
  }
  return temp;
}
double ProfDriver::error(string hname, int binnum, vector<double> P) {
  return _errors[hname][binnum].value(P);
}

vector<double> ProfDriver::errors(string hname, vector<double> P) {
  vector<double> temp;
  for (unsigned int i=0; i<_ipols[hname].size();++i) {
    temp.push_back(error(hname, i, P));
  }
  return temp;
}
