setupATLAS
localSetupROOT 6.08.02-x86_64-slc6-gcc49-opt
export CC=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Gcc/gcc493_x86_64_slc6/slc6/gcc49/bin/gcc
export CXX=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Gcc/gcc493_x86_64_slc6/slc6/gcc49/bin/g++
export PATH=$PATH:/afs/cern.ch/work/g/glu/public/EFT_scan_update/gammacombo/scons-2.5.1
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/sft.cern.ch/lcg/releases/LCG_87/MCGenerators/professor/2.2.1/x86_64-slc6-gcc49-opt/lib
lsetup "lcgenv -p LCG_85a x86_64-slc6-gcc49-opt professor 2.1.3"
