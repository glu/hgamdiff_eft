# Confidence Intervals
intervals = {
  '0.68' : [
    {'var':'a_gaus', 'min':'-1.5', 'max':'0.5', 'central':'-0.5', 'neg':'1.0', 'pos':'1.0', 'cl':'0.68', 'unit':'', 'method':'Prob'},
  ],
  '0.95' : [
    {'var':'a_gaus', 'min':'-2.5', 'max':'1.5', 'central':'-0.5', 'neg':'2.0', 'pos':'2.0', 'cl':'0.95', 'unit':'', 'method':'Prob'},
  ],
  '1.00' : [
    {'var':'a_gaus', 'min':'-2.5', 'max':'2.5', 'central':'-0.5', 'neg':'2.0', 'pos':'3.0', 'cl':'1.00', 'unit':'', 'method':'Prob'},
  ]
}
