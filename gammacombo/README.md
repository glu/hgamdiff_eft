## compile gammacombo
source setup.sh

mkdir -vp build

cd build

cmake ../

make -j4

make install

## enter HGamEFTScanner to measure CL intervals of Wilson coefficients
cd HGamEFTScanner

source run_all.sh

