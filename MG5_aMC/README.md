## The cards used for ggF and VBFVH production have been saved respectively.
the proc_card_*.dat is used to produce the MG5 directory
./mg5_aMC proc_card_*.dat
The config files under ./Cards include run_card param_card pythia8_card and so on.

For more technical details, please refer to the documentation on MG5 website 
