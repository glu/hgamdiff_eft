### Professor2 can be used on lxplus directly
## setup the environment
setupATLAS

lsetup "lcgenv -p LCG_95 x86_64-slc6-gcc62-opt professor 2.2.1"

## execute
prof2-ipol --pname=param.dat --order=2 yoda_200200_200309_May17 vbf_vh_10D_full.dat

# where
# --order = 1 for linear-only case; 2 for linear+quadratic case
# yoda_200200_200309_May17 is the input directory containing param.dat and Rivet.yoda. The input directory has to be constructed using fixed convention. ./Input_CHG_linear_quad can serve as an instance.
# vbf_vh_10D_full.dat is the output file including all the coefficients of the interpolation.

