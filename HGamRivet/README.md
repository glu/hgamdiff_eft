## compile the HiggsDiphotonFiducialCrossSectionAnalysis2018_EFT project
source setup.sh

rivet-buildplugin RivetAnalysis.so HiggsDiphotonFiducialCrossSectionAnalysis2018_EFT.cc

## loop over the .hepmc output by MG5_aMC
rivet -a HiggsDiphotonDifferentialCrossSection2018_EFT --pwd XXX.hepmc

