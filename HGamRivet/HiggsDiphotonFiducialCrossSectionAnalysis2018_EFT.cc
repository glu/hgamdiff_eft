/*
* Rivet routine for ATLAS H->yy differential cross-sections measurement at 13 TeV
* Author: Jim Lacey <jlacey@cern.ch> and Josefina Alconada  <josefina.alconada@cern.ch>
*/

// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "YODA/Histo1D.h"
#include <iostream>

namespace Rivet {

  class HiggsDiphotonDifferentialCrossSection2018_EFT : public Analysis
  {
  public:

    /// Default constructor
    HiggsDiphotonDifferentialCrossSection2018_EFT () :
    Analysis("HiggsDiphotonDifferentialCrossSection2018_EFT"){}

    /// @name Analysis methods
    //@{
    void init ()
    {
     std::cout<<"femtobarn"<<femtobarn<<std::endl; 
     // Project all final state particles
      const FinalState fs ( Cuts::abseta < 5.5 ) ;
      addProjection ( fs , "All final state particles" ) ;
      // Project all photons
      IdentifiedFinalState all_fs_ph ( fs , PID::PHOTON ) ;
      addProjection ( all_fs_ph , "All final state photons" ) ;
      // Project photons which pass kinematic cuts
      IdentifiedFinalState kin_fs_ph ( Cuts::pT > 25.0*GeV && Cuts::abseta < 2.37 &&
        ( Cuts::abseta < 1.37 || Cuts::abseta > 1.52 ), PID::PHOTON ) ;
        addProjection ( kin_fs_ph , "Kinematic final state photons" ) ;
        // Project electrons which pass kinematic cuts
        IdentifiedFinalState fs_el_bare ( Cuts::pT > 15*GeV && Cuts::abseta < 2.47 &&
          ( Cuts::abseta < 1.37 || Cuts::abseta > 1.52 ) ) ;
          fs_el_bare.acceptIdPair ( PID::ELECTRON ) ;
          addProjection ( fs_el_bare , "Bare kinematic final state electrons" ) ;
          // Project dressed electrons
          DressedLeptons fs_el_dr ( all_fs_ph , fs_el_bare , 0.1 , Cuts::open() , true , false ) ;
          addProjection ( fs_el_dr , "Dressed kinematic final state electrons" ) ;
          // Project muons which pass kinematic cuts
          IdentifiedFinalState fs_mu_bare ( Cuts::abseta < 2.70 && Cuts::pT > 15*GeV ) ;
          fs_mu_bare.acceptIdPair ( PID::MUON ) ;
          addProjection ( fs_mu_bare , "Bare kinematic final state muons" ) ;
          // Project dressed muons
          DressedLeptons fs_mu_dr ( all_fs_ph , fs_mu_bare , 0.1 , Cuts::open() , true , false  ) ;
          addProjection ( fs_mu_dr , "Dressed kinematic final state muons" ) ;
          // Project charged particles for iso cone
          ChargedFinalState fs_charged ( Cuts::pT > 1*GeV ) ;
          addProjection ( fs_charged , "Charged particles for iso cone" ) ;
          // Project all particles exept for muons and neutrinos for jet building
          VetoedFinalState all_for_jets ( fs ) ;
          all_for_jets.vetoNeutrinos () ;
          all_for_jets.addVetoPairId ( PID::MUON ) ;
          addProjection ( all_for_jets , "Particles for jets" ) ;
          // Project jets
          FastJets jets ( all_for_jets , FastJets::ANTIKT , 0.4 ) ;
          addProjection ( jets , "All jets" ) ;

          // Declare histograms
          //const std::vector<double> bins_ptyy = {0. , 20. , 30. , 45. , 60. , 80. , 120. , 170. , 220. , 350. , 9999. };
	  _h_fid_regions = bookHisto1D("h_inclusive",2,0,2,"inclusive");
          const std::vector<double> bins_ptyy = {0,5,10,15,20,25,30,35,45,60,80,100,120,140,170,200,250,300,450,650,1000};
          //{0., 5., 10., 15., 20., 25., 30., 35., 45., 60., 80., 100., 120., 140., 170., 200., 250., 350., 9999.};
            //0. , 20. , 30. , 45. , 60. , 80. , 120. , 170. , 220. , 350. , 9999. };
          _h_pT_yy         = bookHisto1D("h_pT_yy",bins_ptyy,"pT_yy");
          const std::vector<double> bins_ptyy_fine = {0., 5., 10., 15., 20., 25., 30., 35., 45., 60., 80., 100., 120., 140., 170., 200., 250., 350., 9999.};
          _h_pT_yy_fine         = bookHisto1D("h_pT_yy_fine",bins_ptyy_fine,"pT_yy_fine");

          const std::vector<double> bins_yAbsyy = { 0.00 , 0.15 , 0.30 , 0.45 , 0.60 , 0.75 , 0.90 , 1.20 , 1.60 , 2.40 };
          _h_yAbs_yy       = bookHisto1D("h_yAbs_yy",bins_yAbsyy,"yAbs_yy");


          _h_excl_N_j_30       = bookHisto1D("h_excl_N_j_30",4,0,4,"excl N_j_30");
          _h_incl_N_j_30       = bookHisto1D("h_incl_N_j_30",4,0,4,"incl N_j_30");
          _h_N_j_50            = bookHisto1D("h_N_j_50",3,0,3,"N_j_50");

          //const std::vector<double> bins_pTj1 = { 30., 55. , 75., 120., 350.} ;
          const std::vector<double> bins_pTj1 = {30 , 60 , 90 , 120 , 350 , 99999}; //without underflow bin
          _h_pT_j1_30       = bookHisto1D("h_pT_j1_30",bins_pTj1,"pT_j1_30");

          const std::vector<double> bins_pTj1_of = { 30., 55. , 75., 120., 350., 1000.} ;
          _h_pT_j1_30_of       = bookHisto1D("h_pT_j1_30_of",bins_pTj1_of,"pT_j1_30_of");

          //const std::vector<double> bins_mjj = { 0. , 170. , 500. , 1500. } ;
          const std::vector<double> bins_mjj = {0 , 120 , 450 , 3000 , 99999}; //without underflow bin
          _h_m_jj_30        = bookHisto1D("h_m_jj_30",bins_mjj,"m_jj_30");

          const std::vector<double> bins_mjj_of = { 0. , 170. , 500. , 1500., 99999. } ;
          _h_m_jj_30_of        = bookHisto1D("h_m_jj_30_of",bins_mjj_of,"m_jj_30_of");

          const std::vector<double> bins_dphijj = { 0, 1.0472,  2.0944,  3.15};
          _h_Dphi_j_j_30    = bookHisto1D("h_Dphi_j_j_30",bins_dphijj,"Dphi_j_j_30");

          //const std::vector<double> bins_dphijj_signed = { -3.15, -1.570796, 0, 1.570796, 3.15};
          const std::vector<double> bins_dphijj_signed = {-3.15 , -1.5708 , 0 , 1.5708 , 3.15};
          _h_Dphi_j_j_30_signed    = bookHisto1D("h_Dphi_j_j_30_signed",bins_dphijj_signed,"Dphi_j_j_30_signed");



        }

        int count=0;
        void analyze ( const Event & event )
        {
          // Event weight
          double weight = event.weight();
  	 // cout<<"the event weight is: "<<weight<<endl;//updated by glu
	 _h_fid_regions -> fill(0.5 , weight);
          // Get photons
          const Particles &kin_ph = applyProjection<IdentifiedFinalState>( event,"Kinematic final state photons" ).particlesByPt();
          Particles fid_photons;
          foreach ( const Particle & ph , kin_ph ) {
            if ( ! notFromHadron ( ph ) ) continue ;
            bool passIso = isoEnergy( event, ph, 0.2 ) / ph.pt() < 0.05;
            if ( !passIso ) continue;
            fid_photons.push_back ( ph ) ;
          }

          // Diphoton selection
          if ( fid_photons.size() < 2 ) vetoEvent;
          FourMomentum y1(fid_photons[0].momentum()), y2(fid_photons[1].momentum());
          FourMomentum yy = (y1+y2);
          double myy = yy.mass();
          if ( y1.pT() < 0.35*myy || y2.pT() < 0.25*myy ) vetoEvent;
          if ( myy < 105 || myy > 160 ) vetoEvent;

	  _h_fid_regions ->fill(1.5, weight);
          // Get electrons
          const Particles & kin_el = applyProjection<DressedLeptons>( event,"Dressed kinematic final state electrons" ).particlesByPt();
          Particles electrons;
          foreach ( const Particle & el , kin_el ){
            if ( ! notFromHadron ( el ) ) continue ;
            if ( ! passOverlap ( fid_photons, el, 0.4 ) ) continue ;
            electrons.push_back ( el ) ;
          }

          // Get muons
          const Particles & kin_mu = applyProjection<DressedLeptons>( event, "Dressed kinematic final state muons" ).particlesByPt() ;
          Particles muons;
          foreach ( const Particle & mu , kin_mu ){
            if ( ! notFromHadron ( mu ) ) continue ;
            muons.push_back ( mu ) ;
          }

          // Get jets
          //const Jets & kin_jets = applyProjection <JetAlg>( event, "All jets" ).jetsByPt( Cuts::pT>25.0*GeV && Cuts::absrap<4.4 ) ;
          const Jets & kin_jets = applyProjection <JetAlg>( event, "All jets" ).jetsByPt( Cuts::pT>30.0*GeV && Cuts::absrap<4.4 ) ;
	  Jets jets30, jets50, bjets;
          foreach ( const Jet & jet , kin_jets ){
            if ( ! passOverlap ( fid_photons, jet, 0.4 ) ) continue ;
            if ( ! passOverlap ( electrons, jet, 0.2 ) ) continue ;
            jets30.push_back ( jet ) ;
            if ( jet.containsBottom()) bjets.push_back ( jet );
            if ( jet.momentum().pT() < 50*GeV ) continue ;
            jets50.push_back ( jet ) ;
          }

          // MET
          FourMomentum invisible(0,0,0,0);
          const Particles & all_ptcls = applyProjection<FinalState>( event, "All final state particles" ).particles() ;
          foreach ( const Particle & ptcl , all_ptcls ){
            if ( ! notFromHadron ( ptcl ) ) continue ;
            if ( ptcl.isHadron() ) continue ;
            if ( ptcl.pid() == PID::PHOTON ) continue ;
            if ( ptcl.pid() == PID::GLUON ) continue ;
            if ( ptcl.charge() != 0  ) continue ;
            invisible += ptcl.momentum () ;
          }
          //double met =  invisible.pt();

          // Diphoton Xsections
          double pTyy = yy.pt();
          _h_pT_yy    ->fill( pTyy/GeV, weight ) ;
          _h_pT_yy_fine    ->fill( pTyy/GeV, weight ) ;
          _h_yAbs_yy  ->fill( yy.absrapidity(), weight ) ;

          // Jet variables
          int njets30 = jets30.size()>3?3:jets30.size();
          int njets50 = jets50.size()>2?2:jets50.size();

          // Fiducial Xsections
          _h_excl_N_j_30->fill( njets30, weight ) ;
          _h_N_j_50->fill( njets50, weight ) ;
          _h_incl_N_j_30->fill( 0.5, weight ) ;
          if ( njets30 > 0 ) _h_incl_N_j_30->fill( 1.5, weight ) ;
          if ( njets30 > 1 ) _h_incl_N_j_30->fill( 2.5, weight ) ;
          if ( njets30 > 2 ) _h_incl_N_j_30->fill( 3.5, weight ) ;

          FourMomentum j1(0,0,0,0);
          if ( njets30 > 0 ) {
            j1 = jets30[0].momentum();
            _h_pT_j1_30      ->fill( j1.pt()/GeV , weight ) ;
            _h_pT_j1_30_of     ->fill( j1.pt()/GeV , weight ) ;
          }
          //if ( njets30 == 0 ) _h_pT_j1_30 ->fill( 0 , weight ) ;

          if ( njets30 > 1 ) {
            FourMomentum j2 = jets30[1].momentum();
            FourMomentum dijet = j1+j2;
            double mjj = dijet.mass();
            double dphi_jj = fabs(Phi_mpi_pi(j1.phi()-j2.phi()));
            double dphi_jj_signed = j1.rapidity()>j2.rapidity()?
            Phi_mpi_pi(j1.phi()-j2.phi()):
            Phi_mpi_pi(j2.phi()-j1.phi());

            _h_Dphi_j_j_30        ->fill( dphi_jj, weight ) ;
            _h_Dphi_j_j_30_signed ->fill( dphi_jj_signed, weight ) ;
            _h_m_jj_30            ->fill( mjj/GeV , weight ) ;
            _h_m_jj_30_of            ->fill( mjj/GeV , weight ) ;

          }



        }

        void finalize ()
        {
          // Scale histograms from nEvents to Xsection

           //double xs = crossSection()*2.27E-03; /// femtobarn ;
	  
         cout<<"xs is: "<<crossSection()<<endl<<"  xsPerEvent is: "<<crossSectionPerEvent()<<endl;
	 cout<<"numEvents is: "<<numEvents()<<endl<<" sumOfWeights is: "<<sumOfWeights()<<endl;
//           double xs = crossSectionPerEvent() / femtobarn;
	   //double xs = crossSectionPerEvent() ;
	   double xs = 1.0;
	   std::cout << "XS XS : " << xs << endl;
	   scale(_h_fid_regions, xs);
           scale( _h_pT_yy, xs);
           scale( _h_yAbs_yy, xs);
           scale( _h_excl_N_j_30, xs);
           scale( _h_incl_N_j_30, xs);
           scale( _h_N_j_50, xs);
           scale( _h_m_jj_30, xs);
           scale( _h_pT_j1_30, xs);
           scale( _h_Dphi_j_j_30, xs);
           scale( _h_Dphi_j_j_30_signed, xs);

        }

        template < typename Ptcl >
        bool passOverlap ( Particles &others, const Ptcl &ptcl , double dR ){
          foreach ( const Particle & other , others ){
            if ( deltaR( other.momentum(), ptcl.momentum() ) > dR ) continue ;
            return false ;
          }
          return true ;
        }

        bool notFromHadron ( const Particle & ptcl ){
          if ( ptcl.isHadron () ) return false ;
          GenVertex * prodVtx = ptcl.genParticle()->production_vertex() ;
          if ( prodVtx == NULL ) return true ;
          const PdgId thisID = ptcl . pid () ;
          const GenParticle *parent = particles( prodVtx , HepMC::parents ) [ 0 ] ;
          const PdgId parentID = parent -> pdg_id () ;
          if ( PID::isHadron( parentID ) ) return false ;
          if ( abs(parentID) == PID::TAU ) return notFromHadron ( parent ) ;
          if ( parentID == thisID ) return notFromHadron ( parent ) ;
          return true ;
        }

        double isoEnergy ( const Event & event , const Particle & ptcl , double dR ){
          FourMomentum ET_iso ( 0, 0, 0, 0 ) ;
          const Particles & others = applyProjection<ChargedFinalState>( event,"Charged particles for iso cone").particles();
          foreach ( const Particle & other , others ){
            if ( deltaR( other.momentum(), ptcl.momentum() ) == 0 ) continue ;
            if ( deltaR( other.momentum(), ptcl.momentum() ) > dR ) continue ;
            ET_iso += other.momentum () ;
          }
          return ET_iso.Et() ;
        }

        double Phi_mpi_pi(double x) {
          while (x >= PI) x -= TWOPI;
          while (x < -PI) x += TWOPI;
          return x;
        }

        double tau_jet ( const FourMomentum & Higgs , const FourMomentum & jet ){
          return sqrt ( pow(jet.pT(),2)+pow(jet.mass(),2) )/(2.0*cosh( jet.rapidity()-Higgs.rapidity() ) ) ;
        }

        double max_tau_jet ( const FourMomentum & Higgs , Jets & jets ){
          double max_tj ( 0 ) ;
          foreach ( const Jet & jet , jets ){
            double tau_j ( tau_jet ( Higgs , jet.momentum () ) ) ;
            if ( tau_j < max_tj ) continue ;
            max_tj = tau_j ;
          }
          return max_tj ;
        }

        double sum_tau_jet ( const FourMomentum & Higgs , Jets & jets ){
          double sum_tj ( 0 ) ; double temp ( -99 ) ;
          foreach ( const Jet & jet , jets ){
            temp =  tau_jet ( Higgs , jet.momentum () )  ;
            sum_tj += temp > 8*1000 ? temp : 0.0;
          }
          return sum_tj ;
        }

      private:

        Histo1DPtr _h_fid_regions;
        Histo1DPtr _h_pT_yy, _h_pT_yy_of, _h_pT_yy_fine, _h_yAbs_yy;
        Histo1DPtr _h_excl_N_j_30, _h_incl_N_j_30, _h_N_j_50 ;
        Histo1DPtr _h_pT_j1_30 , _h_pT_j1_30_of;
        Histo1DPtr _h_m_jj_30, _h_m_jj_30_of, _h_Dphi_j_j_30, _h_Dphi_j_j_30_signed;


        //@}

      } ;
      // This is a required hook for the plugin system
      DECLARE_RIVET_PLUGIN ( HiggsDiphotonDifferentialCrossSection2018_EFT ) ;
    }
